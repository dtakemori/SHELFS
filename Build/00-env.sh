#!/bin/bash
set +h
unset CFLAGS
unset CXXFLAGS

export LC_ALL=POSIX
export PATH="/cross-tools/bin:/tools/bin:/bin:/usr/bin:/sbin:/usr/sbin"
export LFS="/mnt/SHELFS"

export EDITOR=vi
