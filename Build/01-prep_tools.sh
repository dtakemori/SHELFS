#!/bin/bash

if [ -d /etc/ld.so.conf.d ]; then
  echo "/tools/local/lib" > /etc/ld.so.conf.d/tools-local.conf
else
  echo "/tools/local/lib" >> /etc/ld.so.conf
fi

if [ ! -d /mnt/SHELFS/boot ]; then
   mkdir -v /mnt/SHELFS/boot
   mkdir -v /mnt/SHELFS/boot/efi
fi

if [ ! -d /mnt/SHELFS/tools ]; then
   mkdir -v /mnt/SHELFS/tools
fi

if [ ! -d /mnt/SHELFS/cross-tools ]; then
   mkdir -v /mnt/SHELFS/cross-tools
fi

if [ ! -h /tools ]; then
  ln -sv /mnt/SHELFS/tools /
fi

if [ ! -h /cross-tools ]; then
  ln -sv /mnt/SHELFS/cross-tools /
fi

if [ -h /tools/bin/sh ] ; then 
  rm -f /tools/bin/sh
fi
if [ ! -d /tools/bin ] ; then
  mkdir -v /tools/bin
fi
ln -vs /bin/sh /tools/bin/sh &&


if [ ! -f /etc/vimrc ] ; then
cat > /etc/vimrc << "EOF"
set nocompatible
set bs=indent,eol,start
set history=50
set ruler

if &t_Co > 2
  syntax on
  set hlsearch
endif
EOF
fi

