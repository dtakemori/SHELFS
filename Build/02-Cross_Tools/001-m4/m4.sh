#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/m4-${VER_M4}.tar.xz ] || exit -1
rm -rf m4-${VER_M4} &&
tar xf ${LFS}/Sources/m4-${VER_M4}.tar.xz &&

########################################################################## PREP

######################################################################### PATCH
mkdir -v build/ &&
cd build/ &&

env CPPFLAGS="-D_GNU_SOURCE" \
CC=gcc \
CFLAGS="${COPTS_CT}" \
LDFLAGS="${LDOPTS_CT}" \
../m4-${VER_M4}/configure \
	--prefix=/cross-tools &&


######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /cross-tools/bin/m4 ] ; then
	echo "ERROR: m4 is not installed"
	exit -1
fi

/cross-tools/bin/m4 --version | grep "^m4 (GNU M4) ${VER_M4}$"
if [ $? != 0 ] ; then
	echo "ERROR: m4 is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf m4-${VER_M4} &&
set +x +u
