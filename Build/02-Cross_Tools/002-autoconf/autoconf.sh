#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/autoconf-${VER_AUTOCONF}.tar.xz ] || exit -1
rm -rf autoconf-${VER_AUTOCONF} &&
tar xf ${LFS}/Sources/autoconf-${VER_AUTOCONF}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

../autoconf-${VER_AUTOCONF}/configure \
	--prefix=/cross-tools &&

######################################################################### BUILD
make -j${MAKEJOBS:-2}  &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ; then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /cross-tools/bin/autoconf ] ; then
	echo "ERROR: autoconf is not installed"
	exit -1
fi

/cross-tools/bin/autoconf --version | grep "^autoconf (GNU Autoconf) ${VER_AUTOCONF}$"
if [ $? != 0 ] ; then
	echo "ERROR: autoconf is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf autoconf-${VER_AUTOCONF} &&
set +x +u
