#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/automake-${VER_AUTOMAKE}.tar.xz ] || exit -1
rm -rf automake-${VER_AUTOMAKE} &&
tar xf ${LFS}/Sources/automake-${VER_AUTOMAKE}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

../automake-${VER_AUTOMAKE}/configure \
	--prefix=/cross-tools &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ; then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /cross-tools/bin/aclocal ] ; then
	echo "ERROR: aclocal is not installed"
	exit -1
fi

/cross-tools/bin/aclocal --version | grep "^aclocal (GNU automake) ${VER_AUTOMAKE}"
if [ $? != 0 ] ; then
	echo "ERROR: aclocal is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf automake-${VER_AUTOMAKE} &&
set +x +u 
