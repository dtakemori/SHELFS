#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/cmake-${VER_CMAKE}.tar.gz ] || exit -1
rm -rf cmake-${VER_CMAKE} &&
tar xf ${LFS}/Sources/cmake-${VER_CMAKE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_CT}" \
CXXFLAGS="${COPTS_CT}" \
LDFLAGS="${LDOPTS_CT}" \
../cmake-${VER_CMAKE}/bootstrap \
	--prefix=/cross-tools \
	--mandir=/cross-tools/share/man \
	--no-system-libs \
	--parallel=${MAKEJOBS:-2} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log bin/ctest -j ${MAKEJOBS:-2} > ../test.log 
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /cross-tools/bin/cmake ] ; then
	echo "ERROR: cmake is not installed"
	exit -1
fi

/cross-tools/bin/cmake --version | grep "^cmake version ${VER_CMAKE}"
if [ $? != 0 ] ; then
	echo "ERROR: cmake is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf cmake-${VER_CMAKE} &&
set +x +u

