#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/ninja-${VER_NINJA}.tar.gz ] || exit -1
rm -rf ninja-${VER_NINJA} &&
tar xf ${LFS}/Sources/ninja-${VER_NINJA}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd ninja-${VER_NINJA} &&
python3 configure.py --bootstrap &&

########################################################################## TEST

####################################################################### INSTALL
install -vm755 ninja /cross-tools/bin/ &&

###################################################################### VALIDATE
if [ ! -x /cross-tools/bin/ninja ] ; then
	echo "ERROR: ninja is not installed"
	exit -1
fi

/cross-tools/bin/ninja --version | grep -E "^${VER_NINJA}$"
if [ $? != 0 ] ; then
	echo "ERROR: ninja is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf ninja-${VER_NINJA} &&
set +x +u

