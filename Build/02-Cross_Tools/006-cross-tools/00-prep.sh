#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f ${LFS}/Sources/binutils-${VER_BINUTILS}.tar.xz ] || exit -1
rm -rf binutils-${VER_BINUTILS} &&
tar xf ${LFS}/Sources/binutils-${VER_BINUTILS}.tar.xz &&

[ -f ${LFS}/Sources/gcc-${VER_GCC}.tar.xz ] || exit -1
rm -rf gcc-${VER_GCC} &&
tar xf ${LFS}/Sources/gcc-${VER_GCC}.tar.xz &&

[ -f ${LFS}/Sources/gmp-${VER_GMP}.tar.xz ] || exit -1
rm -rf gmp-${VER_GMP} &&
tar xf ${LFS}/Sources/gmp-${VER_GMP}.tar.xz &&

[ -f ${LFS}/Sources/mpfr-${VER_MPFR}.tar.xz ] || exit -1
rm -rf mpfr-${VER_MPFR} &&
tar xf ${LFS}/Sources/mpfr-${VER_MPFR}.tar.xz &&

[ -f ${LFS}/Sources/mpc-${VER_MPC}.tar.gz ] || exit -1
rm -rf mpc-${VER_MPC} &&
tar xf ${LFS}/Sources/mpc-${VER_MPC}.tar.gz &&

[ -f ${LFS}/Sources/isl-${VER_ISL}.tar.xz ] || exit -1
rm -rf isl-${VER_ISL} &&
tar xf ${LFS}/Sources/isl-${VER_ISL}.tar.xz &&

[ -f ${LFS}/Sources/musl-${VER_MUSL}.tar.gz ] || exit -1
rm -rf musl-${VER_MUSL} &&
tar xf ${LFS}/Sources/musl-${VER_MUSL}.tar.gz &&

[ -f ${LFS}/Sources/linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}.tar.xz ] || exit -1
rm -rf linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} &&
tar xf ${LFS}/Sources/linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}.tar.xz &&



cd mpfr-${VER_MPFR}/ &&
for p in ${LFS}/Sources/mpfr-patches/patch*
   do echo "Applying patch ${p}" &&
   patch -Z -p1 -i ${p};
done
cd ../ &&

cd gcc-${VER_GCC}/ &&

for file in gcc/config/{linux,i386/linux{,64},aarch64/aarch64-linux}.h
do
  cp -uv $file{,.orig}
  sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
      -e 's@/usr@/tools@g' $file.orig > $file
  echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
  touch $file.orig
done

### No multilib, so set 64-bit lib directory to lib
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
  ;;
  aarch64)
    sed -e '/lp64=/s/lib64/lib/' \
        -i.orig gcc/config/aarch64/t-aarch64-linux
  ;;
esac

cd ../


mkdir build
mkdir build/src_toolchain
mkdir build/obj_toolchain
mkdir build/obj_musl
mkdir build/obj_kernel_headers
mkdir build/obj_kernel_headers/staged
mkdir build/obj_sysroot
mkdir build/obj_sysroot/include
mkdir build/obj_sysroot/tools
mkdir build/obj_sysroot/tools/include
ln -sf . build/obj_sysroot/usr
ln -sf lib build/obj_sysroot/lib32
ln -sf lib build/obj_sysroot/lib64

cd build/src_toolchain
ln -sf ../../gcc-${VER_GCC}/* .
ln -sf ../../binutils-${VER_BINUTILS}/* .
ln -sf ../../gmp-${VER_GMP} gmp
ln -sf ../../mpfr-${VER_MPFR} mpfr
ln -sf ../../mpc-${VER_MPC} mpc
ln -sf ../../isl-${VER_ISL} isl
cd ../..

set +x +u
