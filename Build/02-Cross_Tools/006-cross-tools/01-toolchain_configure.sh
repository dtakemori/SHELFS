#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


P=$(pwd)

cd build/obj_toolchain

CFLAGS="${COPTS_CT}" \
CXXFLAGS="${CXXOPTS_CT}" \
LDFLAGS="-s" \
../src_toolchain/configure \
	--with-pkgversion="LFS Cross +PIC" \
	--prefix=/cross-tools \
	--target=${LFS_TMP} \
	--with-sysroot=/ \
	--with-lib-path=/tools/lib \
	--with-build-sysroot=${P}/build/obj_sysroot \
	--with-local-prefix=/tools \
	--with-native-system-header-dir=/tools/include \
	--enable-languages=c,c++ \
	--enable-tls \
	--enable-deterministic-archives \
	--enable-libstdcxx-time \
	--disable-multilib \
	--disable-decimal-float \
	--disable-gnu-indirect-function \
	--disable-libctf \
	--disable-libmpx \
	--disable-libmudflap \
	--disable-libsanitizer \
	--disable-werror &&

cd ../..

set +x +u
