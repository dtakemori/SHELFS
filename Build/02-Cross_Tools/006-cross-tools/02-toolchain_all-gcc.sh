
#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


cd build/obj_toolchain

make -j${MAKEJOBS:-2} \
	ac_cv_prog_lex_root=lex.yy \
	all-gcc

cd ../..

set +x +u
