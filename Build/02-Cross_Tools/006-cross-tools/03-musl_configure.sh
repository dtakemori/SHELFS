#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


cd build/obj_musl

../../musl-${VER_MUSL}/configure \
	--prefix=/tools \
	--host=${LFS_TMP} \
	CC="../obj_toolchain/gcc/xgcc -B ../obj_toolchain/gcc" \
	LIBCC="../obj_toolchain/${LFS_TMP}/libgcc/libgcc.a"

cd ../..

set +x +u
