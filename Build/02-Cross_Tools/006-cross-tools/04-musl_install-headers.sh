#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


P=$(pwd)

cd build/obj_musl

make -j${MAKEJOBS:-2} \
	DESTDIR=${P}/build/obj_sysroot \
	install-headers

cd ../..

set +x +u
