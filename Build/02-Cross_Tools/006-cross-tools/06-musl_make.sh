#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


cd build/obj_musl

make -j${MAKEJOBS:-2} \
	AR=../obj_toolchain/binutils/ar \
	RANLIB=../obj_toolchain/binutils/ranlib

cd ../..

set +x +u
