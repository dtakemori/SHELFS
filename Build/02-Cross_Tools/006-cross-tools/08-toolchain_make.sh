#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


cd build/obj_toolchain

make -j${MAKEJOBS:-2}

cd ../..

set +x +u
