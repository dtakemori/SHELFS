#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


P=$(pwd)

cd linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}

make -j${MAKEJOBS:-2} \
	ARCH=x86 \
	O=${P}/build/obj_kernel_headers \
	INSTALL_HDR_PATH=${P}/build/obj_kernel_headers/staged \
	headers_install

cd ..

find build/obj_kernel_headers/staged/include \
	'(' -name .install -o -name ..install.cmd ')' -exec rm -v {} +

set +x +u
