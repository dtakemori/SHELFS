#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


P=$(pwd)

cd build/obj_musl

if [ ! -f lib/libc.a ] ; then
	echo "ERROR: musl libc static library not built"
	exit -1
fi

if [ ! -f lib/libc.so ] ; then
	echo "ERROR: musl libc dynamic library not built"
	exit -1
fi

./lib/libc.so 2>&1 | grep "^Version ${VER_MUSL}"
if [ $? != 0 ] ; then
	echo "ERROR: musl libc wrong version"
	exit -1
fi

cd ../..


cd build/obj_toolchain

if [ ! -f gcc/xgcc ] ; then
	echo "ERROR: gcc not built"
	exit -1
fi

./gcc/xgcc -v 2>&1 | grep "^gcc version ${VER_GCC}"
if [ $? != 0 ] ; then
	echo "ERROR: gcc wrong version"
	exit -1
fi

cd ../..


cd build/obj_kernel_headers
if [ ! -f staged/include/linux/version.h ] ; then
	echo "ERROR: include/linux/version.h not staged"
	exit -1
fi

grep "LINUX_VERSION_CODE $(echo ${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} | \
	perl -ne '($a, $b, $c) = split(/\./, $_); 
		print( (65536 * $a) + (256 * $b) + $c);')"  \
	staged/include/linux/version.h
if [ $? != 0 ] ; then
	echo "ERROR: staged include/linux/version.h is wrong version"
	exit -1
fi

cd ../..


set +x +u
