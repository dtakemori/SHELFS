#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


P=$(pwd)

cd build/obj_musl
make \
	AR=../obj_toolchain/binutils/ar \
	RANLIB=../obj_toolchain/binutils/ranlib \
	DESTDIR=${LFS} \
	install
cd ../..


cd build/obj_toolchain
make \
	DESTDIR=${LFS} \
	install

ln -sfv ${LFS_TMP}-gcc \
	${LFS}/cross-tools/bin/${LFS_TMP}-cc
cd ../..


cd build/obj_kernel_headers
cp -R staged/include/* \
	${LFS}/tools/include 
cd ../..


ln -sv /tools/lib/ld-musl-$(uname -m).so.1 ${LFS}/tools/bin/ldd
mv -v ${LFS}/lib/ld-musl-$(uname -m).so.1 ${LFS}/tools/lib
rmdir -v ${LFS}/lib

mkdir -v ${LFS}/tools/etc
echo '/tools/lib' > ${LFS}/tools/etc/ld-musl-$(uname -m).path

set +x +u
