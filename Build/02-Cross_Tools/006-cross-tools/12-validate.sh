#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


if [ ! -f ${LFS}/tools/lib/libc.a ] ; then
	echo "ERROR: musl libc static library not installed"
	exit -1
fi

if [ ! -f ${LFS}/tools/lib/libc.so ] ; then
	echo "ERROR: musl libc dynamic library not installed"
	exit -1
fi

${LFS}/tools/lib/libc.so 2>&1 | grep "^Version ${VER_MUSL}"
if [ $? != 0 ] ; then
	echo "ERROR: installed musl libc wrong version"
	exit -1
fi


if [ ! -f ${LFS}/cross-tools/bin/${LFS_TMP}-gcc ] ; then
	echo "ERROR: cross gcc not installed"
	exit -1
fi

${LFS}/cross-tools/bin/${LFS_TMP}-gcc -v 2>&1 | grep "^gcc version ${VER_GCC}"
if [ $? != 0 ] ; then
	echo "ERROR: cross gcc wrong version"
	exit -1
fi

if [ ! -f ${LFS}/tools/include/linux/version.h ] ; then
	echo "ERROR: ${LFS}/tools/include/linux/version.h not installed"
	exit -1
fi

grep "LINUX_VERSION_CODE $(echo ${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} | \
	perl -ne '($a, $b, $c) = split(/\./, $_); 
		print( (65536 * $a) + (256 * $b) + $c);')"  \
	${LFS}/tools/include/linux/version.h
if [ $? != 0 ] ; then
	echo "ERROR: installed ${LFS}/tools/include/linux/version.h is wrong version"
	exit -1
fi


set +x +u
