#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
rm -rf binutils-${VER_BINUTILS} &&
rm -rf gcc-${VER_GCC} &&
rm -rf gmp-${VER_GMP} &&
rm -rf mpfr-${VER_MPFR} &&
rm -rf mpc-${VER_MPC} &&
rm -rf isl-${VER_ISL} &&
rm -rf musl-${VER_MUSL} &&
rm -rf linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} &&

rm -rf build &&
rm -rf output &&

set +x +u
