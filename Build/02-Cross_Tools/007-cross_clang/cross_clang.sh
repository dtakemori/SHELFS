#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/llvm-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf llvm-${VER_LLVM} &&
tar xf ${LFS}/Sources/llvm-${VER_LLVM}.src.tar.xz &&

[ -f ${LFS}/Sources/clang-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf clang-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/clang-${VER_LLVM}.src.tar.xz &&
mv -v clang-${VER_LLVM}.src llvm-${VER_LLVM}.src/tools/clang &&

[ -f ${LFS}/Sources/compiler-rt-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf compiler-rt-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/compiler-rt-${VER_LLVM}.src.tar.xz &&
mv -v compiler-rt-${VER_LLVM}.src llvm-${VER_LLVM}.src/projects/compiler-rt &&

[ -f ${LFS}/Sources/lld-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf lld-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/lld-${VER_LLVM}.src.tar.xz &&
mv -v lld-${VER_LLVM}.src llvm-${VER_LLVM}.src/projects/lld &&

[ -f ${LFS}/Sources/libunwind-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf libunwind-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/libunwind-${VER_LLVM}.src.tar.xz &&
mv -v libunwind-${VER_LLVM}.src libunwind &&

[ -f ${LFS}/Sources/cmake-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf cmake-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/cmake-${VER_LLVM}.src.tar.xz &&
mv -v cmake-${VER_LLVM}.src cmake &&

[ -f ${LFS}/Sources/third-party-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf third-party-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/third-party-${VER_LLVM}.src.tar.xz &&
mv -v third-party-${VER_LLVM}.src third-party &&

######################################################################### PATCH
cd llvm-${VER_LLVM}.src/ &&
sed -i 's@/lib/ld-musl-@/tools/lib/ld-musl-@' \
	tools/clang/lib/Driver/ToolChains/Linux.cpp
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="gcc" \
CXX="g++" \
cmake -DCMAKE_INSTALL_PREFIX=/cross-tools \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_CROSSCOMPILING=True \
	-DCMAKE_C_FLAGS="${COPTS_CT}" \
	-DCMAKE_CXX_FLAGS="${CXXOPTS_CT}" \
	-DLLVM_DEFAULT_TARGET_TRIPLE=${LFS_TMP} \
	-DLLVM_BUILD_LLVM_DYLIB=ON \
	-DLLVM_BUILD_BENCHMARKS=OFF \
	-DLLVM_BUILD_EXAMPLES=OFF \
	-DLLVM_BUILD_TESTS=OFF \
	-DLLVM_INCLUDE_BENCHMARKS=OFF \
	-DLLVM_INCLUDE_EXAMPLES=OFF \
	-DLLVM_INCLUDE_TESTS=ON \
	-DLLVM_ENABLE_FFI=ON \
	-DLLVM_LINK_LLVM_DYLIB=ON \
	-DLLVM_TARGETS_TO_BUILD="host" \
	-DCLANG_DEFAULT_RTLIB="compiler-rt" \
	-DCLANG_ENABLE_ARCMT=OFF \
	-DCLANG_ENABLE_STATIC_ANALYZER=OFF \
	-DCOMPILER_RT_BUILD_SANITIZERS=OFF \
	-DCOMPILER_RT_BUILD_PROFILE=OFF \
	-DCOMPILER_RT_BUILD_LIBFUZZER=OFF \
	-DCOMPILER_RT_BUILD_XRAY=OFF \
	-Wno-dev \
	-G Ninja \
	../llvm-${VER_LLVM}.src &&

######################################################################### BUILD
ninja -j ${MAKEJOBS} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 2 ] ;  then
  /usr/bin/time -vao ../test.log ninja -j ${MAKEJOBS} check-all > ../test.log
fi
	
####################################################################### INSTALL
ninja -j ${MAKEJOBS} install &&

for i in ar nm objcopy objdump ranlib readelf strings strip; do 
	ln -sv llvm-${i} /cross-tools/bin/${i}
done
ln -sv clang-cpp /cross-tools/bin/cpp &&
ln -sv ld.lld /cross-tools/bin/ld &&

###################################################################### VALIDATE
if [ ! -x /cross-tools/bin/clang ] ; then
	echo "ERROR: /cross-tools/bin/clang not installed"
	exit -1
fi

/cross-tools/bin/clang -v 2>&1 | grep -E "^clang version ${VER_LLVM}"
if [ $? != 0 ] ; then
	echo "ERROR: /cross-tools/bin/clang is wrong version"
	exit -1
fi

/cross-tools/bin/clang -v 2>&1 | grep -E "^Target: ${LFS_TMP}"
if [ $? != 0 ] ; then
	echo "ERROR: /cross-tools/bin/clang has wrong target"
	exit -1
fi

### Verify compiler output uses temporary environment
/cross-tools/bin/clang -v -Wl,--verbose --sysroot=/tools -fuse-ld=lld \
	${LFS}/Build/Config/dummy.c &> dummy.log &&
if [ ! -x a.out ] ; then
	echo "ERROR: no compiler output"
	exit -1
fi

readelf --string-dump .comment a.out | grep "Linker: LLD ${VER_LLVM}"
if [ $? != 0 ] ; then
	echo "ERROR: LLVM linker not used"
	exit -1
fi

readelf -l a.out | grep 'interpreter: /tools' 
if [ $? != 0 ] ; then
	echo "ERROR: Interpreter not in temporary environment"
	exit -1
fi

grep 'ld.lld: /tools/lib.*crt[1in].o' dummy.log 
if [ $? != 0 ] ; then
	echo "ERROR: C runtime files not found"
	exit -1
fi

grep -B3 '^ /tools/include' dummy.log &&
if [ $? != 0 ] ; then
	echo "ERROR: Include path does not include /tools/include"
	exit -1
fi

grep "ld.lld: /tools/lib/libc.so" dummy.log 
if [ $? != 0 ] ; then
	echo "ERROR: C library not found"
	exit -1
fi

### Verify default PIE output
readelf -l a.out | grep -i '^ELF file type is DYN' 
if [ $? != 0 ] ; then
	echo "ERROR: Default output is not PIE"
	exit -1
fi
rm -vf a.out

### Check we can compile static
/cross-tools/bin/clang -static -v -Wl,--verbose --sysroot=/tools -fuse-ld=lld \
	${LFS}/Build/Config/dummy.c &&
if [ $? != 0 ] ; then
	echo "ERROR: Unable to compile -static"
	exit -1
fi

readelf -l a.out | grep -i '^ELF file type is EXEC' 
if [ $? != 0 ] ; then
	echo "ERROR: -static output is not EXEC"
	exit -1
fi
rm -vf a.out

####################################################################### CLEANUP
cp -v CMakeCache.txt ../CMakeCache.log &&
cp -v dummy.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf cmake &&
rm -rf third-party &&
rm -rf llvm-${VER_LLVM}.src &&
rm -rf libunwind &&
rm -rf cmake-${VER_LLVM}.src &&
set +x +u
