#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


### Strip static libraries
/bin/find ${LFS}/cross-tools/lib		-type f -name '*\.a' -exec /usr/bin/strip -p --strip-debug '{}' ';' 


### Strip dynamic libraries
/bin/find ${LFS}/cross-tools/lib		-type f -name '*\.so*' -exec /usr/bin/strip -p --strip-unneeded '{}' ';' 
/bin/find ${LFS}/cross-tools/libexec		-type f -name '*\.so*' -exec /usr/bin/strip -p --strip-unneeded '{}' ';' 
/bin/find ${LFS}/cross-tools/${LFS_TMP}/lib	-type f -name '*\.so*' -exec /usr/bin/strip -p --strip-unneeded '{}' ';' 


### Strip binaries
/usr/bin/strip -p --strip-all ${LFS}/cross-tools/bin/*
/usr/bin/strip -p --strip-all ${LFS}/cross-tools/libexec/gcc/${LFS_TMP}/${VER_GCC}/{cc1,cc1plus,collect2,g++-mapper-server,lto-wrapper,lto1}


### Purge docs
rm -rf ${LFS}/cross-tools/{,share}/{info,man,doc}


set +x +u
