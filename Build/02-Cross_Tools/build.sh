#!/bin/bash


echo "===> M4 Start" &&
cd 001-m4 &&
/usr/bin/time -vao build.log ./m4.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== M4 End" &&


echo "===> Autoconf Start" &&
cd 002-autoconf &&
/usr/bin/time -vao build.log ./autoconf.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Autoconf End" &&


echo "===> Automake Start" &&
cd 003-automake &&
/usr/bin/time -vao build.log ./automake.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Automake End" &&


echo "===> Cmake Start" &&
cd 004-cmake &&
/usr/bin/time -vao build.log ./cmake.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Cmake End" &&


echo "===> Ninja Start" &&
cd 005-ninja &&
/usr/bin/time -vao build.log ./ninja.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Ninja End" &&


echo "===> Cross-Tools Start" &&
cd 006-cross-tools &&

echo "  Prep" &&
/usr/bin/time -vao build.log ./00-prep.sh   >  build.log 2>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Toolchain Configure" &&
/usr/bin/time -vao build.log ./01-toolchain_configure.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Toolchain all-gcc" &&
/usr/bin/time -vao build.log ./02-toolchain_all-gcc.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Musl Configure" &&
/usr/bin/time -vao build.log ./03-musl_configure.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Musl Install-Headers" &&
/usr/bin/time -vao build.log ./04-musl_install-headers.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Toolchain all-target-libgcc" &&
/usr/bin/time -vao build.log ./05-toolchain_all-target-libgcc.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Musl make" &&
/usr/bin/time -vao build.log ./06-musl_make.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Musl install" &&
/usr/bin/time -vao build.log ./07-musl_install.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Toolchain make" &&
/usr/bin/time -vao build.log ./08-toolchain_make.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Linux headers_install" &&
/usr/bin/time -vao build.log ./09-linux_headers_install.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Build check" &&
/usr/bin/time -vao build.log ./10-obj_check.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Install" &&
/usr/bin/time -vao build.log ./11-install.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Validate" &&
/usr/bin/time -vao build.log ./12-validate.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Cleanup" &&
/usr/bin/time -vao build.log ./13-cleanup.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

cd .. &&
echo "<=== Cross-Tools End"  &&


echo "===> Cross Clang Start" &&
cd 007-cross_clang &&
/usr/bin/time -vao build.log ./cross_clang.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Cross Clang End" &&


echo "===> Cross Strip Start" &&
cd 008-cross_strip &&
/usr/bin/time -vao build.log ./cross_strip.sh  > build.log 2>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&
cd .. &&
echo "<=== Cross Strip End"
