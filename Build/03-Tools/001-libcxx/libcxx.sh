#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
mkdir -v source &&

[ -f ${LFS}/Sources/cmake-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf cmake-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/cmake-${VER_LLVM}.src.tar.xz &&
mv -v cmake-${VER_LLVM}.src source/cmake &&

[ -f ${LFS}/Sources/compiler-rt-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf compiler-rt-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/compiler-rt-${VER_LLVM}.src.tar.xz &&
mv -v compiler-rt-${VER_LLVM}.src source/compiler-rt &&

[ -f ${LFS}/Sources/libcxx-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf libcxx-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/libcxx-${VER_LLVM}.src.tar.xz &&
mv -v libcxx-${VER_LLVM}.src source/libcxx &&

[ -f ${LFS}/Sources/libcxxabi-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf libcxxabi-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/libcxxabi-${VER_LLVM}.src.tar.xz &&
mv -v libcxxabi-${VER_LLVM}.src source/libcxxabi &&

[ -f ${LFS}/Sources/libunwind-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf libunwind-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/libunwind-${VER_LLVM}.src.tar.xz &&
mv -v libunwind-${VER_LLVM}.src source/libunwind &&

[ -f ${LFS}/Sources/runtimes-${VER_LLVM}.src.tar.xz ] || exit -1
rm -rf runtimes-${VER_LLVM}.src &&
tar xf ${LFS}/Sources/runtimes-${VER_LLVM}.src.tar.xz &&
mv -v runtimes-${VER_LLVM}.src source/runtimes &&

echo "cmake_minimum_required(VERSION 3.20.0)"  > source/CMakeLists.txt
echo "project(Runtimes C CXX ASM)"            >> source/CMakeLists.txt
echo "add_subdirectory(libcxxabi)"            >> source/CMakeLists.txt
echo "add_subdirectory(libcxx)"               >> source/CMakeLists.txt
echo "add_subdirectory(libunwind)"            >> source/CMakeLists.txt

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang -fPIC --sysroot=/tools" \
CXX="clang++ -fPIC --sysroot=/tools" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld" \
cmake -DCMAKE_INSTALL_PREFIX=/tools \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_CROSSCOMPILING=True \
	-DCMAKE_C_FLAGS="${COPTS_CT}" \
	-DCMAKE_CXX_FLAGS="${CXXOPTS_CT}" \
	-DCMAKE_AR=/cross-tools/bin/llvm-ar \
	-DCMAKE_LINKER=/cross-tools/bin/lld \
	-DCMAKE_OBJCOPY=/cross-tools/bin/llvm-objcopy \
	-DCMAKE_OBJDUMP=/cross-tools/bin/llvm-objdump \
	-DCMAKE_RANLIB=/cross-tools/bin/llvm-ranlib \
	-DCMAKE_STRIP=/cross-tools/bin/llvm-strip \
	-DLLVM_ENABLE_RUNTIMES="libunwind;compiler-rt" \
	-DLLVM_INCLUDE_TESTS=OFF \
	-DLIBCXX_HAS_ATOMIC_LIB=NO \
	-DLIBCXX_HAS_MUSL_LIBC=YES \
	-DLIBCXX_INCLUDE_BENCHMARKS=NO \
	-DLIBCXX_INCLUDE_TESTS=NO \
	-DLIBCXX_INSTALL_INCLUDE_TARGET_DIR=/tools/include/c++/v1 \
	-DLIBCXX_INSTALL_LIBRARY_DIR=/tools/lib \
	-DLIBCXX_USE_COMPILER_RT=YES \
	-DLIBCXXABI_INCLUDE_TESTS=NO \
	-DLIBCXXABI_INSTALL_LIBRARY_DIR=/tools/lib \
	-DLIBCXXABI_USE_COMPILER_RT=YES \
	-DLIBCXXABI_USE_LLVM_UNWINDER=YES \
	-DLIBUNWIND_USE_COMPILER_RT=ON \
	-DLIBUNWIND_INSTALL_LIBRARY_DIR=/tools/lib \
	-DPython3_EXECUTABLE=/usr/bin/python3 \
	-Wno-dev \
	-G Ninja \
	../source &&


######################################################################### BUILD
ninja -j ${MAKEJOBS} unwind &&
ninja -j ${MAKEJOBS} cxxabi &&
ninja -j ${MAKEJOBS} cxx &&

########################################################################## TEST
	
####################################################################### INSTALL
ninja -j ${MAKEJOBS} install-unwind &&
ninja -j ${MAKEJOBS} install-cxxabi &&
ninja -j ${MAKEJOBS} install-cxx &&

###################################################################### VALIDATE
if [ ! -f ${LFS}/tools/lib/libunwind.a ] ; then
	echo "ERROR: libunwind static library not installed"
	exit -1
fi

if [ ! -f ${LFS}/tools/lib/libc++abi.a ] ; then
	echo "ERROR: libc++abi static library not installed"
	exit -1
fi

if [ ! -f ${LFS}/tools/lib/libc++.a ] ; then
	echo "ERROR: libc++ static library not installed"
	exit -1
fi

if [ ! -h ${LFS}/tools/lib/libunwind.so.1 ] ; then
	echo "ERROR: libunwind dynamic library not installed"
	exit -1
fi

if [ ! -h ${LFS}/tools/lib/libc++abi.so.1 ] ; then
	echo "ERROR: libc++abi dynamic library not installed"
	exit -1
fi

if [ ! -h ${LFS}/tools/lib/libc++.so.1 ] ; then
	echo "ERROR: libc++ dynamic library not installed"
	exit -1
fi

readelf -d ${LFS}/tools/lib/libunwind.so.1 | grep "gcc_s"
if [ $? = 0 ]; then
	echo "ERROR: libunwind library has libgcc dependency"
	exit -1
fi

readelf -d ${LFS}/tools/lib/libc++abi.so.1 | grep "gcc_s"
if [ $? = 0 ]; then
	echo "ERROR: libc++abi library has libgcc dependency"
	exit -1
fi

readelf -d ${LFS}/tools/lib/libc++abi.so.1 | grep "libunwind"
if [ $? != 0 ]; then
	echo "ERROR: libc++abi library missing llvm libunwind dependency"
	exit -1
fi

readelf -d ${LFS}/tools/lib/libc++.so.1 | grep "gcc_s"
if [ $? = 0 ]; then
	echo "ERROR: libc++ library has libgcc dependency"
	exit -1
fi

readelf -d ${LFS}/tools/lib/libc++.so.1 | grep "libunwind"
if [ $? != 0 ]; then
	echo "ERROR: libc++ library missing llvm libunwind dependency"
	exit -1
fi

####################################################################### CLEANUP
cp -v CMakeCache.txt ../CMakeCache.log &&
cd ../ &&
rm -rf build &&
rm -rf source &&
set +x +u
