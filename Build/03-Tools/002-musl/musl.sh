#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/musl-${VER_MUSL}.tar.gz ] || exit -1
rm -rf musl-${VER_MUSL} &&
tar xf ${LFS}/Sources/musl-${VER_MUSL}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../musl-${VER_MUSL}/configure \
	--prefix=/tools \
	--host=${LFS_TMP} \
	--disable-static \
	--syslibdir=/tools/lib &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

####################################################################### INSTALL 
make install &&

###################################################################### VALIDATE
if [ ! -f ${LFS}/tools/lib/libc.a ] ; then
	echo "ERROR: libc static library not installed"
	exit -1
fi

if [ ! -f ${LFS}/tools/lib/libc.so ] ; then
	echo "ERROR: libc dynamic library not installed"
	exit -1
fi

readelf --string-dump .comment ${LFS}/tools/lib/libc.so \
	| grep "clang version ${VER_LLVM}"
if [ $? != 0 ]; then
	echo "ERROR: libc library not built by clang"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf musl-${VER_MUSL} &&
set +x +u
