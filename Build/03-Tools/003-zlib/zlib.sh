#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/zlib-ng-${VER_ZLIBNG}.tar.gz ] || exit -1
rm -rf zlib-ng-${VER_ZLIBNG} &&
tar xf ${LFS}/Sources/zlib-ng-${VER_ZLIBNG}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd zlib-ng-${VER_ZLIBNG} &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt -Wl,--undefined-version" \
./configure \
	--prefix=/tools \
	--includedir=/tools/include \
	--libdir=/tools/lib \
	--sharedlibdir=/tools/lib \
	--zlib-compat &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL 
make install &&
ldconfig &&

###################################################################### VALIDATE
if [ ! -f /tools/include/zlib.h ] ; then
	echo "ERROR: zlib.h header not installed"
	exit -1
fi

grep "ZLIBNG_VERSION \"${VER_ZLIBNG}\"" /tools/include/zlib.h
if [ $? != 0 ]; then
	echo "ERROR: ZLIB header is incorrect version"
	exit -1
fi

if [ ! -f /tools/lib/libz.a ] ; then
	echo "ERROR: ZLIB static library not installed"
	exit -1
fi

if [ ! -h /tools/lib/libz.so ] ; then
	echo "ERROR: ZLIB dynamic library not installed"
	exit -1
fi


####################################################################### CLEANUP
cp -v configure.log ../ &&
cd ../ &&
rm -rf zlib-ng-${VER_ZLIBNG} &&
set +x +u
