#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/bzip2-${VER_BZIP2}.tar.gz ] || exit -1
rm -rf bzip2-${VER_BZIP2} &&
tar xf ${LFS}/Sources/bzip2-${VER_BZIP2}.tar.gz &&

######################################################################### PATCH
cd bzip2-${VER_BZIP2} &&
patch -p1 -i ${LFS}/Patches/bzip2-1.0.8-shared-buildflags.patch &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

ln -v -s ../bzip2-${VER_BZIP2}/* . &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} -f Makefile-libbz2_so \
	CFLAGS="${COPTS_T} -fpic -fPIC -D_FILE_OFFSET_BITS=64 -Wall -Winline" \
	LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt -Wl,-rpath=/tools/lib" \
	CC="clang --sysroot=/tools --target=${LFS_TMP}" \
	AR="llvm-ar" \
	RANLIB="llvm-ranlib" &&

make clean &&

make -j${MAKEJOBS:-2} -f Makefile \
	CFLAGS="${COPTS_T} -fpic -fPIC -D_FILE_OFFSET_BITS=64 -Wall -Winline" \
	LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt -Wl,-rpath=/tools/lib" \
	CC="clang --sysroot=/tools --target=${LFS_TMP}" \
	AR="llvm-ar" \
	RANLIB="llvm-ranlib" &&

########################################################################## TEST

####################################################################### INSTALL
make PREFIX=/tools install &&
install -v bzip2-shared /tools/bin/bzip2 &&

cp -va libbz2.so* /tools/lib &&
ln -vs libbz2.so.${VER_BZIP2}  /tools/lib/libbz2.so &&

ldconfig &&

rm -vf /tools/bin/{bunzip2,bzcat} &&
ln -vs bzip2 /tools/bin/bunzip2 &&
ln -vs bzip2 /tools/bin/bzcat &&

rm -vf /tools/bin/{bzcmp,bzegrep,bzfgrep,bzless} &&
ln -vs bzdiff /tools/bin/bzcmp &&
ln -vs bzmore /tools/bin/bzless &&
ln -vs bzgrep /tools/bin/bzegrep &&
ln -vs bzgrep /tools/bin/bzfgrep &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/bzip2 ] ; then
	echo "ERROR: bzip2 not installed"
	exit -1
fi

/tools/bin/bzip2 --help 2>&1 | grep "Version ${VER_BZIP2}"
if [ $? != 0 ] ; then
	echo "ERROR: bzip2 is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf bzip2-${VER_BZIP2} &&
set +x +u
