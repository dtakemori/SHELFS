#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/xz-${VER_XZ}.tar.xz ] || exit -1
rm -rf xz-${VER_XZ} &&
tar xf ${LFS}/Sources/xz-${VER_XZ}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt -Wl,-rpath=/tools/lib" \
../xz-${VER_XZ}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/xz ] ; then
	echo "ERROR: xz not installed"
	exit -1
fi

/tools/bin/xz -V | grep "^xz (XZ Utils) ${VER_XZ}$"
if [ $? != 0 ] ; then
	echo "ERROR: xz is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf xz-${VER_XZ} &&
set +x +u
