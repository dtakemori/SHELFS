#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/zstd-${VER_ZSTD}.tar.gz ] || exit -1
rm -rf zstd-${VER_ZSTD} &&
tar xf ${LFS}/Sources/zstd-${VER_ZSTD}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd zstd-${VER_ZSTD} &&

cd lib &&
make -j${MAKEJOBS:-2} \
	AR="llvm-ar" \
	CC="clang" \
	CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
	libzstd.a &&

make -j${MAKEJOBS:-2} \
	AR="llvm-ar" \
	CC="clang" \
	CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
	LDFLAGS="${LDOPTS_T} -shared -fPIC -fvisibility=hidden -fuse-ld=lld --rtlib=compiler-rt" \
	libzstd &&

cd ../programs &&
make -j${MAKEJOBS:-2} \
	AR="llvm-ar" \
	CC="clang" \
	CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
	LDFLAGS="${LDOPTS_T} -pthread -lz -llzma -fuse-ld=lld --rtlib=compiler-rt" \

cd .. &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ; then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make prefix=/tools install &&
ldconfig &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/zstd ] ; then
	echo "ERROR: zstd not installed"
	exit -1
fi

/tools/bin/zstd -V | grep -E " Zstandard .* v${VER_ZSTD},"
if [ $? != 0 ] ; then
	echo "ERROR: zstd is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd .. &&
rm -rf zstd-${VER_ZSTD} &&
set +x +u 
