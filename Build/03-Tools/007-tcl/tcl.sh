#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/tcl${VER_TCL}.${VER_TCLPATCH}-src.tar.gz ] || exit -1
rm -rf tcl${VER_TCL}.${VER_TCLPATCH} &&
tar xf ${LFS}/Sources/tcl${VER_TCL}.${VER_TCLPATCH}-src.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../tcl${VER_TCL}.${VER_TCLPATCH}/unix/configure \
	--host=${LFS_TMP} \
	--prefix=/tools \
	--enable-threads \
	ac_cv_func_strtod=no &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
ldconfig &&
make install-private-headers &&
ln -sv tclsh${VER_TCL} /tools/bin/tclsh &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/tclsh ] ; then
	echo "ERROR: tclsh is not installed"
	exit -1
fi

echo 'puts [info patchlevel] ; exit 0' | /tools/bin/tclsh | grep "^${VER_TCL}.${VER_TCLPATCH}$"
if [ $? != 0 ] ; then
	echo "ERROR: tclsh is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf tcl${VER_TCL}.${VER_TCLPATCH} &&
set +x +u
