#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/expect${VER_EXPECT}.tar.gz ] || exit -1
rm -rf expect${VER_EXPECT} &&
tar xf ${LFS}/Sources/expect${VER_EXPECT}.tar.gz &&

######################################################################### PATCH
cd expect${VER_EXPECT} &&
patch -p1 -i ${LFS}/Patches/expect-5.45.4-covscan-fixes.patch &&
patch -p1 -i ${LFS}/Patches/expect-c99.patch &&
patch -p1 -i ${LFS}/Patches/expect-configure-c99.patch &&

cp -v configure{,.orig} &&
sed 's:/usr/local/bin:/bin:' configure.orig > configure &&

aclocal &&
autoreconf &&
cd testsuite &&
autoreconf -I .. &&
cd ../ &&

cp -vf /cross-tools/share/automake-${VER_AUTOMAKE}/config.guess tclconfig/config.guess &&
cp -vf /cross-tools/share/automake-${VER_AUTOMAKE}/config.sub tclconfig/config.sub &&

cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../expect${VER_EXPECT}/configure \
	--prefix=/tools \
	--with-tcl=/tools/lib \
	--with-tclinclude=/tools/include &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make SCRIPTS="" install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/expect ] ; then
	echo "ERROR: expect is not installed"
	exit -1
fi

/tools/bin/expect -v | grep "^expect version ${VER_EXPECT}$"
if [ $? != 0 ] ; then
	echo "ERROR: expect is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf expect${VER_EXPECT} &&
set +x +u
