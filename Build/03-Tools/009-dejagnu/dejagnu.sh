#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/dejagnu-${VER_DEJAGNU}.tar.gz ] || exit -1
rm -rf dejagnu-${VER_DEJAGNU} &&
tar xf ${LFS}/Sources/dejagnu-${VER_DEJAGNU}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
CXX="clang++" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
CXXFLAGS="${COPTS_T} --sysroot=/tools -isystem /tools/include/c++/v1 --stdlib=libc++ --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../dejagnu-${VER_DEJAGNU}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools &&

######################################################################### BUILD

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/runtest ] ; then
	echo "ERROR: runtest is not installed"
	exit -1
fi

/tools/bin/runtest --version | grep "^DejaGnu version$(printf '\t')${VER_DEJAGNU}$"
if [ $? != 0 ] ; then
	echo "ERROR: runtest is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf dejagnu-${VER_DEJAGNU} &&
set +x +u
