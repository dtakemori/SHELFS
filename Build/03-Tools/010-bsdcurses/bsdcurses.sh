#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/netbsd-curses-${VER_BSDCURSES}.tar.gz ] || exit -1
rm -rf netbsd-curses-${VER_BSDCURSES} &&
tar xf ${LFS}/Sources/netbsd-curses-${VER_BSDCURSES}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd netbsd-curses-${VER_BSDCURSES} &&
make -j${MAKEJOBS:-2} \
	CC="clang" \
	CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
	LDFLAGS="${LDOPTS_T} -L/tools/lib -fuse-ld=lld --rtlib=compiler-rt --stdlib=libc++" \
	PREFIX=/tools \
	all-dynamic &&

########################################################################## TEST

####################################################################### INSTALL
make PREFIX=/tools install-dynamic &&
ldconfig &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/tset ] ; then
	echo "ERROR: tset is not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf netbsd-curses-${VER_BSDCURSES} &&
set +x +u
