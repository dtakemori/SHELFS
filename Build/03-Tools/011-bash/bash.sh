#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/bash-${VER_BASH}.tar.gz ] || exit -1
rm -rf bash-${VER_BASH} &&
tar xf ${LFS}/Sources/bash-${VER_BASH}.tar.gz &&

######################################################################### PATCH
cd bash-${VER_BASH}/ &&
for p in ${LFS}/Sources/bash-patches/bash*-???
   do echo "Applying patch ${p}" &&
   patch -p0 -i ${p};
done
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../bash-${VER_BASH}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools \
	--without-bash-malloc &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 2 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k tests > ../test.log
fi

####################################################################### INSTALL
make install &&
if [ -h /tools/bin/sh ] ; then
  rm -vf /tools/bin/sh 
fi
ln -vs bash /tools/bin/sh &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/bash ] ; then
	echo "ERROR: bash is not installed"
	exit -1
fi

/tools/bin/bash --version | grep "^GNU bash, version ${VER_BASH}"
if [ $? != 0 ] ; then
	echo "ERROR: bash is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf bash-${VER_BASH} &&
set +x +u
