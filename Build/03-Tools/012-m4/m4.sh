#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/m4-${VER_M4}.tar.xz ] || exit -1
rm -rf m4-${VER_M4} &&
tar xf ${LFS}/Sources/m4-${VER_M4}.tar.xz &&

########################################################################## PREP

######################################################################### PATCH
mkdir -v build/ &&
cd build/ &&

env CPPFLAGS="-D_GNU_SOURCE" \
CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../m4-${VER_M4}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools &&


######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/m4 ] ; then
	echo "ERROR: m4 is not installed"
	exit -1
fi

/tools/bin/m4 --version | grep "^m4 (GNU M4) ${VER_M4}$"
if [ $? != 0 ] ; then
	echo "ERROR: m4 is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf m4-${VER_M4} &&
set +x +u
