#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/bison-${VER_BISON}.tar.xz ] || exit -1
rm -rf bison-${VER_BISON} &&
tar xf ${LFS}/Sources/bison-${VER_BISON}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

M4="/tools/bin/m4" \
CC="clang" \
CXX="clang++" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
CXXFLAGS="${COPTS_T} --sysroot=/tools -isystem /tools/include/c++/v1 --stdlib=libc++ --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -L/tools/lib -fuse-ld=lld --rtlib=compiler-rt --stdlib=libc++" \
../bison-${VER_BISON}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 2 ] ;  then
  /usr/bin/time -vao ../test.log make -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/bison ] ; then
	echo "ERROR: bison is not installed"
	exit -1
fi

/tools/bin/bison --version | grep "^bison (GNU Bison) ${VER_BISON}$"
if [ $? != 0 ] ; then
	echo "ERROR: bison is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf bison-${VER_BISON} &&
set +x +u
