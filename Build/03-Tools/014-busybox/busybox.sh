#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/busybox-${VER_BUSYBOX}.tar.bz2 ] || exit -1
rm -rf busybox-${VER_BUSYBOX} &&
tar xf ${LFS}/Sources/busybox-${VER_BUSYBOX}.tar.bz2 &&

######################################################################### PATCH
cd busybox-${VER_BUSYBOX} &&
patch -p1 -i ${LFS}/Patches/busybox-1.37.0_hwaccel.patch &&
cd ../ &&

########################################################################## PREP
cd busybox-${VER_BUSYBOX} &&
cp -v ${LFS}/Build/Config/busybox-${VER_BUSYBOX}.config_temp .config &&
make -j${MAKEJOBS:-2} \
	HOSTCC=gcc \
	CC="clang" \
	CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
	oldconfig &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} \
	CC="clang" \
	CFLAGS="-g0 -O0 -pipe --sysroot=/tools --target=${LFS_TMP}" \
	LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" &&

########################################################################## TEST

####################################################################### INSTALL
make CC="clang" \
	CFLAGS="-g0 -O0 -pipe --sysroot=/tools --target=${LFS_TMP}" \
	LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
	install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/busybox ] ; then
	echo "ERROR: busybox is not installed"
	exit -1
fi

/tools/bin/busybox | grep ^"BusyBox v${VER_BUSYBOX}"
if [ $? != 0 ] ; then
	echo "ERROR: busybox is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf busybox-${VER_BUSYBOX} &&
set +x +u
