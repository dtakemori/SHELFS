#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/libffi-${VER_LIBFFI}.tar.gz ] || exit -1
rm -rf libffi-${VER_LIBFFI} &&
tar xf ${LFS}/Sources/libffi-${VER_LIBFFI}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP} -Wno-implicit-function-declaration" \
LDFLAGS="${LDOPTS_T} -L/tools/lib -fuse-ld=lld --rtlib=compiler-rt" \
../libffi-${VER_LIBFFI}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

rm -v /tools/lib/libffi.la &&

###################################################################### VALIDATE
if [ ! -f /tools/include/ffi.h ] ; then
	echo "ERROR: ffi.h header not installed"
	exit -1
fi

if [ ! -f /tools/lib/libffi.so ] ; then
	echo "ERROR: libffi.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libffi-${VER_LIBFFI} &&
set +x +u
