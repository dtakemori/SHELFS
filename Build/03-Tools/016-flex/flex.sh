#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/flex-${VER_FLEX}.tar.gz ] || exit -1
rm -rf flex-${VER_FLEX} &&
tar xf ${LFS}/Sources/flex-${VER_FLEX}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
CXX="clang++" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
CXXFLAGS="${COPTS_T} --sysroot=/tools -isystem /tools/include/c++/v1 --stdlib=libc++ --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../flex-${VER_FLEX}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

ln -sv flex /tools/bin/lex &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/flex ] ; then
	echo "ERROR: flex is not installed"
	exit -1
fi

/tools/bin/flex --version | grep "^flex ${VER_FLEX}$"
if [ $? != 0 ] ; then
	echo "ERROR: flex is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf flex-${VER_FLEX} &&
set +x +u
