#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/gawk-${VER_GAWK}.tar.xz ] || exit -1
rm -rf gawk-${VER_GAWK} &&
tar xf ${LFS}/Sources/gawk-${VER_GAWK}.tar.xz &&

######################################################################### PATCH
cd gawk-${VER_GAWK} &&
sed -i 's/extras//' Makefile.in &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../gawk-${VER_GAWK}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools \
	--libexecdir=/tools/lib \
	--disable-extensions &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/gawk ] ; then
	echo "ERROR: gawk is not installed"
	exit -1
fi

/tools/bin/gawk --version | grep "^GNU Awk ${VER_GAWK}"
if [ $? != 0 ] ; then
	echo "ERROR: gawk is wrong version"
	exit -1
fi

###################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gawk-${VER_GAWK} &&
set +x +u
