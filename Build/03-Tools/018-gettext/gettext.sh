#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/gettext-${VER_GETTEXT}.tar.xz ] || exit -1
rm -rf gettext-${VER_GETTEXT} &&
tar xf ${LFS}/Sources/gettext-${VER_GETTEXT}.tar.xz &&

######################################################################### PATCH
cd gettext-${VER_GETTEXT} &&
sed -e '/^structured/s/xmlError \*/typeof(xmlCtxtGetLastError(NULL)) /' \
	-i gettext-tools/src/its.c &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
CXX="clang++" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
CXXFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
EMACS="no" \
../gettext-${VER_GETTEXT}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools \
	--disable-shared &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} -C libtextstyle &&
make -j${MAKEJOBS:-2} -C gettext-runtime &&
make -j${MAKEJOBS:-2} -C gettext-tools &&

########################################################################## TEST

####################################################################### INSTALL
cp -v gettext-tools/src/{msgfmt,msgmerge,xgettext} /tools/bin &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/msgfmt ] ; then
	echo "ERROR: msgfmt is not installed"
	exit -1
fi

/tools/bin/msgfmt --version | grep "^msgfmt (GNU gettext-tools) ${VER_GETTEXT}$"
if [ $? != 0 ] ; then
	echo "ERROR: msgfmt is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gettext-runtime &&
rm -rf gettext-${VER_GETTEXT} &&
set +x +u
