#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/grep-${VER_GREP}.tar.xz ] || exit -1
rm -rf grep-${VER_GREP} &&
tar xf ${LFS}/Sources/grep-${VER_GREP}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../grep-${VER_GREP}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/grep ] ; then
	echo "ERROR: grep is not installed"
	exit -1
fi

/tools/bin/grep --version | grep "^grep (GNU grep) ${VER_GREP}$"
if [ $? != 0 ] ; then
	echo "ERROR: grep is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf grep-${VER_GREP} &&
set +x +u
