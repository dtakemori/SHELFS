#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/lzip-${VER_LZIP}.tar.gz ] || exit -1
rm -rf lzip-${VER_LZIP} &&
tar xf ${LFS}/Sources/lzip-${VER_LZIP}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

../lzip-${VER_LZIP}/configure \
	CXX="clang++" \
	CXXFLAGS="${COPTS_T} --sysroot=/tools -isystem /tools/include/c++/v1 --stdlib=libc++ --target=${LFS_TMP}" \
	LDFLAGS="${LDOPTS_T} -L/tools/lib -fuse-ld=lld --rtlib=compiler-rt --stdlib=libc++" \
	--prefix=/tools &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/lzip ] ; then
	echo "ERROR: lzip not installed"
	exit -1
fi

/tools/bin/lzip -V | grep "^lzip ${VER_LZIP}$"
if [ $? != 0 ] ; then
	echo "ERROR: lzip is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf lzip-${VER_LZIP} &&
set +x +u
