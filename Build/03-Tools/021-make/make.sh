#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/make-${VER_MAKE}.tar.lz ] || exit -1
rm -rf make-${VER_MAKE} &&
lzip -c -d ${LFS}/Sources/make-${VER_MAKE}.tar.lz | tar xf - &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../make-${VER_MAKE}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools \
	--without-guile \
	make_cv_sys_gnu_glob=no &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
if [ -h /usr/bin/make ] ; then rm -v /usr/bin/make ; fi &&
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/make ] ; then
	echo "ERROR: make is not installed"
	exit -1
fi

/tools/bin/make --version | grep "^GNU Make ${VER_MAKE}$"
if [ $? != 0 ] ; then
	echo "ERROR: make is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf make-${VER_MAKE} &&
set +x +u
