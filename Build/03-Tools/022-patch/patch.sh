#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/patch-${VER_PATCH}.tar.xz ] || exit -1
rm -rf patch-${VER_PATCH} &&
tar xf ${LFS}/Sources/patch-${VER_PATCH}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../patch-${VER_PATCH}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/patch ] ; then
	echo "ERROR: patch is not installed"
	exit -1
fi

/tools/bin/patch --version | grep "^GNU patch ${VER_PATCH}$"
if [ $? != 0 ] ; then
	echo "ERROR: patch is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf patch-${VER_PATCH} &&
set +x +u

