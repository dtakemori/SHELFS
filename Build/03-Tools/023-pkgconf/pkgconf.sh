#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/pkgconf-${VER_PKGCONF}.tar.xz ] || exit -1
rm -rf pkgconf-${VER_PKGCONF} &&
tar xf ${LFS}/Sources/pkgconf-${VER_PKGCONF}.tar.xz &&

cd pkgconf-${VER_PKGCONF} &&
aclocal &&
automake &&
cd ../ &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../pkgconf-${VER_PKGCONF}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

ln -vs ../lib/pkgconfig /tools/share/pkgconfig &&
ln -vs pkgconf /tools/bin/pkg-config &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/pkgconf ] ; then
	echo "ERROR: pkgconf is not installed"
	exit -1
fi

/tools/bin/pkgconf --version | grep "^${VER_PKGCONF}$"
if [ $? != 0 ] ; then
	echo "ERROR: pkgconf is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf pkgconf-${VER_PKGCONF} &&
set +x +u
