#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/util-linux-${VER_UTILLINUX}.tar.xz ] || exit -1
rm -rf util-linux-${VER_UTILLINUX} &&
tar xf ${LFS}/Sources/util-linux-${VER_UTILLINUX}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

NCURSESW6_CONFIG="/tools/bin/ncursesw6-config" \
CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt -Wl,--undefined-version" \
../util-linux-${VER_UTILLINUX}/configure \
	--prefix=/tools \
	--sbindir=/tools/bin \
	--disable-liblastlog2 \
	--disable-makeinstall-chown \
	--disable-raw \
	--with-ncursesw \
	--without-ncurses \
	--without-python \
	--without-systemdsystemunitdir &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make usrsbin_execdir=/tools/bin install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/mount ] ; then
	echo "ERROR: mount is not installed"
	exit -1
fi

/tools/bin/mount --version | grep "^mount from util-linux ${VER_UTILLINUX}"
if [ $? != 0 ] ; then
	echo "ERROR: mount is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf util-linux-${VER_UTILLINUX} &&
set +x +u
