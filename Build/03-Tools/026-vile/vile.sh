#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f ${LFS}/Sources/vile-${VER_VILE}.tgz ] || exit -1
rm -rf vile-${VER_VILE} &&
tar xf ${LFS}/Sources/vile-${VER_VILE}.tgz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang" \
AR="llvm-ar" \
RANLIB="llvm-ranlib" \
CFLAGS="${COPTS_T} --sysroot=/tools --target=${LFS_TMP}" \
LDFLAGS="${LDOPTS_T} -fuse-ld=lld --rtlib=compiler-rt" \
../vile-${VER_VILE}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools \
	--with-curses-dir=/tools \
	--with-ncurses &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ln -sv vile /tools/bin/vi &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/vile ] ; then
	echo "ERROR: vile is not installed"
	exit -1
fi

/tools/bin/vile -V | grep "^vile version ${VER_VILE}"
if [ $? != 0 ] ; then
	echo "ERROR: vile is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf vile-${VER_VILE} &&
set +x +u

