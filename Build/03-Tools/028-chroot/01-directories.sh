#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

### Create standard directory tree
mkdir -pv ${LFS}/{bin,dev,home,mnt,opt,proc,run,sbin,srv,sys} &&
install -dv -m 0750 ${LFS}/root &&

mkdir -pv ${LFS}/etc/{sysconfig,udev} &&
mkdir -pv ${LFS}/lib/{firmware,modules} &&
mkdir -pv ${LFS}/media/{cdrom,floppy} &&

mkdir -pv ${LFS}/var/{log,mail,spool} &&
mkdir -pv ${LFS}/var/{cache,lib/{locate,misc},local,opt} &&
install -dv -m 1777 ${LFS}{/var,}/tmp &&
install -dv -m 0500 ${LFS}/var/jail &&

mkdir -pv ${LFS}/usr/{,local/}{bin,include,libexec,sbin,src} &&
mkdir -pv ${LFS}/usr/{,local/}lib64/pkgconfig &&
mkdir -pv ${LFS}/usr/{,local/}share/{dict,doc,info,locale} &&
mkdir -pv ${LFS}/usr/{,local/}share/man/man{1,2,3,4,5,6,7,8} &&

mkdir -pv ${LFS}/usr/share/{misc,terminfo,zoneinfo} &&

set +x +u
