#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

install -v -m644 ${LFS}/Build/Config/passwd /tools/etc/passwd &&
install -v -m644 ${LFS}/Build/Config/group  /tools/etc/group &&

touch ${LFS}/var/log/{btmp,lastlog,wtmp} &&
chgrp -v utmp ${LFS}/var/log/lastlog &&
chmod -v 644 ${LFS}/var/log/lastlog &&
chmod -v 600 ${LFS}/var/log/btmp &&

set +x +u
