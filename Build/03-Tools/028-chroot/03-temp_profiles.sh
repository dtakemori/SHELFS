#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

install -v -m644 ${LFS}/Build/Config/bash_profile.temp \
	${LFS}/root/.bash_profile &&

set +x +u
