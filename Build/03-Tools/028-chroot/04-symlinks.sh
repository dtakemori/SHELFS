#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


touch /tools/bin/ldconfig &&
chmod +x /tools/bin/ldconfig &&

ln -sv /tools/bin/bash    ${LFS}/bin/ &&
ln -sv /tools/bin/sh      ${LFS}/bin/ &&
ln -sv /tools/bin/cat     ${LFS}/bin/ &&
ln -sv /tools/bin/chmod   ${LFS}/bin/ &&
ln -sv /tools/bin/echo    ${LFS}/bin/ &&
ln -sv /tools/bin/grep    ${LFS}/bin/ &&
ln -sv /tools/bin/pwd     ${LFS}/bin/ &&
ln -sv /tools/bin/mkdir   ${LFS}/bin/ &&
ln -sv /tools/bin/rm      ${LFS}/bin/ &&
ln -sv /tools/bin/stty    ${LFS}/bin/ &&
ln -sv /tools/bin/awk     ${LFS}/usr/bin/ &&
ln -sv /tools/bin/env     ${LFS}/usr/bin/ &&
ln -sv /tools/bin/file    ${LFS}/usr/bin/ &&
ln -sv /tools/bin/install ${LFS}/usr/bin/ &&
ln -sv /tools/bin/perl    ${LFS}/usr/bin/ &&

ln -sv /tools/etc/passwd  ${LFS}/etc/ &&
ln -sv /tools/etc/group   ${LFS}/etc/ &&


set +x +u
