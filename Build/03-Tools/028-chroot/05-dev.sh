#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

mknod -m 666 ${LFS}/dev/null c 1 3 &&
mknod -m 666 ${LFS}/dev/random c 1 8 &&
mknod -m 666 ${LFS}/dev/urandom c 1 9 &&
mknod -m 600 ${LFS}/dev/console c 5 1 &&

install -dv /tools/lib/{firmware,udev/devices/pts} &&
mknod -m0666 /tools/lib/udev/devices/null c 1 3 &&

set +x +u
