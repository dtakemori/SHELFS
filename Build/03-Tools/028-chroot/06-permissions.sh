#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

find ${LFS}/tools/lib -name "*.a" | xargs chmod 444 &&
find ${LFS}/tools/lib -name "*.so" | xargs chmod 555 &&

chmod a-w ${LFS}/tools/bin/*

set +x +u
