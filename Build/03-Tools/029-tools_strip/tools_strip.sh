#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&


### Strip static libraries
/bin/find ${LFS}/tools/lib	-type f -name '*\.a' -exec /usr/bin/strip -p --strip-debug '{}' ';' 


### Strip dynamic libraries
/bin/find ${LFS}/tools/lib	-type f -name '*\.so*' -exec /usr/bin/strip -p --strip-unneeded '{}' ';' 


### Strip binaries
/usr/bin/strip -p --strip-all ${LFS}/tools/bin/*
/usr/bin/strip -p --strip-all ${LFS}/tools/lib/awk/*
/usr/bin/strip -p --strip-all ${LFS}/tools/lib/bash/*


### Purge docs
/bin/rm -rf ${LFS}/tools/{,share}/{info,man,doc}


set +x +u
