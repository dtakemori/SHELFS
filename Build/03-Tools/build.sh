#!/bin/bash


echo "===> Libcxx Start" &&
cd 001-libcxx &&
/usr/bin/time -vao build.log ./libcxx.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libcxx End" &&


echo "===> Musl Start" &&
cd 002-musl &&
/usr/bin/time -vao build.log ./musl.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Musl End" &&


echo "===> Zlib Start" &&
cd 003-zlib &&
/usr/bin/time -vao build.log ./zlib.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Zlib End" &&


echo "===> Bzip2 Start" &&
cd 004-bzip2 &&
/usr/bin/time -vao build.log ./bzip2.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Bzip2 End" &&


echo "===> XZ Start" &&
cd 005-xz &&
/usr/bin/time -vao build.log ./xz.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== XZ End" &&


echo "===> Zstd Start" &&
cd 006-zstd &&
/usr/bin/time -vao build.log ./zstd.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Zstd End" &&


echo "===> Tcl Start" &&
cd 007-tcl &&
/usr/bin/time -vao build.log ./tcl.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Tcl End" &&


echo "===> Expect Start" &&
cd 008-expect &&
/usr/bin/time -vao build.log ./expect.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Expect End" &&


echo "===> DejaGnu Start" &&
cd 009-dejagnu &&
/usr/bin/time -vao build.log ./dejagnu.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== DejaGnu End" &&


echo "===> BSDcurses Start" &&
cd 010-bsdcurses &&
/usr/bin/time -vao build.log ./bsdcurses.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== BSDcurses End" &&


echo "===> Bash Start" &&
cd 011-bash &&
/usr/bin/time -vao build.log ./bash.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Bash End" &&


echo "===> M4 Start" &&
cd 012-m4 &&
/usr/bin/time -vao build.log ./m4.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== M4 End" &&


echo "===> Bison Start" &&
cd 013-bison &&
/usr/bin/time -vao build.log ./bison.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Bison End" &&


echo "===> Busybox Start" &&
cd 014-busybox &&
/usr/bin/time -vao build.log ./busybox.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Busybox End" &&


echo "===> Libffi Start" &&
cd 015-libffi &&
/usr/bin/time -vao build.log ./libffi.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libffi End" &&


echo "===> Flex Start" &&
cd 016-flex &&
/usr/bin/time -vao build.log ./flex.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Flex End" &&


echo "===> Gawk Start" &&
cd 017-gawk &&
/usr/bin/time -vao build.log ./gawk.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Gawk End" &&


echo "===> Gettext Start" &&
cd 018-gettext &&
/usr/bin/time -vao build.log ./gettext.sh > build.log 2>error.log && 
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Gettext End" &&


echo "===> Grep Start" &&
cd 019-grep &&
/usr/bin/time -vao build.log ./grep.sh > build.log 2>error.log && 
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Grep End" &&


echo "===> Lzip Start" &&
cd 020-lzip &&
/usr/bin/time -vao build.log ./lzip.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Lzip End" &&


echo "===> Make Start" &&
cd 021-make &&
/usr/bin/time -vao build.log ./make.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Make End" &&


echo "===> Patch Start" &&
cd 022-patch &&
/usr/bin/time -vao build.log ./patch.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Patch End" &&


echo "===> Pkgconf Start" &&
cd 023-pkgconf &&
/usr/bin/time -vao build.log ./pkgconf.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Pkgconf End" &&


echo "===> Time Start" &&
cd 024-time &&
/usr/bin/time -vao build.log ./time.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Time End" &&


echo "===> Util-linux Start" &&
cd 025-util-linux &&
/usr/bin/time -vao build.log ./util-linux.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Util-linux End" &&


echo "===> Vile Start" &&
cd 026-vile &&
/usr/bin/time -vao build.log ./vile.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Vile End" &&


echo "===> Tools Clang Start" &&
cd 027-tools_clang &&
/usr/bin/time -vao build.log ./tools_clang.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Tools Clang End" &&


echo "===> Configure Chroot Start" &&
cd 028-chroot &&

echo "  Setup Directories" &&
/usr/bin/time -vao build.log ./01-directories.sh   >  build.log 2>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Setup Users and Groups" &&
/usr/bin/time -vao build.log ./02-passwd_group.sh  >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Setup Root Profile" &&
/usr/bin/time -vao build.log ./03-temp_profiles.sh >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Setup Symlinks" &&
/usr/bin/time -vao build.log ./04-symlinks.sh      >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Setup Devices" &&
/usr/bin/time -vao build.log ./05-dev.sh           >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Fix Permissions" &&
/usr/bin/time -vao build.log ./06-permissions.sh   >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

cd .. &&
echo "<=== Setup Chroot End" &&


echo "===> Tools Strip Start" &&
cd 029-tools_strip &&
/usr/bin/time -vao build.log ./tools_strip.sh  > build.log 2>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&
cd .. &&
echo "<=== Tools Strip End"
