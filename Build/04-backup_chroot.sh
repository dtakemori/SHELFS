#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

(cd ${LFS} ;
 tar -cvJ -f Chroot.tar.xz bin boot cross-tools dev etc home \
                           lib lib64 media mnt opt proc root \
                           run sbin srv sys tmp tools usr var) &&

set +x +u
