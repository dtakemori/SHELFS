#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

mount -v --bind /dev ${LFS}/dev &&

mount -vt devpts devpts ${LFS}/dev/pts -o gid=5,mode=620 &&
mount -vt proc proc ${LFS}/proc &&
mount -vt sysfs sysfs ${LFS}/sys &&
mount -vt tmpfs tmpfs ${LFS}/run &&

if [ -d /sys/firmware/efi ] ; then
  mount -vt efivarfs efivarfs ${LFS}/sys/firmware/efi/efivars
fi

if [ -h ${LFS}/dev/shm ] ; then
  mkdir -pv ${LFS}/$(readlink ${LFS}/dev/shm)
fi

set +x +u
