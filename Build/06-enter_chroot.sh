#!/bin/bash
[ -z ${LFS:-} ] && echo "LFS not set" && exit -1
. ${LFS}/Build/Config/config.sh &&
. ${LFS}/Build/Config/versions.sh &&
set -x -u &&

chroot ${LFS} /tools/bin/env -i \
    HOME=/root \
    TERM="${TERM}" \
    PS1='(lfs chroot) \u:\w\$ ' \
    PATH=/bin:/usr/bin:/sbin:/usr/sbin:/recross-tools/bin:/tools/bin \
    LFSCHROOT="LFSCHROOT" \
    /tools/bin/bash --login +h

echo "EXITING CHROOT NOW"
set +x +u
