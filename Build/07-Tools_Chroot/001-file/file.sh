#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/file-${VER_FILE}.tar.gz ] || exit -1
rm -rf file-${VER_FILE} &&
tar xf /Sources/file-${VER_FILE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang -fPIC --sysroot=/tools" \
CXX="clang++ -fPIC --sysroot=/tools" \
CFLAGS="${COPTS_T} --sysroot=/tools -target ${LFS_TMP}" \
AR=llvm-ar \
AS="llvm-as" \
LD="ld.lld" \
LDFLAGS="${LDOPTS_T}" \
RANLIB=llvm-ranlib \
../file-${VER_FILE}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install
ldconfig &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/file ] ; then
	echo "ERROR: file is not installed"
	exit -1
fi

/tools/bin/file --version | grep "^file-${VER_FILE}$"
if [ $? != 0 ] ; then
	echo "ERROR: file is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf file-${VER_FILE} &&
set +x +u
