#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gdbm-${VER_GDBM}.tar.gz ] || exit -1
rm -rf gdbm-${VER_GDBM} &&
tar xf /Sources/gdbm-${VER_GDBM}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang -fPIC --sysroot=/tools" \
CXX="clang++ -fPIC --sysroot=/tools" \
CFLAGS="${COPTS_T} --sysroot=/tools -target ${LFS_TMP}" \
AR=llvm-ar \
AS="llvm-as" \
LD="ld.lld" \
LDFLAGS="${LDOPTS_T}" \
RANLIB=llvm-ranlib \
../gdbm-${VER_GDBM}/configure \
	--host=${LFS_TMP} \
	--prefix=/tools \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/gdbmtool ] ; then
	echo "ERROR: gdbmtool is not installed"
	exit -1
fi

/tools/bin/gdbmtool --version | grep "^gdbmtool (gdbm) ${VER_GDBM}$"
if [ $? != 0 ] ; then
	echo "ERROR: gdbmtool is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gdbm-${VER_GDBM} &&
set +x +u
