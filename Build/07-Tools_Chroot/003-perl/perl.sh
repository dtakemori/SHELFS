#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/perl-${VER_PERL}.tar.xz ] || exit -1
rm -rf perl-${VER_PERL} &&
tar xf /Sources/perl-${VER_PERL}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

../perl-${VER_PERL}/Configure -des \
	-Dcc="clang ${COPTS_T} --sysroot=/tools -target ${LFS_TMP}" \
	-Dar="llvm-ar" \
	-Dnm="llvm-nm" \
	-Dldflags="${LDOPTS_T}" \
	-Dprefix=/tools \
	-Dlibc=/tools/lib/libc.so \
	-Dlibs="-lgdbm -ldl -lm -lcrypt -lutil -lpthread -lc" \
	-Dstatic_ext='Data/Dumper Fcntl IO' \
	-Uloclibpth \
	-Dglibpth="/tools/lib" \
	-Doptimize="-O1" \
	-Dusrinc="/tools/include" \
	-Dlocincpth="/tools/include" \
	-Dpager="/tools/bin/less -isR" \
	-Dusethreads \
	-Dmksymlinks \
	-Duseshrplib &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 2 ] ;  then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make LNS="cp" install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/perl ] ; then
	echo "ERROR: perl is not installed"
	exit -1
fi

/tools/bin/perl --version | grep "^This is perl 5, .* (v${VER_PERL})"
if [ $? != 0 ] ; then
	echo "ERROR: perl is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.sh ../config.log &&
cd ../ &&
rm -rf build &&
rm -rf perl-${VER_PERL} &&
set +x +u
