#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/Python-${VER_PYTHON}.tar.xz ] || exit -1
rm -rf Python-${VER_PYTHON} &&
tar xf /Sources/Python-${VER_PYTHON}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="clang -fPIC --sysroot=/tools" \
CXX="clang++ -fPIC --sysroot=/tools" \
CFLAGS="${COPTS_T} --sysroot=/tools -target ${LFS_TMP}" \
AR=llvm-ar \
AS="llvm-as" \
LD="ld.lld" \
LDFLAGS="${LDOPTS_T}" \
RANLIB=llvm-ranlib \
../Python-${VER_PYTHON}/configure \
	--prefix=/tools \
	--enable-shared \
	ac_cv_build=${LFS_TGT} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/python3 ] ; then
	echo "ERROR: python is not installed"
	exit -1
fi

/tools/bin/python3 --version 2>&1 | grep "^Python ${VER_PYTHON}"
if [ $? != 0 ] ; then
	echo "ERROR: python is wrong version"
	exit -1
fi

##################################################################### CONFIGURE

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf Python-${VER_PYTHON} &&
set +x +u
