#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/flit_core-${VER_FLITCORE}.tar.gz ] || exit -1
rm -rf flit_core-${VER_FLITCORE} &&
tar xf /Sources/flit_core-${VER_FLITCORE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd flit_core-${VER_FLITCORE} &&
pip3 wheel -w dist --no-cache-dir --no-build-isolation --no-deps $PWD &&

########################################################################## TEST

####################################################################### INSTALL
pip3 install --no-index --no-user --find-links dist flit_core &&

###################################################################### VALIDATE

####################################################################### CLEANUP
cd ../ &&
rm -rf flit_core-${VER_FLITCORE} &&
set +x +u
