#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/markupsafe-${VER_MARKUPSAFE}.tar.gz ] || exit -1
rm -rf markupsafe-${VER_MARKUPSAFE} &&
tar xf /Sources/markupsafe-${VER_MARKUPSAFE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd markupsafe-${VER_MARKUPSAFE} &&
pip3 wheel -w dist --no-cache-dir --no-build-isolation --no-deps $PWD &&

########################################################################## TEST

####################################################################### INSTALL
pip3 install --no-index --no-user --find-links dist markupsafe &&

###################################################################### VALIDATE

####################################################################### CLEANUP
cd ../ &&
rm -rf markupsafe-${VER_MARKUPSAFE} &&
set +x +u
