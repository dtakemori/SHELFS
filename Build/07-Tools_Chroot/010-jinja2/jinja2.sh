#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/jinja2-${VER_JINJA2}.tar.gz ] || exit -1
rm -rf jinja2-${VER_JINJA2} &&
tar xf /Sources/jinja2-${VER_JINJA2}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd jinja2-${VER_JINJA2} &&
pip3 wheel -w dist --no-cache-dir --no-build-isolation --no-deps $PWD &&

########################################################################## TEST

####################################################################### INSTALL
pip3 install --no-index --no-user --find-links dist Jinja2 &&

###################################################################### VALIDATE

####################################################################### CLEANUP
cd ../ &&
rm -rf jinja2-${VER_JINJA2} &&
set +x +u
