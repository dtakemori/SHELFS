#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/meson-${VER_MESON}.tar.gz ] || exit -1
rm -rf meson-${VER_MESON} &&
tar xf /Sources/meson-${VER_MESON}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd meson-${VER_MESON} &&

######################################################################### BUILD
pip3 wheel -w dist --no-cache-dir --no-build-isolation --no-deps $PWD &&

########################################################################## TEST

####################################################################### INSTALL
pip3 install --no-index --find-links dist meson &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/meson ] ; then
	echo "ERROR: meson is not installed"
	exit -1
fi

/tools/bin/meson --version 2>&1 | grep "^${VER_MESON}$"
if [ $? != 0 ] ; then
	echo "ERROR: meson is wrong version"
	exit -1
fi

##################################################################### CONFIGURE

####################################################################### CLEANUP
cd ../ &&
rm -rf meson-${VER_MESON} &&
set +x +u
