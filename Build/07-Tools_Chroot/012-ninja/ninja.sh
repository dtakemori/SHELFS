#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/ninja-${VER_NINJA}.tar.gz ] || exit -1
rm -rf ninja-${VER_NINJA} &&
tar xf /Sources/ninja-${VER_NINJA}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd ninja-${VER_NINJA} &&
CXX="clang++ -fPIC --sysroot=/tools" \
CXXFLAGS="${COPTS_T} --sysroot=/tools -target ${LFS_TMP}" \
LD="ld.lld" \
LDFLAGS="${LDOPTS_T}" \
python3 configure.py --bootstrap &&

########################################################################## TEST

####################################################################### INSTALL
install -vm755 ninja /tools/bin/ &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/ninja ] ; then
	echo "ERROR: ninja is not installed"
	exit -1
fi

/tools/bin/ninja --version | grep -E "^${VER_NINJA}$"
if [ $? != 0 ] ; then
	echo "ERROR: ninja is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf ninja-${VER_NINJA} &&
set +x +u

