#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/texinfo-${VER_TEXINFO}.tar.xz ] || exit -1
rm -rf texinfo-${VER_TEXINFO} &&
tar xf /Sources/texinfo-${VER_TEXINFO}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&


CC="clang -fPIC --sysroot=/tools" \
CXX="clang++ -fPIC --sysroot=/tools" \
CFLAGS="${COPTS_T} --sysroot=/tools -target ${LFS_TMP}" \
AR=llvm-ar \
AS="llvm-as" \
LD="ld.lld" \
LDFLAGS="${LDOPTS_T}" \
RANLIB=llvm-ranlib \
../texinfo-${VER_TEXINFO}/configure \
	--prefix=/tools \
	--host=${LFS_TMP} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} \
	CC="clang -fPIC --sysroot=/tools" \
	CFLAGS="${COPTS_T} --sysroot=/tools -target ${LFS_TMP}" \
	LDFLAGS="${LDOPTS_T}" &&

########################################################################## TEST
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /tools/bin/makeinfo ] ; then
	echo "ERROR: makeinfo is not installed"
	exit -1
fi

/tools/bin/makeinfo --version | grep "^texi2any (GNU texinfo) ${VER_TEXINFO}$"
if [ $? != 0 ] ; then
	echo "ERROR: makeinfo is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf texinfo-${VER_TEXINFO} &&
set +x +u
