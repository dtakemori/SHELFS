#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


### Strip static libraries
find /tools/lib	-type f -name '*\.a' -exec strip -p --strip-debug '{}' ';' 


### Strip dynamic libraries
find /tools/lib	-type f -name '*\.so*' -exec strip -p --strip-unneeded '{}' ';' 


### Strip binaries
find /tools/bin	-type f -exec strip -p --strip-all '{}' ';'


### Purge docs
rm -rf /tools/{,share}/{info,man,doc}


set +x +u
