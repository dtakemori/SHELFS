#!/bin/sh


echo "===> File Start" &&
cd 001-file &&
/tools/bin/time -vao build.log ./file.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== File End" &&


echo "===> GDBM Start" &&
cd 002-gdbm &&
/tools/bin/time -vao build.log ./gdbm.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== GDBM End" &&


echo "===> Perl Start" &&
cd 003-perl &&
/tools/bin/time -vao build.log ./perl.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Perl End" &&


echo "===> Openssl Start" &&
cd 004-openssl &&
/tools/bin/time -vao build.log ./openssl.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Openssl End" &&


echo "===> Python Start" &&
cd 005-python &&
/tools/bin/time -vao build.log ./python.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Python End" &&


echo "===> Flit-core Start" &&
cd 006-flit-core &&
/tools/bin/time -vao build.log ./flit-core.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Flit-core End" &&


echo "===> Wheel Start" &&
cd 007-wheel &&
/tools/bin/time -vao build.log ./wheel.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Wheel End" &&


echo "===> Setuptools Start" &&
cd 008-setuptools &&
/tools/bin/time -vao build.log ./setuptools.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Setuptools End" &&


echo "===> MarkupSafe Start" &&
cd 009-markupsafe &&
/tools/bin/time -vao build.log ./markupsafe.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== MarkupSafe End" &&


echo "===> Jinja2 Start" &&
cd 010-jinja2 &&
/tools/bin/time -vao build.log ./jinja2.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Jinja2 End" &&


echo "===> Meson Start" &&
cd 011-meson &&
/tools/bin/time -vao build.log ./meson.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Meson End" &&


echo "===> Ninja Start" &&
cd 012-ninja &&
/tools/bin/time -vao build.log ./ninja.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Ninja End" &&


echo "===> Texinfo Start" &&
cd 013-texinfo &&
/tools/bin/time -vao build.log ./texinfo.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Texinfo End" &&


echo "===> Tools Strip Start" &&
cd 014-tools_strip &&
/tools/bin/time -vao build.log ./tools_strip.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&
cd .. &&
echo "<=== Tools Strip End" 
