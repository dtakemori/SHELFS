#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/binutils-${VER_BINUTILS}.tar.xz ] || exit -1
rm -rf binutils-${VER_BINUTILS} &&
tar xf /Sources/binutils-${VER_BINUTILS}.tar.xz &&

######################################################################### PATCH
cd binutils-${VER_BINUTILS} &&
sed -i '6009s/$add_dir//' ltmain.sh &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC_FOR_BUILD="clang --sysroot=/tools -target ${LFS_TMP}" \
CC="clang --sysroot=/tools -target ${LFS_TMP}" \
CFLAGS=${COPTS_T} \
LD="ld.lld" \
LDFLAGS="${LDOPTS_T}" \
AR="llvm-ar" \
../binutils-${VER_BINUTILS}/configure \
	--host=${LFS_TMP} \
	--target=${LFS_TGT} \
	--prefix=/recross-tools \
	--with-lib-path="/usr/local/lib64:/usr/lib64:/lib64" \
	--enable-gold=no \
	--enable-ld=yes \
	--enable-gprofng=no \
	--disable-multilib \
	--disable-werror 

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST 
if [ ${CONF_TTEST} -ge 1 ] ;  then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /recross-tools/bin/${LFS_TGT}-ld ] ; then
	echo "ERROR: /recross-tools/bin/${LFS_TGT}-ld not installed"
	exit -1
fi

/recross-tools/bin/${LFS_TGT}-ld -v | grep "^GNU ld (GNU Binutils) ${VER_BINUTILS}$"
if [ $? != 0 ] ; then
	echo "ERROR: /recross-tools/bin/${LFS_TGT}-ld is wrong version"
	exit -1
fi

/recross-tools/bin/${LFS_TGT}-ld --verbose | grep 'SEARCH_DIR("/usr/lib64")'
if [ $? != 0 ] ; then
	echo "ERROR: /recross-tools/bin/${LFS_TGT}-ld SEARCH_DIR missing /usr/lib64"
	exit -1
fi

/recross-tools/bin/${LFS_TGT}-ld --verbose | grep "SEARCH_DIR(\"/recross-tools/${LFS_TGT}/lib\""
if [ $? != 0 ] ; then
	echo "ERROR: /recross-tools/bin/${LFS_TGT}-ld SEARCH_DIR missing target lib"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf binutils-${VER_BINUTILS} &&
set +x +u
