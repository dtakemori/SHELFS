#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gcc-${VER_GCC}.tar.xz ] || exit -1
rm -rf gcc-${VER_GCC} &&
tar xf /Sources/gcc-${VER_GCC}.tar.xz &&

[ -f /Sources/gmp-${VER_GMP}.tar.xz ] || exit -1
tar xf /Sources/gmp-${VER_GMP}.tar.xz &&
mv -v gmp-${VER_GMP} gcc-${VER_GCC}/gmp &&

[ -f /Sources/mpfr-${VER_MPFR}.tar.xz ] || exit -1
tar xf /Sources/mpfr-${VER_MPFR}.tar.xz &&
mv -v mpfr-${VER_MPFR} gcc-${VER_GCC}/mpfr &&

[ -f /Sources/mpc-${VER_MPC}.tar.gz ] || exit -1
tar xf /Sources/mpc-${VER_MPC}.tar.gz &&
mv -v mpc-${VER_MPC} gcc-${VER_GCC}/mpc &&

# [ -f /Sources/isl-${VER_ISL}.tar.xz ] || exit -1
# tar xf /Sources/isl-${VER_ISL}.tar.xz &&
# mv -v isl-${VER_ISL} gcc-${VER_GCC}/isl &&

######################################################################### PATCH
cd gcc-${VER_GCC}/ &&

cd mpfr/ &&
for p in /Sources/mpfr-patches/patch*
   do echo "Applying patch ${p}" &&
   patch -Z -p1 -i ${p};
done
cd ../ &&

### gcc doesn't detect libc stack protector correctly for the cross glibc build
sed -i '/k prot/agcc_cv_libc_provides_ssp=yes' gcc/configure &&

###
patch -p1 -i /Patches/gcc-12.1.0_musl_poisoned_calloc.patch &&

cd ../ &&

########################################################################## PREP
mkdir -v build &&
cd build/ &&

ln -s /tools/bin/ls /bin/ls

CC="clang --sysroot=/tools -target ${LFS_TMP}" \
CXX="clang++ --sysroot=/tools -target ${LFS_TMP}" \
AR="llvm-ar" \
AS="llvm-as" \
LD="ld.lld" \
LDFLAGS="${LDOPTS_T}" \
NM="llvm-nm" \
OBJDUMP="llvm-objdump" \
RANLIB="llvm-ranlib" \
../gcc-${VER_GCC}/configure \
	--with-pkgversion="SHELFS Recross +PIC" \
	--build=${LFS_TMP} \
	--host=${LFS_TMP} \
	--target=${LFS_TGT} \
	--prefix=/recross-tools \
	--with-local-prefix=/recross-tools \
	--with-sysroot=/// \
	--with-native-system-header-dir=/usr/include \
	--with-newlib \
	--with-gxx-include-dir=/usr/include/c++/${VER_GCC} \
	--with-glibc=2.38 \
	--with-arch=${LFS_GCC_ARCH} \
	--without-headers \
	--enable-languages=c,c++ \
	--enable-default-pie \
	--enable-default-ssp \
	--disable-bootstrap \
	--disable-fixincludes \
	--disable-shared \
	--disable-multilib \
	--disable-threads \
	--disable-libatomic \
	--disable-libgomp \
	--disable-libmpx \
	--disable-libquadmath \
	--disable-libssp \
	--disable-libstdcxx \
	--disable-libvtv &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&
# make &&

########################################################################## TEST 
if [ ${CONF_TTEST} -ge 2 ] ;  then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k -C gmp check  > ../test.log
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k -C mpc check  >> ../test.log
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k -C mpfr check >> ../test.log
fi

####################################################################### INSTALL
make install &&

### Install limits.h into the system fixed includes
cat ../gcc-${VER_GCC}/gcc/limitx.h \
    ../gcc-${VER_GCC}/gcc/glimits.h \
    ../gcc-${VER_GCC}/gcc/limity.h \
      > /recross-tools/lib/gcc/${LFS_TGT}/${VER_GCC}/include/limits.h &&

ln -sv ../bin/${LFS_TGT}-cpp /recross-tools/lib/cpp &&
ln -sv ${LFS_TGT}-gcc /recross-tools/bin/${LFS_TGT}-cc &&

install -v -dm755 /recross-tools/lib/bfd-plugins &&
ln -svf /recross-tools/libexec/gcc/$(/recross-tools/bin/${LFS_TGT}-gcc -dumpmachine)/${VER_GCC}/liblto_plugin.so \
	/recross-tools/lib/bfd-plugins &&

###################################################################### VALIDATE
if [ ! -x /recross-tools/bin/${LFS_TGT}-gcc ] ; then
	echo "ERROR: /recross-tools/bin/${LFS_TGT}-gcc not installed"
	exit -1
fi

/recross-tools/bin/${LFS_TGT}-gcc -v 2>&1 | grep "^gcc version ${VER_GCC}"
if [ $? != 0 ] ; then
	echo "ERROR: /recross-tools/bin/${LFS_TGT}-gcc is wrong version"
	exit -1
fi

####################################################################### CLEANUP
/recross-tools/bin/${LFS_TGT}-gcc -dumpspecs > ../dumpspecs.log &&
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gcc-${VER_GCC} &&
set +x +u
