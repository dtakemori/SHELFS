#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}.tar.xz ] || exit -1
rm -rf linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} &&
tar xf /Sources/linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}.tar.xz &&

######################################################################### PATCH
cd linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} &&

tar xf /Sources/aufs${VER_LINUXLTS}.${VER_AUFSLTS}.tar.xz &&
find aufs${VER_LINUXLTS}.${VER_AUFSLTS} -type f -name "\.*" | xargs rm -f &&

patch -p1 -i aufs${VER_LINUXLTS}.${VER_AUFSLTS}/aufs6-kbuild.patch &&
patch -p1 -i aufs${VER_LINUXLTS}.${VER_AUFSLTS}/aufs6-base.patch &&
patch -p1 -i aufs${VER_LINUXLTS}.${VER_AUFSLTS}/aufs6-standalone.patch &&
patch -p1 -i aufs${VER_LINUXLTS}.${VER_AUFSLTS}/aufs6-mmap.patch &&

cp -v aufs${VER_LINUXLTS}.${VER_AUFSLTS}/Documentation/ABI/testing/* \
	Documentation/ABI/testing/ &&
cp -rv aufs${VER_LINUXLTS}.${VER_AUFSLTS}/Documentation/filesystems/aufs \
	Documentation/filesystems/ &&
cp -rv aufs${VER_LINUXLTS}.${VER_AUFSLTS}/fs/aufs fs/ &&
cp -v aufs${VER_LINUXLTS}.${VER_AUFSLTS}/include/uapi/linux/aufs_type.h \
	include/uapi/linux &&

cd ../ &&

########################################################################## PREP
mkdir -v build &&
cd build &&

make -C ../linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} \
	O=$(pwd) \
	CC="clang --sysroot=/tools -target ${LFS_TMP}" \
	HOSTCC="clang --sysroot=/tools -target ${LFS_TMP}" \
	defconfig &&

######################################################################### BUILD
make -C ../linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} \
	O=$(pwd) \
	CC="clang --sysroot=/tools -target ${LFS_TMP}" \
	HOSTCC="clang --sysroot=/tools -target ${LFS_TMP}" \
	INSTALL_HDR_PATH=dest \
	headers &&

find usr/include -type f -not -name '*.h' -delete &&

########################################################################## TEST 

####################################################################### INSTALL
cp -v -r usr/include/* /usr/include &&

###################################################################### VALIDATE
if [ ! -f /usr/include/linux/version.h ] ; then
	echo "ERROR: /usr/include/linux/version.h not installed"
	exit -1
fi

grep "LINUX_VERSION_CODE $(echo ${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} | \
	perl -ne '($a, $b, $c) = split(/\./, $_); 
		print( (65536 * $a) + (256 * $b) + $c);')"  \
	/usr/include/linux/version.h
if [ $? != 0 ] ; then
	echo "ERROR: /usr/include/linux/version.h is wrong version"
	exit -1
fi

###################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} &&
set +x +u
