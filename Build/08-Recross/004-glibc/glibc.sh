#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/glibc-${VER_GLIBC}.tar.xz ] || exit -1
rm -rf glibc-${VER_GLIBC} &&
tar xf /Sources/glibc-${VER_GLIBC}.tar.xz &&

######################################################################### PATCH
cd glibc-${VER_GLIBC} &&

patch -p1 -i /Patches/glibc-2.38-fhs-1.patch &&

cd ../ &&

################################################################ PREP LIBRARIES
mkdir -v build/ &&
cd build/ &&

cat > configparms << "EOF" &&
CFLAGS += -fPIC -g0 -O3 -fno-strict-aliasing -pipe
CXXFLAGS += -fPIC -g0 -O3 -fno-strict-aliasing -pipe
LDFLAGS.so += ${LDOPTS_0}
EOF

CC="${LFS_TGT}-gcc -isystem /usr/lib/gcc/${LFS_TGT}/${VER_GCC}/include -isystem /usr/include" \
../glibc-${VER_GLIBC}/configure \
	--prefix=/usr \
	--host=${LFS_TGT} \
	--enable-bind-now \
	--enable-kernel=5.1 \
	--enable-fortify-source=2 \
	--enable-stack-protector=all \
	--disable-nscd \
	--disable-profile \
	--disable-werror \
	--without-gd \
	--without-selinux 

############################################################### BUILD LIBRARIES
make -j${MAKEJOBS:-2} &&

########################################################################## TEST 

####################################################################### INSTALL
touch /etc/ld.so.conf &&

sed -i '/test-installation/s@$(PERL)@echo not running@' \
    ../glibc-${VER_GLIBC}/Makefile &&

make install &&

mkdir -v /usr/share/doc/glibc-${VER_GLIBC} &&
cp -v ../glibc-${VER_GLIBC}/{CONTRIBUTED-BY,COPYING,COPYING.LIB,INSTALL} \
	../glibc-${VER_GLIBC}/{LICENSES,MAINTAINERS,NEWS,README,SECURITY.md} \
	../glibc-${VER_GLIBC}/SHARED-FILES \
	/usr/share/doc/glibc-${VER_GLIBC} &&

mkdir -pv /usr/lib/locale &&
localedef -i C -f UTF-8 C.UTF-8 &&
localedef -i cs_CZ -f UTF-8 cs_CZ.UTF-8 &&
localedef -i de_DE -f ISO-8859-1 de_DE &&
localedef -i de_DE@euro -f ISO-8859-15 de_DE@euro &&
localedef -i de_DE -f UTF-8 de_DE.UTF-8 &&
localedef -i el_GR -f ISO-8859-7 el_GR &&
localedef -i en_GB -f ISO-8859-1 en_GB &&
localedef -i en_GB -f UTF-8 en_GB.UTF-8 &&
localedef -i en_HK -f ISO-8859-1 en_HK &&
localedef -i en_PH -f ISO-8859-1 en_PH &&
localedef -i en_US -f ISO-8859-1 en_US &&
localedef -i en_US -f UTF-8 en_US.UTF-8 &&
localedef -i es_ES -f ISO-8859-15 es_ES@euro &&
localedef -i es_MX -f ISO-8859-1 es_MX &&
localedef -i fa_IR -f UTF-8 fa_IR &&
localedef -i fr_FR -f ISO-8859-1 fr_FR &&
localedef -i fr_FR@euro -f ISO-8859-15 fr_FR@euro &&
localedef -i fr_FR -f UTF-8 fr_FR.UTF-8 &&
localedef -i is_IS -f ISO-8859-1 is_IS &&
localedef -i is_IS -f UTF-8 is_IS.UTF-8 &&
localedef -i it_IT -f ISO-8859-1 it_IT &&
localedef -i it_IT -f ISO-8859-15 it_IT@euro &&
localedef -i it_IT -f UTF-8 it_IT.UTF-8 &&
localedef -i ja_JP -f EUC-JP ja_JP &&
localedef -i ja_JP -f SHIFT_JIS ja_JP.SJIS 2> /dev/null || true
localedef -i ja_JP -f UTF-8 ja_JP.UTF-8 &&
localedef -i nl_NL@euro -f ISO-8859-15 nl_NL@euro &&
localedef -i ru_RU -f KOI8-R ru_RU.KOI8-R &&
localedef -i ru_RU -f UTF-8 ru_RU.UTF-8 &&
localedef -i se_NO -f UTF-8 se_NO.UTF-8 &&
localedef -i ta_IN -f UTF-8 ta_IN.UTF-8 &&
localedef -i tr_TR -f UTF-8 tr_TR.UTF-8 &&
localedef -i zh_CN -f GB18030 zh_CN.GB18030 &&
localedef -i zh_HK -f BIG5-HKSCS zh_HK.BIG5-HKSCS &&
localedef -i zh_TW -f UTF-8 zh_TW.UTF-8 &&

install -v -m0644 /Build/Config/nsswitch.conf /etc &&
install -v -m0644 /Build/Config/ld.so.conf /etc/ld.so.conf &&


### Extract Time Zone data
tar xf /Sources/tzdata${VER_TZDATA}.tar.gz &&

mkdir -pv /usr/share/zoneinfo/{posix,right} &&

for tz in etcetera southamerica northamerica europe africa antarctica \
	asia australasia backward ; do
  zic -L /dev/null   -d /usr/share/zoneinfo       ${tz}
  zic -L /dev/null   -d /usr/share/zoneinfo/posix ${tz}
  zic -L leapseconds -d /usr/share/zoneinfo/right ${tz}
done

cp -v zone.tab zone1970.tab iso3166.tab /usr/share/zoneinfo &&
zic -d /usr/share/zoneinfo -p America/New_York &&


rm -vf /etc/localtime &&
ln -sv /usr/share/zoneinfo/${CONF_TZ} \
	/etc/localtime &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/ldd ] ; then
	echo "ERROR: /bin/ldd not installed"
	exit -1
fi

/usr/bin/ldd --version | grep "(GNU libc) ${VER_GLIBC}$"
if [ $? != 0 ] ; then
	echo "ERROR: /usr/bin/ldd is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf glibc-${VER_GLIBC} &&
set +x +u
