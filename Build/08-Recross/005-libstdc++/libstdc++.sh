#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gcc-${VER_GCC}.tar.xz ] || exit -1
rm -rf gcc-${VER_GCC} &&
tar xf /Sources/gcc-${VER_GCC}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build &&
cd build/ &&

CC="${LFS_TGT}-gcc" \
CXX="${LFS_TGT}-g++" \
AR="${LFS_TGT}-ar" \
LD="${LFS_TGT}-ld" \
LDFLAGS="${LDOPTS_0}"
RANLIB="${LFS_TGT}-ranlib" \
../gcc-${VER_GCC}/libstdc++-v3/configure \
	--host=${LFS_TGT} \
	--target=${LFS_TGT} \
	--prefix=/usr \
	--disable-multilib \
	--disable-libstdcxx-pch \
	--with-gxx-include-dir=/usr/include/c++/${VER_GCC} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

rm -fv /usr/lib64/lib{stdc++,stdc++exp,stdc++fs,supc++}.la &&

###################################################################### VALIDATE
if ! [ -f /usr/lib64/libstdc++.a ] ; then 
	echo "ERROR: libstdc++ does not exist"
	exit -1
fi

if ! [ -f /usr/include/c++/${VER_GCC}/cstdlib ] ; then 
	echo "ERROR: cstdlib includes does not exist"
	exit -1
fi

/recross-tools/bin/${LFS_TGT}-g++ -v -Wl,--verbose \
	/Build/Config/test.cc &> dummy.log &&

if [ ! -x a.out ] ; then
	echo "ERROR: no compiler output"
	exit -1
fi

ldd a.out | grep '/lib64/libc.so.6'
if [ $? != 0 ] ; then
	echo "ERROR: Compiler output not linked to libc"
	exit -1
fi

ldd a.out | grep -E '/lib(/ld-linux.so.2|64/ld-linux-x86-64.so.2|/ld-linux-aarch64.so.1)'
if [ $? != 0 ] ; then
	echo "ERROR: Compiler output not using final loader"
	exit -1
fi

llvm-readelf -l a.out | grep -E 'interpreter: /lib(/ld-linux.so.2|64/ld-linux-x86-64.so.2|/ld-linux-aarch64.so.1)'
if [ $? != 0 ] ; then
	echo "ERROR: Interpreter not in final environment"
	exit -1
fi

grep '/lib.*crt[1in].o succeeded' dummy.log 
if [ $? != 0 ] ; then
	echo "ERROR: C runtime files not found"
	exit -1
fi

grep -B1 "^ /usr/include" dummy.log 
if [ $? != 0 ] ; then
	echo "ERROR: Include path does not include /usr/include"
	exit -1
fi

grep 'SEARCH.*' dummy.log | sed 's|; |\n|g' | grep '/usr'
if [ $? != 0 ] ; then
	echo "ERROR: Search dirs do not include /usr"
	exit -1
fi

grep "/lib64/libc.so.6 " dummy.log 
if [ $? != 0 ] ; then
	echo "ERROR: C library not found"
	exit -1
fi

grep "/.*/libstdc++.so succeeded" dummy.log 
if [ $? != 0 ] ; then
	echo "ERROR: C++ library not found"
	exit -1
fi

ldd a.out | grep '/lib64/libc.so.6'
if [ $? != 0 ] ; then
	echo "ERROR: Compiler output not linked to final libc"
	exit -1
fi
rm -fv a.out &&

####################################################################### CLEANUP
cp -v config.log ../ &&
cp -v dummy.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gcc-${VER_GCC} &&
set +x +u
