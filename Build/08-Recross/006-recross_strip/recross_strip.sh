#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


### Strip static libraries
find /recross-tools/lib -type f -name '*\.a' -exec strip -p --strip-debug '{}' ';' 


### Strip dynamic libraries
find /recross-tools/lib            -type f -name '*\.so*' -exec strip -p --strip-unneeded '{}' ';' 
find /recross-tools/${LFS_TGT}/lib -type f -name '*\.so*' -exec strip -p --strip-unneeded '{}' ';' 
find /recross-tools/libexec        -type f -name '*\.so*' -exec strip -p --strip-unneeded '{}' ';' 


### Strip binaries
find /recross-tools/bin            -type f -exec strip -p --strip-unneeded '{}' ';' 
find /recross-tools/${LFS_TGT}/bin -type f -exec strip -p --strip-unneeded '{}' ';' 
strip -p --strip-all /recross-tools/libexec/gcc/${LFS_TGT}/${VER_GCC}/{install-tools/fixincl,plugin/gengtype}
strip -p --strip-all /recross-tools/libexec/gcc/${LFS_TGT}/${VER_GCC}/{cc1,cc1plus,collect2,g++-mapper-server,lto-wrapper,lto1}


### Purge docs
rm -rf /recross-tools/{,share}/{info,man,doc}


set +x +u
