#!/bin/bash


echo "===> Recross Binutils Start" &&
cd 001-recross_binutils &&
/tools/bin/time -vao build.log ./recross_binutils.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Recross Binutils End" &&


echo "===> Recross GCC Start" &&
cd 002-recross_gcc &&
/tools/bin/time -vao build.log ./recross_gcc.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Recross GCC End" &&


echo "===> Linux Headers Start" &&
cd 003-linux-headers &&
/tools/bin/time -vao build.log ./linux-headers.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Linux Headers End" &&


echo "===> Glibc Start" &&
cd 004-glibc &&
/tools/bin/time -vao build.log ./glibc.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Glibc End" &&


echo "===> Libstdc++ Start" &&
cd 005-libstdc++ &&
/tools/bin/time -vao build.log ./libstdc++.sh > build.log 2>error.log && 
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libstdc++ End" &&


echo "===> Recross Strip" &&
cd 006-recross_strip &&
/tools/bin/time -vao build.log ./recross_strip.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&
cd .. &&
echo "<=== Recross Strip End" 
