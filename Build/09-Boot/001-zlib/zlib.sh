#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/zlib-ng-${VER_ZLIBNG}.tar.gz ] || exit -1
rm -rf zlib-ng-${VER_ZLIBNG} &&
tar xf /Sources/zlib-ng-${VER_ZLIBNG}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd zlib-ng-${VER_ZLIBNG} &&

CC="${LFS_TGT}-gcc" \
CXX="${LFS_TGT}-g++" \
LD="${LFS_TGT}-ld" \
AR="${LFS_TGT}-ar" \
RANLIB="${LFS_TGT}-ranlib" \
CFLAGS="${COPTS_0} -flto -fPIC -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
LDFLAGS="${LDOPTS_0}" \
./configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--sharedlibdir=/lib64 \
	--zlib-compat \
	--without-armv6 &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL 
make install pkgconfigdir=/usr/lib64/pkgconfig/ &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/zlib-ng-${VER_ZLIBNG} &&
cp -v {FAQ.zlib,INDEX.md,LICENSE.md,PORTING.md,README.md} \
	/usr/share/doc/zlib-ng-${VER_ZLIBNG} &&

rm -v /lib64/libz.a &&

###################################################################### VALIDATE
if [ ! -h /lib64/libz.so ] ; then
	echo "ERROR: dynamic library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v configure.log ../ &&
cd ../ &&
rm -rf zlib-ng-${VER_ZLIBNG} &&
set +x +u
