#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/xz-${VER_XZ}.tar.xz ] || exit -1
rm -rf xz-${VER_XZ} &&
tar xf /Sources/xz-${VER_XZ}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="${LFS_TGT}-gcc" \
LD="${LFS_TGT}-ld" \
AR="${LFS_TGT}-ar" \
RANLIB="${LFS_TGT}-ranlib" \
CFLAGS="${COPTS_0} -flto -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
LDFLAGS="${LDOPTS_0}" \
../xz-${VER_XZ}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--docdir=/usr/share/doc/xz-${VER_XZ} \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
ldconfig &&

mv -v /usr/bin/xz /bin &&
for i in lzcat lzma unlzma unxz xzcat ; do
  rm -v /usr/bin/${i};
  ln -sv /bin/xz /usr/bin/${i};
done

rm -v /lib64/liblzma.la &&

###################################################################### VALIDATE
if [ ! -x /bin/xz ] ; then
	echo "ERROR: xz not installed"
	exit -1
fi

/bin/xz -V | grep "^xz (XZ Utils) ${VER_XZ}$"
if [ $? != 0 ] ; then
	echo "ERROR: xz is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf xz-${VER_XZ} &&
set +x +u
