#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/zstd-${VER_ZSTD}.tar.gz ] || exit -1
rm -rf zstd-${VER_ZSTD} &&
tar xf /Sources/zstd-${VER_ZSTD}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd zstd-${VER_ZSTD} &&

cd lib &&
make -j${MAKEJOBS:-2} \
	CC="${LFS_TGT}-gcc" \
	LD="${LFS_TGT}-ld" \
	AR="${LFS_TGT}-ar" \
	RANLIB="${LFS_TGT}-ranlib" \
	CFLAGS="${COPTS_0} -flto -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
	LDFLAGS="-pthread -shared -fPIC -fvisibility=hidden ${LDOPTS_0}" \
	LIBDIR=/lib64 \
	libzstd &&

cd ../programs &&
make -j${MAKEJOBS:-2} \
	CC="${LFS_TGT}-gcc" \
	LD="${LFS_TGT}-ld" \
	AR="${LFS_TGT}-ar" \
	RANLIB="${LFS_TGT}-ranlib" \
	CFLAGS="${COPTS_0} -flto -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
	LDFLAGS="-pthread -lz -llzma ${LDOPTS_0}" \
	LIBDIR=/lib64 &&

cd ../ &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
CC="${LFS_TGT}-gcc" \
	prefix=/usr \
	LIBDIR=/lib64 \
	PKGCONFIGDIR=/usr/lib64/pkgconfig \
	make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/zstd-${VER_ZSTD} &&
cp -vr CHANGELOG CODE_OF_CONDUCT.md CONTRIBUTING.md COPYING LICENSE README.md \
	TESTING.md doc /usr/share/doc/zstd-${VER_ZSTD} &&

mv -v /usr/bin/zstd /bin &&
for i in unzstd zstdcat zstdmt ; do
  rm -v /usr/bin/${i};
  ln -sv /bin/zstd /usr/bin/${i};
done;

rm -v /lib64/libzstd.a &&

###################################################################### VALIDATE
if [ ! -x /bin/zstd ] ; then
	echo "ERROR: zstd not installed"
	exit -1
fi

/bin/zstd -V | grep -E " Zstandard .* v${VER_ZSTD},"
if [ $? != 0 ] ; then
	echo "ERROR: zstd is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf zstd-${VER_ZSTD} &&
set +x +u 
