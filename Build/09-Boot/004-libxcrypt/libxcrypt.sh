#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libxcrypt-${VER_LIBXCRYPT}.tar.xz ] || exit -1
rm -rf libxcrypt-${VER_LIBXCRYPT} &&
tar xf /Sources/libxcrypt-${VER_LIBXCRYPT}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="${LFS_TGT}-gcc" \
CXX="${LFS_TGT}-g++" \
LD="${LFS_TGT}-ld" \
AR="${LFS_TGT}-ar" \
RANLIB="${LFS_TGT}-ranlib" \
CFLAGS="${COPTS_0} -flto -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
LDFLAGS="${LDOPTS_0}" \
../libxcrypt-${VER_LIBXCRYPT}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--with-pkgconfigdir=/usr/lib64/pkgconfig \
	--enable-hashes=strong,glibc \
	--enable-obsolete-api=no \
	--disable-failure-tokens \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libxcrypt-${VER_LIBXCRYPT} &&
cp -v ../libxcrypt-${VER_LIBXCRYPT}/{AUTHORS,COPYING.LIB,ChangeLog} \
	../libxcrypt-${VER_LIBXCRYPT}/{LICENSING,NEWS,README,THANKS,TODO} \
	/usr/share/doc/libxcrypt-${VER_LIBXCRYPT} &&

rm -v /lib64/libcrypt.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/crypt.h ] ; then
	echo "ERROR: crypt.h header not installed"
	exit -1
fi

if [ ! -f /lib64/libcrypt.so.2.0.0 ] ; then
	echo "ERROR: libcrypt.so.2.0.0 library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libxcrypt-${VER_LIBXCRYPT} &&
set +x +u
