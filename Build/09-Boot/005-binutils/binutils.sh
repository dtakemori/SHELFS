#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/binutils-${VER_BINUTILS}.tar.xz ] || exit -1
rm -rf binutils-${VER_BINUTILS} &&
tar xf /Sources/binutils-${VER_BINUTILS}.tar.xz &&

######################################################################### PATCH
cd binutils-${VER_BINUTILS} &&
patch -Np1 -i /Patches/binutils-2.43.1-upstream_fix-1.patch &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="${LFS_TGT}-gcc" \
CXX="${LFS_TGT}-g++" \
LD="${LFS_TGT}-ld" \
AR="${LFS_TGT}-ar" \
RANLIB="${LFS_TGT}-ranlib" \
CFLAGS="${COPTS_0} -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
CXXFLAGS="${CXXOPTS_0} -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
LDFLAGS="${LDOPTS_0}" \
../binutils-${VER_BINUTILS}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--sysconfdir=/etc \
	--host=${LFS_TGT} \
	--target=${LFS_TGT} \
	--with-lib-path="/usr/local/lib64:/usr/lib64:/lib64" \
	--enable-shared \
	--enable-gold=yes \
	--enable-ld=yes \
	--enable-plugins \
	--with-system-zlib \
	--disable-werror &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} tooldir=/usr &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make tooldir=/usr install &&
cp -v /usr/bin/ld.gold /usr/bin/gold &&

install -v -d -m 755 /usr/share/doc/binutils-${VER_BINUTILS} &&
cp -v ../binutils-${VER_BINUTILS}/{COPYING*,ChangeLog*,MAINTAINERS} \
	../binutils-${VER_BINUTILS}/{README*,SECURITY.txt} \
	/usr/share/doc/binutils-${VER_BINUTILS} &&

rm -v /usr/lib64/lib{bfd,ctf,ctf-nobfd,gprofng,opcodes,sframe}.{a,la} &&
rm -f /usr/lib64/gprofng/libgp-collectorAPI.{a,la} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/ld.bfd ] ; then
	echo "ERROR: ld.bfd not installed"
	exit -1
fi

/usr/bin/ld.bfd -v | grep "^GNU ld (GNU Binutils) ${VER_BINUTILS}$"
if [ $? != 0 ] ; then
	echo "ERROR: ld.bfd is wrong version"
	exit -1
fi

if [ ! -x /usr/bin/ld.gold ] ; then
	echo "ERROR: ld.gold not installed"
	exit -1
fi

/usr/bin/ld.gold -V | grep "^GNU gold (GNU Binutils ${VER_BINUTILS})"
if [ $? != 0 ] ; then
	echo "ERROR: ld.gold is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf binutils-${VER_BINUTILS} &&
set +x +u
