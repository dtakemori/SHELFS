#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gmp-${VER_GMP}.tar.xz ] || exit -1
rm -rf gmp-${VER_GMP} &&
tar xf /Sources/gmp-${VER_GMP}.tar.xz &&

######################################################################### PATCH
cd gmp-${VER_GMP} &&
cp -v configfsf.guess config.guess &&
cp -v configfsf.sub config.sub &&

cd ../ &&

########################################################################## PREP
mkdir -v build &&
cd build &&

CC="${LFS_TGT}-gcc" \
CXX="${LFS_TGT}-g++" \
AR="${LFS_TGT}-ar" \
RANLIB="${LFS_TGT}-ranlib" \
CFLAGS+="${COPTS_0} -fexceptions -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
CXXFLAGS+="${CXXOPTS_0} -fexceptions -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
LDFLAGS="${LDOPTS_0}" \
../gmp-${VER_GMP}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--enable-cxx \
	--enable-shared \
	--disable-static \
	--docdir=/usr/share/doc/gmp-${VER_GMP} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&
make -j${MAKEJOBS:-2} html &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
make install-html &&
ldconfig &&

cp -v ../gmp-${VER_GMP}/{AUTHORS,COPYING*,ChangeLog,NEWS,README} \
	/usr/share/doc/gmp-${VER_GMP} &&

rm -v /usr/lib64/libgmp{,xx}.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/gmp.h ] ; then
	echo "ERROR: gmp.h header not installed"
	exit -1
fi

if [ ! -h /usr/lib64/libgmp.so ] ; then
	echo "ERROR: libgmp.so library not installed"
	exit -1
fi

strings /usr/lib64/libgmp.so | grep "^${VER_GMP}$"
if [ $? != 0 ]; then
	echo "ERROR: GMP library is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gmp-${VER_GMP} &&
set +x +u
