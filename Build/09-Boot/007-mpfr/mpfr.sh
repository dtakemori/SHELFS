#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/mpfr-${VER_MPFR}.tar.xz ] || exit -1
rm -rf mpfr-${VER_MPFR} &&
tar xf /Sources/mpfr-${VER_MPFR}.tar.xz &&

######################################################################### PATCH
cd mpfr-${VER_MPFR}/ &&
for p in /Sources/mpfr-patches/patch*
   do echo "Applying patch ${p}" &&
   patch -Z -p1 -i ${p};
done
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="${LFS_TGT}-gcc" \
CXX="${LFS_TGT}-g++" \
AR="${LFS_TGT}-ar" \
RANLIB="${LFS_TGT}-ranlib" \
CFLAGS="${COPTS_0} -flto -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
LDFLAGS="${LDOPTS_0}" \
../mpfr-${VER_MPFR}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--with-gmp=/usr \
	--docdir=/usr/share/doc/mpfr-${VER_MPFR} \
	--enable-shared \
	--enable-thread-safe \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

rm -v /usr/lib64/libmpfr.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/mpfr.h ] ; then
	echo "ERROR: mpfr.h header not installed"
	exit -1
fi

if [ ! -h /usr/lib64/libmpfr.so ] ; then
	echo "ERROR: libmpfr.so library not installed"
	exit -1
fi

strings /usr/lib64/libmpfr.so | grep -E "^${VER_MPFR}$"
if [ $? != 0 ]; then
	echo "ERROR: MPFR library is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf mpfr-${VER_MPFR} &&
set +x +u
