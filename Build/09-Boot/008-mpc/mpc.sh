#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/mpc-${VER_MPC}.tar.gz ] || exit -1
rm -rf mpc-${VER_MPC} &&
tar xf /Sources/mpc-${VER_MPC}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="${LFS_TGT}-gcc" \
CXX="${LFS_TGT}-g++" \
AR="${LFS_TGT}-ar" \
RANLIB="${LFS_TGT}-ranlib" \
CFLAGS="${COPTS_0} -flto -D_FORTIFY_SOURCE=2 -fstack-protector-strong -Wl,-z,relro,-z,now" \
LDFLAGS="${LDOPTS_0}" \
../mpc-${VER_MPC}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--docdir=/usr/share/doc/mpc-${VER_MPC} \
	--with-gmp=/usr \
	--with-mpfr=/usr \
	--enable-shared \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&
make -j${MAKEJOBS:-2} html &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
make install-html &&
ldconfig &&

cp -v ../mpc-${VER_MPC}/{AUTHORS,COPYING.LESSER,ChangeLog,NEWS,README,TODO} \
	/usr/share/doc/mpc-${VER_MPC} &&

rm -v /usr/lib64/libmpc.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/mpc.h ] ; then
	echo "ERROR: mpc.h header not installed"
	exit -1
fi

if [ ! -h /usr/lib64/libmpc.so ] ; then
	echo "ERROR: libmpc.so library not installed"
	exit -1
fi

strings /usr/lib64/libmpc.so | grep "^${VER_MPC}$"
if [ $? != 0 ]; then
	echo "ERROR: MPC library is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf mpc-${VER_MPC} &&
set +x +u
