#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gcc-${VER_GCC}.tar.xz ] || exit -1
rm -rf gcc-${VER_GCC} &&
tar xf /Sources/gcc-${VER_GCC}.tar.xz &&

[ -f /Sources/isl-${VER_ISL}.tar.xz ] || exit -1
tar xf /Sources/isl-${VER_ISL}.tar.xz &&
mv -v isl-${VER_ISL} gcc-${VER_GCC}/isl &&

######################################################################### PATCH
cd gcc-${VER_GCC} &&

### Make -Wformat, -Wformat-security and -Wtrampolines default
patch -p1 -i /Patches/gcc-8.2.0_warn-format-security.patch &&
patch -p1 -i /Patches/gcc-8.2.0_warn-trampolines.patch &&

### Make -z,relro,-z,now the default
patch -p1 -i /Patches/gcc-13.1.0_relro.patch &&

### Make -D_FORTIFY_SOURCE=2 the default
patch -p1 -i /Patches/gcc-13.1.0_fortify_source.patch &&
patch -p1 -i /Patches/gcc-8.2.0_no-fortify-libitm.patch &&

cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

SED=sed \
LD=ld \
LDFLAGS="${LDOPTS_0}" \
../gcc-${VER_GCC}/configure \
        --with-pkgversion="LFS-Final +PIE +SSP +FORTIFY_SOURCE" \
	--prefix=/usr \
	--host=${LFS_TGT} \
	--target=${LFS_TGT} \
	--bindir=/usr/bin \
	--includedir=/usr/include \
	--libdir=/usr/lib \
	--libexecdir=/usr/libexec \
	--datadir=/usr/share \
	--docdir=/usr/share/doc/gcc-${VER_GCC} \
	--infodir=/usr/share/info \
	--mandir=/usr/share/man \
	--enable-clocale=gnu \
	--enable-shared \
	--enable-threads=posix \
	--enable-__cxa_atexit \
	--enable-languages=c,c++ \
	--enable-default-pie \
	--enable-default-ssp \
	--enable-checking=release \
	--enable-lto \
	--with-arch=${LFS_GCC_ARCH} \
	--with-system-zlib \
	--disable-bootstrap \
	--disable-fixincludes \
	--disable-multilib \
	--disable-werror &&

######################################################################### BUILD
### need to increase the stack limit for genautomata
ulimit -s 262144 &&
make -j${MAKEJOBS:-2} &&
ulimit -s 8192 &&
make -j${MAKEJOBS:-2} html &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k -C gcc check > ../test.log
fi

####################################################################### INSTALL
make install &&
make install-html &&
ln -sv ../usr/bin/cpp /lib/cpp &&
ln -sv gcc /usr/bin/cc &&

ln -sfv ../../libexec/gcc/${LFS_TGT}/${VER_GCC}/liblto_plugin.so \
	/usr/lib64/bfd-plugins/ &&

cp -v ../gcc-${VER_GCC}/{ABOUT-NLS,COPYING*,ChangeLog*} \
	../gcc-${VER_GCC}/{LAST_UPDATED,MAINTAINERS,README} \
	/usr/share/doc/gcc-${VER_GCC} &&

rm -v /usr/libexec/gcc/${LFS_TGT}/${VER_GCC}/liblto_plugin.la &&
rm -v /usr/lib/gcc/${LFS_TGT}/${VER_GCC}/plugin/lib{cc,cp}1plugin.la &&
rm -v /usr/lib64/lib{asan,atomic,cc1,gomp,itm,lsan,quadmath}.la &&
rm -v /usr/lib64/lib{ssp,ssp_nonshared,stdc++,stdc++exp,stdc++fs,supc++}.la &&
rm -v /usr/lib64/lib{hwasan,tsan,ubsan}.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/gcc ] ; then
	echo "ERROR: /usr/bin/gcc not installed"
	exit -1
fi

/usr/bin/gcc -v 2>&1 | grep "^gcc version ${VER_GCC}"
if [ $? != 0 ] ; then
	echo "ERROR: /usr/bin/gcc is wrong version"
	exit -1
fi


### Verify compiler output uses final environment
/usr/bin/gcc -v -Wl,--verbose \
	/Build/Config/dummy.c &> dummy.log &&
if [ ! -x a.out ] ; then
	echo "ERROR: no compiler output"
	exit -1
fi

readelf -l a.out | grep 'interpreter: /lib' 
if [ $? != 0 ] ; then
	echo "ERROR: Interpreter not in final environment"
	exit -1
fi

grep -o '/usr/lib.*crt[1in].*succeeded' dummy.log 
if [ $? != 0 ] ; then
	echo "ERROR: C runtime files not found"
	exit -1
fi

grep -o '/libgcc_s.so.*succeeded' dummy.log 
if [ $? != 0 ] ; then
	echo "ERROR: GCC runtime files not found"
	exit -1
fi

grep -B3 '^ /usr/include' dummy.log &&
if [ $? != 0 ] ; then
	echo "ERROR: Include path does not include /usr/include"
	exit -1
fi

grep 'SEARCH.*' dummy.log | sed 's|; |\n|g' | grep '/usr/lib'
if [ $? != 0 ] ; then
	echo "ERROR: Search dirs do not include /usr/lib"
	exit -1
fi

grep "/lib.*/libc.so.6 " dummy.log 
if [ $? != 0 ] ; then
	echo "ERROR: C library not found"
	exit -1
fi


### Verify default PIE output
readelf -l a.out | grep -i '^ELF file type is DYN' 
if [ $? != 0 ] ; then
	echo "ERROR: Default output is not PIE"
	exit -1
fi

### Verify default RELRO
readelf -l a.out | grep 'GNU_RELRO' 
if [ $? != 0 ] ; then
	echo "ERROR: Default output is not RELRO"
	exit -1
fi

### Verify default BIND_NOW
readelf -d a.out | grep 'BIND_NOW' 
if [ $? != 0 ] ; then
	echo "ERROR: Default output is not BIND_NOW"
	exit -1
fi
mv -v dummy.log ../


### Check default FORTIFY_SOURCE
### Turn off stack protector
/usr/bin/gcc -O -fno-stack-protector \
	/Build/Config/fortify_test.c -o fortify_test 2>&1 \
	| grep 'overflows the destination'
if [ $? != 0 ] ; then
	echo "ERROR: Default compile has no FORTIFY_SOURCE warning"
	exit -1
fi

readelf -s fortify_test | grep 'memcpy_chk' 
if [ $? != 0 ] ; then
	echo "ERROR: FORTIFY_SOURCE test missing memcpy_chk"
	exit -1
fi

./fortify_test ||
if [ $? != 134 ] ; then
	echo "ERROR: Unexpected success in FORTIFY_SOURCE test"
	exit -1
fi
rm -vf fortify_test


### Check default Stack Smashing Protector
### Turn off FORTIFY_SOURCE
/usr/bin/gcc -U_FORTIFY_SOURCE \
	/Build/Config/ssptest.c \
	-o ssptest &&
./ssptest ||
if [ $? != 134 ] ; then
	echo "ERROR: Unexpected success in SSP test"
	exit -1
fi
rm -fv ssptest &&


### Check that we can turn off both SSP and FORTIFY_SOURCE
/usr/bin/gcc -O2 -U_FORTIFY_SOURCE -fno-stack-protector \
	/Build/Config/memcpy.c \
	-o memcpy &&
./memcpy 11 &&
if [ $? != 0 ] ; then
	echo "ERROR: Unexpected failure in FORTIFY_SOURCE/SSP test"
	exit -1
fi
rm -rv memcpy &&


### Check we can compile static
/usr/bin/gcc -static \
	/Build/Config/dummy.c
if [ $? != 0 ] ; then
	echo "ERROR: Unable to compile -static"
	exit -1
fi

readelf -l a.out | grep -i '^ELF file type is EXEC' 
if [ $? != 0 ] ; then
	echo "ERROR: Default output is not EXEC"
	exit -1
fi

readelf -s a.out | grep 'stack_chk_fail' 
if [ $? != 0 ] ; then
	echo "ERROR: Default output has no stack canary"
	exit -1
fi
rm -vf a.out


### Dump the specs in case we need to look at them later
echo "/usr/bin/gcc -dumpspecs" &&
/usr/bin/gcc -dumpspecs > ../dumpspecs.log &&


####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gcc-${VER_GCC} &&
set +x +u
