#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/bzip2-${VER_BZIP2}.tar.gz ] || exit -1
rm -rf bzip2-${VER_BZIP2} &&
tar xf /Sources/bzip2-${VER_BZIP2}.tar.gz &&

######################################################################### PATCH
cd bzip2-${VER_BZIP2} &&
patch -Np1 -i /Patches/bzip2-1.0.6-install_docs-1.patch &&
sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

ln -v -s ../bzip2-${VER_BZIP2}/* . &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} -f Makefile-libbz2_so \
	CFLAGS="${COPTS_1} -fPIC -flto -D_FILE_OFFSET_BITS=64 -Wall -Winline" \
	LDFLAGS="${LDOPTS_1}" &&
make clean &&

make -j${MAKEJOBS:-2} -f Makefile \
	AR=gcc-ar \
	RANLIB=gcc-ranlib \
	CFLAGS="${COPTS_1} -fPIC -flto -D_FILE_OFFSET_BITS=64 -Wall -Winline" \
	LDFLAGS="${LDOPTS_1}" &&

####################################################################### INSTALL
make PREFIX=/usr install &&
rm -v /usr/bin/bzip2 &&
install -v bzip2-shared /bin/bzip2 &&

cp -v ../bzip2-${VER_BZIP2}/{CHANGES,LICENSE,README} \
	/usr/share/doc/bzip2-${VER_BZIP2} &&

### Move the libraries to /lib
cp -va libbz2.so* /lib64/ &&
ln -vs libbz2.so.${VER_BZIP2} /lib64/libbz2.so &&
rm -v /usr/lib/libbz2.a &&

ldconfig &&

rm -v /usr/bin/{bunzip2,bzcat} &&
ln -vs /bin/bzip2 /usr/bin/bunzip2 &&
ln -vs /bin/bzip2 /usr/bin/bzcat &&

rm -v /usr/bin/{bzcmp,bzegrep,bzfgrep,bzless} &&
ln -vs bzdiff /usr/bin/bzcmp &&
ln -vs bzmore /usr/bin/bzless &&
ln -vs bzgrep /usr/bin/bzegrep &&
ln -vs bzgrep /usr/bin/bzfgrep &&

###################################################################### VALIDATE
if [ ! -x /bin/bzip2 ] ; then
	echo "ERROR: bzip2 not installed"
	exit -1
fi

/bin/bzip2 --help 2>&1 | grep "Version ${VER_BZIP2}"
if [ $? != 0 ] ; then
	echo "ERROR: bzip2 is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf bzip2-${VER_BZIP2} &&
set +x +u
