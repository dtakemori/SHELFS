#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/lzo-${VER_LZO}.tar.gz ] || exit -1
rm -rf lzo-${VER_LZO} &&
tar xf /Sources/lzo-${VER_LZO}.tar.gz &&

######################################################################### PATCH
mkdir -v build/ &&
cd build/ &&

########################################################################## PREP
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../lzo-${VER_LZO}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--docdir=/usr/share/doc/lzo-${VER_LZO} \
	--enable-shared &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
ldconfig &&

rm -v /lib64/liblzo2.{a,la} &&

###################################################################### VALIDATE
if [ ! -f /lib64/liblzo2.so ] ; then
	echo "ERROR: liblzo2.so is not installed"
	exit -1
fi

grep LZO_VERSION_STRING /usr/include/lzo/lzoconf.h | grep ${VER_LZO}
if [ $? != 0 ] ; then
	echo "ERROR: lzo/lzoconf.h is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf lzo-${VER_LZO} &&
set +x +u 
