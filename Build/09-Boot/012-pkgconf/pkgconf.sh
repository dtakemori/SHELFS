#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/pkgconf-${VER_PKGCONF}.tar.xz ] || exit -1
rm -rf pkgconf-${VER_PKGCONF} &&
tar xf /Sources/pkgconf-${VER_PKGCONF}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../pkgconf-${VER_PKGCONF}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--docdir=/usr/share/doc/pkgconf-${VER_PKGCONF} \
	--disable-static \
	--with-pkg-config-dir="/usr/lib64/pkgconfig:/usr/local/lib64/pkgconfig" &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
ln -sv pkgconf /usr/bin/pkg-config &&

cp -v ../pkgconf-${VER_PKGCONF}/{COPYING,NEWS} \
	/usr/share/doc/pkgconf-${VER_PKGCONF} &&

rm -v /usr/lib64/libpkgconf.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/pkgconf ] ; then
	echo "ERROR: pkgconf is not installed"
	exit -1
fi

/usr/bin/pkgconf --version | grep "^${VER_PKGCONF}$"
if [ $? != 0 ] ; then
	echo "ERROR: pkgconf is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf pkgconf-${VER_PKGCONF} &&
set +x +u
