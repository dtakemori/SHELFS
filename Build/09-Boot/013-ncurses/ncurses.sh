#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/ncurses-${VER_NCURSES}.tar.gz ] || exit -1
rm -rf ncurses-${VER_NCURSES} &&
tar xf /Sources/ncurses-${VER_NCURSES}.tar.gz &&

######################################################################### PATCH
cd ncurses-${VER_NCURSES} &&

if [ ! -z ${VER_NCURSESPATCH:-} ] ; then
   bash /Sources/ncurses-patches/ncurses-${VER_NCURSES}-${VER_NCURSESPATCH}-patch.sh
fi
for p in /Sources/ncurses-patches/ncurses-${VER_NCURSES}-*.patch;
   do echo "Applying patch ${p}" &&
   patch -p1 -i ${p};
done

cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

AR=gcc-ar \
RANLIB=gcc-ranlib \
CFLAGS="${COPTS_1} -flto" \
CXXFLAGS="${CXXOPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../ncurses-${VER_NCURSES}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--mandir=/usr/share/man \
	--with-pkg-config-libdir=/usr/lib64/pkgconfig \
	--with-shared \
	--with-cxx-shared \
	--without-debug \
	--without-normal \
	--enable-pc-files \
	--enable-symlinks \
	--enable-widec \
	--disable-root-environ &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/ncurses-${VER_NCURSES} &&
cp -v ../ncurses-${VER_NCURSES}/{ANNOUNCE,AUTHORS,COPYING,NEWS} \
	../ncurses-${VER_NCURSES}/{README,TO-DO,VERSION} \
	/usr/share/doc/ncurses-${VER_NCURSES} &&

### Create linker script and symlinks for -lcurses etc.
echo "INPUT(-lncurses)"   > /lib64/libcurses.so &&
echo "INPUT(-lncurses++)" > /lib64/libcurses++.so &&
ln -svf libncurses.so /lib64/libcurses.so &&

### Create symlinks for non-wide libraries
for lib in ncurses ncurses++ form panel menu ; do \
	echo "INPUT(-l${lib}w)" > /lib64/lib${lib}.so ; \
	ln -sv ${lib}w.pc /usr/lib64/pkgconfig/${lib}.pc ; \
done

###################################################################### VALIDATE
if [ ! -x /usr/bin/tset ] ; then
	echo "ERROR: tset is not installed"
	exit -1
fi

/usr/bin/tset -V | grep "^ncurses ${VER_NCURSES}"
if [ $? != 0 ] ; then
	echo "ERROR: tset is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf ncurses-${VER_NCURSES} &&
set +x +u
