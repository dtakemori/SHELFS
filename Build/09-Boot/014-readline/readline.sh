#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/readline-${VER_READLINE}.tar.gz ] || exit -1
rm -rf readline-${VER_READLINE} &&
tar xf /Sources/readline-${VER_READLINE}.tar.gz &&

######################################################################### PATCH
cd readline-${VER_READLINE} &&
for p in /Sources/readline-patches/readline*;
   do echo "Applying patch ${p}" &&
   patch -i ${p};
done

### Don't move old readline libraries on reinstallations
sed -i '/MV.*old/d' Makefile.in &&
sed -i '/{OLDSUFF}/c:' support/shlib-install &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_1}" \
../readline-${VER_READLINE}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--docdir=/usr/share/doc/readline-${VER_READLINE} \
	--disable-static \
	--with-curses &&

######################################################################### BUILD
make SHLIB_LIBS=-lncurses -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
ldconfig &&

install -v -m644 ../readline-${VER_READLINE}/doc/*.{ps,pdf,html,dvi} \
	/usr/share/doc/readline-${VER_READLINE} &&

###################################################################### VALIDATE
if [ ! -f /usr/include/readline/readline.h ] ; then
	echo "ERROR: readline.h header not installed"
	exit -1
fi

if [ ! -f /lib64/libreadline.so ] ; then
	echo "ERROR: libreadline.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf readline-${VER_READLINE} &&
set +x +u
