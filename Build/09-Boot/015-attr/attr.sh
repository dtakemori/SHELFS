#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/attr-${VER_ATTR}.tar.xz ] || exit -1
rm -rf attr-${VER_ATTR} &&
tar xf /Sources/attr-${VER_ATTR}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../attr-${VER_ATTR}/configure \
	--prefix=/usr \
	--bindir=/bin \
	--libdir=/lib64 \
	--libexecdir=/usr/libexec \
	--sysconfdir=/etc \
	--docdir=/usr/share/doc/attr-${VER_ATTR} \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make pkgconfdir=/usr/lib64/pkgconfig install &&
ldconfig &&

rm -v /lib64/libattr.la &&

###################################################################### VALIDATE
if [ ! -x /bin/getfattr ] ; then
	echo "ERROR: getfattr not installed"
	exit -1
fi

/bin/getfattr --version | grep "^getfattr ${VER_ATTR}$"
if [ $? != 0 ] ; then
	echo "ERROR: getfattr is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf attr-${VER_ATTR} &&
set +x +u
