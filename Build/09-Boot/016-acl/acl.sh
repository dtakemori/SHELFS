#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/acl-${VER_ACL}.tar.xz ] || exit -1
rm -rf acl-${VER_ACL} &&
tar xf /Sources/acl-${VER_ACL}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../acl-${VER_ACL}/configure \
	--prefix=/usr \
	--bindir=/bin \
	--libdir=/lib64 \
	--libexecdir=/usr/lib \
	--docdir=/usr/share/doc/acl-${VER_ACL} \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make pkgconfdir=/usr/lib64/pkgconfig install &&
ldconfig &&

rm -v /lib64/libacl.la &&

###################################################################### VALIDATE
if [ ! -x /bin/getfacl ] ; then
	echo "ERROR: getfacl not installed"
	exit -1
fi

/bin/getfacl --version | grep "^getfacl ${VER_ACL}$"
if [ $? != 0 ] ; then
	echo "ERROR: getfacl is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf acl-${VER_ACL} &&
set +x +u
