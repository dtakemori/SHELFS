#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libcap-${VER_LIBCAP}.tar.xz ] || exit -1
rm -rf libcap-${VER_LIBCAP} &&
tar xf /Sources/libcap-${VER_LIBCAP}.tar.xz &&

######################################################################### PATCH
cd libcap-${VER_LIBCAP} &&
sed -i "s/-O2/${COPTS_1} -flto/" Make.Rules &&

########################################################################## PREP

######################################################################### BUILD
make -j${MAKEJOBS:-2} LDFLAGS="${LDOPTS_1}" &&

########################################################################## TEST

####################################################################### INSTALL
make LIBDIR=/lib64 lib=lib64 \
	PKGCONFIGDIR=/usr/lib64/pkgconfig install &&

chmod -v 755 /lib64/libcap.so* &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libcap-${VER_LIBCAP} &&
cp -v {CHANGELOG,License,README} \
	/usr/share/doc/libcap-${VER_LIBCAP} &&

rm -v /lib64/lib{cap,psx}.a &&

###################################################################### VALIDATE
if [ ! -x /sbin/setcap ] ; then
	echo "ERROR: setcap is not installed"
	exit -1
fi

if [ ! -f /lib64/libcap.so ] ; then
	echo "ERROR: libcap.so library is not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf libcap-${VER_LIBCAP} &&
set +x +u
