#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libaio-${VER_LIBAIO}.tar.gz ] || exit -1
rm -rf libaio-${VER_LIBAIO} &&
tar xf /Sources/libaio-${VER_LIBAIO}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd libaio-${VER_LIBAIO} &&
make -j${MAKEJOBS:-2} \
	CC="gcc ${COPTS_1}" \
	LDFLAGS="${LDOPTS_1}" &&

########################################################################## TEST

####################################################################### INSTALL
make install libdir=/lib64 && 

install -v -d -m 755 /usr/share/doc/libaio-${VER_LIBAIO} &&
cp -v COPYING ChangeLog README.md TODO \
	/usr/share/doc/libaio-${VER_LIBAIO} &&

rm -v /lib64/libaio.a &&

###################################################################### VALIDATE
if [ ! -f /usr/include/libaio.h ] ; then
	echo "ERROR: libaio.h header not installed"
	exit -1
fi
 
if [ ! -f /lib64/libaio.so.1.0.2 ] ; then
	echo "ERROR: libaio.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf libaio-${VER_LIBAIO} &&
set +x +u
