#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/elfutils-${VER_ELFUTILS}.tar.bz2 ] || exit -1
rm -rf elfutils-${VER_ELFUTILS} &&
tar xf /Sources/elfutils-${VER_ELFUTILS}.tar.bz2 &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto"
LDFLAGS="${LDOPTS_1}" \
../elfutils-${VER_ELFUTILS}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--disable-debuginfod \
	--disable-libdebuginfod &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make -C libelf install &&
ldconfig &&

install -v -m 644 config/libelf.pc /usr/lib64/pkgconfig &&

install -v -d -m 755 /usr/share/doc/libelf-${VER_ELFUTILS} &&
cp -v ../elfutils-${VER_ELFUTILS}/{ABOUT-NLS,AUTHORS,CONTRIBUTING,COPYING*} \
	../elfutils-${VER_ELFUTILS}/{ChangeLog,NEWS,NOTES,README,THANKS,TODO} \
	/usr/share/doc/libelf-${VER_ELFUTILS} &&

rm -v /lib64/libelf.a &&

###################################################################### VALIDATE
if [ ! -f /usr/include/libelf.h ] ; then
	echo "ERROR: libelf.h header not installed"
	exit -1
fi
 
if [ ! -f /lib64/libelf-${VER_ELFUTILS}.so ] ; then
	echo "ERROR: libelf.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf elfutils-${VER_ELFUTILS} &&
set +x +u
