#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libffi-${VER_LIBFFI}.tar.gz ] || exit -1
rm -rf libffi-${VER_LIBFFI} &&
tar xf /Sources/libffi-${VER_LIBFFI}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../libffi-${VER_LIBFFI}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libffi-${VER_LIBFFI} &&
cp -v ../libffi-${VER_LIBFFI}/{ChangeLog*,LICENSE*,README.md} \
	/usr/share/doc/libffi-${VER_LIBFFI} &&

rm -v /usr/lib64/libffi.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/ffi.h ] ; then
	echo "ERROR: ffi.h header not installed"
	exit -1
fi

if [ ! -f /usr/lib64/libffi.so ] ; then
	echo "ERROR: libffi.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libffi-${VER_LIBFFI} &&
set +x +u
