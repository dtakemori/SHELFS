#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libtirpc-${VER_LIBTIRPC}.tar.bz2 ] || exit -1
rm -rf libtirpc-${VER_LIBTIRPC} &&
tar xf /Sources/libtirpc-${VER_LIBTIRPC}.tar.bz2 &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../libtirpc-${VER_LIBTIRPC}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--disable-gssapi \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libtirpc-${VER_LIBTIRPC} &&
cp -v ../libtirpc-${VER_LIBTIRPC}/{AUTHORS,ChangeLog,COPYING,HACKING} \
      ../libtirpc-${VER_LIBTIRPC}/{NEWS,README,THANKS,TODO,VERSION} \
	/usr/share/doc/libtirpc-${VER_LIBTIRPC} &&

rm -v /lib64/libtirpc.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/tirpc/rpc/rpc.h ] ; then
	echo "ERROR: rpc.h header not installed"
	exit -1
fi

if [ ! -f /lib64/libtirpc.so ] ; then
	echo "ERROR: libtirpc.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libtirpc-${VER_LIBTIRPC} &&
set +x +u
