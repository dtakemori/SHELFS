#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libunistring-${VER_LIBUNISTRING}.tar.xz ] || exit -1
rm -rf libunistring-${VER_LIBUNISTRING} &&
tar xf /Sources/libunistring-${VER_LIBUNISTRING}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../libunistring-${VER_LIBUNISTRING}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--docdir=/usr/share/doc/libunistring-${VER_LIBUNISTRING} \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

cp -v ../libunistring-${VER_LIBUNISTRING}/{AUTHORS,BUGS,COPYING*} \
	../libunistring-${VER_LIBUNISTRING}/{ChangeLog,DEPENDENCIES,HACKING} \
	../libunistring-${VER_LIBUNISTRING}/{NEWS,README,THANKS} \
	/usr/share/doc/libunistring-${VER_LIBUNISTRING} &&

rm -v /usr/lib64/libunistring.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/unistr.h ] ; then
	echo "ERROR: unistr.h header not installed"
	exit -1
fi

if [ ! -f /usr/lib64/libunistring.so ] ; then
	echo "ERROR: libunistring.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libunistring-${VER_LIBUNISTRING} &&
set +x +u
