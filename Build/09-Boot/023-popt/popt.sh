#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/popt-${VER_POPT}.tar.gz ] || exit -1
rm -rf popt-${VER_POPT} &&
tar xf /Sources/popt-${VER_POPT}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../popt-${VER_POPT}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/popt-${VER_POPT} &&
cp -v ../popt-${VER_POPT}/{COPYING,CREDITS,README,popt.pdf} \
	/usr/share/doc/popt-${VER_POPT} &&

rm -v /lib64/libpopt.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/popt.h ] ; then
	echo "ERROR: popt.h header file not installed"
	exit -1
fi

if [ ! -f /lib64/libpopt.so ] ; then
	echo "ERROR: libpopt.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf popt-${VER_POPT} &&
set +x +u
