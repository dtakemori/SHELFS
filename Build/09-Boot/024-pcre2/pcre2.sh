#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/pcre2-${VER_PCRE2}.tar.bz2 ] || exit -1
rm -rf pcre2-${VER_PCRE2} &&
tar xf /Sources/pcre2-${VER_PCRE2}.tar.bz2 &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
CXXFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../pcre2-${VER_PCRE2}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--docdir=/usr/share/doc/pcre2-${VER_PCRE2} \
	--enable-jit \
	--enable-pcre2-16 \
	--enable-pcre2-32 \
	--enable-unicode \
	--enable-pcre2grep-libz \
	--enable-pcre2grep-libbz2 \
	--enable-pcre2test-libreadline \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
ldconfig &&

rm -v /lib64/libpcre2-{8,16,32,posix}.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/pcre2-config ] ; then
	echo "ERROR: pcre2-config not installed"
	exit -1
fi

/usr/bin/pcre2-config --version | grep "^${VER_PCRE2}$"
if [ $? != 0 ] ; then
	echo "ERROR: pcre2-config wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf pcre2-${VER_PCRE2} &&
set +x +u
