#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/bash-${VER_BASH}.tar.gz ] || exit -1
rm -rf bash-${VER_BASH} &&
tar xf /Sources/bash-${VER_BASH}.tar.gz &&

######################################################################### PATCH
cd bash-${VER_BASH}/ &&
for p in /Sources/bash-patches/bash*-???
   do echo "Applying patch ${p}" &&
   patch -p0 -i ${p};
done
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H} -Wl,--no-fatal-warnings" \
../bash-${VER_BASH}/configure \
	--prefix=/usr \
	--bindir=/bin \
	--libdir=/lib64 \
	--sysconfdir=/etc \
	--docdir=/usr/share/doc/bash-${VER_BASH} \
	--with-installed-readline \
	--with-curses \
	--without-bash-malloc &&

######################################################################### BUILD
make version.h &&
make -j${MAKEJOBS:-2} -C builtins &&
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
# if [ ${CONF_TEST} -ge 1 ] ; then
#   /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
# fi

####################################################################### INSTALL
if [ -h /bin/bash ] ; then rm -fv /bin/bash; fi &&
make pkgconfigdir=/usr/lib64/pkgconfig install &&

if [ -h /bin/sh ] ; then rm -fv /bin/sh; fi &&
ln -sv bash /bin/sh &&

###################################################################### VALIDATE
if [ ! -x /bin/bash ] ; then
	echo "ERROR: bash is not installed"
	exit -1
fi

/bin/bash --version | grep "^GNU bash, version ${VER_BASH}"
if [ $? != 0 ] ; then
	echo "ERROR: bash is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf bash-${VER_BASH} &&
set +x +u
