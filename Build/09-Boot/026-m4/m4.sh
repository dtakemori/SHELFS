#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/m4-${VER_M4}.tar.xz ] || exit -1
rm -rf m4-${VER_M4} &&
tar xf /Sources/m4-${VER_M4}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../m4-${VER_M4}/configure \
	--prefix=/usr &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ln -sv /usr/bin/m4 /bin/m4 &&

install -v -d -m 755 /usr/share/doc/m4-${VER_M4} &&
cp -v ../m4-${VER_M4}/{AUTHORS,BACKLOG,COPYING,ChangeLog*} \
	../m4-${VER_M4}/{NEWS,README,THANKS,TODO} \
	/usr/share/doc/m4-${VER_M4} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/m4 ] ; then
	echo "ERROR: m4 is not installed"
	exit -1
fi

/usr/bin/m4 --version | grep "^m4 (GNU M4) ${VER_M4}$"
if [ $? != 0 ] ; then
	echo "ERROR: m4 is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf m4-${VER_M4} &&
set +x +u
