#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libtool-${VER_LIBTOOL}.tar.xz ] || exit -1
rm -rf libtool-${VER_LIBTOOL} &&
tar xf /Sources/libtool-${VER_LIBTOOL}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_1}" \
../libtool-${VER_LIBTOOL}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libtool-${VER_LIBTOOL} &&
cp -v ../libtool-${VER_LIBTOOL}/{AUTHORS,COPYING,ChangeLog,NEWS,README} \
	../libtool-${VER_LIBTOOL}/{THANKS,TODO} \
	/usr/share/doc/libtool-${VER_LIBTOOL} &&

rm -v /usr/lib64/libltdl.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/libtool ] ; then
	echo "ERROR: libtool is not installed"
	exit -1
fi

/usr/bin/libtool --version | grep "^libtool (GNU libtool) ${VER_LIBTOOL}$"
if [ $? != 0 ] ; then
	echo "ERROR: libtool is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libtool-${VER_LIBTOOL} &&
set +x +u
