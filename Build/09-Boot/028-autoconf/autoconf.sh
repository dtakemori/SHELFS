#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/autoconf-${VER_AUTOCONF}.tar.xz ] || exit -1
rm -rf autoconf-${VER_AUTOCONF} &&
tar xf /Sources/autoconf-${VER_AUTOCONF}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

../autoconf-${VER_AUTOCONF}/configure \
	--prefix=/usr &&

######################################################################### BUILD
make -j${MAKEJOBS:-2}  &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/autoconf-${VER_AUTOCONF} &&
cp -v ../autoconf-${VER_AUTOCONF}/{AUTHORS,BUGS,COPYING*,ChangeLog*} \
	../autoconf-${VER_AUTOCONF}/{NEWS,README,THANKS,TODO} \
	/usr/share/doc/autoconf-${VER_AUTOCONF} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/autoconf ] ; then
	echo "ERROR: autoconf is not installed"
	exit -1
fi

/usr/bin/autoconf --version | grep "^autoconf (GNU Autoconf) ${VER_AUTOCONF}$"
if [ $? != 0 ] ; then
	echo "ERROR: autoconf is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf autoconf-${VER_AUTOCONF} &&
set +x +u
