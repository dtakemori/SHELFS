#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/automake-${VER_AUTOMAKE}.tar.xz ] || exit -1
rm -rf automake-${VER_AUTOMAKE} &&
tar xf /Sources/automake-${VER_AUTOMAKE}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

../automake-${VER_AUTOMAKE}/configure \
	--prefix=/usr \
	--docdir=/usr/share/doc/automake-${VER_AUTOMAKE} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

cp -v ../automake-${VER_AUTOMAKE}/{AUTHORS,COPYING} \
	../automake-${VER_AUTOMAKE}/{ChangeLog,HACKING} \
	../automake-${VER_AUTOMAKE}/{NEWS*,PLANS} \
	../automake-${VER_AUTOMAKE}/{README,THANKS} \
	/usr/share/doc/automake-${VER_AUTOMAKE} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/aclocal ] ; then
	echo "ERROR: aclocal is not installed"
	exit -1
fi

/usr/bin/aclocal --version | grep "^aclocal (GNU automake) ${VER_AUTOMAKE}"
if [ $? != 0 ] ; then
	echo "ERROR: aclocal is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf automake-${VER_AUTOMAKE} &&
set +x +u 
