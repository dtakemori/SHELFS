#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/Linux-PAM-${VER_LIBPAM}.tar.xz ] || exit -1
rm -rf Linux-PAM-${VER_LIBPAM} &&
tar xf /Sources/Linux-PAM-${VER_LIBPAM}.tar.xz &&

######################################################################### PATCH
cd Linux-PAM-${VER_LIBPAM} &&
sed -e '/service_DATA/d' -i modules/pam_namespace/Makefile.am &&
autoreconf &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
../Linux-PAM-${VER_LIBPAM}/configure \
	--prefix=/usr \
	--sbindir=/usr/sbin \
	--sysconfdir=/etc \
	--libdir=/lib64 \
	--docdir=/usr/share/doc/Linux-PAM-${VER_LIBPAM} \
	--enable-lastlog \
	--enable-securedir=/lib64/security &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  install -v -m755 -d /etc/pam.d 
  cp -v /Build/Config/pam.d/other-test /etc/pam.d/other 
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
  rm -f /etc/pam.d/other 
fi

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
install -v -m755 -d /etc/pam.d &&

cp -v ../Linux-PAM-${VER_LIBPAM}/{AUTHORS,CHANGELOG,COPYING,ChangeLog*} \
	../Linux-PAM-${VER_LIBPAM}/{Copyright,NEWS,README} \
	/usr/share/doc/Linux-PAM-${VER_LIBPAM} &&

cp -v /Build/Config/pam.d/{login,other,system-auth} /etc/pam.d/ &&
cp -v /Build/Config/{securetty,shells} /etc/ &&
chmod -v 4755 /usr/sbin/unix_chkpwd &&

rm -v /lib64/libpam{,_misc,c}.la &&
rm -v /lib64/security/pam_*.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/security/pam_modules.h ] ; then
	echo "ERROR: pam_modules.h header not installed"
	exit -1
fi

if [ ! -f /lib64/libpam.so ] ; then
	echo "ERROR: libpam.so library not installed"
	exit -1
fi

cd ../ &&

######################################################################### BUILD
rm -rf libcap-${VER_LIBCAP} &&
tar xf /Sources/libcap-${VER_LIBCAP}.tar.xz &&

######################################################################### PATCH
cd libcap-${VER_LIBCAP} &&
sed -i "s/-O2/${COPTS_1} -flto/" Make.Rules &&
sed -i 's/xargs -e/xargs/' pam_cap/Makefile &&

########################################################################## PREP

######################################################################### BUILD
make -j${MAKEJOBS:-2} LDFLAGS="-Wl,-z,noexecstack -Wl,-O1,--fatal-warnings" \
	-C pam_cap &&

########################################################################## TEST

####################################################################### INSTALL
install -v -m755 pam_cap/pam_cap.so /lib64/security &&
install -v -m744 pam_cap/capability.conf /etc/security &&

####################################################################### CLEANUP
cp -v ../build/config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf Linux-PAM-${VER_LIBPAM} &&
rm -rf libcap-${VER_LIBCAP} &&
set +x +u
