#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/bc-${VER_BC}.tar.xz ] || exit -1
rm -rf bc-${VER_BC} &&
tar xf /Sources/bc-${VER_BC}.tar.xz &&

########################################################################## PREP

######################################################################### PATCH
cd bc-${VER_BC}/ &&

CC=gcc \
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
PREFIX=/usr \
./configure.sh -G -r &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/bc-${VER_BC} &&
cp -v {LICENSE.md,MEMORY_BUGS.md,NEWS.md,NOTICE.md,README.md} \
	/usr/share/doc/bc-${VER_BC} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/bc ] ; then
	echo "ERROR: bc is not installed"
	exit -1
fi

/usr/bin/bc --version | grep "^bc ${VER_BC}$"
if [ $? != 0 ] ; then
	echo "ERROR: bc is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf bc-${VER_BC} &&
set +x +u
