#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/bison-${VER_BISON}.tar.xz ] || exit -1
rm -rf bison-${VER_BISON} &&
tar xf /Sources/bison-${VER_BISON}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../bison-${VER_BISON}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--docdir=/usr/share/doc/bison-${VER_BISON} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ; then
  /tools/bin/time -vao ../test.log make -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/bison ] ; then
	echo "ERROR: bison is not installed"
	exit -1
fi

/usr/bin/bison --version | grep "^bison (GNU Bison) ${VER_BISON}$"
if [ $? != 0 ] ; then
	echo "ERROR: bison is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf bison-${VER_BISON} &&
set +x +u
