#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/coreutils-${VER_COREUTILS}.tar.xz ] || exit -1
rm -rf coreutils-${VER_COREUTILS} &&
tar xf /Sources/coreutils-${VER_COREUTILS}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

### Use kill and uptime from procps_ng
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
FORCE_UNSAFE_CONFIGURE=1 \
../coreutils-${VER_COREUTILS}/configure \
	--prefix=/usr \
	--enable-no-install-program=kill,uptime &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
if [ -h /bin/pwd ] ; then rm -fv /bin/pwd; fi &&
make install &&

install -v -d -m 755 /usr/share/doc/coreutils-${VER_COREUTILS} &&
cp -v ../coreutils-${VER_COREUTILS}/{AUTHORS,COPYING,ChangeLog,NEWS,README} \
	../coreutils-${VER_COREUTILS}/{THANKS,THANKS-to-translators,TODO} \
	/usr/share/doc/coreutils-${VER_COREUTILS} &&

### Move these to FHS locations
mv -v /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df}	/bin &&
mv -v /usr/bin/{echo,false,ln,ls,mkdir,mknod,mv,pwd,rm}	/bin &&
/bin/mv -v /usr/bin/{rmdir,stty,sync,true,uname}	/bin &&
/bin/mv -v /usr/bin/chroot 				/usr/sbin &&

/bin/mv -v /usr/bin/{head,sleep,nice}			/bin &&

###################################################################### VALIDATE
if [ ! -x /bin/date ] ; then
	echo "ERROR: date is not installed"
	exit -1
fi

/bin/date --version | grep "^date (GNU coreutils) ${VER_COREUTILS}$"
if [ $? != 0 ] ; then
	echo "ERROR: date is wrong version"
	exit -1
fi

####################################################################### CLEANUP
/bin/cp -v config.log ../ &&
cd ../ &&
/bin/rm -rf build &&
/bin/rm -rf coreutils-${VER_COREUTILS} &&
set +x +u
