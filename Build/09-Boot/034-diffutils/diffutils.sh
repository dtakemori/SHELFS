#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/diffutils-${VER_DIFFUTILS}.tar.xz ] || exit -1
rm -rf diffutils-${VER_DIFFUTILS} &&
tar xf /Sources/diffutils-${VER_DIFFUTILS}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../diffutils-${VER_DIFFUTILS}/configure \
	--prefix=/usr &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/diffutils-${VER_DIFFUTILS} &&
cp -v ../diffutils-${VER_DIFFUTILS}/{AUTHORS,COPYING,ChangeLog*,NEWS} \
	../diffutils-${VER_DIFFUTILS}/{README,THANKS,TODO} \
	/usr/share/doc/diffutils-${VER_DIFFUTILS} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/diff ] ; then
	echo "ERROR: diff is not installed"
	exit -1
fi

/usr/bin/diff --version | grep "^diff (GNU diffutils) ${VER_DIFFUTILS}$"
if [ $? != 0 ] ; then
	echo "ERROR: diff is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf diffutils-${VER_DIFFUTILS} &&
set +x +u
