#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/expat-${VER_EXPAT}.tar.xz ] || exit -1
rm -rf expat-${VER_EXPAT} &&
tar xf /Sources/expat-${VER_EXPAT}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../expat-${VER_EXPAT}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--docdir=/usr/share/doc/expat-${VER_EXPAT} \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make cmakedir=/usr/lib64/cmake/expat-${VER_EXPAT} \
	pkgconfigdir=/usr/lib64/pkgconfig install &&
ldconfig &&

install -v -m644 ../expat-${VER_EXPAT}/{AUTHORS,Changes,COPYING,README.md} \
	../expat-${VER_EXPAT}/doc/*.{html,css} \
	/usr/share/doc/expat-${VER_EXPAT} &&

rm -v /lib64/libexpat.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/xmlwf ] ; then
	echo "ERROR: xmlwf is not installed"
	exit -1
fi

strings /lib64/libexpat.so | grep -E "^expat_${VER_EXPAT}$"
if [ $? != 0 ] ; then
	echo "ERROR: libexpat.so library is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf expat-${VER_EXPAT} &&
set +x +u
