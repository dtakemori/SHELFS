#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/file-${VER_FILE}.tar.gz ] || exit -1
rm -rf file-${VER_FILE} &&
tar xf /Sources/file-${VER_FILE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../file-${VER_FILE}/configure \
	--prefix=/usr \
	--libdir=/lib64 &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make pkgconfigexecdir=/usr/lib64/pkgconfig install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/file-${VER_FILE} &&
cp -v ../file-${VER_FILE}/{AUTHORS,COPYING,ChangeLog,NEWS} \
	../file-${VER_FILE}/{README.DEVELOPER,README.md,TODO} \
	/usr/share/doc/file-${VER_FILE} &&

rm -v /lib64/libmagic.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/file ] ; then
	echo "ERROR: file is not installed"
	exit -1
fi

/usr/bin/file --version | grep "^file-${VER_FILE}$"
if [ $? != 0 ] ; then
	echo "ERROR: file is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf file-${VER_FILE} &&
set +x +u
