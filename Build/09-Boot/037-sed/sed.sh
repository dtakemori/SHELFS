#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/sed-${VER_SED}.tar.xz ] || exit -1
rm -rf sed-${VER_SED} &&
tar xf /Sources/sed-${VER_SED}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../sed-${VER_SED}/configure \
	--prefix=/usr \
	--bindir=/bin &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m755 /usr/share/doc/sed-${VER_SED} &&
cp -v ../sed-${VER_SED}/{AUTHORS,BUGS,COPYING,ChangeLog*,NEWS,README,THANKS} \
	/usr/share/doc/sed-${VER_SED} &&

# Fixup libtool to use the newly installed sed
sed -i 's|/tools/bin/sed|/bin/sed|g' /usr/bin/libtool &&
sed -i 's|/tools/bin/sed|/bin/sed|g' /usr/bin/libtoolize &&

###################################################################### VALIDATE
if [ ! -x /bin/sed ] ; then
	echo "ERROR: sed is not installed"
	exit -1
fi

/bin/sed --version | grep "/bin/sed (GNU sed) ${VER_SED}$"
if [ $? != 0 ] ; then
	echo "ERROR: sed is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf sed-${VER_SED} &&
set +x +u
