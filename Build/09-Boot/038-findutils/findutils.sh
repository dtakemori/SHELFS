#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/findutils-${VER_FINDUTILS}.tar.xz ] || exit -1
rm -rf findutils-${VER_FINDUTILS} &&
tar xf /Sources/findutils-${VER_FINDUTILS}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../findutils-${VER_FINDUTILS}/configure \
	--prefix=/usr \
	--libexecdir=/usr/libexec/findutils \
	--localstatedir=/var/lib/locate &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
mv -v /usr/bin/find /bin &&
ln -sv /bin/find /usr/bin/find &&

install -v -d -m 755 /usr/share/doc/findutils-${VER_FINDUTILS} &&
cp -v ../findutils-${VER_FINDUTILS}/{AUTHORS,COPYING,ChangeLog,NEWS} \
	../findutils-${VER_FINDUTILS}/{README*,THANKS,TODO} \
	/usr/share/doc/findutils-${VER_FINDUTILS} &&

sed -i 's|: ${BINDIR=/usr/bin}|: ${BINDIR=/bin}|' /usr/bin/updatedb &&

###################################################################### VALIDATE
if [ ! -x /bin/find ] ; then
	echo "ERROR: find is not installed"
	exit -1
fi

/bin/find --version | grep "^find (GNU findutils) ${VER_FINDUTILS}$"
if [ $? != 0 ] ; then
	echo "ERROR: find is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf findutils-${VER_FINDUTILS} &&
set +x +u
