#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/flex-${VER_FLEX}.tar.gz ] || exit -1
rm -rf flex-${VER_FLEX} &&
tar xf /Sources/flex-${VER_FLEX}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

AR=gcc-ar \
RANLIB=gcc-ranlib \
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../flex-${VER_FLEX}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--docdir=/usr/share/doc/flex-${VER_FLEX} \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

ln -sv flex /usr/bin/lex &&
ln -sv flex.1 /usr/share/man/man1/lex.1 &&

cp -v ../flex-${VER_FLEX}/{ChangeLog,THANKS} \
	/usr/share/doc/flex-${VER_FLEX} &&

rm -v /usr/lib64/libfl.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/flex ] ; then
	echo "ERROR: flex is not installed"
	exit -1
fi

/usr/bin/flex --version | grep "^flex ${VER_FLEX}$"
if [ $? != 0 ] ; then
	echo "ERROR: flex is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf flex-${VER_FLEX} &&
set +x +u
