#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gawk-${VER_GAWK}.tar.xz ] || exit -1
rm -rf gawk-${VER_GAWK} &&
tar xf /Sources/gawk-${VER_GAWK}.tar.xz &&

######################################################################### PATCH
cd gawk-${VER_GAWK} &&
sed -i 's/extras//' Makefile.in &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../gawk-${VER_GAWK}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--sysconfdir=/etc &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
if [ -h /usr/bin/awk ] ; then rm -fv /usr/bin/awk; fi &&
make install &&

install -v -d -m 755 /usr/share/doc/gawk-${VER_GAWK} &&
cp -v ../gawk-${VER_GAWK}/doc/{awkforai.txt,*.{eps,pdf,jpg}} \
	../gawk-${VER_GAWK}/{AUTHORS,COPYING,ChangeLog*,NEWS*,POSIX.STD,README,TODO} \
	/usr/share/doc/gawk-${VER_GAWK} &&
cp -v -r ../gawk-${VER_GAWK}/README_d \
	/usr/share/doc/gawk-${VER_GAWK} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/gawk ] ; then
	echo "ERROR: gawk is not installed"
	exit -1
fi

/usr/bin/gawk --version | grep "^GNU Awk ${VER_GAWK}"
if [ $? != 0 ] ; then
	echo "ERROR: gawk is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gawk-${VER_GAWK} &&
set +x +u
