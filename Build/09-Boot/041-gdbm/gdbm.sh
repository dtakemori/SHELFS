#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gdbm-${VER_GDBM}.tar.gz ] || exit -1
rm -rf gdbm-${VER_GDBM} &&
tar xf /Sources/gdbm-${VER_GDBM}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../gdbm-${VER_GDBM}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/gdbm-${VER_GDBM} &&
cp -v ../gdbm-${VER_GDBM}/{AUTHORS,COPYING,ChangeLog} \
	../gdbm-${VER_GDBM}/{NEWS,NOTE-WARNING,README,THANKS} \
	/usr/share/doc/gdbm-${VER_GDBM} &&

rm -v /usr/lib64/libgdbm.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/gdbmtool ] ; then
	echo "ERROR: gdbmtool is not installed"
	exit -1
fi

/usr/bin/gdbmtool --version | grep "^gdbmtool (gdbm) ${VER_GDBM}$"
if [ $? != 0 ] ; then
	echo "ERROR: gdbmtool is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gdbm-${VER_GDBM} &&
set +x +u
