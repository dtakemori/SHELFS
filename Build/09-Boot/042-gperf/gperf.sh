#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gperf-${VER_GPERF}.tar.gz ] || exit -1
rm -rf gperf-${VER_GPERF} &&
tar xf /Sources/gperf-${VER_GPERF}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
CXXFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../gperf-${VER_GPERF}/configure \
	--prefix=/usr \
	--docdir=/usr/share/doc/gperf-${VER_GPERF} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

cp -v ../gperf-${VER_GPERF}/{AUTHORS,COPYING,ChangeLog,NEWS,README*} \
	/usr/share/doc/gperf-${VER_GPERF} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/gperf ] ; then
	echo "ERROR: gperf is not installed"
	exit -1
fi

/usr/bin/gperf --version | grep "^GNU gperf ${VER_GPERF}$"
if [ $? != 0 ] ; then
	echo "ERROR: gperf is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gperf-${VER_GPERF} &&
set +x +u
