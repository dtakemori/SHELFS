#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/grep-${VER_GREP}.tar.xz ] || exit -1
rm -rf grep-${VER_GREP} &&
tar xf /Sources/grep-${VER_GREP}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../grep-${VER_GREP}/configure \
	--prefix=/usr \
	--bindir=/bin &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
if [ -h /bin/grep ] ; then rm -fv /bin/grep ; fi &&
make install &&

install -v -d -m 755 /usr/share/doc/grep-${VER_GREP} &&
cp -v ../grep-${VER_GREP}/{AUTHORS,COPYING,ChangeLog*,NEWS,README*,THANKS,TODO} \
	/usr/share/doc/grep-${VER_GREP} &&

###################################################################### VALIDATE
if [ ! -x /bin/grep ] ; then
	echo "ERROR: grep is not installed"
	exit -1
fi

/bin/grep --version | grep "^grep (GNU grep) ${VER_GREP}$"
if [ $? != 0 ] ; then
	echo "ERROR: grep is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf grep-${VER_GREP} &&
set +x +u
