#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/icu4c-${VER_ICU}-src.tgz ] || exit -1
rm -rf icu &&
tar xf /Sources/icu4c-${VER_ICU}-src.tgz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CC="gcc" \
CXX="g++" \
CFLAGS="${COPTS_1} -flto" \
CXXFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../icu/source/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/icu4c-${VER_ICU} &&
cp -v ../icu/{APIChangeReport*,LICENSE,icu4c.css,*.html} \
	/usr/share/doc/icu4c-${VER_ICU} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/icuinfo ] ; then
	echo "ERROR: icuinfo is not installed"
	exit -1
fi

/usr/bin/icuinfo -v | grep -E \"version\" | grep -E $(echo ${VER_ICU} | sed 's/_/./')
if [ $? != 0 ] ; then
	echo "ERROR: icuinfo is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf icu &&
set +x +u
