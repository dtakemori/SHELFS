#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libxml2-${VER_LIBXML2}.tar.xz ] || exit -1
rm -rf libxml2-${VER_LIBXML2} &&
tar xf /Sources/libxml2-${VER_LIBXML2}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
CXXFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../libxml2-${VER_LIBXML2}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--sysconfdir=/etc \
	--docdir=/usr/share/doc/libxml2-${VER_LIBXML2} \
	--disable-static \
	--with-history \
	--with-icu \
	--without-python &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
sed -i '/libs=/s/xml2.*/xml2"/' /usr/bin/xml2-config &&

cp -v ../libxml2-${VER_LIBXML2}/{Copyright,NEWS,README.md,README.zOS} \
	/usr/share/doc/libxml2-${VER_LIBXML2} &&

rm -v /usr/lib64/libxml2.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/xml2-config ] ; then
	echo "ERROR: xml2-config is not installed"
	exit -1
fi

/usr/bin/xml2-config --version | grep "^${VER_LIBXML2}$"
if [ $? != 0 ] ; then
	echo "ERROR: libxml2 is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libxml2-${VER_LIBXML2} &&
set +x +u
