#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gettext-${VER_GETTEXT}.tar.xz ] || exit -1
rm -rf gettext-${VER_GETTEXT} &&
tar xf /Sources/gettext-${VER_GETTEXT}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
CXXFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_1}" \
../gettext-${VER_GETTEXT}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--docdir=/usr/share/doc/gettext-${VER_GETTEXT} \
	--enable-threads=isoc+posix \
	--disable-java \
	--disable-static \
	--with-ncurses-prefix=/usr \
	--with-xml2-prefix=/usr &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
chmod -v 0755 /usr/lib64/preloadable_libintl.so &&
ldconfig &&

cp -v ../gettext-${VER_GETTEXT}/{AUTHORS,COPYING,ChangeLog*,DEPENDENCIES} \
	../gettext-${VER_GETTEXT}/{HACKING,NEWS,PACKAGING,README,THANKS} \
	/usr/share/doc/gettext-${VER_GETTEXT} &&

rm -v /usr/lib64/lib{asprintf,gettextlib,gettextpo,gettextsrc,textstyle}.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/gettext ] ; then
	echo "ERROR: gettext is not installed"
	exit -1
fi

if [ ! -x /usr/bin/recode-sr-latin ] ; then
	echo "ERROR: recode-sr-latin is not installed"
	exit -1
fi

/usr/bin/gettext --version | grep "^gettext (GNU gettext-runtime) ${VER_GETTEXT}$"
if [ $? != 0 ] ; then
	echo "ERROR: gettext is wrong version"
	exit -1
fi

/usr/bin/recode-sr-latin --version | grep "^recode-sr-latin (GNU gettext-tools) ${VER_GETTEXT}$"
if [ $? != 0 ] ; then
	echo "ERROR: recode-sr-latin is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gettext-${VER_GETTEXT} &&
set +x +u
