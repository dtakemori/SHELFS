#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/freetype-${VER_FREETYPE}.tar.xz ] || exit -1
rm -rf freetype-${VER_FREETYPE} &&
tar xf /Sources/freetype-${VER_FREETYPE}.tar.xz &&

######################################################################### PATCH
cd freetype-${VER_FREETYPE} &&
sed -ri "s:.*(AUX_MODULES.*valid):\1:" modules.cfg &&
sed -r "s:.*(#.*SUBPIXEL_RENDERING) .*:\1:" \
    -i include/freetype/config/ftoption.h &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../freetype-${VER_FREETYPE}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--enable-freetype-config \
	--enable-static=no \
	--without-harfbuzz &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

rm -v /usr/lib64/libfreetype.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/freetype2/freetype/freetype.h ] ; then
	echo "ERROR: freetype.h header not installed"
	exit -1
fi

if [ ! -f /usr/lib64/libfreetype.so ] ; then
	echo "ERROR: libfreetype.so library not installed" 
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf freetype-${VER_FREETYPE} &&
set +x +u
