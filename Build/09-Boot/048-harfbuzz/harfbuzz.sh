#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/harfbuzz-${VER_HARFBUZZ}.tar.xz ] || exit -1
rm -rf harfbuzz-${VER_HARFBUZZ} &&
tar xf /Sources/harfbuzz-${VER_HARFBUZZ}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

PATH="${PATH}:/cross-tools/bin" meson setup ../harfbuzz-${VER_HARFBUZZ} \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--buildtype=release \
	-Dc_args="${COPTS_1} -flto ${LDOPTS_1}" \
	-Dcpp_args="${CXXOPTS_1} -flto"

######################################################################### BUILD
ninja -v -j ${MAKEJOBS} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
ninja install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/harfbuzz-${VER_HARFBUZZ} &&
cp -v ../harfbuzz-${VER_HARFBUZZ}/{AUTHORS,COPYING,NEWS,README,THANKS} \
	/usr/share/doc/harfbuzz-${VER_HARFBUZZ} &&

rm -vf /usr/lib64/libharfbuzz{-cairo,-icu,-subset}.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/harfbuzz/hb.h ] ; then
	echo "ERROR: hb.h header not installed"
	exit -1
fi

if [ ! -f /usr/lib64/libharfbuzz.so ] ; then
	echo "ERROR: libharfbuzz.so library not installed" 
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf harfbuzz-${VER_HARFBUZZ} &&
set +x +u
