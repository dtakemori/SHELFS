#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/unifont-${VER_UNIFONT}.pcf.gz ] || exit -1
mkdir -pv /usr/share/fonts/unifont/ &&
gunzip -c /Sources/unifont-${VER_UNIFONT}.pcf.gz \
	> /usr/share/fonts/unifont/unifont.pcf &&

[ -f /Sources/grub-${VER_GRUB}.tar.xz ] || exit -1
rm -rf grub-${VER_GRUB} &&
tar xf /Sources/grub-${VER_GRUB}.tar.xz &&

######################################################################### PATCH
cd grub-${VER_GRUB} &&
echo depends bli part_gpt > grub-core/extra_deps.lst &&
cd ../ &&

###############################################################################
mkdir -v build/ &&
cd build/ &&

########################################################################## PREP
if [ $(uname -m) == "x86_64" ] ; then
  env CC="gcc ${COPTS_1} -fcf-protection=return -fno-strict-aliasing -Wl,-O1" \
  LDFLAGS="-Wl,-O1,--sort-common,--warn-textrel,--warn-rwx-segments,--fatal-warnings"
  ../grub-${VER_GRUB}/configure \
	--prefix=/usr \
	--sbindir=/sbin \
	--sysconfdir=/etc \
	--disable-grub-emu-pci \
	--disable-grub-mkfont \
	--disable-efiemu \
	--disable-werror \
	--with-platform=pc &&

  ####################################################################### BUILD
  make -j${MAKEJOBS:-2} &&

  ######################################################################## TEST

  ##################################################################### INSTALL
  make install &&

  make distclean
fi

########################################################################## PREP
env CC="gcc ${COPTS_1} -fno-strict-aliasing -Wl,-O1" \
LDFLAGS="-Wl,-O1,--sort-common,--warn-textrel,--warn-rwx-segments,--fatal-warnings"
../grub-${VER_GRUB}/configure \
	--prefix=/usr \
	--sbindir=/sbin \
	--sysconfdir=/etc \
	--disable-grub-emu-pci \
	--disable-efiemu \
	--disable-werror \
	--with-platform=efi &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

mkdir -pv /usr/share/bash-completion/completions/ &&
mv -v /etc/bash_completion.d/grub /usr/share/bash-completion/completions/ &&
rmdir -v /etc/bash_completion.d &&

install -v -d -m 755 /usr/share/doc/grub-${VER_GRUB} &&
cp -v ../grub-${VER_GRUB}/{AUTHORS,BUGS,COPYING,ChangeLog*} \
	../grub-${VER_GRUB}/{NEWS,README,THANKS,TODO} \
	/usr/share/doc/grub-${VER_GRUB} &&

###################################################################### VALIDATE
if [ ! -x /sbin/grub-install ] ; then
	echo "ERROR: grub-install is not installed"
	exit -1
fi

/sbin/grub-install --version | grep "^/sbin/grub-install (GRUB).*${VER_GRUB}$"
if [ $? != 0 ] ; then
	echo "ERROR: grub-install is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf grub-${VER_GRUB} &&
set +x +u
