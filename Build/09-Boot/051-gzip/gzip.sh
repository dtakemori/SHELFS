#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gzip-${VER_GZIP}.tar.xz ] || exit -1
rm -rf gzip-${VER_GZIP} &&
tar xf /Sources/gzip-${VER_GZIP}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

### The ASM versions have DT_TEXTRELS
env DEFS=NO_ASM \
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../gzip-${VER_GZIP}/configure \
	--prefix=/usr &&

### Don't run test which is broken with busybox's less"
sed -i '/^  help-version.*/d' tests/Makefile &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
mv -v /usr/bin/{gzip,gunzip,zcat} /bin &&

install -v -d -m 755 /usr/share/doc/gzip-${VER_GZIP} &&
cp -v ../gzip-${VER_GZIP}/{AUTHORS,COPYING,ChangeLog*,NEWS,README,THANKS,TODO} \
	/usr/share/doc/gzip-${VER_GZIP} &&

###################################################################### VALIDATE
if [ ! -x /bin/gzip ] ; then
	echo "ERROR: gzip is not installed"
	exit -1
fi

/bin/gzip --version | grep "^gzip ${VER_GZIP}$"
if [ $? != 0 ] ; then
	echo "ERROR: gzip is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf gzip-${VER_GZIP} &&
set +x +u
