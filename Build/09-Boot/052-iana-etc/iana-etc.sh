#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/iana-etc-${VER_IANAETC}.tar.gz ] || exit -1
rm -rf iana-etc-${VER_IANAETC} &&
tar xf /Sources/iana-etc-${VER_IANAETC}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd iana-etc-${VER_IANAETC} &&

######################################################################### BUILD

########################################################################## TEST

####################################################################### INSTALL
cp -v protocols services /etc/ &&

###################################################################### VALIDATE
if [ ! -f /etc/services ] ; then
	echo "ERROR: /etc/services not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf iana-etc-${VER_IANAETC} &&
set +x +u
