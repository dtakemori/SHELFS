#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/inetutils-${VER_INETUTILS}.tar.xz ] || exit -1
rm -rf inetutils-${VER_INETUTILS} &&
tar xf /Sources/inetutils-${VER_INETUTILS}.tar.xz &&

######################################################################### PATCH
mkdir -v build/ &&
cd build/ &&

########################################################################## PREP
CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
../inetutils-${VER_INETUTILS}/configure \
	--prefix=/usr \
	--libexecdir=/usr/sbin \
	--sysconfdir=/etc \
	--localstatedir=/var \
	--with-ncurses-include-dir=/usr/include \
	--disable-logger \
	--disable-syslogd \
	--disable-whois \
	--disable-servers \
	--disable-rcp \
	--disable-rexec \
	--disable-rlogin \
	--disable-rsh &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
mv -v /usr/bin/ifconfig /sbin &&
mv -v /usr/bin/hostname /bin &&

mv -v /usr/bin/ping{,6} /bin &&
chmod -s /bin/ping &&
chmod -s /bin/ping6 &&
setcap cap_net_admin=ep /bin/ping &&
setcap cap_net_admin=ep /bin/ping6 &&

install -v -d -m 755 /usr/share/doc/inetutils-${VER_INETUTILS} &&
cp -v ../inetutils-${VER_INETUTILS}/{AUTHORS,CHECKLIST,COPYING,ChangeLog*} \
	../inetutils-${VER_INETUTILS}/{README,THANKS,TODO} \
	/usr/share/doc/inetutils-${VER_INETUTILS} &&

###################################################################### VALIDATE
if [ ! -x /bin/ping ] ; then
	echo "ERROR: ping not installed"
	exit -1
fi

/bin/ping --version | grep "^ping (GNU inetutils) ${VER_INETUTILS}$"
if [ $? != 0 ] ; then
	echo "ERROR: ping is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf inetutils-${VER_INETUTILS} &&
set +x +u
