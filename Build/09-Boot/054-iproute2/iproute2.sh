#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/iproute2-${VER_IPROUTE}.tar.xz ] || exit -1
rm -rf iproute2-${VER_IPROUTE} &&
tar xf /Sources/iproute2-${VER_IPROUTE}.tar.xz &&

######################################################################### PATCH
cd iproute2-${VER_IPROUTE} &&

sed -i 's|-O2|${COPTS_H} -flto -fno-strict-aliasing ${LDOPTS_H}|' Makefile &&

### Don't build/install arpd
sed -i '/ARPD/d' Makefile &&
rm -fv man/man8/arpd.8 &&

cd ../ &&

########################################################################## PREP

######################################################################### BUILD
cd iproute2-${VER_IPROUTE} &&
make -j1 \
	DESTDIR= \
	CONF_USR_DIR=/etc/iproute2 \
        NETNS_RUN_DIR=/run/netns &&

########################################################################## TEST

####################################################################### INSTALL
make install \
	DESTDIR= \
	SBINDIR=/sbin \
	CONF_USR_DIR=/etc/iproute2 \
	MANDIR=/usr/share/man \
        NETNS_RUN_DIR=/run/netns &&

install -v -d -m 755 /usr/share/doc/iproute2-${VER_IPROUTE} &&
cp -v COPYING CREDITS MAINTAINERS README* /usr/share/doc/iproute2-${VER_IPROUTE} &&

###################################################################### VALIDATE
if [ ! -x /sbin/ip ] ; then
	echo "ERROR: ip is not installed"
	exit -1
fi

if [ ! -d /etc/iproute2 ] ; then
	echo "ERROR: /etc/iproute2 not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf iproute2-${VER_IPROUTE} &&
set +x +u
