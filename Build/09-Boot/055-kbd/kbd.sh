#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/kbd-${VER_KBD}.tar.xz ] || exit -1
rm -rf kbd-${VER_KBD} &&
tar xf /Sources/kbd-${VER_KBD}.tar.xz &&

######################################################################### PATCH
cd kbd-${VER_KBD} &&
patch -p1 -i /Patches/kbd-2.6.0-backspace-1.patch &&
sed -i 's/\(RESIZECONS_PROGS=\)yes/\1no/g' configure &&
sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../kbd-${VER_KBD}/configure \
	--prefix=/usr \
	--sysconfdir=/etc &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
mv -v /usr/bin/{kbd_mode,loadkeys,openvt,setfont} /bin &&

install -v -d -m 755 /usr/share/doc/kbd-${VER_KBD} &&
cp -R -v ../kbd-${VER_KBD}/docs/doc/* /usr/share/doc/kbd-${VER_KBD} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/kbdinfo ] ; then
	echo "ERROR: kbdinfo not installed"
	exit -1
fi

/usr/bin/kbdinfo -V | grep "^kbdinfo from kbd ${VER_KBD}$"
if [ $? != 0 ] ; then
	echo "ERROR: kbdinfo incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf kbd-${VER_KBD} &&
set +x +u
