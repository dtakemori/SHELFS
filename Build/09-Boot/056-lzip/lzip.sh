#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/lzip-${VER_LZIP}.tar.gz ] || exit -1
rm -rf lzip-${VER_LZIP} &&
tar xf /Sources/lzip-${VER_LZIP}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

../lzip-${VER_LZIP}/configure \
	CXXFLAGS="${COPTS_1} -flto -fPIC" \
        LDFLAGS="${LDOPTS_1}" \
	--prefix=/usr &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/lzip-${VER_LZIP} &&
cp -v ../lzip-${VER_LZIP}/{AUTHORS,COPYING,ChangeLog,NEWS,INSTALL,README} \
	/usr/share/doc/lzip-${VER_LZIP} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/lzip ] ; then
	echo "ERROR: lzip not installed"
	exit -1
fi

/usr/bin/lzip -V | grep "^lzip ${VER_LZIP}$"
if [ $? != 0 ] ; then
	echo "ERROR: lzip is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf lzip-${VER_LZIP} &&
set +x +u
