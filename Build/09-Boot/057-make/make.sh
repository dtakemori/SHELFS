#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/make-${VER_MAKE}.tar.lz ] || exit -1
rm -rf make-${VER_MAKE} &&
lzip -c -d /Sources/make-${VER_MAKE}.tar.lz | tar xf - &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../make-${VER_MAKE}/configure \
	--prefix=/usr \
	--without-guile &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make PERL5LIB=${PWD}/tests/ -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/make-${VER_MAKE} &&
cp -v ../make-${VER_MAKE}/{AUTHORS,COPYING,ChangeLog,NEWS,README*,SCOPTIONS} \
	/usr/share/doc/make-${VER_MAKE} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/make ] ; then
	echo "ERROR: make is not installed"
	exit -1
fi

/usr/bin/make --version | grep "^GNU Make ${VER_MAKE}$"
if [ $? != 0 ] ; then
	echo "ERROR: make is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf make-${VER_MAKE} &&
set +x +u
