#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/openssl-${VER_OPENSSL}.tar.gz ] || exit -1
rm -rf openssl-${VER_OPENSSL} &&
tar xf /Sources/openssl-${VER_OPENSSL}.tar.gz &&

######################################################################### PATCH
cd openssl-${VER_OPENSSL} &&

########################################################################## PREP
./config \
	--prefix=/usr \
	--libdir=/lib64 \
	--openssldir=/etc/ssl shared zlib-dynamic \
	-DSSL_FORBID_ENULL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j1 -k test > ../test.log
fi

####################################################################### INSTALL
sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile &&
make DOCDIR=/usr/share/doc/openssl-${VER_OPENSSL} \
	CMAKECONFIGDIR=/usr/lib64/cmake/OpenSSL \
	PKGCONFIGDIR=/usr/lib64/pkgconfig \
	install &&
ldconfig &&

cp -vr doc/* /usr/share/doc/openssl-${VER_OPENSSL} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/openssl ] ; then
	echo "ERROR: openssl not installed"
	exit -1
fi

/usr/bin/openssl version | grep "^OpenSSL ${VER_OPENSSL}"
if [ $? != 0 ] ; then
	echo "ERROR: openssl incorrect version"
	exit -1
fi

####################################################################### CLEANUP
perl configdata.pm --dump > ../config.log &&
cd ../ &&
rm -rf openssl-${VER_OPENSSL} &&
set +x +u
