#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/patch-${VER_PATCH}.tar.xz ] || exit -1
rm -rf patch-${VER_PATCH} &&
tar xf /Sources/patch-${VER_PATCH}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../patch-${VER_PATCH}/configure \
	--prefix=/usr &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/patch-${VER_PATCH} &&
cp -v ../patch-${VER_PATCH}/{AUTHORS,COPYING,ChangeLog*,NEWS,README,TODO} \
	/usr/share/doc/patch-${VER_PATCH} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/patch ] ; then
	echo "ERROR: patch is not installed"
	exit -1
fi

/usr/bin/patch --version | grep "^GNU patch ${VER_PATCH}$"
if [ $? != 0 ] ; then
	echo "ERROR: patch is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf patch-${VER_PATCH} &&
set +x +u
