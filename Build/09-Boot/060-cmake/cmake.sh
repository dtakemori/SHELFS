#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/cmake-${VER_CMAKE}.tar.gz ] || exit -1
rm -rf cmake-${VER_CMAKE} &&
tar xf /Sources/cmake-${VER_CMAKE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
CXXFLAGS="${COPTS_1} -flto" \
LDFLAGS="${COPTS_1}" \
../cmake-${VER_CMAKE}/bootstrap \
	--prefix=/usr \
	--mandir=/usr/share/man \
	--no-system-curl \
	--no-system-jsoncpp \
	--no-system-librhash \
	--no-system-nghttp2 \
	--docdir=/share/doc/cmake-${VER_CMAKE} \
	--parallel=${MAKEJOBS:-2} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log bin/ctest -j ${MAKEJOBS:-2} > ../test.log 
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/cmake-${VER_CMAKE} &&
cp -v ../cmake-${VER_CMAKE}/{CONTRIBUTING.rst,README.rst} \
	/usr/share/doc/cmake-${VER_CMAKE} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/cmake ] ; then
	echo "ERROR: cmake is not installed"
	exit -1
fi

/usr/bin/cmake --version | grep "^cmake version ${VER_CMAKE}"
if [ $? != 0 ] ; then
	echo "ERROR: cmake is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf cmake-${VER_CMAKE} &&
set +x +u
