#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/ninja-${VER_NINJA}.tar.gz ] || exit -1
rm -rf ninja-${VER_NINJA} &&
tar xf /Sources/ninja-${VER_NINJA}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd ninja-${VER_NINJA} &&
CFLAGS="${COPTS_1} -flto" \
CXXFLAGS="${COPTS_1} -flto" \
LDFLAGS="${COPTS_1}" \
python3 configure.py --bootstrap &&

########################################################################## TEST

####################################################################### INSTALL
install -vm755 ninja /usr/bin/ &&

install -v -d -m 755 /usr/share/doc/ninja-${VER_NINJA} &&
cp -v {CONTRIBUTING.md,COPYING,README.md} /usr/share/doc/ninja-${VER_NINJA} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/ninja ] ; then
	echo "ERROR: ninja is not installed"
	exit -1
fi

/usr/bin/ninja --version | grep -E "^${VER_NINJA}$"
if [ $? != 0 ] ; then
	echo "ERROR: ninja is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf ninja-${VER_NINJA} &&
set +x +u

