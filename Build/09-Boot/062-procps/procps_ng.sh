#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/procps-ng-${VER_PROCPS}.tar.xz ] || exit -1
rm -rf procps-ng-${VER_PROCPS} &&
tar xf /Sources/procps-ng-${VER_PROCPS}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
cd procps-ng-${VER_PROCPS}/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
./configure \
	--prefix=/ \
	--includedir=/usr/include \
	--libdir=/lib64 \
	--datarootdir=/usr/share \
	--docdir=/usr/share/doc/procps-ng-${VER_PROCPS} \
	--disable-kill \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ; then
  sed -i -r 's|(pmap_initname)\\\$|\1|' testsuite/pmap.test/pmap.exp
  sed -i '/set tty/d' testsuite/pkill.test/pkill.exp
  rm testsuite/pgrep.test/pgrep.exp
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
ldconfig &&

cp -v AUTHORS COPYING* ChangeLog NEWS README.md \
	/usr/share/doc/procps-ng-${VER_PROCPS} &&

rm -v /lib64/libproc2.la &&

###################################################################### VALIDATE
if [ ! -x /bin/free ] ; then
	echo "ERROR: free not installed"
	exit -1
fi

/bin/free -V | grep "^free from procps-ng ${VER_PROCPS}$"
if [ $? != 0 ] ; then
	echo "ERROR: free incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf procps-ng-${VER_PROCPS} &&
set +x +u
