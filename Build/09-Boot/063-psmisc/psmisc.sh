#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/psmisc-${VER_PSMISC}.tar.xz ] || exit -1
rm -rf psmisc-${VER_PSMISC} &&
tar xf /Sources/psmisc-${VER_PSMISC}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
cd psmisc-${VER_PSMISC}/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
./configure \
	--prefix=/usr \
	--exec-prefix="" &&

######################################################################### BUILD
make -j${MAKEJOBS:-2}  &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
mv -v /bin/pstree /usr/bin &&
mv -v /bin/pstree.x11 /usr/bin &&

install -d -v -m 755 /usr/share/doc/psmisc-${VER_PSMISC} &&
cp -v AUTHORS COPYING ChangeLog NEWS README README.md \
	/usr/share/doc/psmisc-${VER_PSMISC} &&

###################################################################### VALIDATE
if [ ! -x /bin/fuser ] ; then
	echo "ERROR: fuser not installed"
	exit -1
fi

/bin/fuser -V 2>&1 | grep "^fuser (PSmisc) ${VER_PSMISC}$"
if [ $? != 0 ] ; then
	echo "ERROR: fuser incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf psmisc-${VER_PSMISC} &&
set +x +u
