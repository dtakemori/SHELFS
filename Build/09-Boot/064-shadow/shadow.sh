#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/shadow-${VER_SHADOW}.tar.xz ] || exit -1
rm -rf shadow-${VER_SHADOW} &&
tar xf /Sources/shadow-${VER_SHADOW}.tar.xz &&

######################################################################### PATCH
cd shadow-${VER_SHADOW} &&

sed -i 's/groups$(EXEEXT) //' src/Makefile.in &&
find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \; &&
find man -name Makefile.in -exec sed -i 's/sedspnam\.3 / /' {} \; &&
find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \; &&

sed -i  -e 's@#ENCRYPT_METHOD DES@ENCRYPT_METHOD YESCRYPT@' \
	-e 's@/var/spool/mail@/var/mail@' \
	etc/login.defs &&

cd ../ &&

########################################################################## PREP
cd shadow-${VER_SHADOW} &&

CFLAGS="${COPTS_H} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_H}" \
./configure \
	--libdir=/lib64 \
	--bindir=/bin \
	--sbindir=/sbin \
	--sysconfdir=/etc \
	--disable-static \
	--with-fcaps \
	--with-{b,yes}crypt \
	--with-group-name-max-length=32 \
	--without-libbsd \
	--without-nscd \
	--without-selinux \
	--without-sssd &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make ubindir=/bin usbindir=/sbin install &&

install -v -d -m 755 /usr/share/doc/shadow-${VER_SHADOW}/doc &&
cp -v AUTHORS.md COPYING ChangeLog NEWS README \
	/usr/share/doc/shadow-${VER_SHADOW} &&
cp -v doc/{HOWTO,README*,WISHLIST,console.c.spec.txt} \
	/usr/share/doc/shadow-${VER_SHADOW}/doc &&

[ -h /etc/passwd ] && rm -fv /etc/passwd
install -v -m644 /Build/Config/passwd /etc/passwd

[ -h /etc/group ] && rm -fv /etc/group
install -v -m644 /Build/Config/group /etc/group

cp -fv /Build/Config/pam.d/{chpasswd,login,postlogin,system-auth} /etc/pam.d/ &&
sed -i '/pam_selinux/d' /etc/pam.d/su &&

rm -v /lib64/libsubid.la &&

###################################################################### VALIDATE
if [ ! -x /sbin/chpasswd ] ; then
	echo "ERROR: chpasswd is not installed"
	exit -1
fi

if [ ! -x /sbin/pwck ] ; then
	echo "ERROR: pwck is not installed"
	exit -1
fi

##################################################################### CONFIGURE
pwconv &&

if ! getent group users ; then
  groupadd -g 100 users &&
  useradd -D -g 100 &&
  useradd -D -b /home &&
  sed 's/CREATE_MAIL_SPOOL.*/CREATE_MAIL_SPOOL=no/' -i /etc/default/useradd
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf shadow-${VER_SHADOW} &&
set +x +u
