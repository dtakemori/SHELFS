#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/sysvinit-${VER_SYSVINIT}.tar.xz ] || exit -1
rm -rf sysvinit-${VER_SYSVINIT} &&
tar xf /Sources/sysvinit-${VER_SYSVINIT}.tar.xz &&

######################################################################### PATCH
cd sysvinit-${VER_SYSVINIT} &&

patch -p1 -i /Patches/sysvinit-3.12-consolidated-1.patch &&

sed -i 's|/var/run/|/run/|' src/init.h &&
sed -i 's|/var/run/|/run/|' src/paths.h &&

########################################################################## PREP

######################################################################### BUILD
CFLAGS+="${COPTS_H}" \
LDFLAGS="${LDOPTS_H}" \
make -C src &&

########################################################################## TEST

####################################################################### INSTALL
make -C src install &&

install -v -d -m 755 /usr/share/doc/sysvinit-${VER_SYSVINIT} &&
cp -v COPYING COPYRIGHT README \
	doc/{Changelog,Propaganda,bootlogd.README,initctl} \
	/usr/share/doc/sysvinit-${VER_SYSVINIT} &&

install -v -m644 /Build/Config/inittab /etc/inittab &&
sed -i -e "s/__SERIAL__/${CONF_SERIAL}/" /etc/inittab &&

###################################################################### VALIDATE
if [ ! -x /sbin/init ] ; then
	echo "ERROR: init is not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf sysvinit-${VER_SYSVINIT} &&
set +x +u
