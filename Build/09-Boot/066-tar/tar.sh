#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/tar-${VER_TAR}.tar.xz ] || exit -1
rm -rf tar-${VER_TAR} &&
tar xf /Sources/tar-${VER_TAR}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
FORCE_UNSAFE_CONFIGURE=1 \
../tar-${VER_TAR}/configure \
	--prefix=/usr \
	--libexecdir=/usr/bin &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
make -C doc install-html docdir=/usr/share/doc/tar-${VER_TAR} &&

cp -v ../tar-${VER_TAR}/{AUTHORS,COPYING,ChangeLog*,NEWS,README,THANKS,TODO} \
	/usr/share/doc/tar-${VER_TAR} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/tar ] ; then
	echo "ERROR: tar is not installed"
	exit -1
fi

/usr/bin/tar --version | grep "^tar (GNU tar) ${VER_TAR}$"
if [ $? != 0 ] ; then
	echo "ERROR: tar is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf tar-${VER_TAR} &&
set +x +u
