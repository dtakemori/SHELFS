#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/time-${VER_TIME}.tar.gz ] || exit -1
rm -rf time-${VER_TIME} &&
tar xf /Sources/time-${VER_TIME}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto -DHAVE_STRERROR" \
LDFLAGS="${LDOPTS_1}" \
../time-${VER_TIME}/configure \
	--prefix=/usr

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/time-${VER_TIME} &&
cp -v ../time-${VER_TIME}/{AUTHORS,COPYING,ChangeLog,NEWS,README} \
	/usr/share/doc/time-${VER_TIME} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/time ] ; then
	echo "ERROR: time is not installed"
	exit -1
fi

/usr/bin/time --version | grep "^time (GNU Time) ${VER_TIME}$"
if [ $? != 0 ] ; then
	echo "ERROR: time is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf time-${VER_TIME} &&
set +x +u
