#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/cpio-${VER_CPIO}.tar.bz2 ] || exit -1
rm -rf cpio-${VER_CPIO} &&
tar xf /Sources/cpio-${VER_CPIO}.tar.bz2 &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_1}" \
../cpio-${VER_CPIO}/configure \
	--prefix=/usr \
	--enable-mt \
	--with-rmt=/usr/bin/rmt &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/cpio-${VER_CPIO} &&
cp -v ../cpio-${VER_CPIO}/{AUTHORS,COPYING,ChangeLog,NEWS,README,THANKS,TODO} \
	/usr/share/doc/cpio-${VER_CPIO} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/cpio ] ; then
	echo "ERROR: cpio is not installed"
	exit -1
fi

/usr/bin/cpio --version | grep "^cpio (GNU cpio) ${VER_CPIO}$"
if [ $? != 0 ] ; then
	echo "ERROR: cpio is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd .. &&
rm -rf build &&
rm -rf cpio-${VER_CPIO} &&
set +x +u
