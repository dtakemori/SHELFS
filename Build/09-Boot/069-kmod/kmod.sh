#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/kmod-${VER_KMOD}.tar.xz ] || exit -1
rm -rf kmod-${VER_KMOD} &&
tar xf /Sources/kmod-${VER_KMOD}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../kmod-${VER_KMOD}/configure \
	--prefix=/usr \
	--bindir=/bin \
	--libdir=/lib64 \
	--sysconfdir=/etc \
	--datarootdir=/usr/share \
	--disable-manpages \
	--with-openssl \
	--with-xz \
	--with-zlib \
	--with-zstd &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
for i in depmod insmod lsmod modinfo modprobe rmmod;
  do
    rm -fv /sbin/$i;
    ln -sv ../bin/kmod /sbin/$i;
done

install -v -d -m 755 /usr/share/doc/kmod-${VER_KMOD} &&
cp -v ../kmod-${VER_KMOD}/{COPYING,NEWS,README.md} \
	/usr/share/doc/kmod-${VER_KMOD} &&

rm -v /lib64/libkmod.la &&

###################################################################### VALIDATE
if [ ! -x /bin/kmod ] ; then
	echo "ERROR: kmod is not installed"
	exit -1
fi

/bin/kmod --version | grep "^kmod version ${VER_KMOD}$"
if [ $? != 0 ] ; then
	echo "ERROR: kmod is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf kmod-${VER_KMOD} &&
set +x +u
