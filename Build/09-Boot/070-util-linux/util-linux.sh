#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/util-linux-${VER_UTILLINUX}.tar.xz ] || exit -1
rm -rf util-linux-${VER_UTILLINUX} &&
tar xf /Sources/util-linux-${VER_UTILLINUX}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
../util-linux-${VER_UTILLINUX}/configure \
	ADJTIME_PATH=/var/lib/hwclock/adjtime \
	--libdir=/lib64 \
	--disable-chnfn-chsh \
	--disable-liblastlog2 \
	--disable-logger \
	--disable-login \
	--disable-nologin \
	--disable-pylibmount \
	--disable-runuser \
	--disable-setpriv \
	--disable-su \
	--disable-raw \
	--disable-static \
	--without-python \
	--without-systemd \
	--without-systemdsystemunitdir \
	--docdir=/usr/share/doc/util-linux-${VER_UTILLINUX} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
### Don't run util-linux-ng tests - will attempt to unmount the running filesys

####################################################################### INSTALL
mkdir -pv /var/lib/hwclock &&
make pkgconfigdir=/usr/lib64/pkgconfig install &&
mv -v /usr/bin/logger /bin/logger &&

chmod -v -s /bin/mount &&
setcap CAP_DAC_OVERRIDE,CAP_SYS_ADMIN=ep /bin/mount &&
chmod -v -s /bin/umount &&
setcap CAP_DAC_OVERRIDE,CAP_SYS_ADMIN=ep /bin/umount &&

cp -v ../util-linux-${VER_UTILLINUX}/{AUTHORS,COPYING,ChangeLog,NEWS,README,README.licensing} \
	/usr/share/doc/util-linux-${VER_UTILLINUX} &&
cp -vr ../util-linux-${VER_UTILLINUX}/Documentation \
	/usr/share/doc/util-linux-${VER_UTILLINUX} &&

cp -v /Build/Config/pam.d/{su,su-l} /etc/pam.d/ &&

rm -v /usr/lib64/lib{blkid,fdisk,mount,smartcols,uuid}.la &&

###################################################################### VALIDATE
if [ ! -x /bin/mount ] ; then
	echo "ERROR: mount is not installed"
	exit -1
fi

/bin/mount --version | grep "^mount from util-linux ${VER_UTILLINUX}"
if [ $? != 0 ] ; then
	echo "ERROR: mount is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf util-linux-${VER_UTILLINUX} &&
set +x +u
