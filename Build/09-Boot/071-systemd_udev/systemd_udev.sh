#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/systemd-${VER_SYSTEMD}.tar.gz ] || exit -1
rm -rf systemd-${VER_SYSTEMD} &&
tar xf /Sources/systemd-${VER_SYSTEMD}.tar.gz &&

######################################################################### PATCH
cd systemd-${VER_SYSTEMD} &&
sed -i -e 's/GROUP="render"/GROUP="video"/' \
       -e 's/GROUP="sgx", //' rules.d/50-udev-default.rules.in &&

sed '/systemd-sysctl/s/^/#/' -i rules.d/99-systemd.rules.in &&

sed '/NETWORK_DIRS/s/systemd/udev/' -i src/basic/path-lookup.h &&

patch -p1 -i /Patches/systemd-255_split_usr.patch &&
patch -p1 -i /Patches/systemd-255_superblock-ids.patch &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
meson setup \
	--prefix=/ \
	--buildtype=release \
	-Dmode=release \
	-Ddev-kvm-mode=0660 \
	-Dlink-udev-shared=false \
	-Dlogind=false \
	-Dvconsole=false \
	../systemd-${VER_SYSTEMD} 

######################################################################### BUILD
UDEV_HELPERS=$(grep "'name' :" ../systemd-${VER_SYSTEMD}/src/udev/meson.build | \
	awk '{ print $3 }' | tr -d ",'" | grep -v 'udevadm')

ninja udevadm systemd-hwdb \
	$(ninja -n | grep -Eo '(src/(lib)?udev|rules.d|hwdb.d)[^ ]*') \
	$(realpath libudev.so --relative-to .) \
	$UDEV_HELPERS &&

########################################################################## TEST

####################################################################### INSTALL
install -vm755 -d {/lib,/etc}/udev/{hwdb.d,rules.d,network} &&

install -vm755 udevadm      /sbin/ &&
install -vm755 systemd-hwdb /bin/udev-hwdb &&
ln -svfn udevadm            /sbin/udevd &&

cp -av libudev.so{,*[0-9]}  /lib64/ &&

install -vm644 ../systemd-${VER_SYSTEMD}/src/libudev/libudev.h /usr/include/ &&

install -vm644 src/libudev/*.pc /usr/lib64/pkgconfig/ &&
install -vm644 src/udev/*.pc    /usr/lib64/pkgconfig/ &&

install -vm644 ../systemd-${VER_SYSTEMD}/src/udev/udev.conf /etc/udev &&

install -vm644 rules.d/* ../systemd-${VER_SYSTEMD}/rules.d/README \
               /lib/udev/rules.d/ &&
install -vm644 $(find ../systemd-${VER_SYSTEMD}/rules.d/*.rules \
                 -not -name '*power-switch*') \
               /lib/udev/rules.d/ &&
install -vm644 hwdb.d/* ../systemd-${VER_SYSTEMD}/hwdb.d/{*.hwdb,README} \
               /lib/udev/hwdb.d/ &&

install -vm755 $UDEV_HELPERS /lib/udev &&

install -vm644 ../systemd-${VER_SYSTEMD}/network/99-default.link /lib/udev/network &&


tar xvf /Sources/udev-lfs-${VER_UDEVLFS}.tar.xz &&
sed -i 's|/usr/lib/|/lib/|' udev-lfs-${VER_UDEVLFS}/Makefile.lfs &&
make -f udev-lfs-${VER_UDEVLFS}/Makefile.lfs install &&

tar xf /Sources/systemd-man-pages-${VER_SYSTEMD}.tar.xz \
	--no-same-owner --strip-components=1 \
	-C /usr/share/man --wildcards '*/udev*' '*/libudev*' \
	                              '*/systemd-'{hwdb,udevd.service}.8 &&
sed 's/systemd\(\\\?-\)/udev\1/' /usr/share/man/man8/systemd-hwdb.8 \
                               > /usr/share/man/man8/udev-hwdb.8 &&
sed 's|lib.*udevd|sbin/udevd|' \
    /usr/share/man/man8/systemd-udevd.service.8 \
  > /usr/share/man/man8/udevd.8 &&
rm -v /usr/share/man/man*/systemd* &&

install -v -d -m 755 /usr/share/doc/udevd-${VER_SYSTEMD} &&
cp -rv ../systemd-${VER_SYSTEMD}/{LICENSE*,NEWS,README,README.md,TODO} \
	/usr/share/doc/udevd-${VER_SYSTEMD} &&

udev-hwdb update &&

###################################################################### VALIDATE
if [ ! -x /sbin/udevd ] ; then
	echo "ERROR: udevd is not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf systemd-${VER_SYSTEMD} &&
set +x +u
