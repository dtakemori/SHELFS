#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/perl-${VER_PERL}.tar.xz ] || exit -1
rm -rf perl-${VER_PERL} &&
tar xf /Sources/perl-${VER_PERL}.tar.xz &&

######################################################################### PATCH
cd perl-${VER_PERL} &&

sed -i 's/-fstack-protector/&-all/' Configure &&

cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

../perl-${VER_PERL}/Configure -des \
	-Dprefix=/usr \
	-Dusrinc="/usr/include" \
	-Dlibc="/lib64/libc.so.6" \
	-Dprivlib="/usr/lib64/perl5/${VER_PERL}" \
	-Darchlib="/usr/lib64/perl5/${VER_PERL}/$(uname -m)-linux-thread-multi" \
	-Dsitearch="/usr/lib64/perl5/site_perl/${VER_PERL}/$(uname -m)-linux-thread-multi " \
	-Dsitelib="/usr/lib64/perl5/site_perl/${VER_PERL}" \
	-Dvendorarch="/usr/lib64/perl5/vendor_perl/${VER_PERL}/$(uname -m)-linux-thread-multi" \
	-Dvendorlib="/usr/lib64/perl5/vendor_perl/${VER_PERL}" \
	-Doptimize="${COPTS_1} -flto" \
	-Dldflags="${LDOPTS_1}" \
	-Dpager="/usr/bin/less -isR" \
	-Dman1dir=/usr/share/man/man1 \
	-Dman3dir=/usr/share/man/man3 \
	-Duseshrplib \
	-Dusethreads \
	-Dmksymlinks

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ;  then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
if [ -h /usr/bin/perl ] ; then rm -fv /usr/bin/perl; fi &&
make install &&

install -v -d -m 755 /usr/share/doc/perl-${VER_PERL} &&
cp -v ../perl-${VER_PERL}/{AUTHORS,Artistic,CODE_OF_CONDUCT.md,Changes} \
	../perl-${VER_PERL}/{Copying,PACKAGING,README*,SECURITY.md} \
	/usr/share/doc/perl-${VER_PERL} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/perl ] ; then
	echo "ERROR: perl is not installed"
	exit -1
fi

/usr/bin/perl --version | grep "^This is perl 5, .* (v${VER_PERL})"
if [ $? != 0 ] ; then
	echo "ERROR: perl is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.sh ../config.log &&
cd .. &&
rm -rf build &&
rm -rf perl-${VER_PERL} &&
set +x +u
