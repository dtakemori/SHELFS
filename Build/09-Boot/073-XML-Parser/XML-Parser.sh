#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/XML-Parser-${VER_PERL_XMLPARSER}.tar.gz ] || exit -1
rm -rf XML-Parser-${VER_PERL_XMLPARSER} &&
tar xf /Sources/XML-Parser-${VER_PERL_XMLPARSER}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd XML-Parser-${VER_PERL_XMLPARSER}/
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -k test > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/XML-Parser-${VER_PERL_XMLPARSER} &&
cp -v Changes README \
	/usr/share/doc/XML-Parser-${VER_PERL_XMLPARSER} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/$(uname -m)-linux-thread-multi/XML/Parser.pm ] ; then
	echo "ERROR: XML::Parser not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf XML-Parser-${VER_PERL_XMLPARSER} &&

set +x +u
