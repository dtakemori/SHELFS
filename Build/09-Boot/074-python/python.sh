#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/Python-${VER_PYTHON}.tar.xz ] || exit -1
rm -rf Python-${VER_PYTHON} &&
tar xf /Sources/Python-${VER_PYTHON}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1}" \
LDFLAGS="${LDOPTS_1}" \
../Python-${VER_PYTHON}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--enable-optimizations \
	--enable-shared \
	--with-platlibdir=lib64 \
	--with-system-expat \
	--with-system-ffi \
	--with-ensurepip=yes &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make install &&

mkdir -v /usr/share/doc/python-${VER_PYTHON} &&
cp -v ../Python-${VER_PYTHON}/{LICENSE,README.rst} \
	/usr/share/doc/python-${VER_PYTHON} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/python3 ] ; then
	echo "ERROR: python is not installed"
	exit -1
fi

/usr/bin/python3 --version 2>&1 | grep "^Python ${VER_PYTHON}"
if [ $? != 0 ] ; then
	echo "ERROR: python is wrong version"
	exit -1
fi

##################################################################### CONFIGURE

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf Python-${VER_PYTHON} &&
set +x +u
