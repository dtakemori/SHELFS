#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/wheel-${VER_WHEEL}.tar.gz ] || exit -1
rm -rf wheel-${VER_WHEEL} &&
tar xf /Sources/wheel-${VER_WHEEL}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd wheel-${VER_WHEEL} &&
pip3 wheel -w dist --no-cache-dir --no-build-isolation --no-deps $PWD &&

########################################################################## TEST

####################################################################### INSTALL
pip3 install --no-index --find-links=dist wheel &&

install -v -d -m 755 /usr/share/doc/wheel-${VER_WHEEL} &&
cp -v LICENSE.txt README.rst \
	/usr/share/doc/wheel-${VER_WHEEL} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/wheel ] ; then
	echo "ERROR: wheel is not installed"
	exit -1
fi

/usr/bin/wheel version 2>&1 | grep "^wheel ${VER_WHEEL}"
if [ $? != 0 ] ; then
	echo "ERROR: wheel is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf wheel-${VER_WHEEL} &&
set +x +u
