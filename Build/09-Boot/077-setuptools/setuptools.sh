#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/setuptools-${VER_SETUPTOOLS}.tar.gz ] || exit -1
rm -rf setuptools-${VER_SETUPTOOLS} &&
tar xf /Sources/setuptools-${VER_SETUPTOOLS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd setuptools-${VER_SETUPTOOLS} &&
pip3 wheel -w dist --no-cache-dir --no-build-isolation --no-deps $PWD &&

########################################################################## TEST

####################################################################### INSTALL
pip3 install --no-index --find-links=dist setuptools &&

install -v -d -m 755 /usr/share/doc/setuptools-${VER_SETUPTOOLS} &&
cp -v LICENSE NEWS.rst README.rst \
	/usr/share/doc/setuptools-${VER_SETUPTOOLS} &&

###################################################################### VALIDATE

####################################################################### CLEANUP
cd ../ &&
rm -rf setuptools-${VER_SETUPTOOLS} &&
set +x +u
