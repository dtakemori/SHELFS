#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/meson-${VER_MESON}.tar.gz ] || exit -1
rm -rf meson-${VER_MESON} &&
tar xf /Sources/meson-${VER_MESON}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd meson-${VER_MESON} &&
pip3 wheel -w dist --no-cache-dir --no-build-isolation --no-deps $PWD &&

########################################################################## TEST

####################################################################### INSTALL
pip3 install --no-index --find-links=dist meson &&

install -v -d -m 755 /usr/share/doc/meson-${VER_MESON} &&
cp -v COPYING README.md \
	/usr/share/doc/meson-${VER_MESON} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/meson ] ; then
	echo "ERROR: meson is not installed"
	exit -1
fi

/usr/bin/meson --version 2>&1 | grep "^${VER_MESON}$"
if [ $? != 0 ] ; then
	echo "ERROR: meson is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf meson-${VER_MESON} &&
set +x +u
