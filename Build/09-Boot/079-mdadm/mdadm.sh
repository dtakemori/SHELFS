#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/mdadm-${VER_MDADM}.tar.xz ] || exit -1
rm -rf mdadm-${VER_MDADM} &&
tar xf /Sources/mdadm-${VER_MDADM}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd mdadm-${VER_MDADM}/ &&
make -j${MAKEJOBS:-2} \
	CC="gcc ${COPTS_1}" \
        LDFLAGS="${LDOPTS_1}" &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/mdadm-${VER_MDADM} &&
cp -v ANNOUNCE* COPYING ChangeLog README.initramfs TODO \
	/usr/share/doc/mdadm-${VER_MDADM} &&

###################################################################### VALIDATE
if [ ! -x /sbin/mdadm ] ; then
	echo "ERROR: mdadm is not installed"
	exit -1
fi

/sbin/mdadm --version 2>&1 | grep -E "^mdadm - v${VER_MDADM} "
if [ $? != 0 ] ; then
	echo "ERROR: mdadm is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf mdadm-${VER_MDADM} &&
set +x +u
