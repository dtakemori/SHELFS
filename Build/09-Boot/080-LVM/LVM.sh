#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/LVM2.${VER_LVM}.tgz ] || exit -1
rm -rf LVM2.${VER_LVM} &&
tar xf /Sources/LVM2.${VER_LVM}.tgz &&

######################################################################### PATCH

########################################################################## PREP
cd LVM2.${VER_LVM}/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
./configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--sbindir=/sbin \
	--enable-cmdline \
	--enable-pkgconfig \
	--enable-udev_sync \
	--with-udev-prefix=/ &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&

rm -fv /lib/udev/rules.d/69-dm-lvm.rules &&
rm -fv /usr/lib/libdevmapper.so &&
ln -sv libdevmapper.so.1.02 /lib64/libdevmapper.so &&

install -v -d -m 755 /usr/share/doc/LVM2-${VER_LVM} &&
cp -v COPYING* README TESTING VERSION* WHATS_NEW* \
	/usr/share/doc/LVM2-${VER_LVM} &&

###################################################################### VALIDATE
if [ ! -x /sbin/lvm ] ; then
	echo "ERROR: lvm is not installed"
	exit -1
fi

lvm version | grep -E "^ *LVM version: *${VER_LVM}"
if [ $? != 0 ]; then
	echo "ERROR: lvm is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf install &&
rm -rf LVM2.${VER_LVM} &&
set +x +u
