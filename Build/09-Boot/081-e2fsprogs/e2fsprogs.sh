#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/e2fsprogs-${VER_E2FSPROGS}.tar.gz ] || exit -1
rm -rf e2fsprogs-${VER_E2FSPROGS} &&
tar xf /Sources/e2fsprogs-${VER_E2FSPROGS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

### Use ifsck, uuidd and lib{blk,uuid} from util-linux-ng
CFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_1}" \
../e2fsprogs-${VER_E2FSPROGS}/configure \
	--prefix=/usr \
	--libdir=/lib64 \
	--sysconfdir=/etc \
	--with-root-prefix="" \
	--enable-elf-shlibs \
	--disable-libblkid \
	--disable-libuuid \
	--disable-uuidd \
	--disable-fsck &&

######################################################################### BUILD
make -j1 &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
make install-libs &&
ldconfig &&

gunzip -v /usr/share/info/libext2fs.info.gz &&
install-info --dir-file=/usr/share/info/dir /usr/share/info/libext2fs.info &&

makeinfo -o doc/com_err.info ../e2fsprogs-${VER_E2FSPROGS}/lib/et/com_err.texinfo &&
install -v -m 644 doc/com_err.info /usr/share/info &&
install-info --dir-file=/usr/share/info/dir /usr/share/info/com_err.info &&

install -v -d -m 755 /usr/share/doc/e2fsprogs-${VER_E2FSPROGS} &&
cp ../e2fsprogs-${VER_E2FSPROGS}/{NOTICE,README,RELEASE-NOTES,SHLIBS} \
	/usr/share/doc/e2fsprogs-${VER_E2FSPROGS} &&

### Makefile does not honor setting pkgconfigdir
mv -v /lib64/pkgconfig/{com_err,e2p,ext2fs,ss}.pc \
	/usr/lib64/pkgconfig &&
rmdir -v /lib64/pkgconfig &&

rm -v /lib64/lib{com_err,e2p,ext2fs,ss}.a &&

###################################################################### VALIDATE
if [ ! -x /sbin/e2fsck ] ; then
	echo "ERROR: e2fsck is not installed"
	exit -1
fi

/sbin/e2fsck -V 2>&1 | grep "^e2fsck ${VER_E2FSPROGS}"
if [ $? != 0 ]; then
	echo "ERROR: e2fsck is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf e2fsprogs-${VER_E2FSPROGS} &&
set +x +u
