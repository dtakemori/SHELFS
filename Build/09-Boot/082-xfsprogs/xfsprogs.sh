#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/xfsprogs-${VER_XFSPROGS}.tar.xz ] || exit -1
rm -rf xfsprogs-${VER_XFSPROGS} &&
tar xf /Sources/xfsprogs-${VER_XFSPROGS}.tar.xz &&

[ -f /Sources/inih-${VER_INIH}.tar.gz ] || exit -1
rm -rf inih-${VER_INIH} &&
tar xf /Sources/inih-${VER_INIH}.tar.gz &&

[ -f /Sources/userspace-rcu-${VER_USERSPACE_RCU}.tar.bz2 ] || exit -1
rm -rf userspace-rcu-${VER_USERSPACE_RCU} &&
tar xf /Sources/userspace-rcu-${VER_USERSPACE_RCU}.tar.bz2 &&

######################################################################### PATCH
cd xfsprogs-${VER_XFSPROGS}/ &&
sed -i 's/icu-i18n/icu-uc &/' configure &&
cd ../ &&

########################################################################## PREP
cd inih-${VER_INIH} &&

mkdir -v build &&
cd build/ &&
PATH="${PATH}:/cross-tools/bin" meson .. \
	--prefix=$(pwd)/../../install \
	-Ddefault_library=static \
	-Doptimization=s \
	-Db_lto=true \
	-Dc_args="${COPTS_1} -flto ${LDOPTS_1}" \
	-Dcpp_args="${CXXOPTS_1} -flto"

######################################################################### BUILD
ninja -v -j ${MAKEJOBS} &&

########################################################################## TEST

####################################################################### INSTALL
ninja install &&

install -v -d -m 755 /usr/share/doc/inih-${VER_INIH} &&
cp -v ../{LICENSE.txt,README.md} \
	/usr/share/doc/inih-${VER_INIH} &&

cd ../../ &&

######################################################################### PATCH

########################################################################## PREP
cd userspace-rcu-${VER_USERSPACE_RCU} &&

mkdir -v build &&
cd build/ &&
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../configure --prefix=$(pwd)/../../install \
	--libdir=$(pwd)/../../install/lib64 \
	--enable-static \
	--disable-shared &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -v make -j${MAKEJOBS:-2} -k check
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/userspace-rcu-${VER_USERSPACE_RCU} &&
cp -v ../{ChangeLog,LICENSE.md,README.md} \
	/usr/share/doc/userspace-rcu-${VER_USERSPACE_RCU} &&

cd ../../ &&

########################################################################## PREP

######################################################################### BUILD
cd xfsprogs-${VER_XFSPROGS}/ &&

CFLAGS="${COPTS_1} -flto -I$(pwd)/../install/include" \
LDFLAGS="${LDOPTS_1} -L$(pwd)/../install/lib64" \
make DEBUG=-DNDEBUG \
	INSTALL_USER=root \
	INSTALL_GROUP=root \
        LOCAL_CONFIGURE_OPTIONS="--enable-lto=yes" \
	-j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make PKG_DOC_DIR=/usr/share/doc/xfsprogs-${VER_XFSPROGS} install &&
make PKG_DOC_DIR=/usr/share/doc/xfsprogs-${VER_XFSPROGS} install-dev &&

cp -rv {LICENSES,VERSION} /usr/share/doc/xfsprogs-${VER_XFSPROGS} &&

rm -v /lib64/libhandle.{a,la} &&
rm -v /usr/lib64/libhandle.{a,la} &&

###################################################################### VALIDATE
if [ ! -x /usr/sbin/xfs_admin ] ; then
	echo "ERROR: xfs_admin is not installed"
	exit -1
fi

/usr/sbin/xfs_admin -V | grep "^xfs_admin version ${VER_XFSPROGS}$"
if [ $? != 0 ]; then
	echo "ERROR: xfs_admin is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf install &&
rm -rf inih-${VER_INIH} &&
rm -rf xfsprogs-${VER_XFSPROGS} &&
rm -rf userspace-rcu-${VER_USERSPACE_RCU} &&
set +x +u
