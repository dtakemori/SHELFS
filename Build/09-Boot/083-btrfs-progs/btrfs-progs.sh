#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/btrfs-progs-v${VER_BTRFSPROGS}.tar.xz ] || exit -1
rm -rf btrfs-progs-v${VER_BTRFSPROGS} &&
tar xf /Sources/btrfs-progs-v${VER_BTRFSPROGS}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
cd btrfs-progs-v${VER_BTRFSPROGS}/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
./configure \
	--prefix=/usr \
	--bindir=/sbin \
	--libdir=/lib64 \
	--disable-documentation \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&

for i in 5 8; do
  install -v -m 644 Documentation/*.${i} /usr/share/man/man${i}/ 
done

install -v -d -m 755 /usr/share/doc/btrfs-progs-v${VER_BTRFSPROGS} &&
cp -v CHANGES COPYING README.md VERSION \
	/usr/share/doc/btrfs-progs-v${VER_BTRFSPROGS} &&

###################################################################### VALIDATE
if [ ! -x /sbin/btrfsck ] ; then
	echo "ERROR: btrfsck is not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf btrfs-progs-v${VER_BTRFSPROGS} &&
set +x +u
