#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/dosfstools-${VER_DOSFSTOOLS}.tar.gz ] || exit -1
rm -rf dosfstools-${VER_DOSFSTOOLS} &&
tar xf /Sources/dosfstools-${VER_DOSFSTOOLS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../dosfstools-${VER_DOSFSTOOLS}/configure --prefix=/ \
	--enable-compat-symlinks \
	--docdir=/usr/share/doc/dosfstools-${VER_DOSFSTOOLS} \
	--mandir=/usr/share/man &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /sbin/mkfs.fat ] ; then
	echo "ERROR: mkfs.fat is not installed"
	exit -1
fi

/sbin/mkfs.fat 2>/dev/null | grep "^mkfs.fat ${VER_DOSFSTOOLS}"
if [ $? != 0 ]; then
	echo "ERROR: mkfs.fat library is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf dosfstools-${VER_DOSFSTOOLS} &&
set +x +u
