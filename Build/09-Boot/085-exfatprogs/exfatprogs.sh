#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/exfatprogs-${VER_EXFATPROGS}.tar.xz ] || exit -1
rm -rf exfatprogs-${VER_EXFATPROGS} &&
tar xf /Sources/exfatprogs-${VER_EXFATPROGS}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../exfatprogs-${VER_EXFATPROGS}/configure --prefix=/ \
	--mandir=/usr/share/man &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/exfatprogs-${VER_EXFATPROGS} &&
cp -v ../exfatprogs-${VER_EXFATPROGS}/{COPYING,NEWS,README.md} \
	/usr/share/doc/exfatprogs-${VER_EXFATPROGS} &&

###################################################################### VALIDATE
if [ ! -x /sbin/mkfs.exfat ] ; then
	echo "ERROR: mkfs.exfat is not installed"
	exit -1
fi

/sbin/fsck.exfat -V | grep "^exfatprogs version : ${VER_EXFATPROGS}"
if [ $? != 0 ]; then
	echo "ERROR: fsck.exfat is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf exfatprogs-${VER_EXFATPROGS} &&
set +x +u
