#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/json-c-${VER_JSONC}.tar.gz ] || exit -1
rm -rf json-c-${VER_JSONC} &&
tar xf /Sources/json-c-${VER_JSONC}.tar.gz &&

[ -f /Sources/cryptsetup-${VER_CRYPTSETUP}.tar.xz ] || exit -1
rm -rf cryptsetup-${VER_CRYPTSETUP} &&
tar xf /Sources/cryptsetup-${VER_CRYPTSETUP}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
cmake -DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_INSTALL_PREFIX=$(pwd)/../install \
	-DBUILD_SHARED_LIBS=OFF \
	-DBUILD_STATIC_LIBS=ON \
	../json-c-${VER_JSONC} &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make install &&

install -dv -m755 /usr/share/doc/json-c-${VER_JSONC} &&
cp -v ../json-c-${VER_JSONC}/doc/html/* \
	/usr/share/doc/json-c-${VER_JSONC} &&

cd ../ &&
rm -rf build/ &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto -I$(pwd)/../install/include" \
LDFLAGS="${LDOPTS_1} -L$(pwd)/../install/lib64" \
PKG_CONFIG_PATH="$(pwd)/../install/lib64/pkgconfig" \
../cryptsetup-${VER_CRYPTSETUP}/configure --prefix=/ \
	--sbindir=/sbin \
	--libdir=/lib64 \
	--includedir=/usr/include \
	--mandir=/usr/share/man \
	--disable-ssh-token \
	--disable-asciidoc &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
rmdir /lib64/cryptsetup &&
rm -v /lib64/libcryptsetup.la &&

# mv -v /lib64/pkgconfig/libcryptsetup.pc /usr/lib/pkgconfig/ &&
# rmdir -v /lib64/pkgconfig &&

install -dv -m755 /usr/share/doc/cryptsetup-${VER_CRYPTSETUP} &&
cp -rv ../cryptsetup-${VER_CRYPTSETUP}/{AUTHORS,*.md,COPYING*,docs} \
	/usr/share/doc/cryptsetup-${VER_CRYPTSETUP} &&

###################################################################### VALIDATE
if [ ! -x /sbin/cryptsetup ] ; then
	echo "ERROR: cryptsetup is not installed"
	exit -1
fi

/sbin/cryptsetup -V | grep "^cryptsetup ${VER_CRYPTSETUP}"
if [ $? != 0 ]; then
	echo "ERROR: cryptsetup is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf install &&
rm -rf json-c-${VER_JSONC} &&
rm -rf cryptsetup-${VER_CRYPTSETUP} &&
set +x +u
