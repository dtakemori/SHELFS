#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/intltool-${VER_INTLTOOL}.tar.gz ] || exit -1
rm -rf intltool-${VER_INTLTOOL} &&
tar xf /Sources/intltool-${VER_INTLTOOL}.tar.gz &&

######################################################################### PATCH
cd intltool-${VER_INTLTOOL}/ &&
sed -i 's:\\\${:\\\$\\{:' intltool-update.in &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../intltool-${VER_INTLTOOL}/configure \
	--prefix=/usr && 

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/intltool-${VER_INTLTOOL} &&
cp -v ../intltool-${VER_INTLTOOL}/{AUTHORS,COPYING,ChangeLog,NEWS,README,TODO} \
	../intltool-${VER_INTLTOOL}/doc/I18N-HOWTO \
	/usr/share/doc/intltool-${VER_INTLTOOL} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/intltoolize ] ; then
	echo "ERROR: intltoolize not installed"
	exit -1
fi

/usr/bin/intltoolize --version | grep "^intltoolize (GNU intltool) ${VER_INTLTOOL}$"
if [ $? != 0 ] ; then
	echo "ERROR: intltoolize incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf intltool-${VER_INTLTOOL} &&
set +x +u
