#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/linux-${VER_LINUX}.${VER_LINUX_PATCH}.tar.xz ] || exit -1
rm -rf linux-${VER_LINUX}.${VER_LINUX_PATCH} &&
tar xf /Sources/linux-${VER_LINUX}.${VER_LINUX_PATCH}.tar.xz &&

######################################################################### PATCH
cd linux-${VER_LINUX}.${VER_LINUX_PATCH} &&

### Our linker has default --fatal-warnings which breaks the old linker check
sed -i 's/cc-ldoption,/cc-ldoption, -Wl$(comma)--no-fatal-warnings/' Makefile &&

tar xf /Sources/aufs${VER_LINUX}.${VER_AUFS}.tar.xz &&
find aufs${VER_LINUX}.${VER_AUFS} -type f -name "\.*" | xargs rm -f &&

patch -p1 -i aufs${VER_LINUX}.${VER_AUFS}/aufs6-kbuild.patch &&
patch -p1 -i aufs${VER_LINUX}.${VER_AUFS}/aufs6-base.patch &&
patch -p1 -i aufs${VER_LINUX}.${VER_AUFS}/aufs6-standalone.patch &&
patch -p1 -i aufs${VER_LINUX}.${VER_AUFS}/aufs6-mmap.patch &&

cp -v aufs${VER_LINUX}.${VER_AUFS}/Documentation/ABI/testing/* \
	Documentation/ABI/testing/ &&
cp -rv aufs${VER_LINUX}.${VER_AUFS}/Documentation/filesystems/aufs \
	Documentation/filesystems/ &&
cp -rv aufs${VER_LINUX}.${VER_AUFS}/fs/aufs fs/ &&
cp -v aufs${VER_LINUX}.${VER_AUFS}/include/uapi/linux/aufs_type.h \
	include/uapi/linux &&
rm -rf aufs${VER_LINUX}.${VER_AUFS} &&

make mrproper &&

########################################################################## PREP

######################################################################### BUILD
cp /Build/Config/$(uname -m)/linux-config-${VER_LINUX}.${VER_LINUX_PATCH} \
	.config &&

make -j${MAKEJOBS:-2} \
	oldconfig &&

make -j${MAKEJOBS:-2} \
	scripts &&

make -j$(( ${MAKEJOBS:-2} + 1 )) &&

########################################################################## TEST

####################################################################### INSTALL
case $(uname -m) in
  x86_64)
    cp -v arch/x86/boot/bzImage \
      /boot/vmlinuz-${VER_LINUX}.${VER_LINUX_PATCH}
  ;;
  aarch64)
    cp -v arch/arm64/boot/Image.gz \
      /boot/vmlinuz-${VER_LINUX}.${VER_LINUX_PATCH}
  ;;
esac

make -j$(( ${MAKEJOBS:-2} + 1 )) \
	modules &&

make -j${MAKEJOBS:-2} \
	INSTALL_MOD_STRIP=1 \
	INSTALL_MOD_PATH=/ \
	modules_install &&

cp -v System.map /boot/System.map-${VER_LINUX}.${VER_LINUX_PATCH} &&
cp -v .config /boot/config-${VER_LINUX}.${VER_LINUX_PATCH} &&

cd ../ &&


if [ -h /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build ] ; then
  rm -fv /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build 
fi

if [ -h /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/source ] ; then
  rm -fv /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/source 
  ln -sv build /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/source 
fi

mkdir /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build &&

cp -v linux-${VER_LINUX}.${VER_LINUX_PATCH}/{.config,Module.symvers,System.map} \
	/lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build &&

tar -cf - $(find linux-${VER_LINUX}.${VER_LINUX_PATCH} -type f -name "Kconfig*") \
    | tar -xv -C /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build --strip-components=1 -f - &&

tar -cf - $(find linux-${VER_LINUX}.${VER_LINUX_PATCH} -type f -name "Makefile*") \
    | tar -xv -C /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build --strip-components=1 -f - &&


case $(uname -m) in
  x86_64)
    tar -cf - $(find linux-${VER_LINUX}.${VER_LINUX_PATCH}/arch/{x86,x86_64} -name "*.h") \
      | tar -xv -C /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build --strip-components=1 -f -
  ;;
  aarch64)
    tar -cf - $(find linux-${VER_LINUX}.${VER_LINUX_PATCH}/arch/{arm,arm64} -name "*.h") \
      | tar -xv -C /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build --strip-components=1 -f -
  ;;
esac

tar -c -f - $(find linux-${VER_LINUX}.${VER_LINUX_PATCH}/include -type f) \
    | tar -xv -C /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build --strip-components=1 -f - &&

tar -c -f - $(find linux-${VER_LINUX}.${VER_LINUX_PATCH}/scripts -type f) \
    | tar -xv -C /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build --strip-components=1 -f - &&

if [ $(uname -m) == 'x86_64' ] ; then
  cp -v linux-${VER_LINUX}.${VER_LINUX_PATCH}/tools/objtool/objtool \
    /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/tools/objtool/
fi

cp -rv linux-${VER_LINUX}.${VER_LINUX_PATCH}/Documentation \
	-T /usr/share/doc/linux-${VER_LINUX}.${VER_LINUX_PATCH} &&

####################################################################### CLEANUP
rm -rf linux-${VER_LINUX}.${VER_LINUX_PATCH} &&
set +x +u
