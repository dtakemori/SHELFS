#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/zfs-${VER_ZFS}.tar.gz ] || exit -1
rm -rf zfs-${VER_ZFS} &&
tar xf /Sources/zfs-${VER_ZFS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd zfs-${VER_ZFS} &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
./configure \
	--prefix=/usr \
	--bindir=/bin \
	--sbindir=/sbin \
	--libdir=/lib64 \
	--sysconfdir=/etc \
	--disable-static \
	--with-linux=/lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&

install -v -d -m 755 /usr/share/doc/zfs-${VER_ZFS} &&
cp -v ../zfs-${VER_ZFS}/{AUTHORS,CODE_OF_CONDUCT.md,COPYRIGHT,LICENSE,NEWS} \
      ../zfs-${VER_ZFS}/{NOTICE,README.md,RELEASES.md} \
	/usr/share/doc/zfs-${VER_ZFS} &&

mkdir -v /etc/rc.d &&
mv -v /etc/init.d/ /etc/rc.d/ &&

rm -f /lib64/lib{nvpair,uutil,zfs,zfs_core,zfsbootenv,libzpool}.la &&

###################################################################### VALIDATE
if [ ! -x /sbin/zfs ] ; then
	echo "ERROR: zfs is not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf zfs-${VER_ZFS} &&
set +x +u
