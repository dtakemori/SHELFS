#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/dracut-ng-${VER_DRACUTNG}.tar.gz ] || exit -1
rm -rf dracut-ng-${VER_DRACUTNG} &&
tar xf /Sources/dracut-ng-${VER_DRACUTNG}.tar.gz &&

######################################################################### PATCH
cd dracut-ng-${VER_DRACUTNG} &&
sed -i 's/^enable_documentation=yes/enable_documentation=no/' configure &&
cd ../ &&

########################################################################## PREP
cd dracut-ng-${VER_DRACUTNG} &&
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
./configure &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} \
	DRACUT_MAIN_VERSION=${VER_DRACUTNG} \
	DRACUT_FULL_VERSION=${VER_DRACUTNG} &&

########################################################################## TEST

####################################################################### INSTALL
make sbindir=/sbin sysconfdir=/etc pkgconfigdatadir=/usr/lib64/pkgconfig \
	DRACUT_MAIN_VERSION=${VER_DRACUTNG} \
	DRACUT_FULL_VERSION=${VER_DRACUTNG} \
	install &&

install -v -d -m 755 /usr/share/doc/dracut-${VER_DRACUTNG} &&
cp -v AUTHORS CONTRIBUTING.md COPYING NEWS.md README.md \
	/usr/share/doc/dracut-${VER_DRACUTNG} &&

echo 'compress="zstd -19 -T0"' > /etc/dracut.conf.d/compress.conf &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/dracut ] ; then
	echo "ERROR: dracut not installed"
	exit -1
fi

/usr/bin/dracut -h | grep "^Version: ${VER_DRACUTNG}$"
if [ $? != 0 ]; then
	echo "ERROR: dracut is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf dracut-ng-${VER_DRACUTNG} &&
set +x +u
