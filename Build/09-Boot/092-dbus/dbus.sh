#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/dbus-${VER_DBUS}.tar.xz ] || exit -1
rm -rf dbus-${VER_DBUS} &&
tar xf /Sources/dbus-${VER_DBUS}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
groupadd -g 18 messagebus &&
useradd -u 18 -g messagebus -s /bin/false -c 'D-Bus Message Daemon' \
	-d /run/dbus -M -r messagebus &&

mkdir -v build/ &&
cd build/ &&

meson setup --prefix=/usr \
	--bindir=/bin \
	--libdir=/lib64 \
	--libexecdir=/usr/libexec/dbus-1 \
	--localstatedir=/var \
	--sysconfdir=/etc \
	--buildtype=release \
	--wrap-mode=nofallback \
	-D systemd=disabled \
	-D c_args="${COPTS_H} -flto ${LDOPTS_1}" \
	-D b_lto=true \
	../dbus-${VER_DBUS} &&


######################################################################### BUILD
ninja -v -j ${MAKEJOBS} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log ninja test > ../test.log
fi

####################################################################### INSTALL
ninja install 

mv -v {,/usr}/lib64/pkgconfig/dbus-1.pc &&
rmdir -v /lib64/pkgconfig &&

mv -v {,/usr}/lib64/cmake/DBus1 &&
rmdir -v /lib64/cmake &&

mv -v /usr/share/doc/dbus{,-${VER_DBUS}} &&
cp -v ../dbus-${VER_DBUS}/{AUTHORS,CONTRIBUTING.md,COPYING,NEWS*,README*} \
	/usr/share/doc/dbus-${VER_DBUS} &&

dbus-uuidgen --ensure &&

###################################################################### VALIDATE
if [ ! -x /bin/dbus-launch ] ; then
	echo "ERROR: dbus-launch not installed"
	exit -1
fi

/bin/dbus-launch --version | grep "^D-Bus Message Bus Launcher ${VER_DBUS}$"
if [ $? != 0 ] ; then
	echo "ERROR: dbus-launch incorrect version"
	exit -1
fi

cd ../ &&

##################################################################### CONFIGURE
rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

tar xf /Sources/blfs-bootscripts-${VER_BLFSBOOTSCRIPTS}.tar.xz &&

cd blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&
sed -i 's|/usr/bin|/bin|' blfs/init.d/dbus &&
make install-dbus &&

cd ../ &&
rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

####################################################################### CLEANUP

rm -rf build &&
rm -rf dbus-${VER_DBUS} &&
set +x +u
