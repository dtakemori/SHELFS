#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/dhcpcd-${VER_DHCPCD}.tar.xz ] || exit -1
rm -rf dhcpcd-${VER_DHCPCD} &&
tar xf /Sources/dhcpcd-${VER_DHCPCD}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
groupadd -g 52 dhcpcd &&
useradd -u 52 -g dhcpcd -s /bin/false -d /var/lib/dhcpcd -M -r dhcpcd &&
install -v -m700 -d /var/lib/dhcpcd &&
chown -v dhcpcd:dhcpcd /var/lib/dhcpcd &&

cd dhcpcd-${VER_DHCPCD} &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
./configure \
	--prefix=/usr \
	--runstatedir=/run \
	--sbindir=/sbin \
	--sysconfdir=/etc \
	--libexecdir=/lib/dhcpcd \
	--dbdir=/var/lib/dhcpcd \
	--privsepuser=dhcpcd &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/dhcpcd-${VER_DHCPCD} &&
cp -v BUILDING.md LICENSE README.md \
	/usr/share/doc/dhcpcd-${VER_DHCPCD} &&

###################################################################### VALIDATE
if [ ! -x /sbin/dhcpcd ] ; then
	echo "ERROR: dhcpcd is not installed"
	exit -1
fi

/sbin/dhcpcd --version | grep "^dhcpcd ${VER_DHCPCD}$"
if [ $? != 0 ] ; then
	echo "ERROR: dhcpcd incorrect version"
	exit -1
fi

##################################################################### CONFIGURE
rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

tar xf /Sources/blfs-bootscripts-${VER_BLFSBOOTSCRIPTS}.tar.xz &&
cd blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&
make install-service-dhcpcd &&

cd ../ &&
rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf dhcpcd-${VER_DHCPCD} &&
set +x +u
