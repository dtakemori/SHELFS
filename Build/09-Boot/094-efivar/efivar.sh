#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/efivar-${VER_EFIVAR}.tar.gz ] || exit -1
rm -rf efivar-${VER_EFIVAR} &&
tar xf /Sources/efivar-${VER_EFIVAR}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd efivar-${VER_EFIVAR} &&

if [ $(uname -m) == "x86_64" ] ; then
  CFLAGS="${COPTS_1}" \
  LDFLAGS="${LDOPTS_1}" \
  make -j${MAKEJOBS:-2} \
	ENABLE_DOCS=0 \
	ERRORS= \
	libdir=/lib64 src
elif [ $(uname -m) == "aarch64" ] ; then
  CFLAGS="${COPTS_1} -mbranch-protection=none" \
  LDFLAGS="${LDOPTS_1}" \
  make -j${MAKEJOBS:-2} \
	ENABLE_DOCS=0 \
	ERRORS= \
	libdir=/lib64 src
fi

########################################################################## TEST

####################################################################### INSTALL
CFLAGS="${COPTS_1}" \
LDFLAGS="${LDOPTS_1}" \
make ERRORS= \
	ENABLE_DOCS=0 \
	PCDIR=/usr/lib64/pkgconfig \
	libdir=/lib64 install &&

install -v -d -m 755 /usr/share/doc/efivar-${VER_EFIVAR} &&
cp -v CODE_OF_CONDUCT.md COPYING README.md TODO \
	/usr/share/doc/efivar-${VER_EFIVAR} &&

for i in 1 3; do
  install -v -m 644 docs/*.${i} /usr/share/man/man${i}/ 
done

###################################################################### VALIDATE
if [ ! -x /usr/bin/efivar ] ; then
	echo "ERROR: efivar is not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf efivar-${VER_EFIVAR} &&
set +x +u
