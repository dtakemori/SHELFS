#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/efibootmgr-${VER_EFIBOOTMGR}.tar.bz2 ] || exit -1
rm -rf efibootmgr-${VER_EFIBOOTMGR} &&
tar xf /Sources/efibootmgr-${VER_EFIBOOTMGR}.tar.bz2 &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD
cd efibootmgr-${VER_EFIBOOTMGR} &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
make -j${MAKEJOBS:-2} \
	EFIDIR=LFS \
       	EFI_LOADER=grubx64.efi &&

########################################################################## TEST

####################################################################### INSTALL
make EFIDIR=LFS sbindir=/sbin install &&

install -v -d -m 755 /usr/share/doc/efibootmgr-${VER_EFIBOOTMGR} &&
cp -v AUTHORS CODE_OF_CONDUCT.md COPYING MODULE_LICENSE_GPL2 NOTICE README* TODO \
	/usr/share/doc/efibootmgr-${VER_EFIBOOTMGR} &&

###################################################################### VALIDATE
if [ ! -x /sbin/efibootmgr ] ; then
	echo "ERROR: efibootmgr is not installed"
	exit -1
fi

/sbin/efibootmgr --version | grep "^version ${VER_EFIBOOTMGR}"
if [ $? != 0 ] ; then
	echo "ERROR: efibootmgr is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf efibootmgr-${VER_EFIBOOTMGR} &&
set +x +u
