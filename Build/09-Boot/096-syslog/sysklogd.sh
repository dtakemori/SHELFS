#!/bin/bash
# [ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/sysklogd-${VER_SYSKLOGD}.tar.gz ] || exit -1
rm -rf sysklogd-${VER_SYSKLOGD} &&
tar xf /Sources/sysklogd-${VER_SYSKLOGD}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
../sysklogd-${VER_SYSKLOGD}/configure \
	--prefix=/usr \
	--sbindir=/sbin \
	--libdir=/lib64 \
	--sysconfdir=/etc \
	--runstatedir=/run \
	--localstatedir=/var \
	--docdir=/usr/share/doc/sysklogd-${VER_SYSKLOGD} \
	--enable-static=no &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&

rm -v /lib64/libsyslog.la &&

###################################################################### VALIDATE
if [ ! -x /sbin/syslogd ] ; then
	echo "ERROR: syslogd not installed"
	exit -1
fi

/sbin/syslogd -v | grep "^sysklogd v${VER_SYSKLOGD}$"
if [ $? != 0 ] ; then
	echo "ERROR: syslogd incorrect version"
	exit -1
fi

##################################################################### CONFIGURE
install -v -m644 /Build/Config/syslog.conf /etc/syslog.conf &&

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf sysklogd-${VER_SYSKLOGD} &&
set +x +u
