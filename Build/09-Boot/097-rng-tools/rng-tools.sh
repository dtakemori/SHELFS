#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/jitterentropy-library-${VER_JITTERENTROPY}.tar.gz ] || exit -1
rm -rf jitterentropy-library-${VER_JITTERENTROPY} &&
tar xf /Sources/jitterentropy-library-${VER_JITTERENTROPY}.tar.gz &&

[ -f /Sources/rng-tools-${VER_RNGTOOLS}.tar.gz ] || exit -1
rm -rf rng-tools-${VER_RNGTOOLS} &&
tar xf /Sources/rng-tools-${VER_RNGTOOLS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd jitterentropy-library-${VER_JITTERENTROPY} &&

sed -i "s/-O0/-O0 -fstack-clash-protection -flto/" Makefile &&
sed -i "s/-lpthread/-lpthread ${LDOPTS_0}/" Makefile &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -v make -j${MAKEJOBS:-2} -k check
fi

####################################################################### INSTALL
make PREFIX=$(pwd)/../install \
  LIBDIR=lib64 \
  install-static &&

make PREFIX=$(pwd)/../install \
  LIBDIR=lib64 \
  install-includes &&

install -v -d -m 755 /usr/share/doc/jitterentropy-library-${VER_JITTERENTROPY} &&
cp -v {CHANGES.md,LICENSE*,README.md} \
	/usr/share/doc/jitterentropy-library-${VER_JITTERENTROPY} &&

cd ../ &&

######################################################################### PATCH
cd rng-tools-${VER_RNGTOOLS}/ &&
sed -i 's|/var/run/rngd.pid|/run/rngd.pid|' rngd.8.in rngd.c && 

########################################################################## PREP
./autogen.sh &&

######################################################################### BUILD
CFLAGS="${COPTS_1} -flto " \
CPPFLAGS="-I$(pwd)/../install/include " \
LDFLAGS="${LDOPTS_1} " \
./configure \
	--prefix=/usr \
	--sbindir=/sbin \
	--without-nistbeacon \
	--without-qrypt \
	--without-pkcs11 \
	--without-rtlsdr \
	--enable-jitterentropy="$(pwd)/../install/lib64" &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/rng-tools-${VER_RNGTOOLS} &&
cp -v {AUTHORS,COPYING,ChangeLog,NEWS,README,README.md} \
	/usr/share/doc/rng-tools-${VER_RNGTOOLS} &&

install -m0755 /Build/Config/rngd.init /etc/rc.d/init.d/rngd &&
ln -svf ../init.d/rngd /etc/rc.d/rc0.d/K90rngd &&
ln -svf ../init.d/rngd /etc/rc.d/rc1.d/K90rngd &&
ln -svf ../init.d/rngd /etc/rc.d/rc2.d/K90rngd &&
ln -svf ../init.d/rngd /etc/rc.d/rc3.d/S21rngd &&
ln -svf ../init.d/rngd /etc/rc.d/rc4.d/S21rngd &&
ln -svf ../init.d/rngd /etc/rc.d/rc5.d/S21rngd &&
ln -svf ../init.d/rngd /etc/rc.d/rc6.d/K90rngd &&


###################################################################### VALIDATE
if [ ! -x /sbin/rngd ] ; then
	echo "ERROR: rngd is not installed"
	exit -1
fi

/sbin/rngd -v 2>&1 | grep "^rngd ${VER_RNGTOOLS}$"
if [ $? != 0 ]; then
	echo "ERROR: rngd is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf install &&
rm -rf jitterentropy-library-${VER_JITTERENTROPY} &&
rm -rf rng-tools-${VER_RNGTOOLS}
