#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

[ -f /Sources/lfs-bootscripts-${VER_LFSBOOTSCRIPTS}.tar.xz ] || exit -1
rm -rf lfs-bootscripts-${VER_LFSBOOTSCRIPTS} &&
tar xf /Sources/lfs-bootscripts-${VER_LFSBOOTSCRIPTS}.tar.xz &&

cd lfs-bootscripts-${VER_LFSBOOTSCRIPTS} &&

patch -p1 -i /Patches/lfs-bootscripts_sysctl_d.patch &&
patch -p1 -i /Patches/lfs-bootscripts_sysklogd-flags.patch &&
patch -p1 -i /Patches/lfs-bootscripts_udevadm.patch &&

make install &&

cd ../ &&
rm -rf lfs-bootscripts-${VER_LFSBOOTSCRIPTS} &&

#################################################################### CONFIGURE
install -v -m644 /Build/Config/clock /etc/sysconfig/clock &&

### Set hostname
echo "HOSTNAME=SHELFS-Live" > /etc/sysconfig/network &&

grep '^HOSTNAME=' /etc/sysconfig/network \
	| sed 's/^HOSTNAME=//' > /etc/hostname &&

### Install /etc/hosts
install -v -m644 /Build/Config/hosts /etc/hosts &&

### Install NIC config
install -v -m644 /Build/Config/ifconfig.CONF_NIC \
	/etc/sysconfig/ifconfig.${CONF_NIC} &&
sed -i -e "s/CONF_NIC/${CONF_NIC}/" /etc/sysconfig/ifconfig.${CONF_NIC} &&

### install /etc/profile
install -v -m644 /Build/Config/profile /etc/profile &&
install -v -m644 /Build/Config/bash_profile /root/.bash_profile &&

### Install sysctl.d files
mkdir -v /etc/sysctl.d &&
install -v -m400 /Build/Config/sysctl-*.conf /etc/sysctl.d/ &&


### Set MOTD
echo                                                             > /etc/motd &&
echo -n "### SHELFS ${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} "     >> /etc/motd &&
echo "$(date) $(uname -m) ###"                                  >> /etc/motd &&
echo                                                            >> /etc/motd &&
echo "Temporary Tools Toolchain:"                               >> /etc/motd &&
echo -e "\t$(/tools/bin/clang --version | grep 'version')"      >> /etc/motd &&
echo -e "\t$(/tools/bin/ld.lld --version)"                      >> /etc/motd &&
echo -e "\tmusl $(/tools/lib/libc.so 2>&1 | grep 'Version')"    >> /etc/motd &&
echo -ne "\t"                                                   >> /etc/motd &&
/tools/bin/busybox | grep "${VER_BUSYBOX}"                      >> /etc/motd &&
echo                                                            >> /etc/motd &&
echo "Final Build Toolchain:"                                   >> /etc/motd &&
echo -e "\t$(gcc -v 2>&1 | grep 'gcc version')"                 >> /etc/motd &&
echo -e "\t$(ld -v)"                                            >> /etc/motd &&
echo -e "\t$(/lib/libc.so.6 | grep 'release version')"          >> /etc/motd &&
echo -e "\t$(ls --version | grep 'coreutils')"                  >> /etc/motd && 
echo                                                            >> /etc/motd


### Set passwords
echo "root:${CONF_ROOTPASS}"     | /sbin/chpasswd
echo "shelfs:${CONF_SHELFSPASS}" | /sbin/chpasswd
grpconv
install -dv -m700 -o shelfs -g shelfs /home/shelfs

set +x +u
