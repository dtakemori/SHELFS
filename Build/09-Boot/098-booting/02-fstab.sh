#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

# Booting - fstab
ln -sv /proc/self/mounts /etc/mtab &&

# Create /etc/fstab
if [ -d /sys/firmware/efi ] ; then
  install -v -m644 /Build/Config/efi/fstab /etc/fstab
else
  install -v -m644 /Build/Config/bios/fstab /etc/fstab
fi

sed -i "s/__FSTYPE_ROOT__/${CONF_FSTYPE_ROOT}/" /etc/fstab &&

sed -i "s/__UUID_BOOTEFI__/${CONF_UUID_BOOTEFI}/" /etc/fstab &&
sed -i "s/__UUID_ROOT__/${CONF_UUID_ROOT}/" /etc/fstab &&
sed -i "s/__UUID_SWAP__/${CONF_UUID_SWAP}/" /etc/fstab &&

set +x +u
