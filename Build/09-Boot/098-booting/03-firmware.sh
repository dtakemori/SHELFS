#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

cp -rv /Sources/FIRMWARE/* /lib/firmware/

if [ $(uname -m) == 'x86_64' ] ; then
  mkdir -v /lib/firmware/amd-ucode/
  cp -v /Sources/UCODE/microcode_amd*.bin /lib/firmware/amd-ucode/

  mkdir -v /lib/firmware/intel-ucode/
  tar xvzf /Sources/UCODE/Intel-Linux-Processor-Microcode-Data-Files-*.tar.gz
  cp -v Intel-Linux-Processor-Microcode-Data-Files-*/intel-ucode/* /lib/firmware/intel-ucode/
  rm -rf Intel-Linux-Processor-Microcode-Data-Files-*
fi

set +x +u
