#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

install -v -m755 /Build/Config/10_SHELFS /etc/grub.d/10_SHELFS &&

sed -i "s/__VERSIONLTS__/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/" /etc/grub.d/10_SHELFS &&
sed -i "s/__VERSION__/${VER_LINUX}.${VER_LINUX_PATCH}/" /etc/grub.d/10_SHELFS &&
sed -i "s/__DRIVE__/${CONF_DRIVE}/" /etc/grub.d/10_SHELFS &&
sed -i "s/__PARTITION_ROOT__/${CONF_PARTITION_ROOT}/" /etc/grub.d/10_SHELFS &&
sed -i "s/__UUID_ROOT__/${CONF_UUID_ROOT}/" /etc/grub.d/10_SHELFS &&

mkdir -v /boot/grub &&
/sbin/grub-mkconfig -o     /boot/grub/grub.cfg &&
/usr/bin/grub-script-check /boot/grub/grub.cfg &&

grub-install --verbose /dev/${CONF_DRIVE} &&

dracut --strip --verbose --zstd \
    --omit "zfs" \
    /boot/initramfs-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}.img \
    ${VER_LINUXLTS}.${VER_LINUXLTS_PATCH} &&

dracut --strip --verbose --zstd \
    --omit "zfs" \
    /boot/initramfs-${VER_LINUX}.${VER_LINUX_PATCH}.img \
    ${VER_LINUX}.${VER_LINUX_PATCH} &&

ln -vs initramfs-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}.img \
    /boot/initramfs.img &&

set +x +u
