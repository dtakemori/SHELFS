#!/bin/bash
[ -z ${LFSCHROOT:-} ] && echo "Not in CHROOT" && exit -1
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

### Strip static libraries
find /lib64       -type f -name '*\.a' -exec strip -p --strip-debug '{}' ';' 
find /usr/lib64   -type f -name '*\.a' -exec strip -p --strip-debug '{}' ';' 
find /usr/libexec -type f -name '*\.a' -exec strip -p --strip-debug '{}' ';' 


### Strip dynamic libraries under /lib64
find /lib64       -type f -name '*\.so*' -not \
			\( -name "ld-*" \
				-or -name "libc\.*" \
				-or -name "libdl-*" \
				-or -name "libhistory\.*" \
				-or -name "libm\.*" \
				-or -name "libncursesw\.*" \
				-or -name "libnss_*" \
				-or -name "libreadline\.*" \
				-or -name "libz\.*" \) \
			-exec strip -p --strip-unneeded '{}' ';'


### Strip dynamic libraries under /usr/lib64
find /usr/lib64   -type f -name '*\.so*' -not -name "libbfd-*" \
			-exec strip -p --strip-unneeded '{}' ';' 


### Strip dynamic libraries under /usr/libexec
find /usr/libexec -type f -name '*\.so*' -exec strip -p --strip-unneeded '{}' ';' 


### Strip binaries under /bin
find /bin         -type f -not \( -name "bash" -or -name "find" \) \
			-exec strip -p --strip-all '{}' ';' 


### Strip binaries under /sbin
find /sbin        -type f -and -not -name "dhcpcd" \
			-exec strip -p --strip-all '{}' ';' 


### Strip binaries under /usr/bin
find /usr/bin     -type f -and -not -name "strip" \
			-exec strip -p --strip-all '{}' ';' 


### Strip binaries under /usr/sbin
find /usr/sbin    -type f -exec strip -p --strip-all '{}' ';' 


### Strip misc binaries
strip -p --strip-all /lib64/bash/*
strip -p --strip-all /lib64/e2initrd_helper
strip -p --strip-all /usr/lib64/gettext/*
strip -p --strip-all /usr/libexec/{awk,findutils}/*
strip -p --strip-all /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/tools/objtool/objtool
strip -p --strip-all /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/scripts/*
strip -p --strip-all /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/scripts/*/*
strip -p --strip-all /lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build/tools/objtool/objtool
strip -p --strip-all /lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build/scripts/*
strip -p --strip-all /lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build/scripts/*/*
strip -p --strip-all /usr/libexec/gcc/${LFS_TGT}/${VER_GCC}/{cc1,cc1plus,collect2,g++-mapper-server,lto-wrapper,lto1}
strip -p --strip-all /usr/libexec/gcc/${LFS_TGT}/${VER_GCC}/install-tools/*
strip -p --strip-all /usr/libexec/gcc/${LFS_TGT}/${VER_GCC}/plugin/*


