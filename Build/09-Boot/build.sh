#!/bin/bash


echo "===> Zlib Start" &&
cd 001-zlib &&
/tools/bin/time -vao build.log ./zlib.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Zlib End" &&


echo "===> Xz Start" &&
cd 002-xz &&
/tools/bin/time -vao build.log ./xz.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Xz End" &&


echo "===> Zstd Start" &&
cd 003-zstd &&
/tools/bin/time -vao build.log ./zstd.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Zstd End" &&


echo "===> Libxcrypt Start" &&
cd 004-libxcrypt &&
/tools/bin/time -vao build.log ./libxcrypt.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libxcrypt End" &&


echo "===> Binutils Start" &&
cd 005-binutils &&
/tools/bin/time -vao build.log ./binutils.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Binutils End" &&


echo "===> GMP Start" &&
cd 006-gmp &&
/tools/bin/time -vao build.log ./gmp.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== GMP End" &&


echo "===> MPFR Start" &&
cd 007-mpfr &&
/tools/bin/time -vao build.log ./mpfr.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== MPFR End" &&


echo "===> MPC Start" &&
cd 008-mpc &&
/tools/bin/time -vao build.log ./mpc.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== MPC End" &&


echo "===> GCC Start" &&
cd 009-gcc &&
/tools/bin/time -vao build.log ./gcc.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== GCC End" &&


echo "===> Bzip2 Start" &&
cd 010-bzip2 &&
/tools/bin/time -vao build.log ./bzip2.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Bzip2 End" &&


echo "===> LZO Start" &&
cd 011-lzo &&
/tools/bin/time -vao build.log ./lzo.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== LZO End" &&


echo "===> Pkgconf Start" &&
cd 012-pkgconf &&
/tools/bin/time -vao build.log ./pkgconf.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Pkgconf End" &&


echo "===> Ncurses Start" &&
cd 013-ncurses &&
/tools/bin/time -vao build.log ./ncurses.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Ncurses End" &&


echo "===> Readline Start" &&
cd 014-readline &&
/tools/bin/time -vao build.log ./readline.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Readline End" &&


echo "===> Attr Start" &&
cd 015-attr &&
/tools/bin/time -vao build.log ./attr.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Attr End" &&


echo "===> Acl Start" &&
cd 016-acl &&
/tools/bin/time -vao build.log ./acl.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Acl End" &&


echo "===> Libcap2 Start" &&
cd 017-libcap2 &&
/tools/bin/time -vao build.log ./libcap2.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libcap2 End" &&


echo "===> Libaio Start" &&
cd 018-libaio &&
/tools/bin/time -vao build.log ./libaio.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libaio End" &&


echo "===> Libelf Start" &&
cd 019-libelf &&
/tools/bin/time -vao build.log ./libelf.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libelf End" &&


echo "===> Libffi Start" &&
cd 020-libffi &&
/tools/bin/time -vao build.log ./libffi.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libffi End" &&


echo "===> Libtirpc Start" &&
cd 021-libtirpc &&
/tools/bin/time -vao build.log ./libtirpc.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libtirpc End" &&


echo "===> Libunistring Start" &&
cd 022-libunistring &&
/tools/bin/time -vao build.log ./libunistring.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libunistring End" &&


echo "===> Popt Start" &&
cd 023-popt &&
/tools/bin/time -vao build.log ./popt.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Popt End" &&


echo "===> PCRE2 Start" &&
cd 024-pcre2 &&
/tools/bin/time -vao build.log ./pcre2.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== PCRE2 End" &&


echo "===> Bash Start" &&
cd 025-bash &&
/tools/bin/time -vao build.log ./bash.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Bash End" &&


echo "===> M4 Start" &&
cd 026-m4 &&
/tools/bin/time -vao build.log ./m4.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== M4 End" &&


echo "===> Libtool Start" &&
cd 027-libtool &&
/tools/bin/time -vao build.log ./libtool.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libtool End" &&


echo "===> Autoconf Start" &&
cd 028-autoconf &&
/tools/bin/time -vao build.log ./autoconf.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Autoconf End" &&


echo "===> Automake Start" &&
cd 029-automake &&
/tools/bin/time -vao build.log ./automake.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Automake End" &&


echo "===> LibPAM Start" &&
cd 030-libpam &&
/tools/bin/time -vao build.log ./libpam.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== LibPAM End" &&


echo "===> BC Start" &&
cd 031-bc &&
/tools/bin/time -vao build.log ./bc.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== BC End" &&


echo "===> Bison Start" &&
cd 032-bison &&
/tools/bin/time -vao build.log ./bison.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Bison End" &&


echo "===> Coreutils Start" &&
cd 033-coreutils &&
/tools/bin/time -vao build.log ./coreutils.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Coreutils End" &&


echo "===> Diffutils Start" &&
cd 034-diffutils &&
/tools/bin/time -vao build.log ./diffutils.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Diffutils End" &&


echo "===> Expat Start" &&
cd 035-expat &&
/tools/bin/time -vao build.log ./expat.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Expat End" &&


echo "===> File Start" &&
cd 036-file &&
/tools/bin/time -vao build.log ./file.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== File End" &&


echo "===> Sed Start" &&
cd 037-sed &&
/tools/bin/time -vao build.log ./sed.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Sed End" &&


echo "===> Findutils Start" &&
cd 038-findutils &&
/tools/bin/time -vao build.log ./findutils.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Findutils End" &&


echo "===> Flex Start" &&
cd 039-flex &&
/tools/bin/time -vao build.log ./flex.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Flex End" &&


echo "===> Gawk Start" &&
cd 040-gawk &&
/tools/bin/time -vao build.log ./gawk.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Gawk End" &&


echo "===> GDBM Start" &&
cd 041-gdbm &&
/tools/bin/time -vao build.log ./gdbm.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== GDBM End" &&


echo "===> Gperf Start" &&
cd 042-gperf &&
/tools/bin/time -vao build.log ./gperf.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Gperf End" &&


echo "===> Grep Start" &&
cd 043-grep &&
/tools/bin/time -vao build.log ./grep.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Grep End" &&


echo "===> ICU Start" &&
cd 044-icu &&
/tools/bin/time -vao build.log ./icu.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== ICU End" &&


echo "===> Libxml2 Start" &&
cd 045-libxml2 &&
/tools/bin/time -vao build.log ./libxml2.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libxml2 End" &&


echo "===> Gettext Start" &&
cd 046-gettext &&
/tools/bin/time -vao build.log ./gettext.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Gettext End" &&


echo "===> Freetype Start" &&
cd 047-freetype &&
/tools/bin/time -vao build.log ./freetype.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Freetype End" &&


echo "===> Harfbuzz Start" &&
cd 048-harfbuzz &&
/tools/bin/time -vao build.log ./harfbuzz.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Harfbuzz End" &&


echo "===> Freetype Start" &&
cd 049-freetype &&
/tools/bin/time -vao build.log ./freetype.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Freetype End" &&


echo "===> Grub Start" &&
cd 050-grub2 &&
/tools/bin/time -vao build.log ./grub.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Grub End" &&


echo "===> Gzip Start" &&
cd 051-gzip &&
/tools/bin/time -vao build.log ./gzip.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Gzip End" &&


echo "===> IANA-etc Start" &&
cd 052-iana-etc &&
/tools/bin/time -vao build.log ./iana-etc.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== IANA-etc End" &&


echo "===> Inetutils Start" &&
cd 053-inetutils &&
/tools/bin/time -vao build.log ./inetutils.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Inetutils End" &&


echo "===> Iproute2 Start" &&
cd 054-iproute2 &&
/tools/bin/time -vao build.log ./iproute2.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Iproute2 End" &&


echo "===> Kbd Start" &&
cd 055-kbd &&
/tools/bin/time -vao build.log ./kbd.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Kbd End" &&


echo "===> Lzip Start" &&
cd 056-lzip &&
/tools/bin/time -vao build.log ./lzip.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Lzip End" &&


echo "===> Make Start" &&
cd 057-make &&
/tools/bin/time -vao build.log ./make.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Make End" &&


echo "===> OpenSSL Start" &&
cd 058-openssl &&
/tools/bin/time -vao build.log ./openssl.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== OpenSSL End" &&


echo "===> Patch Start" &&
cd 059-patch &&
/tools/bin/time -vao build.log ./patch.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Patch End" &&


echo "===> Cmake Start" &&
cd 060-cmake &&
/tools/bin/time -vao build.log ./cmake.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Cmake End" &&


echo "===> Ninja Start" &&
cd 061-ninja &&
/tools/bin/time -vao build.log ./ninja.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Ninja End" &&


echo "===> Procps Start" &&
cd 062-procps &&
/tools/bin/time -vao build.log ./procps_ng.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Procps End" &&


echo "===> Psmisc Start" &&
cd 063-psmisc &&
/tools/bin/time -vao build.log ./psmisc.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Psmisc End" &&


echo "===> Shadow Start" &&
cd 064-shadow &&
/tools/bin/time -vao build.log ./shadow.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Shadow End" &&


echo "===> Sysvinit Start" &&
cd 065-sysvinit &&
/tools/bin/time -vao build.log ./sysvinit.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Sysvinit End" &&


echo "===> Tar Start" &&
cd 066-tar &&
/tools/bin/time -vao build.log ./tar.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Tar End" &&


echo "===> Time Start" &&
cd 067-time &&
/tools/bin/time -vao build.log ./time.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Time End" &&


echo "===> Cpio Start" &&
cd 068-cpio &&
/tools/bin/time -vao build.log ./cpio.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Cpio End" &&


echo "===> Kmod Start" &&
cd 069-kmod &&
/tools/bin/time -vao build.log ./kmod.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Kmod End" &&


echo "===> Util-Linux Start" &&
cd 070-util-linux &&
/tools/bin/time -vao build.log ./util-linux.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Util-Linux End" &&


echo "===> Systemd_Udev Start" &&
cd 071-systemd_udev &&
/tools/bin/time -vao build.log ./systemd_udev.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Systemd_Udev End" &&


echo "===> Perl Start" &&
cd 072-perl &&
/tools/bin/time -vao build.log ./perl.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Perl End" &&


echo "===> XML-Parser Start" &&
cd 073-XML-Parser &&
/tools/bin/time -vao build.log ./XML-Parser.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== XML-Parser End" &&


echo "===> Python Start" &&
cd 074-python &&
/tools/bin/time -vao build.log ./python.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Python End" &&


echo "===> Flit-core Start" &&
cd 075-flit-core &&
/tools/bin/time -vao build.log ./flit-core.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Flit-core End" &&


echo "===> Wheel Start" &&
cd 076-wheel &&
/tools/bin/time -vao build.log ./wheel.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Wheel End" &&


echo "===> Setuptools Start" &&
cd 077-setuptools &&
/tools/bin/time -vao build.log ./setuptools.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Setuptools End" &&


echo "===> Meson Start" &&
cd 078-meson &&
/tools/bin/time -vao build.log ./meson.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Meson End" &&


echo "===> Mdadm Start" &&
cd 079-mdadm &&
/tools/bin/time -vao build.log ./mdadm.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Mdadm End" &&


echo "===> LVM Start" &&
cd 080-LVM &&
/tools/bin/time -vao build.log ./LVM.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== LVM End" &&


echo "===> E2fsprogs Start" &&
cd 081-e2fsprogs &&
/tools/bin/time -vao build.log ./e2fsprogs.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== E2fsprogs End" &&


echo "===> XFSprogs Start" &&
cd 082-xfsprogs &&
/tools/bin/time -vao build.log ./xfsprogs.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== XFSprogs End" &&


echo "===> Btrfs-progs Start" &&
cd 083-btrfs-progs &&
/tools/bin/time -vao build.log ./btrfs-progs.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Btrfs-progs End" &&


echo "===> DOSfstools Start" &&
cd 084-dosfstools &&
/tools/bin/time -vao build.log ./dosfstools.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== DOSfstools End" &&


echo "===> Exfatprogs Start" &&
cd 085-exfatprogs &&
/tools/bin/time -vao build.log ./exfatprogs.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Exfatprogs End" &&


echo "===> Cryptsetup Start" &&
cd 086-cryptsetup &&
/tools/bin/time -vao build.log ./cryptsetup.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Cryptsetup End" &&


echo "===> Intltool Start" &&
cd 087-intltool &&
/tools/bin/time -vao build.log ./intltool.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Intltool End" &&


echo "===> Linux Kernel LTS Start" &&
cd 088-linux-lts &&
/tools/bin/time -vao build.log ./linux-kernel-lts.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Linux Kernel LTS End" &&


echo "===> Linux Kernel Start" &&
cd 089-linux &&
/tools/bin/time -vao build.log ./linux-kernel.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Linux Kernel End" &&


echo "===> ZFS Start" &&
cd 090-zfs &&
/tools/bin/time -vao build.log ./zfs.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== ZFS End" &&


echo "===> Dracut Start" &&
cd 091-dracut &&
/tools/bin/time -vao build.log ./dracut.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Dracut End" &&


echo "===> DBus Start" &&
cd 092-dbus &&
/tools/bin/time -vao build.log ./dbus.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== DBus End" &&


echo "===> Dhcpcd Start" &&
cd 093-dhcpcd &&
/tools/bin/time -vao build.log ./dhcpcd.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Dhcpcd End" &&


echo "===> Efivar Start" &&
cd 094-efivar &&
/tools/bin/time -vao build.log ./efivar.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Efivar End" &&


echo "===> Efibootmgr Start" &&
cd 095-efibootmgr &&
/tools/bin/time -vao build.log ./efibootmgr.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Efibootmgr End" &&


echo "===> Syslog Start" &&
cd 096-syslog &&
/tools/bin/time -vao build.log ./sysklogd.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Syslog End" &&


echo "===> Rng-Tools Start" &&
cd 097-rng-tools &&
/tools/bin/time -vao build.log ./rng-tools.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Rng-Tools End" &&


echo "===> Bootableize Start" &&
cd 098-booting &&

echo "  Setup Bootscripts" &&
/tools/bin/time -vao build.log ./01-bootscripts.sh  > build.log 2>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Setup Fstab" &&
/tools/bin/time -vao build.log ./02-fstab.sh        >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Install Firmware" &&
/tools/bin/time -vao build.log ./03-firmware.sh     >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

echo "  Setup Grub" &&
/tools/bin/time -vao build.log ./04-grub-install.sh >> build.log 2>>error.log &&
grep -E " time \(|Percent" build.log | tail -3 &&

cd .. &&
echo "<=== Bootableize End" 


echo "===> Strip Start" &&
cd 099-strip &&
/tools/bin/time -vao build.log ./strip.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Strip End"
