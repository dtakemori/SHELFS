#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/tcl${VER_TCL}.${VER_TCLPATCH}-src.tar.gz ] || exit -1
rm -rf tcl${VER_TCL}.${VER_TCLPATCH} &&
tar xf /Sources/tcl${VER_TCL}.${VER_TCLPATCH}-src.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1}" \
LDFLAGS="${LDOPTS_1}" \
../tcl${VER_TCL}.${VER_TCLPATCH}/unix/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--mandir=/usr/share/man &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

TCL_SRC="$(echo $(pwd) | sed "s/build/tcl${VER_TCL}.${VER_TCLPATCH}/")"
VER_TDBC=$(echo pkgs/tdbc[0-9]* | sed 's|pkgs/tdbc||')
VER_ITCL=$(echo pkgs/itcl[0-9]* | sed 's|pkgs/itcl||')

sed -e "s|$(pwd) -ltclstub|/usr/lib64 -ltclstub|" \
    -e "s|$(pwd) -ltcl${VER_TCL}|/usr/lib64 -ltcl${VER_TCL}|" \
    -e "s|$(pwd)/libtclstub${VER_TCL}|/usr/lib64/libtclstub${VER_TCL}|" \
    -e "s|${TCL_SRC}|/usr/include|" \
    -i tclConfig.sh &&

sed -e "s|$(pwd)/pkgs/tdbc${VER_TDBC} -ltdbc|/usr/lib64/tdbc${VER_TDBC} -ltdbc|" \
    -e "s|$(pwd)/pkgs/tdbc${VER_TDBC}/libtdbcstub|/usr/lib64/tdbc${VER_TDBC}/libtdbcstub|" \
    -e "s|${TCL_SRC}/pkgs/tdbc${VER_TDBC}/generic|/usr/include|" \
    -e "s|${TCL_SRC}/pkgs/tdbc${VER_TDBC}/library|/usr/lib64/tcl${VER_TCL}|" \
    -e "s|${TCL_SRC}/pkgs|/usr/include|" \
    -i pkgs/tdbc${VER_TDBC}/tdbcConfig.sh &&

sed -e "s|$(pwd)/pkgs/itcl${VER_ITCL} -litcl|/usr/lib64/itcl${VER_ITCL} -litcl|" \
    -e "s|$(pwd)/pkgs/itcl${VER_ITCL}/libitclstub|/usr/lib64/itcl${VER_ITCL}/libitclstub|" \
    -e "s|${TCL_SRC}/pkgs/itcl${VER_ITCL}/generic|/usr/include|" \
    -e "s|${TCL_SRC}/pkgs|/usr/include|" \
    -i pkgs/itcl${VER_ITCL}/itclConfig.sh &&

mv -v ../tcl${VER_TCL}.${VER_TCLPATCH}/doc/{,Tcl_}Thread.3 &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ; then
  /usr/bin/time -vao ../test.log make TZ=UTC -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make install &&
make install-private-headers &&
ln -sv tclsh${VER_TCL} /usr/bin/tclsh &&
ldconfig &&

chmod -v u+w /usr/lib64/libtcl${VER_TCL}.so &&
chmod -v 644 /usr/lib64/libtclstub${VER_TCL}.a &&

install -v -d -m 755 /usr/share/doc/tcl-${VER_TCL}.${VER_TCLPATCH} &&
cp -v ../tcl${VER_TCL}.${VER_TCLPATCH}/{ChangeLog*,README.md,license.terms} \
	/usr/share/doc/tcl-${VER_TCL}.${VER_TCLPATCH} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/tclsh ] ; then
	echo "ERROR: tclsh is not installed"
	exit -1
fi

echo 'puts [info patchlevel] ; exit 0' | /usr/bin/tclsh | grep "^${VER_TCL}.${VER_TCLPATCH}$"
if [ $? != 0 ] ; then
	echo "ERROR: tclsh is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf tcl${VER_TCL}.${VER_TCLPATCH} &&
set +x +u
