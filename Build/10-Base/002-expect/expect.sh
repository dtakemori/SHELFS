#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/expect${VER_EXPECT}.tar.gz ] || exit -1
rm -rf expect${VER_EXPECT} &&
tar xf /Sources/expect${VER_EXPECT}.tar.gz &&

######################################################################### PATCH
cd expect${VER_EXPECT} &&
patch -p1 -i /Patches/expect-5.45.4-covscan-fixes.patch &&
patch -p1 -i /Patches/expect-c99.patch &&
patch -p1 -i /Patches/expect-configure-c99.patch &&

sed -i 's:/usr/local/bin:/bin:' configure &&

aclocal &&
autoconf &&
cd testsuite &&
autoconf -I .. &&
cd ../ &&

cp -vf /usr/share/automake-${VER_AUTOMAKE}/config.guess tclconfig/config.guess &&
cp -vf /usr/share/automake-${VER_AUTOMAKE}/config.sub tclconfig/config.sub &&

cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_1}" \
../expect${VER_EXPECT}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--mandir=/usr/share/man \
	--enable-shared \
	--with-tcl=/usr/lib64 \
	--with-tclinclude=/usr/include \
	--with-tk=no \
	--with-x=no &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

ln -sv expect${VER_EXPECT}/libexpect${VER_EXPECT}.so /usr/lib64 &&

install -v -d -m 755 /usr/share/doc/expect-${VER_EXPECT} &&
cp -v ../expect${VER_EXPECT}/{ChangeLog,FAQ,HISTORY,NEWS,README} \
	/usr/share/doc/expect-${VER_EXPECT} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/expect ] ; then
	echo "ERROR: expect is not installed"
	exit -1
fi

/usr/bin/expect -v | grep "^expect version ${VER_EXPECT}$"
if [ $? != 0 ] ; then
	echo "ERROR: expect is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf expect${VER_EXPECT} &&
set +x +u
