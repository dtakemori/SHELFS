#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/dejagnu-${VER_DEJAGNU}.tar.gz ] || exit -1
rm -rf dejagnu-${VER_DEJAGNU} &&
tar xf /Sources/dejagnu-${VER_DEJAGNU}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../dejagnu-${VER_DEJAGNU}/configure \
	--prefix=/usr &&

makeinfo --html --no-split -o doc/dejagnu.html ../dejagnu-${VER_DEJAGNU}/doc/dejagnu.texi &&
makeinfo --plaintext -o doc/dejagnu.txt ../dejagnu-${VER_DEJAGNU}/doc/dejagnu.texi &&

######################################################################### BUILD

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /usr/bin/time -vao ../test.log make -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -d -v -m 755 /usr/share/doc/dejagnu-${VER_DEJAGNU} &&
cp -v doc/dejagnu.{html,txt} /usr/share/doc/dejagnu-${VER_DEJAGNU} &&

cp -v ../dejagnu-${VER_DEJAGNU}/{AUTHORS,COPYING,ChangeLog*,MAINTAINERS} \
	../dejagnu-${VER_DEJAGNU}/{NEWS,README,TODO} \
	/usr/share/doc/dejagnu-${VER_DEJAGNU} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/runtest ] ; then
	echo "ERROR: runtest is not installed"
	exit -1
fi

/usr/bin/runtest --version | grep "^DejaGnu version$(printf '\t')${VER_DEJAGNU}$"
if [ $? != 0 ] ; then
	echo "ERROR: runtest is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf dejagnu-${VER_DEJAGNU} &&
set +x +u
