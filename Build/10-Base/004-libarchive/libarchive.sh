#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libarchive-${VER_LIBARCHIVE}.tar.xz ] || exit -1
rm -rf libarchive-${VER_LIBARCHIVE} &&
tar xf /Sources/libarchive-${VER_LIBARCHIVE}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../libarchive-${VER_LIBARCHIVE}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -d -v -m 755 /usr/share/doc/libarchive-${VER_LIBARCHIVE} &&
cp -rv ../libarchive-${VER_LIBARCHIVE}/doc/{html,text} \
	../libarchive-${VER_LIBARCHIVE}/{COPYING,NEWS,README.md} \
	/usr/share/doc/libarchive-${VER_LIBARCHIVE} &&

rm -v /usr/lib64/libarchive.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/archive.h ] ; then
	echo "ERROR: archive.h header not installed"
	exit -1
fi

if [ ! -f /usr/lib64/libarchive.so ] ; then
	echo "ERROR: libarchive.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libarchive-${VER_LIBARCHIVE} &&
set +x +u
