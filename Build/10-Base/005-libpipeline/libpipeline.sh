#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libpipeline-${VER_LIBPIPELINE}.tar.gz ] || exit -1
rm -rf libpipeline-${VER_LIBPIPELINE} &&
tar xf /Sources/libpipeline-${VER_LIBPIPELINE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto"
LDFLAGS="${LDOPTS_1}" \
../libpipeline-${VER_LIBPIPELINE}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--enable-threads=isoc+posix \
	--enable-socketpair-pipe \
	--enable-shared \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
ldconfig &&

install -d -v -m 755 /usr/share/doc/libpipeline-${VER_LIBPIPELINE} &&
cp -rv ../libpipeline-${VER_LIBPIPELINE}/{COPYING,ChangeLog*,NEWS.md,README.md,TODO} \
	/usr/share/doc/libpipeline-${VER_LIBPIPELINE} &&

rm -v /usr/lib64/libpipeline.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/pipeline.h ] ; then
	echo "ERROR: pipeline.h header not installed"
	exit -1
fi

if [ ! -f /usr/lib64/libpipeline.so ] ; then
	echo "ERROR: libpipeline.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libpipeline-${VER_LIBPIPELINE} &&
set +x +u
