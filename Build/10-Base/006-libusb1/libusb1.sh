#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libusb-${VER_LIBUSB}.tar.bz2 ] || exit -1
rm -rf libusb-${VER_LIBUSB} &&
tar xf /Sources/libusb-${VER_LIBUSB}.tar.bz2 &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../libusb-${VER_LIBUSB}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
ldconfig &&

install -d -v -m 755 /usr/share/doc/libusb-${VER_LIBUSB} &&
cp -rv ../libusb-${VER_LIBUSB}/{AUTHORS,COPYING,ChangeLog,NEWS,PORTING,README,TODO} \
	/usr/share/doc/libusb-${VER_LIBUSB} &&

rm -v /usr/lib64/libusb-1.0.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/libusb-1.0/libusb.h ] ; then
	echo "ERROR: libusb.h header not installed"
	exit -1
fi

if [ ! -f /usr/lib64/libusb-1.0.so ] ; then
	echo "ERROR: libusb-1.0.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libusb-${VER_LIBUSB} &&
set +x +u
