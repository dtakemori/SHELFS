#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libuv-${VER_LIBUV}.tar.gz ] || exit -1
rm -rf libuv-${VER_LIBUV} &&
tar xf /Sources/libuv-${VER_LIBUV}.tar.gz &&

######################################################################### PATCH
cd libuv-${VER_LIBUV} &&
./autogen.sh &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1}" \
LDFLAGS="${LDOPTS_1}" \
../libuv-${VER_LIBUV}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -d -v -m 755 /usr/share/doc/libuv-${VER_LIBUV} &&
cp -v ../libuv-${VER_LIBUV}/{AUTHORS,CONTRIBUTING.md,ChangeLog,LICENSE*,LINKS.md} \
	../libuv-${VER_LIBUV}/{MAINTAINERS.md,README.md,SUPPORTED_PLATFORMS.md} \
	/usr/share/doc/libuv-${VER_LIBUV} &&

rm -v /usr/lib64/libuv.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/uv.h ] ; then
	echo "ERROR: uv.h header not installed"
	exit -1
fi

if [ ! -f /usr/lib64/libuv.so ] ; then
	echo "ERROR: libuv.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libuv-${VER_LIBUV} &&
set +x +u
