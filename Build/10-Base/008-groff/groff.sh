#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/groff-${VER_GROFF}.tar.gz ] || exit -1
rm -rf groff-${VER_GROFF} &&
tar xf /Sources/groff-${VER_GROFF}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
PAGE=letter \
../groff-${VER_GROFF}/configure \
	--prefix=/usr \
	--without-gs &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make docdir=/usr/share/doc/groff-${VER_GROFF} install &&

ln -sv eqn /usr/bin/geqn &&
ln -sv tbl /usr/bin/gtbl &&

cp -v ../groff-${VER_GROFF}/{AUTHORS,BUG-REPORT,COPYING,ChangeLog*,FDL} \
	../groff-${VER_GROFF}/{FOR-RELEASE,HACKING,LICENSES,MANIFEST,MORE.STUFF} \
	../groff-${VER_GROFF}/{NEWS,PROBLEMS,PROJECTS,README*,THANKS,TODO} \
	/usr/share/doc/groff-${VER_GROFF} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/groff ] ; then
	echo "ERROR: groff is not installed"
	exit -1
fi

/usr/bin/groff --version | grep "^GNU groff version ${VER_GROFF}$"
if [ $? != 0 ] ; then
	echo "ERROR: groff is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf groff-${VER_GROFF} &&
set +x +u
