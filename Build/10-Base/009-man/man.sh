#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/man-pages-${VER_MANPAGES}.tar.xz ] || exit -1
rm -rf man-pages-${VER_MANPAGES} &&
tar xf /Sources/man-pages-${VER_MANPAGES}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
cd man-pages-${VER_MANPAGES} &&

### don't overwrite the libxcrypt versions
rm -v man3/crypt{,_r}.3 &&

######################################################################### BUILD

########################################################################## TEST

####################################################################### INSTALL
make -R prefix=/usr install &&

install -v -d -m 755 /usr/share/doc/man-${VER_MANPAGES} &&
cp -vr {CONTRIBUTING,Changes*,LICENSES,README,RELEASE} \
	/usr/share/doc/man-${VER_MANPAGES} &&

####################################################################### CLEANUP
cd ../ &&
rm -rf man-pages-${VER_MANPAGES} &&

######################################################################## UNPACK
rm -rf man-db-${VER_MANDB} &&
tar xf /Sources/man-db-${VER_MANDB}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
groupadd -g 89 man &&
useradd -u 89 -g man -s /sbin/nologin -c 'man' -d /var/cache/man -M -r man &&
install -v -d -m 755 -o man -g man /var/cache/man &&

mkdir -v build/ &&
cd build/ &&

PKG_CONFIG_LIBDIR="/usr/lib64/pkgconfig" \
CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_1}" \
../man-db-${VER_MANDB}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--sysconfdir=/etc \
	--docdir=/usr/share/doc/man-db-${VER_MANDB} \
	--enable-threads=isoc+posix \
	--with-browser=/usr/bin/lynx \
	--with-systemdtmpfilesdir= \
	--with-systemdsystemunitdir= &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

mandb &&

cp -v ../man-db-${VER_MANDB}/{COPYING,ChangeLog*,FAQ,NEWS.md,README.md} \
	/usr/share/doc/man-db-${VER_MANDB} &&

rm -v /usr/lib64/man-db/libman{,db}.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/man ] ; then
	echo "ERROR: man not installed"
	exit -1
fi

/usr/bin/man --version | grep "^man ${VER_MANDB}$"
if [ $? != 0 ] ; then
	echo "ERROR: man incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf man-db-${VER_MANDB} &&
set +x +u
