#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/texinfo-${VER_TEXINFO}.tar.xz ] || exit -1
rm -rf texinfo-${VER_TEXINFO} &&
tar xf /Sources/texinfo-${VER_TEXINFO}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_1}" \
../texinfo-${VER_TEXINFO}/configure \
	--prefix=/usr &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -d -v -m 755 /usr/share/doc/texinfo-${VER_TEXINFO} &&
cp -v ../texinfo-${VER_TEXINFO}/{AUTHORS,COPYING,ChangeLog*,NEWS,README*,TODO} \
	/usr/share/doc/texinfo-${VER_TEXINFO} &&

rm -v /usr/lib/texinfo/{MiscXS,Parsetexi,XSParagraph}.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/makeinfo ] ; then
	echo "ERROR: makeinfo is not installed"
	exit -1
fi

/usr/bin/makeinfo --version | grep "^texi2any (GNU texinfo) ${VER_TEXINFO}$"
if [ $? != 0 ] ; then
	echo "ERROR: makeinfo is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf texinfo-${VER_TEXINFO} &&
set +x +u
