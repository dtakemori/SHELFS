#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gptfdisk-${VER_GPTFDISK}.tar.gz ] || exit -1
rm -rf gptfdisk-${VER_GPTFDISK} &&
tar xf /Sources/gptfdisk-${VER_GPTFDISK}.tar.gz &&

######################################################################### PATCH
cd gptfdisk-${VER_GPTFDISK} &&
patch -p1 -i /Patches/gptfdisk-1.0.4-convenience-1.patch &&

# https://sourceforge.net/p/gptfdisk/discussion/939590/thread/bc29ca06f8/
sed -i '/^#include /s|ncursesw/||' gptcurses.cc &&

sed -i '/UUID_H/s/^.*$/#if defined (_UUID_UUID_H) || defined (_UL_LIBUUID_UUID_H)/' \
	guid.cc &&

cd ../ &&

########################################################################## PREP

######################################################################### BUILD
cd gptfdisk-${VER_GPTFDISK} &&

CXXFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_1}" \
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/gptftdisk-${VER_GPTFDISK} &&
cp -v {COPYING,NEWS,README} /usr/share/doc/gptftdisk-${VER_GPTFDISK} &&

###################################################################### VALIDATE
if [ ! -x /sbin/sgdisk ] ; then
	echo "ERROR: sgdisk not installed"
	exit -1
fi

/sbin/sgdisk -V | grep "^GPT fdisk (sgdisk) version ${VER_GPTFDISK}$"
if [ $? != 0 ] ; then
	echo "ERROR: sgdisk incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf build &&
rm -rf gptfdisk-${VER_GPTFDISK} &&
set +x +u
