#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/less-${VER_LESS}.tar.gz ] || exit -1
rm -rf less-${VER_LESS} &&
tar xf /Sources/less-${VER_LESS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_1}" \
../less-${VER_LESS}/configure \
	--prefix=/usr \
	--sysconfdir=/etc \
	--with-secure  &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/less-${VER_LESS} &&
cp -v ../less-${VER_LESS}/{COPYING,LICENSE,NEWS,README} \
	/usr/share/doc/less-${VER_LESS} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/less ] ; then
	echo "ERROR: less not installed"
	exit -1
fi

/usr/bin/less -V | grep "^less ${VER_LESS}"
if [ $? != 0 ] ; then
	echo "ERROR: less is incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf less-${VER_LESS} &&
set +x +u
