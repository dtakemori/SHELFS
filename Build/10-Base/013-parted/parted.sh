#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/parted-${VER_PARTED}.tar.xz ] || exit -1
rm -rf parted-${VER_PARTED} &&
tar xf /Sources/parted-${VER_PARTED}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../parted-${VER_PARTED}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

make -C doc html &&
makeinfo --html -o doc/html ../parted-${VER_PARTED}/doc/parted.texi &&
makeinfo --plaintext -o doc/parted.txt ../parted-${VER_PARTED}/doc/parted.texi &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/parted-${VER_PARTED}/html &&
install -v -m 644 doc/html/* /usr/share/doc/parted-${VER_PARTED}/html &&
install -v -m 644 doc/parted.{txt,html} /usr/share/doc/parted-${VER_PARTED} &&
install -v -m 644 ../parted-${VER_PARTED}/doc/{FAT,API} \
		/usr/share/doc/parted-${VER_PARTED} &&

cp -v ../parted-${VER_PARTED}/{AUTHORS,BUGS,COPYING,ChangeLog} \
	../parted-${VER_PARTED}/{NEWS,README,THANKS,TODO} \
	/usr/share/doc/parted-${VER_PARTED} &&

rm -v /usr/lib64/libparted{,-fs-resize}.la &&

###################################################################### VALIDATE
if [ ! -x /usr/sbin/parted ] ; then
	echo "ERROR: parted is not installed"
	exit -1
fi

/usr/sbin/parted --version | grep "^parted (GNU parted) ${VER_PARTED}$"
if [ $? != 0 ] ; then
	echo "ERROR: parted is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf parted-${VER_PARTED} &&
set +x +u
