#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/dmidecode-${VER_DMIDECODE}.tar.xz ] || exit -1
rm -rf dmidecode-${VER_DMIDECODE} &&
tar xf /Sources/dmidecode-${VER_DMIDECODE}.tar.xz &&

######################################################################### PATCH
cd dmidecode-${VER_DMIDECODE} &&
patch -p1 -i /Patches/dmidecode-3.5_80de3762.patch &&
patch -p1 -i /Patches/dmidecode-3.5_c76ddda0.patch &&
patch -p1 -i /Patches/dmidecode-3.5_de392ff0.patch &&
cd ../ &&

########################################################################## PREP

######################################################################### BUILD
cd dmidecode-${VER_DMIDECODE} &&
CC="gcc" \
CFLAGS="${COPTS_1} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_1} -flto" \
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make prefix=/usr docdir=/usr/share/doc/dmidecode-${VER_DMIDECODE} install &&

cp -v LICENSE /usr/share/doc/dmidecode-${VER_DMIDECODE} &&

###################################################################### VALIDATE
if [ ! -x /usr/sbin/dmidecode ] ; then
	echo "ERROR: dmidecode not installed"
	exit -1
fi

/usr/sbin/dmidecode --version | grep "^${VER_DMIDECODE}$"
if [ $? != 0 ] ; then
	echo "ERROR: dmidecocde incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf dmidecode-${VER_DMIDECODE} &&
set +x +u
