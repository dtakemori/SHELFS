#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/pciutils-${VER_PCIUTILS}.tar.gz ] || exit -1
rm -rf pciutils-${VER_PCIUTILS} &&
tar xf /Sources/pciutils-${VER_PCIUTILS}.tar.gz &&

######################################################################### PATCH
cd pciutils-${VER_PCIUTILS} &&
sed -i "s/-O2/${COPTS_1} -flto/" Makefile &&

########################################################################## PREP

########################################################## BUILD/INSTALL SHARED
sed -i 's/SHARED=no/SHARED=yes/' Makefile &&

####################################################################### INSTALL
make PREFIX=/usr \
	LIBDIR=/usr/lib64 \
	SHAREDIR=/usr/share/hwdata \
	SHARED=yes \
	install install-lib &&
chmod -v 755 /usr/lib/libpci.so.${VER_PCIUTILS} &&
ldconfig &&

install -v -m 755 -d /usr/include/pci &&
install -v -m 644 lib/*.h /usr/include/pci &&

gzip -c /Build/Config/pci-${VER_PCIIDS}.ids > /usr/share/hwdata/pci.ids.gz &&

install -d -v -m 755 /usr/share/doc/pciutils-${VER_PCIUTILS} &&
cp -v {COPYING,ChangeLog,README*,TODO} \
	/usr/share/doc/pciutils-${VER_PCIUTILS} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/lspci ] ; then
	echo "ERROR: lspci not installed"
	exit -1
fi

/usr/bin/lspci --version | grep "^lspci version ${VER_PCIUTILS}$"
if [ $? != 0 ] ; then
	echo "ERROR: lspci incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf pciutils-${VER_PCIUTILS} &&
set +x +u 
