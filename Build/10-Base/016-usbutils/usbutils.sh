#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/usbutils-${VER_USBUTILS}.tar.xz ] || exit -1
rm -rf usbutils-${VER_USBUTILS} &&
tar xf /Sources/usbutils-${VER_USBUTILS}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

PKG_CONFIG_PATH="/usr/lib64/pkgconfig" \
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../usbutils-${VER_USBUTILS}/configure \
	--prefix=/usr \
	--datadir=/usr/share/hwdata \
	--mandir=/usr/share/man &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} V=1 &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&

gzip -c /usr/share/hwdata/usb.ids > /usr/share/hwdata/usb.ids.gz &&

install -d -v -m 755 /usr/share/doc/usbutils-${VER_USBUTILS} &&
cp -rv ../usbutils-${VER_USBUTILS}/{LICENSES,NEWS,README.md} \
	/usr/share/doc/usbutils-${VER_USBUTILS} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/lsusb ] ; then
	echo "ERROR: lsusb not installed"
	exit -1
fi

/usr/bin/lsusb --version | grep "^lsusb (usbutils) ${VER_USBUTILS}$"
if [ $? != 0 ] ; then
	echo "ERROR: lsusb incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf usbutils-${VER_USBUTILS} &&
set +x +u
