#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/nano-${VER_NANO}.tar.xz ] || exit -1
rm -rf nano-${VER_NANO} &&
tar xf /Sources/nano-${VER_NANO}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../nano-${VER_NANO}/configure \
	--prefix=/usr \
	--bindir=/bin \
	--sysconfdir=/etc \
	--docdir=/usr/share/doc/nano-${VER_NANO} \
	--enable-utf8 &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

cp -v ../nano-${VER_NANO}/{AUTHORS,COPYING*,ChangeLog,IMPROVEMENTS} \
	../nano-${VER_NANO}/{NEWS,README,THANKS,TODO} \
	/usr/share/doc/nano-${VER_NANO} &&

###################################################################### VALIDATE
if [ ! -x /bin/nano ] ; then
	echo "ERROR: nano not installed"
	exit -1
fi

/bin/nano --version | grep "^ GNU nano, version ${VER_NANO}$"
if [ $? != 0 ] ; then
	echo "ERROR: nano incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf nano-${VER_NANO} &&
set +x +u
