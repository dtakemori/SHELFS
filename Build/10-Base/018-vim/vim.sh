#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/vim-${VER_VIM}.${VER_VIMPATCH}.tar.gz ] || exit -1
rm -rf vim-${VER_VIM}.${VER_VIMPATCH} &&
tar xf /Sources/vim-${VER_VIM}.${VER_VIMPATCH}.tar.gz &&

######################################################################### PATCH
cd vim-${VER_VIM}.${VER_VIMPATCH} &&
echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h &&

########################################################################## PREP
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
./configure \
	--prefix=/usr \
	--with-features=huge \
	--enable-multibyte &&

sed -i "s/^CFLAGS =.*/CFLAGS= ${COPTS_1} -flto -Wall -Wextra -Wshadow -Wmissing-prototypes -Wunreachable-code/" src/Makefile &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -k test > ../test.log
fi

####################################################################### INSTALL
make install &&
ln -sv vim /usr/bin/vi &&

install -v -d -m 755 /usr/share/doc/vim-${VER_VIM}.${VER_VIMPATCH} &&
cp -rv ../vim-${VER_VIM}.${VER_VIMPATCH}/{CONTRIBUTING.md,LICENSE,README*,SECURITY.md} \
	/usr/share/doc/vim-${VER_VIM}.${VER_VIMPATCH} &&
ln -sv ../../vim/vim$(echo ${VER_VIM} | sed 's/\.//')/doc \
	/usr/share/doc/vim-${VER_VIM}.${VER_VIMPATCH}/doc &&

install -v -m 644 /Build/Config/vimrc /etc/vimrc &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/vim ] ; then
	echo "ERROR: vim not installed"
	exit -1
fi

/usr/bin/vim --version | grep "^VIM - Vi IMproved ${VER_VIM}"
if [ $? != 0 ] ; then
	echo "ERROR: vim incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v src/auto/config.log ../ &&
cd ../ &&
rm -rf vim-${VER_VIM}.${VER_VIMPATCH} &&
set +x +u
