#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/which-${VER_WHICH}.tar.gz ] || exit -1
rm -rf which-${VER_WHICH} &&
tar xf /Sources/which-${VER_WHICH}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../which-${VER_WHICH}/configure \
	--prefix=/usr && 

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

install -d -v -m 755 /usr/share/doc/which-${VER_WHICH} &&
cp -v ../which-${VER_WHICH}/{AUTHORS,COPYING,EXAMPLES,NEWS,README*} \
	/usr/share/doc/which-${VER_WHICH} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/which ] ; then
	echo "ERROR: which not installed"
	exit -1
fi

/usr/bin/which --version | grep "^GNU which v${VER_WHICH}"
if [ $? != 0 ] ; then
	echo "ERROR: which incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf which-${VER_WHICH} &&
set +x +u
