#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/chrony-${VER_CHRONY}.tar.gz ] || exit -1
rm -rf chrony-${VER_CHRONY} &&
tar xf /Sources/chrony-${VER_CHRONY}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
groupadd -g 87 ntp &&
useradd -u 87 -g ntp -s /bin/false -c 'ntp' -d /var/lib/chrony -M -r ntp &&
install -v -d -m 755 -o ntp -g ntp /var/lib/chrony &&

cd chrony-${VER_CHRONY} &&

CFLAGS+="${COPTS_H} -flto" \
LDFLAGS+="${LDOPTS_H}" \
./configure --prefix=/usr \
	--bindir=/usr/sbin \
	--sysconfdir=/etc \
	--chronyrundir=/run/chrony \
	--with-pidfile=/run/chrony/chronyd.pid &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
cp -v examples/chrony.conf.example2 /etc/chrony.conf &&

install -d -v -m 755 /usr/share/doc/chrony-${VER_CHRONY} &&
cp -v {COPYING,FAQ,NEWS,README} \
	/usr/share/doc/chrony-${VER_CHRONY} &&

###################################################################### VALIDATE
if [ ! -x /usr/sbin/chronyc ] ; then
	echo "ERROR: chronyc not installed"
	exit -1
fi

/usr/sbin/chronyc --version 2>&1 | grep "version ${VER_CHRONY}"
if [ $? != 0 ] ; then
	echo "ERROR: chronyc incorrect version"
	exit -1
fi

##################################################################### CONFIGURE
rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

tar xf /Sources/blfs-bootscripts-${VER_BLFSBOOTSCRIPTS}.tar.xz &&
cd blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&
sed -i 's/ntp/chrony/g' Makefile &&
sed -i 's/ntp/chrony/g' blfs/init.d/ntpd &&
sed -i 's/-g -u chrony:chrony/-u ntp/' blfs/init.d/ntpd &&
mv blfs/init.d/ntpd blfs/init.d/chronyd &&
make install-chronyd &&
cd ../ &&

rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf chrony-${VER_CHRONY} &&
set +x +u
