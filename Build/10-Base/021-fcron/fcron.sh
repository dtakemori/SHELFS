#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/fcron-${VER_FCRON}.src.tar.gz ] || exit -1
rm -rf fcron-${VER_FCRON} &&
tar xf /Sources/fcron-${VER_FCRON}.src.tar.gz &&

######################################################################### PATCH
cd fcron-${VER_FCRON} &&
find doc -type f -exec sed -i 's|/usr/local||g' {} \; &&
cd ../ &&

########################################################################## PREP
groupadd -g 22 fcron &&
useradd -u 22 -g fcron -s /bin/false -c 'fcron' -d /dev/null -M -r fcron &&

mkdir -v build &&
cd build/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
../fcron-${VER_FCRON}/configure \
	--prefix=/usr \
	--sysconfdir=/etc \
	--localstatedir=/var \
	--with-piddir=/run \
	--with-fifodir=/run \
	--without-sendmail \
	--with-boot-install=no \
	--with-systemdsystemunitdir=no &&

######################################################################### BUILD
CFLAGS="${COPTS_1} -flto" \
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

cp -v ../fcron-${VER_FCRON}/{README,VERSION} \
	/usr/share/doc/fcron-${VER_FCRON} &&

###################################################################### VALIDATE
if [ ! -x /usr/sbin/fcron ] ; then
	echo "ERROR: fcron not installed"
	exit -1
fi

/usr/sbin/fcron -V 2>&1 | grep "^fcron ${VER_FCRON}"
if [ $? != 0 ] ; then
	echo "ERROR: fcron incorrect version"
	exit -1
fi

##################################################################### CONFIGURE
rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

tar xf /Sources/blfs-bootscripts-${VER_BLFSBOOTSCRIPTS}.tar.xz &&
cd blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&
make install-fcron &&
cd ../ &&

rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf fcron-${VER_FCRON} &&
set +x +u
