#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libmnl-${VER_LIBMNL}.tar.bz2 ] || exit -1
rm -rf libmnl-${VER_LIBMNL} &&
tar xf /Sources/libmnl-${VER_LIBMNL}.tar.bz2 &&

[ -f /Sources/libnftnl-${VER_LIBNFTNL}.tar.xz ] || exit -1
rm -rf libnftnl-${VER_LIBNFTNL} &&
tar xf /Sources/libnftnl-${VER_LIBNFTNL}.tar.xz &&

[ -f /Sources/iptables-${VER_IPTABLES}.tar.xz ] || exit -1
rm -rf iptables-${VER_IPTABLES} &&
tar xf /Sources/iptables-${VER_IPTABLES}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build-libmnl/ &&
cd build-libmnl/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
../libmnl-${VER_LIBMNL}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} V=1 &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libmnl-${VER_LIBMNL} &&
cp -v ../libmnl-${VER_LIBMNL}/{COPYING,README} \
	/usr/share/doc/libmnl-${VER_LIBMNL} &&

rm -v /usr/lib64/libmnl.la &&

cd ../ &&



########################################################################## PREP
mkdir -v build-libnftnl/ &&
cd build-libnftnl/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
../libnftnl-${VER_LIBNFTNL}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} V=1 &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libnftnl-${VER_LIBNFTNL} &&
cp -v ../libnftnl-${VER_LIBNFTNL}/COPYING \
	/usr/share/doc/libnftnl-${VER_LIBNFTNL} &&

rm -v /usr/lib64/libnftnl.la &&

cd ../ &&



########################################################################## PREP
mkdir -v build-iptables/ &&
cd build-iptables/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
../iptables-${VER_IPTABLES}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--sysconfdir=/etc \
	--enable-libipq &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} V=1 &&

########################################################################## TEST

####################################################################### INSTALL
make pkgconfigdir=/usr/lib64/pkgconfig install &&
ln -sfv xtables-legacy-multi /usr/sbin/iptables-xml &&

install -v -d -m 755 /usr/share/doc/iptables-${VER_IPTABLES} &&
cp -v ../iptables-${VER_IPTABLES}/COPYING \
	/usr/share/doc/iptables-${VER_IPTABLES} &&

rm -v /usr/lib64/lib{ip4tc,ip6tc,ipq,xtables}.la &&

cd ../ &&



###################################################################### VALIDATE
if [ ! -x /usr/sbin/iptables ] ; then
	echo "ERROR: iptables not installed"
	exit -1
fi

/usr/sbin/iptables --version | grep "^iptables v${VER_IPTABLES}"
if [ $? != 0 ] ; then
	echo "ERROR: iptables incorrect version"
	exit -1
fi

##################################################################### CONFIGURE
# rm -rf Sources/blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&
# 
# tar xf /Sources/blfs-bootscripts-${VER_BLFSBOOTSCRIPTS}.tar.xz &&
# cd blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&
# make install-iptables &&
# install -v -m700 /Build/Config/rc.iptables \
# 	/etc/rc.d/rc.iptables &&
# cd ../ &&
# 
# rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

####################################################################### CLEANUP
cp -v build-libmnl/config.log ./libmnl-config.log &&
cp -v build-libnftnl/config.log ./libnftnl-config.log &&
cp -v build-iptables/config.log ./iptables-config.log &&
rm -rf build-libmnl &&
rm -rf build-libnftnl &&
rm -rf build-iptables &&
rm -rf libmnl-${VER_LIBMNL} &&
rm -rf libnftnl-${VER_LIBNFTNL} &&
rm -rf iptables-${VER_IPTABLES} &&
set +x +u
