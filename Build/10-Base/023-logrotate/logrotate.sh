#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/logrotate-${VER_LOGROTATE}.tar.xz ] || exit -1
rm -rf logrotate-${VER_LOGROTATE} &&
tar xf /Sources/logrotate-${VER_LOGROTATE}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../logrotate-${VER_LOGROTATE}/configure \
	--prefix=/usr 

######################################################################### BUILD
make -j${MAKEJOBS:-2} V=1 &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/logrotate-${VER_LOGROTATE} &&
cp -v ../logrotate-${VER_LOGROTATE}/{COPYING,ChangeLog.md,README.md} \
	/usr/share/doc/logrotate-${VER_LOGROTATE} &&

###################################################################### VALIDATE
if [ ! -x /usr/sbin/logrotate ] ; then
	echo "ERROR: logrotate not installed"
	exit -1
fi

/usr/sbin/logrotate --version 2>&1 | grep "^logrotate ${VER_LOGROTATE}"
if [ $? != 0 ] ; then
	echo "ERROR: logrotate incorrect version"
	exit -1
fi

##################################################################### CONFIGURE
install -v -m644 /Build/Config/logrotate.conf \
	/etc/logrotate.conf &&

fcrontab /Build/Config/logrotate.fcron &&

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf logrotate-${VER_LOGROTATE} &&
set +x +u
