#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/openssh-${VER_OPENSSH}.tar.gz ] || exit -1
rm -rf openssh-${VER_OPENSSH} &&
tar xf /Sources/openssh-${VER_OPENSSH}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
groupadd -g 50 sshd &&
useradd -u 50 -g sshd -s /bin/false -c 'sshd PrivSep' -d /var/jail/sshd sshd &&
install -v -d -m 700 -o root -g sys /var/jail/sshd &&

mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${COPTS_H}" \
../openssh-${VER_OPENSSH}/configure \
	--prefix=/usr \
	--sysconfdir=/etc/ssh \
	--libexecdir=/usr/libexec/openssh \
	--with-pam \
	--with-pid-dir=/run \
	--with-privsep-path=/var/jail/sshd &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k tests > ../test.log
fi

####################################################################### INSTALL
make install &&

sed -i -e 's/^\(#UsePAM.*\)/\1\nUsePAM yes/' /etc/ssh/sshd_config &&

install -v -d -m 755 /usr/share/doc/openssh-${VER_OPENSSH} &&
cp -v ../openssh-${VER_OPENSSH}/{CREDITS,ChangeLog,LICENCE,OVERVIEW,PROTOCOL*} \
	../openssh-${VER_OPENSSH}/{README*,SECURITY.md,TODO} \
	/usr/share/doc/openssh-${VER_OPENSSH} &&

cp -v /Build/Config/pam.d/sshd /etc/pam.d/ &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/ssh ] ; then
	echo "ERROR: ssh not installed"
	exit -1
fi

/usr/bin/ssh -V 2>&1 | grep "^OpenSSH_${VER_OPENSSH}"
if [ $? != 0 ] ; then
	echo "ERROR: ssh incorrect version"
	exit -1
fi

cd ../ &&

##################################################################### CONFIGURE
rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

tar xf /Sources/blfs-bootscripts-${VER_BLFSBOOTSCRIPTS}.tar.xz &&
cd blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&
make install-sshd &&

cd ../ &&

rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&

####################################################################### CLEANUP
cp -v build/config.log . &&
rm -rf build &&
rm -rf openssh-${VER_OPENSSH} &&
set +x +u
