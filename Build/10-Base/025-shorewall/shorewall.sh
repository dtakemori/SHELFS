#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/shorewall-core-${VER_SHOREWALL}.tar.bz2 ] || exit -1
rm -rf shorewall-core-${VER_SHOREWALL} &&
tar xf /Sources/shorewall-core-${VER_SHOREWALL}.tar.bz2 &&

[ -f /Sources/shorewall-${VER_SHOREWALL}.tar.bz2 ] || exit -1
rm -rf shorewall-${VER_SHOREWALL} &&
tar xf /Sources/shorewall-${VER_SHOREWALL}.tar.bz2 &&

[ -f /Sources/shorewall-init-${VER_SHOREWALL}.tar.bz2 ] || exit -1
rm -rf shorewall-init-${VER_SHOREWALL} &&
tar xf /Sources/shorewall-init-${VER_SHOREWALL}.tar.bz2 &&

######################################################################### PATCH

########################################################################## PREP
cd shorewall-core-${VER_SHOREWALL}/ &&
./configure shorewallrc.default MANDIR="/usr/share/man" &&

####################################################################### INSTALL
./install.sh &&

install -v -d -m 755 /usr/share/doc/shorewall-${VER_SHOREWALL} &&
cp -v COPYING *.txt /usr/share/doc/shorewall-${VER_SHOREWALL} &&



########################################################################## PREP
cd ../shorewall-${VER_SHOREWALL}/ &&
./configure shorewallrc.default MANDIR="/usr/share/man" &&

####################################################################### INSTALL
./install.sh &&
cp -v Samples/Universal/{interfaces,params,policy,rules,shorewall.conf,zones} \
	/etc/shorewall/ &&
sed -i 's|/log/messages|/log/kern.log|' /etc/shorewall/shorewall.conf &&

cp -rv Samples /usr/share/doc/shorewall-${VER_SHOREWALL} &&


########################################################################## PREP
cd ../shorewall-init-${VER_SHOREWALL}/ &&
./configure shorewallrc.default &&

####################################################################### INSTALL
./install.sh 

ln -sv ../init.d/shorewall /etc/rc.d/rc3.d/S19shorewall &&
ln -sv ../init.d/shorewall /etc/rc.d/rc4.d/S19shorewall && 
ln -sv ../init.d/shorewall /etc/rc.d/rc5.d/S19shorewall &&

###################################################################### VALIDATE
if [ ! -x /sbin/shorewall ] ; then
	echo "ERROR: shorewall is not installed"
	exit -1
fi

/sbin/shorewall version | grep "^${VER_SHOREWALL}$"
if [ $? != 0 ] ; then
	echo "ERROR: shorewall is wrong version"
	exit -1
fi

##################################################################### CONFIGURE

####################################################################### CLEANUP
cd ../ &&
rm -rf shorewall-core-${VER_SHOREWALL} &&
rm -rf shorewall-${VER_SHOREWALL} &&
rm -rf shorewall-init-${VER_SHOREWALL} &&
set +x +u
