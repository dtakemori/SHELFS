#!/bin/bash


echo "===> TCL Start" &&
cd 001-tcl &&
/usr/bin/time -vao build.log ./tcl.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== TCL End" &&


echo "===> Expect Start" &&
cd 002-expect &&
/usr/bin/time -vao build.log ./expect.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== TCL End" &&


echo "===> DejaGNU Start" &&
cd 003-dejagnu &&
/usr/bin/time -vao build.log ./dejagnu.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== DejaGNU End" &&


echo "===> Libarchive Start" &&
cd 004-libarchive &&
/usr/bin/time -vao build.log ./libarchive.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libarchive End" &&


echo "===> Libpipeline Start" &&
cd 005-libpipeline &&
/usr/bin/time -vao build.log ./libpipeline.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libpipeline End" &&


echo "===> LibUSB1 Start" &&
cd 006-libusb1 &&
/tools/bin/time -vao build.log ./libusb1.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== LibUSB1 End" &&


echo "===> Libuv Start" &&
cd 007-libuv &&
/usr/bin/time -vao build.log ./libuv.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libuv End" &&


echo "===> Groff Start" &&
cd 008-groff &&
/usr/bin/time -vao build.log ./groff.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Groff End" &&


echo "===> Man Start" &&
cd 009-man &&
/usr/bin/time -vao build.log ./man.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Man End" &&


echo "===> Texinfo Start" &&
cd 010-texinfo &&
/usr/bin/time -vao build.log ./texinfo.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Texinfo End" &&


echo "===> Gptfdisk Start" &&
cd 011-gptfdisk &&
/usr/bin/time -vao build.log ./gptfdisk.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Gptfdisk End" &&


echo "===> Less Start" &&
cd 012-less &&
/tools/bin/time -vao build.log ./less.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Less End" &&


echo "===> Parted Start" &&
cd 013-parted &&
/usr/bin/time -vao build.log ./parted.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Parted End" &&


echo "===> DMIdecode Start" &&
cd 014-dmidecode &&
/usr/bin/time -vao build.log ./dmidecode.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== DMIdecode End" &&


echo "===> PCIutils Start" &&
cd 015-pciutils &&
/tools/bin/time -vao build.log ./pciutils.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== PCIutils End" &&


echo "===> USButils Start" &&
cd 016-usbutils &&
/tools/bin/time -vao build.log ./usbutils.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== USButils End" &&


echo "===> Nano Start" &&
cd 017-nano &&
/tools/bin/time -vao build.log ./nano.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Nano End" &&


echo "===> Vim Start" &&
cd 018-vim &&
/tools/bin/time -vao build.log ./vim.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Vim End" &&


echo "===> Which Start" &&
cd 019-which &&
/tools/bin/time -vao build.log ./which.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Which End" &&


echo "===> Chrony Start" &&
cd 020-chrony &&
/usr/bin/time -vao build.log ./chrony.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Chrony End" &&


echo "===> Fcron Start" &&
cd 021-fcron &&
/usr/bin/time -vao build.log ./fcron.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Fcron End" &&


echo "===> Iptables Start" &&
cd 022-iptables &&
/usr/bin/time -vao build.log ./iptables.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Iptables End" &&


echo "===> Logrotate Start" &&
cd 023-logrotate &&
/usr/bin/time -vao build.log ./logrotate.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Logrotate End" &&


echo "===> OpenSSH Start" &&
cd 024-openssh &&
/usr/bin/time -vao build.log ./openssh.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== OpenSSH End" &&


echo "===> Shorewall Start" &&
cd 025-shorewall &&
/usr/bin/time -vao build.log ./shorewall.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Shorewall End" 

