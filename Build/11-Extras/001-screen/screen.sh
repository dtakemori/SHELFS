#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/screen-${VER_SCREEN}.tar.gz ] || exit -1
rm -rf screen-${VER_SCREEN} &&
tar xf /Sources/screen-${VER_SCREEN}.tar.gz &&

######################################################################### PATCH
cd screen-${VER_SCREEN} &&
./autogen.sh &&

sed -i -e "s%/usr/local/etc/screenrc%/etc/screenrc%" {etc,doc}/* &&
cd ../ &&

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1} -flto" \
../screen-${VER_SCREEN}/configure \
	--prefix=/usr \
	--infodir=/usr/share/info \
	--mandir=/usr/share/man \
	--with-socket-dir=/run/screen \
	--with-pty-group=5 \
	--with-sys-screenrc=/etc/screenrc &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
install -v -m 644 ../screen-${VER_SCREEN}/etc/etcscreenrc /etc/screenrc &&

install -v -d -m 755 /usr/share/doc/screen-${VER_SCREEN} &&
cp -v ../screen-${VER_SCREEN}/{COPYING,ChangeLog,FAQ,HACKING,NEWS*,README,TODO} \
	/usr/share/doc/screen-${VER_SCREEN} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/screen ] ; then
	echo "ERROR: screen is not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf screen-${VER_SCREEN} &&
set +x +u
