#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/dash-${VER_DASH}.tar.gz ] || exit -1
rm -rf dash-${VER_DASH} &&
tar xf /Sources/dash-${VER_DASH}.tar.gz &&

[ -f /Sources/libedit-${VER_LIBEDIT}.tar.gz ] || exit -1
rm -rf libedit-${VER_LIBEDIT} &&
tar xf /Sources/libedit-${VER_LIBEDIT}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build-libedit &&
cd build-libedit/ &&
CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H} -flto" \
../libedit-${VER_LIBEDIT}/configure \
	--prefix=$(pwd)/../install \
	--enable-static \
	--disable-shared &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/libedit-${VER_LIBEDIT} &&
cp -v ../libedit-${VER_LIBEDIT}/{COPYING,ChangeLog,THANKS} \
	/usr/share/doc/libedit-${VER_LIBEDIT} &&

cd ../ &&



######################################################################### PATCH

########################################################################## PREP
mkdir -v build-dash &&
cd build-dash/ &&
CFLAGS="${COPTS_1} -mharden-sls=all -fstack-protector-all -fstrict-flex-arrays=1 -fzero-call-used-regs=all -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=3 -flto -I$(pwd)/../install/include" \
LDFLAGS="${LDOPTS_1} -flto -L$(pwd)/../install/lib -lncurses" \
../dash-${VER_DASH}/configure \
	--bindir=/bin \
	--mandir=/usr/share/man \
	--with-libedit &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -v make -j${MAKEJOBS:-2} -k check
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/dash-${VER_DASH} &&
cp -v ../libedit-${VER_LIBEDIT}/{COPYING,ChangeLog} \
	/usr/share/doc/dash-${VER_DASH} &&

###################################################################### VALIDATE
if [ ! -x /bin/dash ] ; then
	echo "ERROR: dash is not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
cp -v build-libedit/config.log libedit-config.log &&
cp -v build-dash/config.log dash-config.log &&
rm -rf install &&
rm -rf build-dash &&
rm -rf build-libedit &&
rm -rf dash-${VER_DASH} &&
rm -rf libedit-${VER_LIBEDIT} &&
set +x +u
