#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/zsh-${VER_ZSH}.tar.xz ] || exit -1
rm -rf zsh-${VER_ZSH} &&
tar xf /Sources/zsh-${VER_ZSH}.tar.xz &&

######################################################################### PATCH
cd zsh-${VER_ZSH}/ &&
sed -i -e 's/^main/int &/' -e 's/exit(/return(/' aczsh.m4 configure.ac &&
sed -i 's/test = /&(char**)/' configure.ac &&
autoconf &&

cd ../ &&

########################################################################## PREP
cd zsh-${VER_ZSH}/ &&

### zsh uses mktemp; remove --fatal-warnings from LDFLAGS
CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H} -Wl,--no-fatal-warnings -flto" \
../zsh-${VER_ZSH}/configure \
	--prefix=/usr \
	--bindir=/bin \
	--libdir=/lib64 \
	--sysconfdir=/etc/zsh \
	--enable-etcdir=/etc/zsh \
	--enable-cap \
	--enable-pcre &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&
makeinfo ../zsh-${VER_ZSH}/Doc/zsh.texi --plaintext -o Doc/zsh.txt &&
makeinfo ../zsh-${VER_ZSH}/Doc/zsh.texi --html      -o Doc/html &&

install -v -d -m 755 /usr/share/doc/zsh-${VER_ZSH} &&
cp -vr Doc/html /usr/share/doc/zsh-${VER_ZSH} &&
cp -v Doc/zsh.txt /usr/share/doc/zsh-${VER_ZSH} &&
cp -v ../zsh-${VER_ZSH}/{ChangeLog,FEATURES,LICENCE,MACHINES,META-FAQ,NEWS,README} \
	/usr/share/doc/zsh-${VER_ZSH} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

###################################################################### VALIDATE
if [ ! -x /bin/zsh ] ; then
	echo "ERROR: zsh is not installed"
	exit -1
fi

/bin/zsh --version | grep "^zsh ${VER_ZSH}"
if [ $? != 0 ] ; then
	echo "ERROR: zsh is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf zsh-${VER_ZSH} &&
set +x +u
