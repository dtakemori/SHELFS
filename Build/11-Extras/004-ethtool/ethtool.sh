#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/ethtool-${VER_ETHTOOL}.tar.xz ] || exit -1
rm -rf ethtool-${VER_ETHTOOL} &&
tar xf /Sources/ethtool-${VER_ETHTOOL}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../ethtool-${VER_ETHTOOL}/configure \
	--prefix=/usr &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/ethtool-${VER_ETHTOOL} &&
cp -v ../ethtool-${VER_ETHTOOL}/{AUTHORS,COPYING,ChangeLog,LICENSE,NEWS,README} \
	/usr/share/doc/ethtool-${VER_ETHTOOL} &&

###################################################################### VALIDATE
if [ ! -x /usr/sbin/ethtool ] ; then
	echo "ERROR: ethtool is not installed"
	exit -1
fi

/usr/sbin/ethtool --version | grep "^ethtool version ${VER_ETHTOOL}$"
if [ $? != 0 ] ; then
	echo "ERROR: ethtool is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf ethtool-${VER_ETHTOOL} &&
set +x +u
