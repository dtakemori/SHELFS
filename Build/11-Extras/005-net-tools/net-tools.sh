#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/net-tools-${VER_NETTOOLS}.tar.xz ] || exit -1
rm -rf net-tools-${VER_NETTOOLS} &&
tar xf /Sources/net-tools-${VER_NETTOOLS}.tar.xz &&

######################################################################### PATCH
cd net-tools-${VER_NETTOOLS} &&
patch -p1 -i /Patches/net-tools-2.10_lfs.patch &&

########################################################################## PREP
touch config.h &&

######################################################################### BUILD
AR=gcc-ar \
CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/net-tools-${VER_NETTOOLS} &&
cp -v {COPYING,README,THANKS,TODO} /usr/share/doc/net-tools-${VER_NETTOOLS} &&

###################################################################### VALIDATE
if [ ! -x /bin/netstat ] ; then
	echo "ERROR: netstat not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf net-tools-${VER_NETTOOLS} &&
set +x +u
