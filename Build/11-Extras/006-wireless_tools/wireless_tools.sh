#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/wireless_tools.${VER_WIRELESSTOOLS}.tar.gz ] || exit -1
rm -rf wireless_tools.${VER_WIRELESSTOOLS} &&
tar xf /Sources/wireless_tools.${VER_WIRELESSTOOLS}.tar.gz &&

######################################################################### PATCH
cd wireless_tools.${VER_WIRELESSTOOLS} &&
patch -Np1 -i /Patches/wireless_tools-29-fix_iwlist_scanning-1.patch &&
cd ../ &&

########################################################################## PREP

######################################################################### BUILD
cd wireless_tools.${VER_WIRELESSTOOLS} &&
make -j${MAKEJOBS:-2} \
	CFLAGS+="${COPTS_H} -flto" &&

########################################################################## TEST

####################################################################### INSTALL
make PREFIX=/usr INSTALL_LIB=/usr/lib64 INSTALL_MAN=/usr/share/man install &&

install -v -d -m 755 /usr/share/doc/wireless_tools.${VER_WIRELESSTOOLS} &&
cp -v {*.txt,CHANGELOG.h,COPYING,README*} \
	/usr/share/doc/wireless_tools.${VER_WIRELESSTOOLS} &&

###################################################################### VALIDATE
if [ ! -x /usr/sbin/iwconfig ] ; then
	echo "ERROR: iwconfig is not installed"
	exit -1
fi

/usr/sbin/iwconfig --version | grep "version ${VER_WIRELESSTOOLS}$"
if [ $? != 0 ] ; then
	echo "ERROR: iwconfig is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf wireless_tools.${VER_WIRELESSTOOLS} &&
set +x +u
