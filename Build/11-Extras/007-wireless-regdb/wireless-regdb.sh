#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/wireless-regdb-${VER_WIRELESSREGDB}.tar.xz ] || exit -1
rm -rf wireless-regdb-${VER_WIRELESSREGDB} &&
tar xf /Sources/wireless-regdb-${VER_WIRELESSREGDB}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD

########################################################################## TEST

####################################################################### INSTALL
cd wireless-regdb-${VER_WIRELESSREGDB} &&
install -v -m 644 regulatory.db regulatory.db.p7s /lib/firmware &&

install -v -d -m 755 /usr/share/doc/wireless-regdb-${VER_WIRELESSREGDB} &&
cp -v {CONTRIBUTING,LICENSE,README} \
	/usr/share/doc/wireless-regdb-${VER_WIRELESSREGDB} &&

###################################################################### VALIDATE
if [ ! -f /lib/firmware/regulatory.db ] ; then
	echo "ERROR: regulatory.db is not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf wireless-regdb-${VER_WIRELESSREGDB} &&
set +x +u
