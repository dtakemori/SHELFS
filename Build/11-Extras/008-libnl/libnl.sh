#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libnl-${VER_LIBNL}.tar.gz ] || exit -1
rm -rf libnl-${VER_LIBNL} &&
tar xf /Sources/libnl-${VER_LIBNL}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../libnl-${VER_LIBNL}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--sysconfdir=/etc \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
ldconfig &&

install -d -v -m 755 /usr/share/doc/libnl-${VER_LIBNL} &&
cp -v ../libnl-${VER_LIBNL}/{COPYING,ChangeLog} \
	/usr/share/doc/libnl-${VER_LIBNL} &&

rm -v /usr/lib64/libnl{,-cli,-genl,-idiag,-nf,-route,-xfrm}-3.la &&
rm -v /usr/lib64/libnl/cli/cls/{basic,cgroup}.la &&
rm -v /usr/lib64/libnl/cli/qdisc/{bfifo,blackhole,fq_codel,hfsc,htb,ingress,pfifo,plug}.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/nl-link-list ] ; then
	echo "ERROR: nl-link-list is not installed"
	exit -1
fi

/usr/bin/nl-link-list --version | grep "^libnl tools version ${VER_LIBNL}"
if [ $? != 0 ] ; then
	echo "ERROR: nl-link-list is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd .. &&
rm -rf build &&
rm -rf libnl-${VER_LIBNL} &&
set +x +u
