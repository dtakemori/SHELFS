#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/iw-${VER_IW}.tar.xz ] || exit -1
rm -rf iw-${VER_IW} &&
tar xf /Sources/iw-${VER_IW}.tar.xz &&

######################################################################### PATCH
cd iw-${VER_IW} &&
sed -i "/INSTALL.*gz/s/.gz//" Makefile &&
cd ../ &&

########################################################################## PREP

######################################################################### BUILD
cd iw-${VER_IW} &&
CFLAGS+="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H} -flto" \
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make SBINDIR=/sbin install &&

install -v -d -m 755 /usr/share/doc/iw-${VER_IW} &&
cp -v {CONTRIBUTING,COPYING,README} /usr/share/doc/iw-${VER_IW} &&

###################################################################### VALIDATE
if [ ! -x /sbin/iw ] ; then
	echo "ERROR: iw is not installed"
	exit -1
fi

/sbin/iw --version | grep "^iw version ${VER_IW}$"
if [ $? != 0 ] ; then
	echo "ERROR: iw is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd .. &&
rm -rf iw-${VER_IW} &&
set +x +u
