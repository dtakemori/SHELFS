#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/wpa_supplicant-${VER_WPASUPPLICANT}.tar.gz ] || exit -1
rm -rf wpa_supplicant-${VER_WPASUPPLICANT} &&
tar xf /Sources/wpa_supplicant-${VER_WPASUPPLICANT}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cp -v /Build/Config/wpa_supplicant.config \
	wpa_supplicant-${VER_WPASUPPLICANT}/wpa_supplicant/.config &&

######################################################################### BUILD
cd wpa_supplicant-${VER_WPASUPPLICANT} &&
cd wpa_supplicant &&
CFLAGS+="${COPTS_H} -flto" \
LDFLAGS+="${LDOPTS_H} -flto" \
make -j${MAKEJOBS:-2} \
	BINDIR=/sbin \
	LIBDIR=/lib64 &&

########################################################################## TEST

####################################################################### INSTALL
install -v -m 755 wpa_{cli,passphrase,supplicant} /sbin/ &&

install -v -m 644 doc/docbook/wpa_supplicant.conf.5 /usr/share/man/man5/ &&
install -v -m 644 doc/docbook/wpa_{cli,passphrase,supplicant}.8 \
	/usr/share/man/man8 &&

install -v -m 644 dbus/fi.w1.wpa_supplicant1.service \
	/usr/share/dbus-1/system-services/ &&
install -v -d -m 755 /etc/dbus-1/system.d &&
install -v -m 644 dbus/dbus-wpa_supplicant.conf \
	/etc/dbus-1/system.d/wpa_supplicant.conf &&

install -v -d -m 755 /usr/share/doc/wpa_supplicant-${VER_WPASUPPLICANT} &&
cp -v {ChangeLog,README*} ../{CONTRIBUTIONS,COPYING} \
	/usr/share/doc/wpa_supplicant-${VER_WPASUPPLICANT} &&

cd ../.. &&
tar xf /Sources/blfs-bootscripts-${VER_BLFSBOOTSCRIPTS}.tar.xz &&
cd blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&
make install-service-wpa &&
cd ../ &&

###################################################################### VALIDATE
if [ ! -x /sbin/wpa_cli ] ; then
	echo "ERROR: wpa_cli is not installed"
	exit -1
fi

/sbin/wpa_cli -v | grep "^wpa_cli v${VER_WPASUPPLICANT}$"
if [ $? != 0 ] ; then
	echo "ERROR: wpa_cli is wrong version"
	exit -1
fi

####################################################################### CLEANUP
rm -rf blfs-bootscripts-${VER_BLFSBOOTSCRIPTS} &&
rm -rf wpa_supplicant-${VER_WPASUPPLICANT} &&
set +x +u
