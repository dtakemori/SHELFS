#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/nettle-${VER_NETTLE}.tar.gz ] || exit -1
rm -rf nettle-${VER_NETTLE} &&
tar xf /Sources/nettle-${VER_NETTLE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

### --disable-openssl      Do not include openssl glue in the benchmark program
CFLAGS="${COPTS_1} -flto"
LDFLAGS="${LDOPTS_1} -flto" \
../nettle-${VER_NETTLE}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--enable-shared \
	--disable-openssl \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/nettle-${VER_NETTLE} &&
cp -v ../nettle-${VER_NETTLE}/{AUTHORS,CONTRIBUTING.md,COPYING*,ChangeLog,NEWS,README} \
	/usr/share/doc/nettle-${VER_NETTLE} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/nettle-hash ] ; then
	echo "ERROR: nettle-hash is not installed"
	exit -1
fi

/usr/bin/nettle-hash --version | grep "^nettle-hash (nettle ${VER_NETTLE})$"
if [ $? != 0 ] ; then
	echo "ERROR: nettle-hash is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf nettle-${VER_NETTLE} &&
set +x +u
