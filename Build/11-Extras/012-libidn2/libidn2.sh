#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libidn2-${VER_LIBIDN2}.tar.gz ] || exit -1
rm -rf libidn2-${VER_LIBIDN2} &&
tar xf /Sources/libidn2-${VER_LIBIDN2}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto"
LDFLAGS="${LDOPTS_1} -flto" \
../libidn2-${VER_LIBIDN2}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--enable-shared \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libidn-${VER_LIBIDN2} &&
cp -v ../libidn2-${VER_LIBIDN2}/{AUTHORS,CONTRIBUTING.md,COPYING*,ChangeLog,NEWS,README*} \
	/usr/share/doc/libidn-${VER_LIBIDN2} &&

rm -v /usr/lib64/libidn2.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/idn2 ] ; then
	echo "ERROR: idn2 is not installed"
	exit -1
fi

/usr/bin/idn2 --version | grep "^idn2 (Libidn2) ${VER_LIBIDN2}$"
if [ $? != 0 ] ; then
	echo "ERROR: idn2 is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libidn2-${VER_LIBIDN2} &&
set +x +u
