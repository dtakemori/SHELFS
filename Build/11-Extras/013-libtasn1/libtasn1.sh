#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libtasn1-${VER_LIBTASN1}.tar.gz ] || exit -1
rm -rf libtasn1-${VER_LIBTASN1} &&
tar xf /Sources/libtasn1-${VER_LIBTASN1}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto"
LDFLAGS="${LDOPTS_1}" \
../libtasn1-${VER_LIBTASN1}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libtasn1-${VER_LIBTASN1} &&
cp -v ../libtasn1-${VER_LIBTASN1}/{AUTHORS,CONTRIBUTING.md,COPYING} \
	../libtasn1-${VER_LIBTASN1}/{ChangeLog,NEWS,README*,THANKS} \
	/usr/share/doc/libtasn1-${VER_LIBTASN1} &&

rm -v /usr/lib64/libtasn1.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/libtasn1.h ] ; then
	echo "ERROR: libtasn1.h header not installed"
	exit -1
fi

if [ ! -f /usr/lib64/libtasn1.so ] ; then
	echo "ERROR: libtasn1.so library not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libtasn1-${VER_LIBTASN1} &&
set +x +u
