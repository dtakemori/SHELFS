#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/p11-kit-${VER_P11KIT}.tar.xz ] || exit -1
rm -rf p11-kit-${VER_P11KIT} &&
tar xf /Sources/p11-kit-${VER_P11KIT}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto"
LDFLAGS="${LDOPTS_1}" \
../p11-kit-${VER_P11KIT}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--sysconfdir=/etc \
	--with-trust-paths=/etc/pki/anchors \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/p11-kit-${VER_P11KIT} &&
cp -v ../p11-kit-${VER_P11KIT}/{AUTHORS,CONTRIBUTING.md,COPYING,ChangeLog,NEWS,README} \
	/usr/share/doc/p11-kit-${VER_P11KIT} &&

rm -v /usr/lib64/libp11-kit.la &&
rm -v /usr/lib64/pkcs11/p11-kit-{client,trust}.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/p11-kit ] ; then
	echo "ERROR: p11-kit is not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf p11-kit-${VER_P11KIT} &&
set +x +u
