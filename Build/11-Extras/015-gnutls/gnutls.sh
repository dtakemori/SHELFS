#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/gnutls-${VER_GNUTLS}.tar.xz ] || exit -1
rm -rf gnutls-${VER_GNUTLS} &&
tar xf /Sources/gnutls-${VER_GNUTLS}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
cd gnutls-${VER_GNUTLS} &&

CFLAGS="${COPTS_1} -flto"
LDFLAGS="${LDOPTS_1}" \
./configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--docdir=/usr/share/doc/gnutls-${VER_GNUTLS} \
	--with-default-trust-store-pkcs11="pkcs11:" \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

cp -v {AUTHORS,CONTRIBUTING.md,ChangeLog,LICENSE,NEWS,README.md,RELEASES.md,THANKS} \
	/usr/share/doc/gnutls-${VER_GNUTLS} &&

rm -v /usr/lib64/libgnutls{,xx}.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/gnutls-cli ] ; then
	echo "ERROR: gnutls-cli is not installed"
	exit -1
fi

/usr/bin/gnutls-cli --version | grep "^gnutls-cli ${VER_GNUTLS}$"
if [ $? != 0 ] ; then
	echo "ERROR: gnutls-cli is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf gnutls-${VER_GNUTLS} &&
set +x +u
