#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/bind-${VER_BIND}.tar.xz ] || exit -1
rm -rf bind-${VER_BIND} &&
tar xf /Sources/bind-${VER_BIND}.tar.xz &&

[ -f /Sources/userspace-rcu-${VER_USERSPACE_RCU}.tar.bz2 ] || exit -1
rm -rf userspace-rcu-${VER_USERSPACE_RCU} &&
tar xf /Sources/userspace-rcu-${VER_USERSPACE_RCU}.tar.bz2 &&

######################################################################### PATCH

########################################################################## PREP
cd userspace-rcu-${VER_USERSPACE_RCU} &&

mkdir -v build &&
cd build/ &&
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
../configure --prefix=$(pwd)/../../install \
	--libdir=$(pwd)/../../install/lib64 \
	--enable-static \
	--disable-shared \
	--with-pic &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ; then
  /tools/bin/time -v make -j${MAKEJOBS:-2} -k check
fi

####################################################################### INSTALL
make install &&

cd ../../ &&

########################################################################## PREP
cd bind-${VER_BIND}/ &&

CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
LIBURCU_CFLAGS="-I$(pwd)/../install/include" \
LIBURCU_LIBS="-L$(pwd)/../install/lib64 -lurcu -lurcu-cds" \
../bind-${VER_BIND}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--with-readline=readline \
	--disable-doh &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} -C lib &&
make -j${MAKEJOBS:-2} -C bin/dig &&

########################################################################## TEST

####################################################################### INSTALL
make -C lib install &&
make -C bin/dig install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/bind-${VER_BIND} &&
cp -v ../bind-${VER_BIND}/{AUTHORS,CHANGES,COPYING,COPYRIGHT,ChangeLog,LICENSE,NEWS,*.md} \
	/usr/share/doc/bind-${VER_BIND} &&

rm -v /usr/lib64/lib{bind9,isccfg,dns,irs,isc,isccc,ns}.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/dig ] ; then
	echo "ERROR: dig not installed"
	exit -1
fi

/usr/bin/dig -v 2>&1 | grep "^DiG ${VER_BIND}$"
if [ $? != 0 ] ; then
	echo "ERROR: dig incorrect version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf install &&
rm -rf bind-${VER_BIND} &&
rm -rf userspace-rcu-${VER_USERSPACE_RCU} &&
set +x +u
