#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/curl-${VER_CURL}.tar.xz ] || exit -1
rm -rf curl-${VER_CURL} &&
tar xf /Sources/curl-${VER_CURL}.tar.xz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
../curl-${VER_CURL}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--disable-static \
	--enable-threaded-resolver \
	--with-ca-path=/etc/ssl/certs \
	--with-openssl \
	--without-libpsl &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 2 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ldconfig &&

rm -rf ../curl-${VER_CURL}/docs/examples.deps &&
find ../curl-${VER_CURL}/docs \( -name Makefile\* -o \
	-name \*.1 -o \
	-name \*.3 -o \
	-name CMakeLists.txt \) -delete &&

install -v -d -m 755 /usr/share/doc/curl-${VER_CURL} &&
cp -vr ../curl-${VER_CURL}/docs /usr/share/doc/curl-${VER_CURL} &&
cp -v ../curl-${VER_CURL}/{CHANGES,COPYING,README,RELEASE-NOTES} \
	/usr/share/doc/curl-${VER_CURL} &&

rm -v /usr/lib64/libcurl.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/curl ] ; then
	echo "ERROR: curl is not installed"
	exit -1
fi

/usr/bin/curl --version | grep "^curl ${VER_CURL}"
if [ $? != 0 ] ; then
	echo "ERROR: curl is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf curl-${VER_CURL} &&
set +x +u
