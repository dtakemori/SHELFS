#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/lftp-${VER_LFTP}.tar.gz ] || exit -1
rm -rf lftp-${VER_LFTP} &&
tar xf /Sources/lftp-${VER_LFTP}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build &&
cd build &&

AR=gcc-ar \
RANLIB=gcc-ranlib \
CFLAGS="${COPTS_H}" \
CXXFLAGS="${COPTS_H}" \
LDFLAGS="${LDOPTS_H}" \
../lftp-${VER_LFTP}/configure \
	--prefix=/usr \
	--enable-shared=no \
	--enable-static=no &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
# if [ ${CONF_TEST} -ge 2 ] ;  then
#   /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
# fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/lftp-${VER_LFTP} &&
cp -v ../lftp-${VER_LFTP}/{AUTHORS,BUGS,COPYING,ChangeLog,FAQ,FEATURES} \
	../lftp-${VER_LFTP}/{MIRRORS,NEWS,README*,THANKS,TODO} \
	/usr/share/doc/lftp-${VER_LFTP} &&

rm -v /usr/lib/liblftp-{jobs,tasks}.{a,la} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/lftp ] ; then
	echo "ERROR: lftp is not installed"
	exit -1
fi

/usr/bin/lftp -v | grep "^LFTP | Version ${VER_LFTP}"
if [ $? != 0 ] ; then
	echo "ERROR: lftp is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf lftp-${VER_LFTP} &&
set +x +u
