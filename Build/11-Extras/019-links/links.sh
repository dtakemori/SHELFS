#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/links-${VER_LINKS}.tar.bz2 ] || exit -1
rm -rf links-${VER_LINKS} &&
tar xf /Sources/links-${VER_LINKS}.tar.bz2 &&

######################################################################### PATC
cd links-${VER_LINKS} &&
autoconf &&

cd ../ &&

########################################################################## PREP
mkdir -v build &&
cd build &&

CFLAGS="${COPTS_H} -flto -fno-strict-aliasing -fstrict-flex-arrays=1" \
LDFLAGS="${LDOPTS_H}" \
../links-${VER_LINKS}/configure \
	--prefix=/usr \
	--mandir=/usr/share/man \
	--without-x \
	--with-ssl &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/links-${VER_LINKS} &&
cp -v ../links-${VER_LINKS}/{AUTHORS,BRAILLE_HOWTO,COPYING,ChangeLog} \
	../links-${VER_LINKS}/{KEYS,NEWS,README,SITES} \
	/usr/share/doc/links-${VER_LINKS} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/links ] ; then
	echo "ERROR: links is not installed"
	exit -1
fi

/usr/bin/links -version | grep "^Links ${VER_LINKS}$"
if [ $? != 0 ] ; then
	echo "ERROR: links is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf links-${VER_LINKS} &&
set +x +u
