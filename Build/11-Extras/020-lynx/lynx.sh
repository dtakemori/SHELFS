#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/lynx${VER_LYNX}.tar.bz2 ] || exit -1
rm -rf lynx${VER_LYNX} &&
tar xf /Sources/lynx${VER_LYNX}.tar.bz2 &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build &&
cd build &&

CFLAGS="${COPTS_H} -flto -fno-strict-aliasing" \
LDFLAGS="${LDOPTS_H}" \
../lynx${VER_LYNX}/configure \
	--prefix=/usr \
	--sysconfdir=/etc/lynx \
	--datadir=/usr/share/doc/lynx-${VER_LYNX} \
	--with-ssl \
	--with-bzlib \
	--with-zlib \
	--with-screen=ncursesw \
	--without-x \
	--enable-locale-charset &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install-full &&

cp -v ../lynx${VER_LYNX}/{AUTHORS,CHANGES,COPYHEADER,COPYHEADER.asc,COPYING} \
	../lynx${VER_LYNX}/{COPYING.asc,INSTALLATION,PROBLEMS,README} \
	/usr/share/doc/lynx-${VER_LYNX} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/lynx ] ; then
	echo "ERROR: lynx is not installed"
	exit -1
fi

/usr/bin/lynx --version | grep "^Lynx Version ${VER_LYNX}"
if [ $? != 0 ] ; then
	echo "ERROR: lynx is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf lynx${VER_LYNX} &&
set +x +u
