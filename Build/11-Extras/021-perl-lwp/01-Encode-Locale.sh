#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Encode-Locale-${VER_PERL_ENCODELOCALE}.tar.gz ] || exit -1
rm -rf Encode-Locale-${VER_PERL_ENCODELOCALE} &&
tar xf /Sources/Encode-Locale-${VER_PERL_ENCODELOCALE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Encode-Locale-${VER_PERL_ENCODELOCALE} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Encode-Locale-${VER_PERL_ENCODELOCALE} &&
cp -v {Changes,README} /usr/share/doc/Encode-Locale-${VER_PERL_ENCODELOCALE} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Encode/Locale.pm ] ; then
	echo "ERROR: Encode::Locale not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Encode-Locale-${VER_PERL_ENCODELOCALE} &&

set +x +u
