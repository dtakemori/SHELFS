#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Try-Tiny-${VER_PERL_TRYTINY}.tar.gz ] || exit -1
rm -rf Try-Tiny-${VER_PERL_TRYTINY} &&
tar xf /Sources/Try-Tiny-${VER_PERL_TRYTINY}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Try-Tiny-${VER_PERL_TRYTINY} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Try-Tiny-${VER_PERL_TRYTINY} &&
cp -v {CONTRIBUTING,Changes,LICENCE,README} \
	/usr/share/doc/Try-Tiny-${VER_PERL_TRYTINY} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Try/Tiny.pm ] ; then
	echo "ERROR: Try::Tiny not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Try-Tiny-${VER_PERL_TRYTINY} &&


set +x +u
