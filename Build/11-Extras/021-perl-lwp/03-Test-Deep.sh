#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Test-Deep-${VER_PERL_TESTDEEP}.tar.gz ] || exit -1
rm -rf Test-Deep-${VER_PERL_TESTDEEP} &&
tar xf /Sources/Test-Deep-${VER_PERL_TESTDEEP}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Test-Deep-${VER_PERL_TESTDEEP} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Test-Deep-${VER_PERL_TESTDEEP} &&
cp -v {Changes,LICENSE,README,TODO} \
	/usr/share/doc/Test-Deep-${VER_PERL_TESTDEEP} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Test/Deep.pm ] ; then
	echo "ERROR: Test::Deep not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Test-Deep-${VER_PERL_TESTDEEP} &&


set +x +u
