#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Test-Needs-${VER_PERL_TESTNEEDS}.tar.gz ] || exit -1
rm -rf Test-Needs-${VER_PERL_TESTNEEDS} &&
tar xf /Sources/Test-Needs-${VER_PERL_TESTNEEDS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Test-Needs-${VER_PERL_TESTNEEDS} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Test-Needs-${VER_PERL_TESTNEEDS} &&
cp -v {Changes,LICENSE,README} \
	/usr/share/doc/Test-Needs-${VER_PERL_TESTNEEDS} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Test/Needs.pm ] ; then
	echo "ERROR: Test::Needs not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Test-Needs-${VER_PERL_TESTNEEDS} &&


set +x +u
