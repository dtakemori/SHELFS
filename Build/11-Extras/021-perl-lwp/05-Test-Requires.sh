#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Test-Requires-${VER_PERL_TESTREQUIRES}.tar.gz ] || exit -1
rm -rf Test-Requires-${VER_PERL_TESTREQUIRES} &&
tar xf /Sources/Test-Requires-${VER_PERL_TESTREQUIRES}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Test-Requires-${VER_PERL_TESTREQUIRES} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Test-Requires-${VER_PERL_TESTREQUIRES} &&
cp -v {Changes,LICENSE,README.md} \
	/usr/share/doc/Test-Requires-${VER_PERL_TESTREQUIRES} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Test/Requires.pm ] ; then
	echo "ERROR: Test::Requires not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Test-Requires-${VER_PERL_TESTREQUIRES} &&


set +x +u
