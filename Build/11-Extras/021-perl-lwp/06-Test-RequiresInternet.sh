#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Test-RequiresInternet-${VER_PERL_TESTREQUIRESINTERNET}.tar.gz ] || exit -1
rm -rf Test-RequiresInternet-${VER_PERL_TESTREQUIRESINTERNET} &&
tar xf /Sources/Test-RequiresInternet-${VER_PERL_TESTREQUIRESINTERNET}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Test-RequiresInternet-${VER_PERL_TESTREQUIRESINTERNET} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Test-RequiresInternet-${VER_PERL_TESTREQUIRESINTERNET} &&
cp -v {Changes,LICENSE,README} \
	/usr/share/doc/Test-RequiresInternet-${VER_PERL_TESTREQUIRESINTERNET} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Test/RequiresInternet.pm ] ; then
	echo "ERROR: Test::RequiresInternet not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Test-RequiresInternet-${VER_PERL_TESTREQUIRESINTERNET} &&


set +x +u
