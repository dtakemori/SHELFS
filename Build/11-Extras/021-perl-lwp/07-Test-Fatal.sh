#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Test-Fatal-${VER_PERL_TESTFATAL}.tar.gz ] || exit -1
rm -rf Test-Fatal-${VER_PERL_TESTFATAL} &&
tar xf /Sources/Test-Fatal-${VER_PERL_TESTFATAL}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Test-Fatal-${VER_PERL_TESTFATAL} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Test-Fatal-${VER_PERL_TESTFATAL} &&
cp -v {Changes,LICENSE,README} \
	/usr/share/doc/Test-Fatal-${VER_PERL_TESTFATAL} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Test/Fatal.pm ] ; then
	echo "ERROR: Test::Fatal not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Test-Fatal-${VER_PERL_TESTFATAL} &&


set +x +u
