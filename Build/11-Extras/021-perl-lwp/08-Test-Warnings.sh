#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Test-Warnings-${VER_PERL_TESTWARNINGS}.tar.gz ] || exit -1
rm -rf Test-Warnings-${VER_PERL_TESTWARNINGS} &&
tar xf /Sources/Test-Warnings-${VER_PERL_TESTWARNINGS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Test-Warnings-${VER_PERL_TESTWARNINGS} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Test-Warnings-${VER_PERL_TESTWARNINGS} &&
cp -v {CONTRIBUTING,Changes,LICENCE,README} \
	/usr/share/doc/Test-Warnings-${VER_PERL_TESTWARNINGS} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Test/Warnings.pm ] ; then
	echo "ERROR: Test::Warnings not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Test-Warnings-${VER_PERL_TESTWARNINGS} &&


set +x +u
