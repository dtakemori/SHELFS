#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/ExtUtils-Config-${VER_PERL_EXTUTILSCONFIG}.tar.gz ] || exit -1
rm -rf ExtUtils-Config-${VER_PERL_EXTUTILSCONFIG} &&
tar xf /Sources/ExtUtils-Config-${VER_PERL_EXTUTILSCONFIG}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd ExtUtils-Config-${VER_PERL_EXTUTILSCONFIG} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/ExtUtils-Config-${VER_PERL_EXTUTILSCONFIG} &&
cp -v {Changes,LICENSE,README} \
	/usr/share/doc/ExtUtils-Config-${VER_PERL_EXTUTILSCONFIG} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/ExtUtils/Config.pm ] ; then
	echo "ERROR: ExtUtils::Config not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf ExtUtils-Config-${VER_PERL_EXTUTILSCONFIG} &&


set +x +u
