#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/ExtUtils-Helpers-${VER_PERL_EXTUTILSHELPERS}.tar.gz ] || exit -1
rm -rf ExtUtils-Helpers-${VER_PERL_EXTUTILSHELPERS} &&
tar xf /Sources/ExtUtils-Helpers-${VER_PERL_EXTUTILSHELPERS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd ExtUtils-Helpers-${VER_PERL_EXTUTILSHELPERS} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/ExtUtils-Helpers-${VER_PERL_EXTUTILSHELPERS} &&
cp -v {Changes,LICENSE,README} \
	/usr/share/doc/ExtUtils-Helpers-${VER_PERL_EXTUTILSHELPERS} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/ExtUtils/Helpers.pm ] ; then
	echo "ERROR: ExtUtils::Helpers not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf ExtUtils-Helpers-${VER_PERL_EXTUTILSHELPERS} &&


set +x +u
