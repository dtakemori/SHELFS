#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/ExtUtils-InstallPaths-${VER_PERL_EXTUTILSINSTALLPATHS}.tar.gz ] || exit -1
rm -rf ExtUtils-InstallPaths-${VER_PERL_EXTUTILSINSTALLPATHS} &&
tar xf /Sources/ExtUtils-InstallPaths-${VER_PERL_EXTUTILSINSTALLPATHS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd ExtUtils-InstallPaths-${VER_PERL_EXTUTILSINSTALLPATHS} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/ExtUtils-InstallPaths-${VER_PERL_EXTUTILSINSTALLPATHS} &&
cp -v {Changes,LICENSE} \
	/usr/share/doc/ExtUtils-InstallPaths-${VER_PERL_EXTUTILSINSTALLPATHS} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/ExtUtils/InstallPaths.pm ] ; then
	echo "ERROR: ExtUtils::InstallPaths not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf ExtUtils-InstallPaths-${VER_PERL_EXTUTILSINSTALLPATHS} &&


set +x +u
