#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Module-Build-Tiny-${VER_PERL_MODULEBUILDTINY}.tar.gz ] || exit -1
rm -rf Module-Build-Tiny-${VER_PERL_MODULEBUILDTINY} &&
tar xf /Sources/Module-Build-Tiny-${VER_PERL_MODULEBUILDTINY}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Module-Build-Tiny-${VER_PERL_MODULEBUILDTINY} &&
perl Build.PL &&

######################################################################### BUILD
./Build &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log ./Build test >> ../test.log
fi

####################################################################### INSTALL
./Build install &&

install -v -d -m 755 /usr/share/doc/Module-Build-Tiny-${VER_PERL_MODULEBUILDTINY} &&
cp -v {Changes,LICENSE,README} \
	/usr/share/doc/Module-Build-Tiny-${VER_PERL_MODULEBUILDTINY} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Module/Build/Tiny.pm ] ; then
	echo "ERROR: Module::Build::Tiny not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Module-Build-Tiny-${VER_PERL_MODULEBUILDTINY} &&


set +x +u
