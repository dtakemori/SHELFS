#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/B-COW-${VER_PERL_BCOW}.tar.gz ] || exit -1
rm -rf B-COW-${VER_PERL_BCOW} &&
tar xf /Sources/B-COW-${VER_PERL_BCOW}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd B-COW-${VER_PERL_BCOW} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/B-COW-${VER_PERL_BCOW} &&
cp -v {Changes,LICENSE,README} \
	/usr/share/doc/B-COW-${VER_PERL_BCOW} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/$(uname -m)-linux-thread-multi/B/COW.pm ] ; then
	echo "ERROR: B::COW not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf B-COW-${VER_PERL_BCOW} &&


set +x +u
