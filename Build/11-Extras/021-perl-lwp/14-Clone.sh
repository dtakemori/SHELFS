#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Clone-${VER_PERL_CLONE}.tar.gz ] || exit -1
rm -rf Clone-${VER_PERL_CLONE} &&
tar xf /Sources/Clone-${VER_PERL_CLONE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Clone-${VER_PERL_CLONE} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Clone-${VER_PERL_CLONE} &&
cp -v {Changes,README.md} \
	/usr/share/doc/Clone-${VER_PERL_CLONE} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/$(uname -m)-linux-thread-multi/Clone.pm ] ; then
	echo "ERROR: Clone not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Clone-${VER_PERL_CLONE} &&


set +x +u
