#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/TimeDate-${VER_PERL_TIMEDATE}.tar.gz ] || exit -1
rm -rf TimeDate-${VER_PERL_TIMEDATE} &&
tar xf /Sources/TimeDate-${VER_PERL_TIMEDATE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd TimeDate-${VER_PERL_TIMEDATE} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/TimeDate-${VER_PERL_TIMEDATE} &&
cp -v {ChangeLog,README} \
	/usr/share/doc/TimeDate-${VER_PERL_TIMEDATE} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/TimeDate.pm ] ; then
	echo "ERROR: TimeDate not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf TimeDate-${VER_PERL_TIMEDATE} &&


set +x +u
