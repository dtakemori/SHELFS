#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/URI-${VER_PERL_URI}.tar.gz ] || exit -1
rm -rf URI-${VER_PERL_URI} &&
tar xf /Sources/URI-${VER_PERL_URI}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd URI-${VER_PERL_URI} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/URI-${VER_PERL_URI} &&
cp -v {CONTRIBUTING.md,Changes,LICENSE,README} \
	/usr/share/doc/URI-${VER_PERL_URI} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/URI.pm ] ; then
	echo "ERROR: URI not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf URI-${VER_PERL_URI} &&


set +x +u
