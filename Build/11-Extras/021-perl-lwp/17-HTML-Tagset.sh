#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/HTML-Tagset-${VER_PERL_HTMLTAGSET}.tar.gz ] || exit -1
rm -rf HTML-Tagset-${VER_PERL_HTMLTAGSET} &&
tar xf /Sources/HTML-Tagset-${VER_PERL_HTMLTAGSET}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd HTML-Tagset-${VER_PERL_HTMLTAGSET} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/HTML-Tagset-${VER_PERL_HTMLTAGSET} &&
cp -v {Changes,README.md} \
	/usr/share/doc/HTML-Tagset-${VER_PERL_HTMLTAGSET} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/HTML/Tagset.pm ] ; then
	echo "ERROR: HTML::Tagset not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf HTML-Tagset-${VER_PERL_HTMLTAGSET} &&


set +x +u
