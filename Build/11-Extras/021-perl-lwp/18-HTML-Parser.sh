#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/HTML-Parser-${VER_PERL_HTMLPARSER}.tar.gz ] || exit -1
rm -rf HTML-Parser-${VER_PERL_HTMLPARSER} &&
tar xf /Sources/HTML-Parser-${VER_PERL_HTMLPARSER}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd HTML-Parser-${VER_PERL_HTMLPARSER} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/HTML-Parser-${VER_PERL_HTMLPARSER} &&
cp -v {Changes,LICENSE,README,TODO} \
	/usr/share/doc/HTML-Parser-${VER_PERL_HTMLPARSER} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/$(uname -m)-linux-thread-multi/HTML/Parser.pm ] ; then
	echo "ERROR: HTML::Parser not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf HTML-Parser-${VER_PERL_HTMLPARSER} &&


set +x +u
