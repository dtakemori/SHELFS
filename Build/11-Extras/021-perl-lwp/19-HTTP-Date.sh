#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/HTTP-Date-${VER_PERL_HTTPDATE}.tar.gz ] || exit -1
rm -rf HTTP-Date-${VER_PERL_HTTPDATE} &&
tar xf /Sources/HTTP-Date-${VER_PERL_HTTPDATE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd HTTP-Date-${VER_PERL_HTTPDATE} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/HTTP-Date-${VER_PERL_HTTPDATE} &&
cp -v {CONTRIBUTORS,Changes,LICENSE,README.md} \
	/usr/share/doc/HTTP-Date-${VER_PERL_HTTPDATE} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/HTTP/Date.pm ] ; then
	echo "ERROR: HTTP::Date not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf HTTP-Date-${VER_PERL_HTTPDATE} &&


set +x +u
