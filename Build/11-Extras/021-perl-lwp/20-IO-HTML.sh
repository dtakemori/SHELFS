#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/IO-HTML-${VER_PERL_IOHTML}.tar.gz ] || exit -1
rm -rf IO-HTML-${VER_PERL_IOHTML} &&
tar xf /Sources/IO-HTML-${VER_PERL_IOHTML}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd IO-HTML-${VER_PERL_IOHTML} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/IO-HTML-${VER_PERL_IOHTML} &&
cp -v {Changes,LICENSE,README} \
	/usr/share/doc/IO-HTML-${VER_PERL_IOHTML} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/IO/HTML.pm ] ; then
	echo "ERROR: IO::HTML not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf IO-HTML-${VER_PERL_IOHTML} &&


set +x +u
