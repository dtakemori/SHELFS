#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/LWP-MediaTypes-${VER_PERL_LWPMEDIATYPES}.tar.gz ] || exit -1
rm -rf LWP-MediaTypes-${VER_PERL_LWPMEDIATYPES} &&
tar xf /Sources/LWP-MediaTypes-${VER_PERL_LWPMEDIATYPES}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd LWP-MediaTypes-${VER_PERL_LWPMEDIATYPES} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/LWP-MediaTypes-${VER_PERL_LWPMEDIATYPES} &&
cp -v {Changes,LICENSE,README} \
	/usr/share/doc/LWP-MediaTypes-${VER_PERL_LWPMEDIATYPES} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/LWP/MediaTypes.pm ] ; then
	echo "ERROR: LWP::MediaTypes not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf LWP-MediaTypes-${VER_PERL_LWPMEDIATYPES} &&


set +x +u
