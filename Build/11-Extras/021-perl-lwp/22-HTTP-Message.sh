#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/HTTP-Message-${VER_PERL_HTTPMESSAGE}.tar.gz ] || exit -1
rm -rf HTTP-Message-${VER_PERL_HTTPMESSAGE} &&
tar xf /Sources/HTTP-Message-${VER_PERL_HTTPMESSAGE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd HTTP-Message-${VER_PERL_HTTPMESSAGE} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/HTTP-Message-${VER_PERL_HTTPMESSAGE} &&
cp -v {CONTRIBUTING.md,CONTRIBUTORS,Changes,LICENSE,README.md} \
	/usr/share/doc/HTTP-Message-${VER_PERL_HTTPMESSAGE} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/HTTP/Message.pm ] ; then
	echo "ERROR: HTTP::Message not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf HTTP-Message-${VER_PERL_HTTPMESSAGE} &&


set +x +u
