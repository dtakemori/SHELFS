#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/HTML-Form-${VER_PERL_HTMLFORM}.tar.gz ] || exit -1
rm -rf HTML-Form-${VER_PERL_HTMLFORM} &&
tar xf /Sources/HTML-Form-${VER_PERL_HTMLFORM}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd HTML-Form-${VER_PERL_HTMLFORM} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/HTML-Form-${VER_PERL_HTMLFORM} &&
cp -v {Changes,LICENSE} \
	/usr/share/doc/HTML-Form-${VER_PERL_HTMLFORM} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/HTML/Form.pm ] ; then
	echo "ERROR: HTML::Form not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf HTML-Form-${VER_PERL_HTMLFORM} &&


set +x +u
