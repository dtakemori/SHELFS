#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/HTTP-Cookies-${VER_PERL_HTTPCOOKIES}.tar.gz ] || exit -1
rm -rf HTTP-Cookies-${VER_PERL_HTTPCOOKIES} &&
tar xf /Sources/HTTP-Cookies-${VER_PERL_HTTPCOOKIES}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd HTTP-Cookies-${VER_PERL_HTTPCOOKIES} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/HTTP-Cookies-${VER_PERL_HTTPCOOKIES} &&
cp -v {CONTRIBUTORS,Changes,LICENSE,README.md} \
	/usr/share/doc/HTTP-Cookies-${VER_PERL_HTTPCOOKIES} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/HTTP/Cookies.pm ] ; then
	echo "ERROR: HTTP::Cookies not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf HTTP-Cookies-${VER_PERL_HTTPCOOKIES} &&


set +x +u
