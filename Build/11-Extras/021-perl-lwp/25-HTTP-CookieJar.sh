#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/HTTP-CookieJar-${VER_PERL_HTTPCOOKIEJAR}.tar.gz ] || exit -1
rm -rf HTTP-CookieJar-${VER_PERL_HTTPCOOKIEJAR} &&
tar xf /Sources/HTTP-CookieJar-${VER_PERL_HTTPCOOKIEJAR}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd HTTP-CookieJar-${VER_PERL_HTTPCOOKIEJAR} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/HTTP-CookieJar-${VER_PERL_HTTPCOOKIEJAR} &&
cp -v {CONTRIBUTING.mkdn,Changes,LICENSE,README} \
	/usr/share/doc/HTTP-CookieJar-${VER_PERL_HTTPCOOKIEJAR} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/HTTP/CookieJar.pm ] ; then
	echo "ERROR: HTTP::CookieJar not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf HTTP-CookieJar-${VER_PERL_HTTPCOOKIEJAR} &&


set +x +u
