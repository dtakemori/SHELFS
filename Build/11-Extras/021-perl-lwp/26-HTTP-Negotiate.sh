#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/HTTP-Negotiate-${VER_PERL_HTTPNEGOTIATE}.tar.gz ] || exit -1
rm -rf HTTP-Negotiate-${VER_PERL_HTTPNEGOTIATE} &&
tar xf /Sources/HTTP-Negotiate-${VER_PERL_HTTPNEGOTIATE}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd HTTP-Negotiate-${VER_PERL_HTTPNEGOTIATE} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/HTTP-Negotiate-${VER_PERL_HTTPNEGOTIATE} &&
cp -v {Changes,README} \
	/usr/share/doc/HTTP-Negotiate-${VER_PERL_HTTPNEGOTIATE} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/HTTP/Negotiate.pm ] ; then
	echo "ERROR: HTTP::Negotiate not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf HTTP-Negotiate-${VER_PERL_HTTPNEGOTIATE} &&


set +x +u
