#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Net-HTTP-${VER_PERL_NETHTTP}.tar.gz ] || exit -1
rm -rf Net-HTTP-${VER_PERL_NETHTTP} &&
tar xf /Sources/Net-HTTP-${VER_PERL_NETHTTP}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Net-HTTP-${VER_PERL_NETHTTP} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Net-HTTP-${VER_PERL_NETHTTP} &&
cp -v {CONTRIBUTORS,Changes,LICENSE,README.md} \
	/usr/share/doc/Net-HTTP-${VER_PERL_NETHTTP} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Net/HTTP.pm ] ; then
	echo "ERROR: Net::HTTP not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Net-HTTP-${VER_PERL_NETHTTP} &&


set +x +u
