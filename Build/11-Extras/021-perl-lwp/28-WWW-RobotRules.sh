#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/WWW-RobotRules-${VER_PERL_WWWROBOTRULES}.tar.gz ] || exit -1
rm -rf WWW-RobotRules-${VER_PERL_WWWROBOTRULES} &&
tar xf /Sources/WWW-RobotRules-${VER_PERL_WWWROBOTRULES}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd WWW-RobotRules-${VER_PERL_WWWROBOTRULES} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/WWW-RobotRules-${VER_PERL_WWWROBOTRULES} &&
cp -v {Changes,README} \
	/usr/share/doc/WWW-RobotRules-${VER_PERL_WWWROBOTRULES} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/WWW/RobotRules.pm ] ; then
	echo "ERROR: WWW::RobotRules not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf WWW-RobotRules-${VER_PERL_WWWROBOTRULES} &&


set +x +u
