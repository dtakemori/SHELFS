#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/HTTP-Daemon-${VER_PERL_HTTPDAEMON}.tar.gz ] || exit -1
rm -rf HTTP-Daemon-${VER_PERL_HTTPDAEMON} &&
tar xf /Sources/HTTP-Daemon-${VER_PERL_HTTPDAEMON}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd HTTP-Daemon-${VER_PERL_HTTPDAEMON} &&
perl Build.PL &&

######################################################################### BUILD
./Build &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log ./Build make test >> ../test.log
fi

####################################################################### INSTALL
./Build install &&

install -v -d -m 755 /usr/share/doc/HTTP-Daemon-${VER_PERL_HTTPDAEMON} &&
cp -v {CONTRIBUTING,Changes,LICENCE,README} \
	/usr/share/doc/HTTP-Daemon-${VER_PERL_HTTPDAEMON} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/HTTP/Daemon.pm ] ; then
	echo "ERROR: HTTP::Daemon not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf HTTP-Daemon-${VER_PERL_HTTPDAEMON} &&


set +x +u
