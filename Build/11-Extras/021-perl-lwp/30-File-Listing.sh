#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/File-Listing-${VER_PERL_FILELISTING}.tar.gz ] || exit -1
rm -rf File-Listing-${VER_PERL_FILELISTING} &&
tar xf /Sources/File-Listing-${VER_PERL_FILELISTING}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd File-Listing-${VER_PERL_FILELISTING} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/File-Listing-${VER_PERL_FILELISTING} &&
cp -v {Changes,Changes.original,LICENSE,README} \
	/usr/share/doc/File-Listing-${VER_PERL_FILELISTING} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/HTTP/Daemon.pm ] ; then
	echo "ERROR: HTTP::Daemon not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf File-Listing-${VER_PERL_FILELISTING} &&


set +x +u
