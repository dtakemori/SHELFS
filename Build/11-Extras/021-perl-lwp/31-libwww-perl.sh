#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/libwww-perl-${VER_LIBWWWPERL}.tar.gz ] || exit -1
rm -rf libwww-perl-${VER_LIBWWWPERL} &&
tar xf /Sources/libwww-perl-${VER_LIBWWWPERL}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd libwww-perl-${VER_LIBWWWPERL} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/libwww-perl-${VER_LIBWWWPERL} &&
cp -v {CONTRIBUTING.md,Changes,LICENSE,README.SSL} \
	/usr/share/doc/libwww-perl-${VER_LIBWWWPERL} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/LWP.pm ] ; then
	echo "ERROR: LWP.pm not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf libwww-perl-${VER_LIBWWWPERL} &&


set +x +u
