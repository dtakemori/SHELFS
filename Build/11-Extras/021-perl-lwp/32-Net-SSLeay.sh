#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Net-SSLeay-${VER_PERL_NETSSLEAY}.tar.gz ] || exit -1
rm -rf Net-SSLeay-${VER_PERL_NETSSLEAY} &&
tar xf /Sources/Net-SSLeay-${VER_PERL_NETSSLEAY}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Net-SSLeay-${VER_PERL_NETSSLEAY} &&
yes "" | perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Net-SSLeay-${VER_PERL_NETSSLEAY} &&
cp -v {CONTRIBUTING.md,Changes,Credits,LICENSE,QuickRef,README*} \
	/usr/share/doc/Net-SSLeay-${VER_PERL_NETSSLEAY} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/$(uname -m)-linux-thread-multi/Net/SSLeay.pm ] ; then
	echo "ERROR: Net::SSLeay not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Net-SSLeay-${VER_PERL_NETSSLEAY} &&


set +x +u
