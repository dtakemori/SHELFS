#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/Mozilla-CA-${VER_PERL_MOZILLACA}.tar.gz ] || exit -1
rm -rf Mozilla-CA-${VER_PERL_MOZILLACA} &&
tar xf /Sources/Mozilla-CA-${VER_PERL_MOZILLACA}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd Mozilla-CA-${VER_PERL_MOZILLACA} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/Mozilla-CA-${VER_PERL_MOZILLACA} &&
cp -v {Changes,README} \
	/usr/share/doc/Mozilla-CA-${VER_PERL_MOZILLACA} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/Mozilla/CA.pm ] ; then
	echo "ERROR: Mozilla::CA not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf Mozilla-CA-${VER_PERL_MOZILLACA} &&


set +x +u
