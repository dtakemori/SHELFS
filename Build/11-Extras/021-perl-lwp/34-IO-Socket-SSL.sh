#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/IO-Socket-SSL-${VER_PERL_IOSOCKETSSL}.tar.gz ] || exit -1
rm -rf IO-Socket-SSL-${VER_PERL_IOSOCKETSSL} &&
tar xf /Sources/IO-Socket-SSL-${VER_PERL_IOSOCKETSSL}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd IO-Socket-SSL-${VER_PERL_IOSOCKETSSL} &&
yes "" | perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/IO-Socket-SSL-${VER_PERL_IOSOCKETSSL} &&
cp -v {BUGS,Changes,README*} \
	/usr/share/doc/IO-Socket-SSL-${VER_PERL_IOSOCKETSSL} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/IO/Socket/SSL.pm ] ; then
	echo "ERROR: IO::Socket:SSL not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf IO-Socket-SSL-${VER_PERL_IOSOCKETSSL} &&


set +x +u
