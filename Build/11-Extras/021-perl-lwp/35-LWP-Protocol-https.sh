#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


######################################################################## UNPACK
[ -f /Sources/LWP-Protocol-https-${VER_PERL_LWPPROTOCOLHTTPS}.tar.gz ] || exit -1
rm -rf LWP-Protocol-https-${VER_PERL_LWPPROTOCOLHTTPS} &&
tar xf /Sources/LWP-Protocol-https-${VER_PERL_LWPPROTOCOLHTTPS}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd LWP-Protocol-https-${VER_PERL_LWPPROTOCOLHTTPS} &&
perl Makefile.PL &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k test >> ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/LWP-Protocol-https-${VER_PERL_LWPPROTOCOLHTTPS} &&
cp -v {CONTRIBUTING.md,Changes,LICENSE} \
	/usr/share/doc/LWP-Protocol-https-${VER_PERL_LWPPROTOCOLHTTPS} &&

###################################################################### VALIDATE
if [ ! -f /usr/lib64/perl5/site_perl/${VER_PERL}/LWP/Protocol/https.pm ] ; then
	echo "ERROR: LWP::Protocol:https not installed"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf LWP-Protocol-https-${VER_PERL_LWPPROTOCOLHTTPS} &&


set +x +u
