#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&


./01-Encode-Locale.sh &&
./02-Try-Tiny.sh &&
./03-Test-Deep.sh &&
./04-Test-Needs.sh &&
./05-Test-Requires.sh &&
./06-Test-RequiresInternet.sh &&
./07-Test-Fatal.sh &&
./08-Test-Warnings.sh &&
./09-ExtUtils-Config.sh &&
./10-ExtUtils-Helpers.sh &&
./11-ExtUtils-InstallPaths.sh &&
./12-Module-Build-Tiny.sh &&
./13-B-COW.sh &&
./14-Clone.sh &&
./15-TimeDate.sh &&
./16-URI.sh &&
./17-HTML-Tagset.sh &&
./18-HTML-Parser.sh &&
./19-HTTP-Date.sh &&
./20-IO-HTML.sh &&
./21-LWP-MediaTypes.sh &&
./22-HTTP-Message.sh &&
./23-HTML-Form.sh &&
./24-HTTP-Cookies.sh &&
./25-HTTP-CookieJar.sh &&
./26-HTTP-Negotiate.sh &&
./27-Net-HTTP.sh &&
./28-WWW-RobotRules.sh &&
./29-HTTP-Daemon.sh &&
./30-File-Listing.sh &&
./31-libwww-perl.sh &&
./32-Net-SSLeay.sh &&
./33-Mozilla-CA.sh &&
./34-IO-Socket-SSL.sh &&
./35-LWP-Protocol-https.sh


set +x +u
