#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/rsync-${VER_RSYNC}.tar.gz ] || exit -1
rm -rf rsync-${VER_RSYNC} &&
tar xf /Sources/rsync-${VER_RSYNC}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd rsync-${VER_RSYNC} &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
./configure \
	--prefix=/usr \
	--disable-lz4 \
	--disable-xxhash \
	--without-included-zlib &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&

install -v -d -m 755 /usr/share/doc/rsync-${VER_RSYNC} &&
cp -v {COPYING,NEWS.md,README.md,SECURITY.md,TODO} \
	/usr/share/doc/rsync-${VER_RSYNC} &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/rsync ] ; then
	echo "ERROR: rsync is not installed"
	exit -1
fi

/usr/bin/rsync --version | grep "^rsync  version ${VER_RSYNC}"
if [ $? != 0 ] ; then
	echo "ERROR: rsync is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf rsync-${VER_RSYNC} &&
set +x +u
