#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/wget2-${VER_WGET2}.tar.lz ] || exit -1
rm -rf wget2-${VER_WGET2} &&
tar xf /Sources/wget2-${VER_WGET2}.tar.lz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_H} -flto" \
LDFLAGS="${LDOPTS_H}" \
../wget2-${VER_WGET2}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--sysconfdir=/etc \
	--with-linux-crypto \
	--with-lzma \
	--with-ssl=openssl \
	--enable-year2038 \
	--enable-static=no &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST
if [ ${CONF_TEST} -ge 1 ] ;  then
  /usr/bin/time -vao ../test.log make -j${MAKEJOBS:-2} -k check > ../test.log
fi

####################################################################### INSTALL
make install &&
ln -sv wget2 /usr/bin/wget &&
rm -v /usr/bin/wget2_noinstall &&

cp -v ../wget2-${VER_WGET2}/docs/man/man1/* /usr/share/man/man1 &&
cp -v ../wget2-${VER_WGET2}/docs/man/man3/* /usr/share/man/man3 &&

install -v -d -m 755 /usr/share/doc/wget2-${VER_WGET2} &&
cp -v ../wget2-${VER_WGET2}/{AUTHORS,COPYING*,ChangeLog,NEWS,README*} \
	/usr/share/doc/wget2-${VER_WGET2} &&

rm -v /usr/lib64/libwget.la &&

###################################################################### VALIDATE
if [ ! -x /usr/bin/wget2 ] ; then
	echo "ERROR: wget2 is not installed"
	exit -1
fi

/usr/bin/wget2 --version | grep "^GNU Wget2 ${VER_WGET2}"
if [ $? != 0 ] ; then
	echo "ERROR: wget2 is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf wget2-${VER_WGET2} &&
set +x +u
