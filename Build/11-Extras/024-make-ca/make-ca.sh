#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/make-ca-${VER_MAKECA}.tar.gz ] || exit -1
rm -rf make-ca-${VER_MAKECA} &&
tar xf /Sources/make-ca-${VER_MAKECA}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP

######################################################################### BUILD

########################################################################## TEST

####################################################################### INSTALL
cd make-ca-${VER_MAKECA} &&
make install &&

install -v -d -m 755 /usr/share/doc/make-ca-${VER_MAKECA} &&
cp -v {CHANGELOG,LICENSE*,README} /usr/share/doc/make-ca-${VER_MAKECA} &&

tar -C / -xvf /Sources/ca-snapshot-${VER_CASNAPSHOT}.tar.xz &&

###################################################################### VALIDATE
if [ ! -x /usr/sbin/make-ca ] ; then
	echo "ERROR: make-ca is not installed"
	exit -1
fi

/usr/sbin/make-ca --version | grep "^make-ca ${VER_MAKECA}"
if [ $? != 0 ] ; then
	echo "ERROR: make-ca is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cd ../ &&
rm -rf make-ca-${VER_MAKECA} &&
set +x +u
