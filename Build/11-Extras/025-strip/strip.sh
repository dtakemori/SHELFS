#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

### Strip static libraries
find /usr/lib     -type f -name '*\.a' -exec strip -p --strip-debug '{}' ';' 
find /lib64       -type f -name '*\.a' -exec strip -p --strip-debug '{}' ';' 
find /usr/lib64   -type f -name '*\.a' -exec strip -p --strip-debug '{}' ';' 


### Strip dynamic libraries under /lib64
find /lib64 -type f -name '*\.so*' -not \
			\( -name "ld-*" \
				-or -name "libblkid\.*" \
				-or -name "libc\.*" \
				-or -name "libcrypto\.*" \
				-or -name "libdl-*" \
				-or -name "libhistory\.*" \
				-or -name "libkmod\.*" \
				-or -name "liblzma\.*" \
				-or -name "libm\.*" \
				-or -name "libncursesw\.*" \
				-or -name "libnss_*" \
				-or -name "libreadline\.*" \
				-or -name "libz\.*" \
				-or -name "libzstd\.*" \) \
			-exec strip -p --strip-unneeded '{}' ';'
for f in $(find /lib64 -type f -name '*\.so*' \
			\( -name "ld-*" \
				-or -name "libblkid\.*" \
				-or -name "libc\.*" \
				-or -name "libcrypto\.*" \
				-or -name "libdl-*" \
				-or -name "libhistory\.*" \
				-or -name "libkmod\.*" \
				-or -name "liblzma\.*" \
				-or -name "libm\.*" \
				-or -name "libncursesw\.*" \
				-or -name "libnss_*" \
				-or -name "libreadline\.*" \
				-or -name "libz\.*" \
				-or -name "libzstd\.*" \) ); do 
	l=$(basename ${f})
	cp -v -p ${f} /tmp/${l}
	strip -p --strip-unneeded /tmp/${l}
	install -v -p -m0755 /tmp/${l} ${f}	
	rm -v -f /tmp/${l}
done


### Strip dynamic libraries under /usr/lib
find /usr/lib64 -type f -name '*\.so*' -not \
			\( -name "libbfd-*" \) \
			-exec strip -p --strip-unneeded '{}' ';' 
for f in $(find /usr/lib64 -type f -name '*\.so*' \
			\( -name "libbfd-*" \) ); do 
	l=$(basename ${f})
	cp -v -p ${f} /tmp/${l}
	strip -p --strip-unneeded /tmp/${l}
	install -v -p -m0755 /tmp/${l} ${f}	
	rm -v -f /tmp/${l}
done


### Strip dynamic libraries under /usr/libexec
find /usr/libexec -type f -name '*\.so*' -exec strip -p --strip-unneeded '{}' ';' 


### Strip binaries under /bin
find /bin         -type f -not \( -name "bash" \
				-or -name "dbus-daemon" \
				-or -name "find" \) \
			-exec strip -p --strip-all '{}' ';' 
for f in $(find /bin -type f \( -name "bash" \
				-or -name "dbus-daemon" \
				-or -name "find" \) ); do 
	b=$(basename ${f})
	cp -v -p ${f} /tmp/${b}
	strip -p --strip-all /tmp/${b}
	install -v -p -m0755 /tmp/${b} ${f}	
	rm -v -f /tmp/${b}
done


### Strip binaries under /sbin
find /sbin        -type f -not \( -name "agetty" \
				-or -name "dhcpcd" \
				-or -name "init" \
				-or -name "syslogd" \
				-or -name "udevd" \) \
			-exec strip -p --strip-all '{}' ';' 
for f in $(find /sbin -type f \( -name "agetty" \
				-or -name "init" \
				-or -name "syslogd" \
				-or -name "udevd" \) ); do 
	b=$(basename ${f})
	cp -v -p ${f} /tmp/${b}
	strip -p --strip-all /tmp/${b}
	install -v -p -m0755 /tmp/${b} ${f}	
	rm -v -f /tmp/${b}
done
for f in $(find /sbin -type f -and -name "dhcpcd"); do 
	b=$(basename ${f})
	cp -v -p ${f} /tmp/${b}
	strip -p --strip-all /tmp/${b}
	install -v -p -m0555 /tmp/${b} ${f}	
	rm -v -f /tmp/${b}
done


### Strip binaries under /usr/bin
find /usr/bin     -type f -and -not -name "strip" \
			-exec strip -p --strip-all '{}' ';' 
for f in $(find /usr/bin -type f -and -name "strip"); do 
	b=$(basename ${f})
	cp -v -p ${f} /tmp/${b}
	strip -p --strip-all /tmp/${b}
	install -v -p -m0755 /tmp/${b} ${f}	
	rm -v -f /tmp/${b}
done


### Strip binaries under /usr/sbin
find /usr/sbin    -type f -not \( -name "chronyd" \
				-or -name "fcron" \
				-or -name "haveged" \
				-or -name "sshd" \) \
			-exec strip -p --strip-all '{}' ';' 
for f in $(find /usr/sbin -type f \( -name "chronyd" \
				-or -name "haveged" \
				-or -name "sshd" \) ); do 
	b=$(basename ${f})
	cp -v -p ${f} /tmp/${b}
	strip -p --strip-all /tmp/${b}
	install -v -p -m0755 /tmp/${b} ${f}	
	rm -v -f /tmp/${b}
done
for f in $(find /usr/sbin -type f -and -name "fcron"); do 
	b=$(basename ${f})
	cp -v -p ${f} /tmp/${b}
	strip -p --strip-all /tmp/${b}
	install -v -p -m0110 /tmp/${b} ${f}	
	rm -v -f /tmp/${b}
done


### Strip misc binaries
strip -p --strip-all /lib/bash/*
strip -p --strip-all /lib/e2initrd_helper
strip -p --strip-all /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/scripts/*
strip -p --strip-all /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/scripts/basic/fixdep
strip -p --strip-all /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/scripts/genksyms/genksyms
strip -p --strip-all /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/scripts/kconfig/conf
strip -p --strip-all /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/scripts/mod/mk_elfconfig
strip -p --strip-all /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/scripts/mod/modpost
strip -p --strip-all /lib/modules/${VER_LINUX}.${VER_LINUX_PATCH}/build/tools/objtool/objtool
strip -p --strip-all /lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build/scripts/*
strip -p --strip-all /lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build/scripts/basic/fixdep
strip -p --strip-all /lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build/scripts/genksyms/genksyms
strip -p --strip-all /lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build/scripts/kconfig/conf
strip -p --strip-all /lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build/scripts/mod/mk_elfconfig
strip -p --strip-all /lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build/scripts/mod/modpost
strip -p --strip-all /lib/modules/${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}/build/tools/objtool/objtool
strip -p --strip-all /lib/udev/*
strip -p --strip-all /usr/lib64/gettext/*
strip -p --strip-all /usr/libexec/*
strip -p --strip-all /usr/libexec/{awk,coreutils,dbus-1,findutils,getconf,man-db,openssh,p11-kit}/*
strip -p --strip-all /usr/libexec/gcc/${LFS_TGT}/${VER_GCC}/{cc1,cc1plus,collect2,g++-mapper-server,lto-wrapper,lto1}
strip -p --strip-all /usr/libexec/gcc/${LFS_TGT}/${VER_GCC}/install-tools/*
strip -p --strip-all /usr/libexec/gcc/${LFS_TGT}/${VER_GCC}/plugin/*


set +x +u
