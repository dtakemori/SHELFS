#!/bin/bash

echo "===> Screen Start" &&
cd 001-screen &&
/usr/bin/time -vao build.log ./screen.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Screen End" &&


echo "===> Dash Start" &&
cd 002-dash &&
/usr/bin/time -vao build.log ./dash.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Dash End" &&


echo "===> Zsh Start" &&
cd 003-zsh &&
/usr/bin/time -vao build.log ./zsh.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Zsh End" &&


echo "===> Ethtool Start" &&
cd 004-ethtool &&
/usr/bin/time -vao build.log ./ethtool.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Ethtool End" &&


echo "===> Net-Tools Start" &&
cd 005-net-tools &&
/usr/bin/time -vao build.log ./net-tools.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Net-Tools End" &&


echo "===> Wireless_Tools Start" &&
cd 006-wireless_tools &&
/usr/bin/time -vao build.log ./wireless_tools.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Wireless_Tools End" &&


echo "===> Wireless-regdb Start" &&
cd 007-wireless-regdb &&
/usr/bin/time -vao build.log ./wireless-regdb.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Wireless-regdb End" &&


echo "===> Libnl Start" &&
cd 008-libnl &&
/usr/bin/time -vao build.log ./libnl.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libnl End" &&


echo "===> Iw Start" &&
cd 009-iw &&
/usr/bin/time -vao build.log ./iw.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Iw End" &&


echo "===> WPA_Supplicant Start" &&
cd 010-wpa_supplicant &&
/usr/bin/time -vao build.log ./wpa_supplicant.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== WPA_Supplicant End" &&


echo "===> Nettle Start" &&
cd 011-nettle &&
/usr/bin/time -vao build.log ./nettle.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Nettle End" &&


echo "===> Libidn2 Start" &&
cd 012-libidn2 &&
/usr/bin/time -vao build.log ./libidn2.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libidn2 End" &&


echo "===> Libtasn1 Start" &&
cd 013-libtasn1 &&
/usr/bin/time -vao build.log ./libtasn1.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libtasn1 End" &&


echo "===> P11-kit Start" &&
cd 014-p11-kit &&
/usr/bin/time -vao build.log ./p11-kit.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== P11-kit End" &&


echo "===> GnuTLS Start" &&
cd 015-gnutls &&
/usr/bin/time -vao build.log ./gnutls.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== GnuTLS End" &&


echo "===> Bind-Utils Start" &&
cd 016-bind-utils &&
/usr/bin/time -vao build.log ./bind-utils.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Bind-Utils End" &&


echo "===> Curl Start" &&
cd 017-curl &&
/usr/bin/time -vao build.log ./curl.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Curl End" &&


echo "===> Lftp Start" &&
cd 018-lftp &&
/usr/bin/time -vao build.log ./lftp.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Lftp End" &&


echo "===> Links Start" &&
cd 019-links &&
/usr/bin/time -vao build.log ./links.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Links End" &&


echo "===> Lynx Start" &&
cd 020-lynx &&
/usr/bin/time -vao build.log ./lynx.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Lynx End" &&


echo "===> Perl LWP Start" &&
cd 021-perl-lwp &&
/usr/bin/time -vao build.log ./perl-lwp.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Perl LWP End" &&


echo "===> Rsync Start" &&
cd 022-rsync &&
/usr/bin/time -vao build.log ./rsync.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Rsync End" &&


echo "===> Wget2 Start" &&
cd 023-wget2 &&
/usr/bin/time -vao build.log ./wget2.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Wget2 End" &&


echo "===> Make-CA Start" &&
cd 024-make-ca &&
/usr/bin/time -vao build.log ./make-ca.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Make-CA End"
