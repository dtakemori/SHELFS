#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/squashfs-tools-${VER_SQUASHFS}.tar.gz ] || exit -1
rm -rf squashfs-tools-${VER_SQUASHFS} &&
tar xf /Sources/squashfs-tools-${VER_SQUASHFS}.tar.gz &&

######################################################################### PATCH
cd squashfs-tools-${VER_SQUASHFS}/squashfs-tools/ &&
sed -i 's|#XZ_SUPPORT|XZ_SUPPORT|' Makefile &&
sed -i 's|#LZO_SUPPORT|LZO_SUPPORT|' Makefile &&
sed -i 's|#ZSTD_SUPPORT|ZSTD_SUPPORT|' Makefile &&
sed -i 's|COMP_DEFAULT = gzip|COMP_DEFAULT = zstd|' Makefile &&

########################################################################## PREP

######################################################################### BUILD
CC=gcc \
CFLAGS="${COPTS_1} -flto" \
LDFLAGS="${LDOPTS_1}" \
make -j${MAKEJOBS:-2} &&

####################################################################### INSTALL
cp -v mksquashfs unsquashfs /usr/sbin/ &&

cd ../ &&
install -v -d -m 755 /usr/share/doc/squashfs-tools-${VER_SQUASHFS} &&
cp -v {ACKNOWLEDGEMENTS,ACTIONS-README,CHANGES*} \
	{COPYING,README*,TECHNICAL-INFO,USAGE*} \
	/usr/share/doc/squashfs-tools-${VER_SQUASHFS} &&

###################################################################### VALIDATE

####################################################################### CLEANUP
cd ../ &&
rm -rf squashfs-tools-${VER_SQUASHFS} &&
set +x +u
