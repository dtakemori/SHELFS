#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libburn-${VER_LIBBURN}.tar.gz ] || exit -1
rm -rf libburn-${VER_LIBBURN} &&
tar xf /Sources/libburn-${VER_LIBBURN}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto"
LDFLAGS="${LDOPTS_1}" \
../libburn-${VER_LIBBURN}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--enable-shared \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libburn-${VER_LIBBURN} &&
cp -v ../libburn-${VER_LIBBURN}/{AUTHORS,CONTRIBUTORS,COPYING} \
	../libburn-${VER_LIBBURN}/{COPYRIGHT,ChangeLog,NEWS,README} \
	/usr/share/doc/libburn-${VER_LIBBURN} &&

rm -v /usr/lib64/libburn.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/libburn/libburn.h ] ; then
	echo "ERROR: libburn.h header not installed"
	exit -1
fi

if [ ! -x /usr/bin/cdrskin ] ; then
	echo "ERROR: cdrskin not installed"
	exit -1
fi

/usr/bin/cdrskin --version | grep "^cdrskin ${VER_LIBBURN}"
if [ $? != 0 ] ; then
	echo "ERROR: cdrskin is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libburn-${VER_LIBBURN} &&
set +x +u
