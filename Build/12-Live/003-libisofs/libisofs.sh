#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libisofs-${VER_LIBISOFS}.pl01.tar.gz ] || exit -1
rm -rf libisofs-${VER_LIBISOFS} &&
tar xf /Sources/libisofs-${VER_LIBISOFS}.pl01.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
cd libisofs-${VER_LIBISOFS}/ &&

CFLAGS="${COPTS_1} -flto"
LDFLAGS="${LDOPTS_1}" \
./configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--enable-shared \
	--disable-static &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libisofs-${VER_LIBISOFS}.pl01 &&
cp -v {AUTHORS,COPYING,COPYRIGHT,ChangeLog,NEWS,README,Roadmap,TODO} \
	/usr/share/doc/libisofs-${VER_LIBISOFS}.pl01 &&

rm -v /usr/lib64/libisofs.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/libisofs/libisofs.h ] ; then
	echo "ERROR: libisofs.h header not installed"
	exit -1
fi

if [ ! -f /usr/lib64/libisofs.so ] ; then
	echo "ERROR: libisofs.so not installed"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf libisofs-${VER_LIBISOFS} &&
set +x +u
