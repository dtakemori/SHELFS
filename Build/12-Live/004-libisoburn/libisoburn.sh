#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

######################################################################## UNPACK
[ -f /Sources/libisoburn-${VER_LIBISOBURN}.tar.gz ] || exit -1
rm -rf libisoburn-${VER_LIBISOBURN} &&
tar xf /Sources/libisoburn-${VER_LIBISOBURN}.tar.gz &&

######################################################################### PATCH

########################################################################## PREP
mkdir -v build/ &&
cd build/ &&

CFLAGS="${COPTS_1} -flto"
LDFLAGS="${LDOPTS_1}" \
../libisoburn-${VER_LIBISOBURN}/configure \
	--prefix=/usr \
	--libdir=/usr/lib64 \
	--enable-shared \
	--disable-static \
	--enable-pkg-check-modules &&

######################################################################### BUILD
make -j${MAKEJOBS:-2} &&

########################################################################## TEST

####################################################################### INSTALL
make install &&
ln -vs xorrisofs /usr/bin/mkisofs &&
ldconfig &&

install -v -d -m 755 /usr/share/doc/libisoburn-${VER_LIBISOBURN} &&
cp -v ../libisoburn-${VER_LIBISOBURN}/{AUTHORS,CONTRIBUTORS,COPYING,COPYRIGHT} \
	../libisoburn-${VER_LIBISOBURN}/{ChangeLog,NEWS,README,TODO} \
	/usr/share/doc/libisoburn-${VER_LIBISOBURN} &&

rm -v /usr/lib64/libisoburn.la &&

###################################################################### VALIDATE
if [ ! -f /usr/include/libisoburn/libisoburn.h ] ; then
	echo "ERROR: libisoburn.h header not installed"
	exit -1
fi

if [ ! -x /usr/bin/xorriso ] ; then
	echo "ERROR: xorriso not installed"
	exit -1
fi

/usr/bin/xorriso --version | grep "^xorriso ${VER_LIBISOBURN}"
if [ $? != 0 ] ; then
	echo "ERROR: xorriso is wrong version"
	exit -1
fi

####################################################################### CLEANUP
cp -v config.log ../ &&
cd ../ &&
rm -rf build &&
rm -rf libisoburn-${VER_LIBISOBURN} &&
set +x +u
