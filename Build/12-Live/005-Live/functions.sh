#!/bin/bash

preflight()
{
  [ -f "/Sources/linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}.tar.xz" ] \
    || return 1

  [ -f "/Sources/fortify-headers-${VER_FORTIFYHEADERS}.tar.gz" ] \
    || return 1

  [ -f "/Sources/busybox-${VER_BUSYBOX}.tar.bz2" ] \
    || return 1

  [ -f "/Sources/musl-${VER_MUSL}.tar.gz" ] \
    || return 1

  [ -f "/Sources/util-linux-${VER_UTILLINUX}.tar.xz" ] \
    || return 1

  [ -f "/Sources/linux-live-${VER_LINUXLIVE}.tar.gz" ] \
    || return 1


  [ -f "/Build/Config/busybox-${VER_BUSYBOX}.config_live" ] \
    || return 1

  [ -f "/Build/Config/linux-live-config" ] \
    || return 1

  [ -f /Build/Config/bootlogo.png ] \
    || return 1
}


install_kernel_headers()
{
  INITRD="${1}"

  tar xf "/Sources/linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}.tar.xz" &&
  cd "linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}" &&
  make -j"${MAKEJOBS:-2}" \
	INSTALL_HDR_PATH="${INITRD}" \
	headers_install &&
  find "${INITRD}/include" \( -name .install -o -name ..install.cmd \) -delete &&
  cd .. &&
  rm -rf "linux-${VER_LINUXLTS}.${VER_LINUXLTS_PATCH}"

  return ${?}
}


install_fortify_headers()
{ 
  INITRD="${1}"

  tar xf "/Sources/fortify-headers-${VER_FORTIFYHEADERS}.tar.gz" &&
  cd "fortify-headers-${VER_FORTIFYHEADERS}" &&
  make PREFIX="${INITRD}" install &&
  cd .. &&
  rm -rf "fortify-headers-${VER_FORTIFYHEADERS}"

  return ${?}
}


install_musl()
{ 
  INITRD="${1}"
  ARCH="${2}"

  tar xf "/Sources/musl-${VER_MUSL}.tar.gz" &&
  cd "musl-${VER_MUSL}" &&

  CFLAGS="-I ${INITRD}/include/fortify -fstack-protector-all -fstack-clash-protection -fzero-call-used-regs=used-gpr -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack" \
  ./configure \
	--disable-static \
	--prefix="${INITRD}" \
	--syslibdir=/lib &&
  make -j"${MAKEJOBS:-2}" &&

  make install &&
  rm -fv "/lib/ld-musl-${ARCH}.so.1" &&
  ln -sv libc.so "${INITRD}/lib/ld-musl-${ARCH}.so.1" &&
  ln -sv "../lib/ld-musl-${ARCH}.so.1" "${INITRD}/bin/ldd" &&

  cd .. &&
  rm -rf "musl-${VER_MUSL}"

  return ${?}
}


install_busybox()
{
  INITRD="${1}"

  tar xf "/Sources/busybox-${VER_BUSYBOX}.tar.bz2" &&
  cd "busybox-${VER_BUSYBOX}" &&
  patch -p1 -i /Patches/busybox-1.37.0_hwaccel.patch &&
  cp "/Build/Config/busybox-${VER_BUSYBOX}.config_live" .config &&
  make oldconfig &&
  sed -i "s|\$(CROSS_COMPILE)gcc|${INITRD}/bin/musl-gcc|" Makefile &&
  make -j"${MAKEJOBS:-2}" \
	CFLAGS="-I ${INITRD}/include/fortify -flto -fPIC -fstack-clash-protection -fstack-protector-all -fzero-call-used-regs=all -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack" \
	CONFIG_PREFIX="${INITRD}" &&
  cp -v busybox "${INITRD}/bin" &&
  cd .. &&
  rm -rf "busybox-${VER_BUSYBOX}"

  return ${?}
}


install_util_linux()
{
  INITRD="${1}"
  TRIPLET="${2}"

  tar xf "/Sources/util-linux-${VER_UTILLINUX}.tar.xz" &&
  cd "util-linux-${VER_UTILLINUX}" &&
  CC="${INITRD}/bin/musl-gcc" \
  CFLAGS="-I ${INITRD}/include/fortify -pie -flto -Oz -fPIC -fstack-clash-protection -fstack-protector-all -fzero-call-used-regs=all -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack" \
  ./configure \
	--host="${TRIPLET}" \
	--prefix="${INITRD}" \
	--disable-static \
	--disable-libfdisk \
	--disable-liblastlog2 \
	--disable-libsmartcols \
	--disable-libuuid \
	--disable-raw \
	--without-python &&
  make -j"${MAKEJOBS:-2}" &&
  make install &&
  cd .. &&
  rm -rf "util-linux-${VER_UTILLINUX}"

  return ${?}
}


strip_initrd()
{
  INITRD="${1}"

  strip -p --strip-unneeded "${INITRD}/lib/libc.so" &&
  strip -p --strip-unneeded "${INITRD}/lib/libblkid.so.1" &&
  strip -p --strip-unneeded "${INITRD}/lib/libmount.so.1" &&

  strip -p "${INITRD}/bin/busybox" "${INITRD}/bin/eject" "${INITRD}/sbin/blkid"

  return ${?}
}


build_linux_live()
{
  ARCH="${1}"

  rm -rf "linux-live-${VER_LINUXLIVE}" &&
  tar xf "/Sources/linux-live-${VER_LINUXLIVE}.tar.gz" &&
  cd "linux-live-${VER_LINUXLIVE}" &&

  cp -fv /Build/Config/bootlogo.png bootfiles/bootlogo.png &&
  patch -p1 -i /Patches/linux-live-2.12_fstab.patch &&
  patch -p1 -i "/Patches/linux-live-2.12_musl_${ARCH}.patch" &&
  patch -p1 -i /Patches/linux-live-2.12_syslinux.patch &&
  patch -p1 -i /Patches/linux-live-2.12_compressors.patch &&

  cp -fv /Build/Config/linux-live-config config &&

  ./build

  return ${?}
}


configure_shelfs()
{
  cd /tmp/SHELFS-data/ &&
  mkdir -v image &&
  mkdir -v image/{boot,SHELFS} &&
  mv -v boot/syslinux/{initrfs.img,vmlinuz} image/boot/ &&
  mv -v SHELFS/{changes,modules} image/SHELFS/ &&
  mv -v readme.txt image/ &&
  touch image/SHELFS_Live &&
  cp -v /{LICENSE,README} image/ &&

  cp -vf /Build/Config/bootlogo.png image/boot/

  return ${?}
}


grub_efi()
{
  FORMAT="${1}"
  EFIFILE="${2}"

  grub-mkstandalone -v \
     --format="${FORMAT}" \
     --output "scratch/${EFIFILE}" \
     --locales="" \
     boot/grub/grub.cfg=scratch/grub-efi.cfg \
     boot/grub/bootlogo.png=scratch/bootlogo.png &&

  mkdir -v scratch/efiboot &&

  dd if=/dev/zero of=scratch/efiboot.img bs=1M count=10 &&
  mkfs.vfat -v scratch/efiboot.img &&
  mount -t vfat scratch/efiboot.img scratch/efiboot &&

  mkdir -p scratch/efiboot/efi/boot &&
  cp -v "scratch/${EFIFILE}" scratch/efiboot/efi/boot/ &&

  umount scratch/efiboot

  return ${?}
}


grub_bios()
{
  grub-mkstandalone -v \
     --format=i386-pc \
     --output scratch/core.img \
     --install-modules="linux normal iso9660 biosdisk memdisk search tar ls" \
     --modules="linux normal iso9660 biosdisk search" \
     --locales="" \
     --fonts="" \
     boot/grub/grub.cfg=scratch/grub-bios.cfg &&

  cat /usr/lib/grub/i386-pc/cdboot.img scratch/core.img > scratch/bios.img

  return ${?}
}
