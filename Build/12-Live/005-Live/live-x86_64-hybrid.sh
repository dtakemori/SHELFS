#!/bin/bash
. /Build/Config/config.sh &&
. /Build/Config/versions.sh &&
set -x -u &&

TEMP=/tmp/LIVEBUILD
INITRD="${TEMP}/initrd"

. ./functions.sh &&

preflight || exit 1
[ -f /Build/Config/live-grub-efi.cfg ] \
  || exit 1 
[ -f /Build/Config/live-grub-bios.cfg ] \
  || exit 1 

rm -rf "${TEMP}" &&
rm -rf "${TEMP}-data" &&
mkdir -pv "${INITRD}" &&

modprobe loop

install_kernel_headers "${INITRD}" &&
install_fortify_headers "${INITRD}" &&
install_musl "${INITRD}" "x86_64" &&
install_busybox "${INITRD}" &&
install_util_linux "${INITRD}" "x86_64-musl-linux-gnu" &&

strip_initrd "${INITRD}" &&


pushd ${TEMP} &&

build_linux_live "x86_64" &&
configure_shelfs &&


mkdir -v scratch &&
cp -v /Build/Config/live-grub-efi.cfg scratch/grub-efi.cfg &&
cp -v /Build/Config/live-grub-bios.cfg scratch/grub-bios.cfg &&

grub_efi "x86_64-efi" "bootx64.efi" &&
grub_bios &&


xorriso \
    -volume_date uuid "$(date +%Y%m%d%H%M%S00)" \
    -as mkisofs \
    -iso-level 3 \
    -full-iso9660-filenames \
    -volid "SHELFS_LIVE_$(date +%Y%m%d)" \
    --grub2-boot-info \
    --grub2-mbr /usr/lib/grub/i386-pc/boot_hybrid.img \
    -eltorito-boot boot/grub/bios.img \
      -no-emul-boot \
      -boot-load-size 4 \
      -boot-info-table \
      --eltorito-catalog boot/grub/boot.cat \
    -eltorito-alt-boot \
      -e EFI/efiboot.img \
      -no-emul-boot \
    -append_partition 2 0xef scratch/efiboot.img \
    -output "/tmp/SHELFS-Live_$(date +%Y%m%d)_x86_64.iso" \
    -graft-points image \
                  /EFI/efiboot.img=scratch/efiboot.img \
		  /boot/grub/bios.img=scratch/bios.img \
                  /boot/grub/fonts/unicode.pf2=/boot/grub/fonts/unicode.pf2 \
		  /boot/grub/i386-pc=/boot/grub/i386-pc &&


popd &&
set +x +u
