#!/bin/bash

echo "===> Squashfs Start" &&
cd 001-squashfs &&
/usr/bin/time -vao build.log ./squashfs.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Squashfs End" &&


echo "===> Libburn Start" &&
cd 002-libburn &&
/usr/bin/time -vao build.log ./libburn.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libburn End" &&


echo "===> Libisofs Start" &&
cd 003-libisofs &&
/usr/bin/time -vao build.log ./libisofs.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libisofs End" &&


echo "===> Libisoburn Start" &&
cd 004-libisoburn &&
/usr/bin/time -vao build.log ./libisoburn.sh > build.log 2>error.log &&
grep -E " time \(|Percent" build.log &&
cd .. &&
echo "<=== Libisoburn End" 


