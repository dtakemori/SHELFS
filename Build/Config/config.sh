#!/bin/bash

########################################################### BUILD CONFIGURATION
LFS_HST=${MACHTYPE}
LFS_TMP=$(uname -m)-lfs-linux-musl
LFS_TGT=$(uname -m)-pc-linux-gnu

if [ $(uname -m) == "x86_64" ] ; then
  ### Generic
  # LFS_GCC_ARCH="x86-64"
  ### V2
  LFS_GCC_ARCH="x86-64-v2"
  ### V3
  # LFS_GCC_ARCH="x86-64-v3"
  ### Native
  # LFS_GCC_ARCH="native"
elif [ $(uname -m) == "aarch64" ] ; then
  LFS_TGT=$(uname -m)-unknown-linux-gnu
  ### x86_64   - KVM/Qemu - Cortex A72
  # LFS_GCC_ARCH="armv8.2-a"
  ### Apple M1 - UTM/Qemu - Cortex A76
  # LFS_GCC_ARCH="armv8.2-a+aes+crypto+dotprod+flagm+fp16fml+predres+rcpc+sha2"
  ### Apple M1 - UTM/Apple Hypervisor
  ### Apple M1 - UTM/Qemu
  ### Apple M1 - VMware Fusion
  LFS_GCC_ARCH="armv8.2-a+aes+crypto+dotprod+fp16fml+predres+rcpc+sb+ssbs+sha2+sha3+sm4"
  ### Native
  # LFS_GCC_ARCH="native"
fi

MAKEJOBS=$(( $(grep -c processor /proc/cpuinfo) ))

### Compiler options for building the initial cross tools
COPTS_CT="-g0 -O1 -pipe"
CXXOPTS_CT="${COPTS_CT} -D_GLIBXX_ASSERTIONS"
LDOPTS_CT="-Wl,-z,noexecstack"

### Compiler options for building the temporary tools
COPTS_T="-fPIC -g0 -O1 -pipe"
CXXOPTS_T="${CXXOPTS_T} -D_GLIBCXX_ASSERTIONS"
LDOPTS_T="-Wl,-z,noexecstack -Wl,--fatal-warnings"


### Compiler options for building the final system
### Before GCC
COPTS_0="-g0 -Oz -pipe -fstack-clash-protection"
if [ $(uname -m) == "x86_64" ] ; then
  COPTS_0="${COPTS_0} -fcf-protection=full"
elif [ $(uname -m) == "aarch64" ] ; then
  COPTS_0="${COPTS_0} -mbranch-protection=standard"
fi
CXXOPTS_0="${COPTS_0} -D_GLIBCXX_ASSERTIONS"
LDOPTS_0="-Wl,-z,noexecstack -Wl,-O1,--sort-common,--warn-textrel,--warn-rwx-segments,--fatal-warnings"

### After GCC
COPTS_1="-g0 -Oz -pipe -fstack-clash-protection"
if [ $(uname -m) == "x86_64" ] ; then
  COPTS_1="${COPTS_1} -fcf-protection=full"
elif [ $(uname -m) == "aarch64" ] ; then
  COPTS_1="${COPTS_1} -mbranch-protection=standard"
fi
CXXOPTS_1="${COPTS_1} -D_GLIBCXX_ASSERTIONS"
LDOPTS_1="-Wl,-z,noexecstack -Wl,-O1,--sort-common,--warn-textrel,--warn-rwx-segments,--fatal-warnings"

### After GCC - Harden
COPTS_H="${COPTS_1} -mharden-sls=all -fstack-protector-all -fstrict-flex-arrays=3 -fzero-call-used-regs=all -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=3"
CXXOPTS_H="${COPTS_H} -D_GLIBCXX_ASSERTIONS"
LDOPTS_H="${LDOPTS_1}"

### Temporary environment
### "0" to skip all tests.
### "1" to run some tests.
### "2" to run more tests.
CONF_TTEST="0"

### Final environment
### "0" to skip all tests.
### "1" to run some tests.
### "2" to run more tests.
CONF_TEST="0"

########################################################## SYSTEM CONFIGURATION

### Time Zone configuration
CONF_TZ="Pacific/Honolulu"

### Disk
CONF_DRIVE="vda"
CONF_PARTITION_ROOT=4
CONF_FSTYPE_ROOT="ext4"
CONF_UUID_BOOTEFI="5EE9-957A"
CONF_UUID_ROOT="1ef77af9-c955-415f-ba29-5c84eeab9ea9"
CONF_UUID_SWAP="8af7003a-99fa-4727-8432-5f5c42d6fa4f"

### Serial
CONF_SERIAL="ttyS0"

### Network
CONF_NIC="enp1s0"

### Passwords
CONF_ROOTPASS="shelfs-live"
CONF_SHELFSPASS="shelfs"
