#include <stdio.h>
#include <string.h>

//#ifndef _FORTIFY_SOURCE
//# error _FORTIFY_SOURCE is not defined
//#endif

int main(int argc, char **argv) {
char buffer[5];

printf("Buffer contains: %s, Size of buffer is %d\n",
	buffer, sizeof(buffer));

strcpy (buffer, "deadbeef");

printf("Buffer contains: %s, Size of buffer is %d\n",
	buffer, sizeof(buffer));
}
