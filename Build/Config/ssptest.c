#include <stdio.h>
#include <stdlib.h>

void buffer_overflow()
{
  long int i = 0;
  char str[29];

  for (i = 0 ; i < 50; i++) {
    str[i] = '\0';
  }
}

int main()
{
  buffer_overflow();
  exit(0);
}
