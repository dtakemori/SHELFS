#!/bin/bash

### KERNEL LTS
VER_LINUXLTS="6.12"
VER_LINUXLTS_PATCH="16"
VER_AUFSLTS="20250113"

### KERNEL
VER_LINUX="6.13"
VER_LINUX_PATCH="4"
VER_AUFS="20250127"

### GCC
VER_GCC="14.2.0"
VER_GMP="6.3.0"
VER_MPFR="4.2.1"
VER_MPC="1.3.1"
VER_ISL="0.24"

### GLIBC
VER_GLIBC="2.41"
VER_TZDATA="2025a"

### LFS
VER_LFSBOOTSCRIPTS="20240825"
VER_BLFSBOOTSCRIPTS="20241209"
VER_UDEVLFS="20230818"

### IDS
VER_PCIIDS="20241125"
VER_USBIDS="20241204"


VER_ACL="2.3.2"
VER_ATTR="2.5.2"
VER_AUTOCONF="2.72"
VER_AUTOMAKE="1.17"
VER_BASH="5.2.37"
VER_BC="7.0.3"
VER_BIND="9.20.6"
VER_BINUTILS="2.43.1"
VER_BISON="3.8.2"
VER_BSDCURSES="0.3.2"
VER_BTRFSPROGS="6.13"
VER_BUSYBOX="1.37.0"
VER_BZIP2="1.0.8"
VER_CASNAPSHOT="20240615"
VER_CHRONY="4.5"
VER_CMAKE="3.31.5"
VER_COREUTILS="9.6"
VER_CPIO="2.15"
VER_CRYPTSETUP="2.7.5"
VER_CURL="8.12.1"
VER_DASH="0.5.12"
VER_DBUS="1.16.0"
VER_DEJAGNU="1.6.3"
VER_DHCPCD="10.2.0"
VER_DIFFUTILS="3.11"
VER_DMIDECODE="3.5"
VER_DOSFSTOOLS="4.2"
VER_DRACUTNG="105"
VER_E2FSPROGS="1.47.2"
VER_EFIBOOTMGR="18"
VER_EFIVAR="39"
VER_ELFUTILS="0.192"
VER_ETHTOOL="6.9"
VER_EXFATPROGS="1.2.5"
VER_EXPAT="2.6.4"
VER_EXPECT="5.45.4"
VER_FCRON="3.2.1"
VER_FILE="5.46"
VER_FINDUTILS="4.10.0"
VER_FLEX="2.6.4"
VER_FLITCORE="3.9.0"
VER_FORTIFYHEADERS="2.1"
VER_FREETYPE="2.13.3"
VER_GAWK="5.3.1"
VER_GDBM="1.24"
VER_GETTEXT="0.23.1"
VER_GNUTLS="3.8.9"
VER_GPERF="3.1"
VER_GPTFDISK="1.0.10"
VER_GREP="3.11"
VER_GROFF="1.23.0"
VER_GRUB="2.12"
VER_GZIP="1.13"
VER_HARFBUZZ="10.2.0"
VER_IANAETC="20250123"
VER_ICU="76_1"
VER_INETUTILS="2.5"
VER_INIH="r58"
VER_INTLTOOL="0.51.0"
VER_IPROUTE="6.13.0"
VER_IPTABLES="1.8.11"
VER_IW="6.9"
VER_JINJA2="3.1.4"
VER_JITTERENTROPY="3.6.0"
VER_JSONC="0.18"
VER_KBD="2.7.1"
VER_KMOD="33"
VER_LESS="668"
VER_LFTP="4.9.3"
VER_LIBAIO="0.3.113"
VER_LIBARCHIVE="3.7.7"
VER_LIBBURN="1.5.6"
VER_LIBCAP="2.73"
VER_LIBEDIT="20250104-3.1"
VER_LIBFFI="3.4.7"
VER_LIBIDN2="2.3.7"
VER_LIBISOBURN="1.5.6"
VER_LIBISOFS="1.5.6"
VER_LIBMNL="1.0.5"
VER_LIBNFTNL="1.2.6"
VER_LIBNL="3.10.0"
VER_LIBPAM="1.6.1"
VER_LIBPIPELINE="1.5.8"
VER_LIBTASN1="4.19.0"
VER_LIBTIRPC="1.3.6"
VER_LIBTOOL="2.5.4"
VER_LIBUNISTRING="1.3"
VER_LIBUSB="1.0.27"
VER_LIBUV="v1.50.0"
VER_LIBWWWPERL="6.77"
VER_LIBXCRYPT="4.4.38"
VER_LIBXML2="2.13.5"
VER_LINKS="2.30"
VER_LINUXLIVE="2.12"
VER_LLVM="19.1.7"
VER_LOGROTATE="3.22.0"
VER_LVM="2.03.31"
VER_LYNX="2.9.2"
VER_LZIP="1.25"
VER_LZO="2.10"
VER_M4="1.4.19"
VER_MAKE="4.4.1"
VER_MAKECA="1.15"
VER_MANDB="2.13.0"
VER_MANPAGES="6.12"
VER_MARKUPSAFE="3.0.2"
VER_MDADM="4.3"
VER_MESON="1.7.0"
VER_MUSL="1.2.5"
VER_NANO="8.3"
VER_NCURSES="6.5"
VER_NCURSESPATCH="20210619"
VER_NETTLE="3.10"
VER_NETTOOLS="2.10"
VER_NINJA="1.12.1"
VER_OPENSSL="3.4.1"
VER_OPENSSH="9.9p2"
VER_P11KIT="0.25.5"
VER_PARTED="3.6"
VER_PATCH="2.7.6"
VER_PCIUTILS="3.13.0"
VER_PCRE2="10.45"
VER_PERL="5.40.1"
VER_PERL_BCOW="0.007"
VER_PERL_CLONE="0.47"
VER_PERL_ENCODELOCALE="1.05"
VER_PERL_EXTUTILSCONFIG="0.010"
VER_PERL_EXTUTILSHELPERS="0.028"
VER_PERL_EXTUTILSINSTALLPATHS="0.014"
VER_PERL_FILELISTING="6.16"
VER_PERL_HTMLFORM="6.12"
VER_PERL_HTMLPARSER="3.83"
VER_PERL_HTMLTAGSET="3.24"
VER_PERL_HTTPCOOKIEJAR="0.014"
VER_PERL_HTTPCOOKIES="6.11"
VER_PERL_HTTPDAEMON="6.16"
VER_PERL_HTTPDATE="6.06"
VER_PERL_HTTPMESSAGE="7.00"
VER_PERL_HTTPNEGOTIATE="6.01"
VER_PERL_IOHTML="1.004"
VER_PERL_IOSOCKETSSL="2.089"
VER_PERL_LWPMEDIATYPES="6.04"
VER_PERL_LWPPROTOCOLHTTPS="6.14"
VER_PERL_MODULEBUILDTINY="0.051"
VER_PERL_MOZILLACA="20240924"
VER_PERL_NETHTTP="6.23"
VER_PERL_NETSSLEAY="1.94"
VER_PERL_TESTDEEP="1.204"
VER_PERL_TESTFATAL="0.017"
VER_PERL_TESTNEEDS="0.002010"
VER_PERL_TESTREQUIRES="0.11"
VER_PERL_TESTREQUIRESINTERNET="0.05"
VER_PERL_TESTWARNINGS="0.033"
VER_PERL_TIMEDATE="2.33"
VER_PERL_TRYTINY="0.32"
VER_PERL_URI="5.31"
VER_PERL_WWWROBOTRULES="6.02"
VER_PERL_XMLPARSER="2.47"
VER_PKGCONF="2.3.0"
VER_POPT="1.19"
VER_PROCPS="4.0.5"
VER_PSMISC="23.7"
VER_PYTHON="3.13.2"
VER_READLINE="8.2.13"
VER_RNGTOOLS="6.17"
VER_RSYNC="3.4.1"
VER_SCREEN="5.0.0"
VER_SED="4.9"
VER_SETUPTOOLS="75.8.0"
VER_SHADOW="4.17.2"
VER_SHOREWALL="5.2.8"
VER_SQUASHFS="4.6.1"
VER_SYSKLOGD="2.7.0"
VER_SYSTEMD="255"
VER_SYSVINIT="3.14"
VER_TAR="1.35"
VER_TCL="8.6"
VER_TCLPATCH="16"
VER_TEXINFO="7.2"
VER_TIME="1.9"
VER_UNIFONT="16.0.02"
VER_USBUTILS="017"
VER_USERSPACE_RCU="0.15.0"
VER_UTILLINUX="2.40.4"
VER_VILE="9.8z"
VER_VIM="9.1"
VER_VIMPATCH="1016"
VER_WGET2="2.2.0"
VER_WHEEL="0.45.1"
VER_WHICH="2.22"
VER_WIRELESSREGDB="2024.10.07"
VER_WIRELESSTOOLS="29"
VER_WPASUPPLICANT="2.11"
VER_XFSPROGS="6.13.0"
VER_XZ="5.6.4"
VER_ZFS="2.3.0"
VER_ZLIBNG="2.2.3"
VER_ZSH="5.9"
VER_ZSTD="1.5.7"
